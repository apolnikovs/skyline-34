# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- #
call UpgradeSchemaVersion('2.487');

# ---------------------------------------------------------------------- #
#  ISSUE 2293 - stuart brownlee									 		 #	
# ---------------------------------------------------------------------- #
ALTER TABLE `sp_part_stock_item`
	ADD COLUMN `RepairJobID` INT NULL DEFAULT NULL AFTER `ManufacturerOrderType`,
	ADD INDEX `RepairJobID` (`SPPartStockItem`);

ALTER TABLE `sp_part_stock_template`
	ADD COLUMN `Repair` ENUM('Yes','No') NOT NULL DEFAULT 'Yes' AFTER `OOWCOS`;
	
CREATE TABLE `inbound_pallet_items` (
	`InboundPalletItemID` INT(11) NOT NULL AUTO_INCREMENT,
	`NetworkID` INT(11) NOT NULL DEFAULT '0',
	`ClientID` INT(11) NOT NULL DEFAULT '0',
	`ManufacturerID` INT(11) NOT NULL DEFAULT '0',
	`ModelID` INT(11) NOT NULL DEFAULT '0',
	`SerialNo` VARCHAR(50) NULL DEFAULT NULL,
	`FaultCodeID` INT(11) NOT NULL DEFAULT '0',
	`PalletID` INT(11) NOT NULL DEFAULT '0',
	`Received` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	`ReportedMissing` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	`JobID` INT(11) NOT NULL DEFAULT '0',
	`RepairJobID` INT(11) NOT NULL DEFAULT '0',
	`ReceivedDate` DATETIME NULL DEFAULT NULL,
	`ReceivedBy` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`InboundPalletItemID`),
	INDEX `PalletID` (`PalletID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
INSERT into version (VersionNo) VALUES ('2.488');