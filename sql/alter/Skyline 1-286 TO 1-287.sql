# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.286');

# ---------------------------------------------------------------------- #
# Modify repair_type table     								 #
# ---------------------------------------------------------------------- # 
ALTER TABLE repair_type ADD ExcludeFromBouncerTable ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No' AFTER ExcludeFromInvoicing;

ALTER TABLE repair_type CHANGE Type Type ENUM( 'Repair', 'Modification', 'Exchange', 'LiquidDamage', 'NFF', 'BER', 'RTM', 'RNR', 'Excluded', 'Exchange Estimate', 'Exchange Fee', 'Handling Fee' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.287');
