<script type="text/javascript">   
    $(document).ready(function(){
            $( 'input[name="AppointmentDate"]' ).datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: "button",
                    buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/date-picker.png",
                    buttonImageOnly: true
            }); 
    })
</script>    

<div id="FormPanel" class="SystemAdminFormPanel" >
<form action="#" id="createAppointments" name="AddAppointments"  class="SystemAdminFormPanel"  method="post">
    <fieldset>
        <legend>{$form_legend|escape:'html'}</legend>
        <p class="error" style="display:none;"></p>
        <p >
            <label  for="AppointmentDate" >{$page['Labels']['appointment_date']|escape:'html'}:</label>
            
                <input type="text" id="AppointmentDate" name="AppointmentDate" value="{$smarty.now|date_format:'%d/%m/%Y'}"  >
            
           
         </p>   
        <p>
            <label for="AppointmentTime" >{$page['Labels']['appointment_time']|escape:'html'}:</label>
              
                <input type="text" id="AppointmentTime" name="AppointmentTime" value="{$smarty.now|date_format:'%H:%M'}">
            
           
         </p>
         <p>
             <label>{$page['Labels']['appointment_type']|escape:'html'}</label>
             {*html_options id=AppointmentType name=AppointmentType options=$AppointmentTypeList selected=''*}
             <select  name="AppointmentType" id="AppointmentType" class="" >
                {foreach from=$AppointmentTypeList item=AppointmentType}
                <option value="{$AppointmentType.Name}">{$AppointmentType.Name|escape:'html'}</option>
                {/foreach}
            </select>
         </p>
         <p class="right">
            <input type="submit" name="insert_save_btn_appointments" class="textSubmitButton centerBtn" id="insert_save_btn_appointments"  value="{$page['Buttons']['save']|escape:'html'}" >

            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >
         
         </p>
    </fieldset>
</form>
            </div>
            
              {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" >{$form_legend|escape:'html'}</legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
         