<?php
/**
 * RemoteEngineerHistory Model Class
 * Capture Remote Engineer Histroy Data into Skyline Database
 * 
 * @package skyline
 * @author     Krishnam Raju Nalla <k.nalla@pccsuk.com>
 * * @copyright  2012 - 2013 PC Control Systems
 * @version 1.00
 * 
 * Changes
 * Date        Version Author                Reason
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');

class RemoteEngineerHistory extends CustomModel {  
    public function __construct($controller) {
        parent::__construct($controller);
        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->table = "remote_engineer_history";
        $this->fields = [
            "AppointmentID",
            "JobID",
            "SBJobNo",
            "Date",
            "Time",
            "UserCode",
            "Action",
            "Notes",
            "URL",
            "Latitude",
            "Longitude"
        ];
    }
    
    public function captureRemoteEngineerHistoryDetails($data) {
        $id = $this->SQLGen->dbInsert($this->table, $this->fields, $data, false, true);
        return $id;
    }    
}
?>