<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Short Description 
 * 
 * Long description 
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 21/03/2013  1.0     Brian Etherington     Initial Version
 * **************************************************************************** */

class Shipping extends CustomModel {
    
    private $conn;
    private $table;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Shipping();

    }
    
    /**
     * Add row to table.
     *  
     * @param array $params
     */
     
    public function Select( $sql, $params=null ) {
        return $this->Query( $this->conn, $sql, $params );
    }
    
    /**
     * find rows in table.
     *
     * @param array $params
     */
     
    public function Find( $params=null ) {
        return $this->FindRows( $this->conn, $this->table, $params );
    }
    
    /**
     * Add row to table.
     *  
     * @param array $params
     */
     
    public function Add( $params=array() ) {
        if (isset($this->controller->user->UserID)) {
            $params['ModifiedUserID'] = $this->controller->user->UserID;
        }
        $params['ModifiedDate'] = date("Y-m-d H:i:s");
        return $this->InsertRow( $this->conn, $this->table, $params );
    }
    
    /**
     * Update row in table.
     *  
     * @param array $params
     */
     
    public function Update( $params=array() ) {
        if (isset($this->controller->user->UserID)) {
            $params['ModifiedUserID'] = $this->controller->user->UserID;
        }
        $params['ModifiedDate'] = date("Y-m-d H:i:s");
        return $this->UpdateRow( $this->conn, $this->table, $params );
    }
    
    public function UpdateRows( $params=array(), $where ) {
        
        if (isset($this->controller->user->UserID)) {
            $params['ModifiedUserID'] = $this->controller->user->UserID;
        }
        $params['ModifiedDate'] = date("Y-m-d H:i:s");

        $cmd = $this->table->updateCommand( $params, $where );
        return $this->Execute($this->conn, $cmd, $params);
        
    }
    
    /**
     * Delete row(s) from table.
     *  
     * @param array/string $where
     */
     
    public function Delete( $params ) {
        return $this->DeleteRow( $this->conn, $this->table, $params );
    }
}

?>
