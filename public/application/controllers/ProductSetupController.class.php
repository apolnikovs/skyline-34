<?php

require_once('CustomSmartyController.class.php');

/**
 * Description
 *
 * This class handles all actions of Product Setup under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.11
 * 
 * Changes
 * Date        Version Author                 Reason
 * ??/??/2012  1.00    Nageswara Rao Kanteti  Initial Version   
 * 02/10/2012   1.1 Brian Etherington    Changed user->DefaultBrand to user->DefaultBrandID
 * 01/02/2013  1.11    Andrew J. Williams     Added productGroups 
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 * ***************************************************************************** */
class ProductSetupController extends CustomSmartyController {

    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    public $statuses = array(
        0 => array('Name' => 'Active', 'Code' => 'Active'),
        1 => array('Name' => 'In-Active', 'Code' => 'In-active')
    );

    public function __construct() {

        parent::__construct();

        /* ==========================================
         * Read Application Config file.
         * ==========================================
         */

        $this->config = $this->readConfig('application.ini');

        /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */

        $this->session = $this->loadModel('Session');

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */

        $this->messages = $this->loadModel('Messages');

        //$this->smarty->assign('_theme', 'skyline');

        if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser($this->session->UserID);
            $this->smarty->assign('loggedin_user', $this->user);

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);

            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - " . $this->user->BranchName . " " . $this->config['General']['Branch'] . " ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }

            if ($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - " . $this->config['General']['LastLoggedin'] . " " . date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            }

            $topLogoBrandID = $this->user->DefaultBrandID;
        } else {
            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;

            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('name', '');
            $this->smarty->assign('last_logged_in', '');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index', null, null);
        }

        if ($topLogoBrandID) {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo'])) ? $topBrandLogo[0]['BrandLogo'] : '');
        } else {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);

        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
    }

    /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function ProcessDataAction($args) {

        $modelName = isset($args[0]) ? $args[0] : '';
        $type = isset($args[1]) ? $args[1] : '';

        if ($modelName && $modelName != '') {

            $this->page = $this->messages->getPage(lcfirst($modelName), $this->lang);

            $model = $this->loadModel($modelName);

            if ($type == 'fetch') {
                $_POST['firstArg'] = isset($args[2]) ? $args[2] : '';
                $_POST['secondArg'] = isset($args[3]) ? $args[3] : '';
                $_POST['thirdArg'] = isset($args[4]) ? $args[4] : '';
                $result = $model->fetch($_POST);
            } else {
                $result = $model->processData($_POST);
            }

            echo json_encode($result);
        }

        return;
    }

    public function indexAction(/* $args */) {
        
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Manufacturer Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function manufacturersAction_OLD($args) {

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';
        $NetworkID = $thirdParam = isset($args[2]) ? $args[2] : '';
        $fourthParam = isset($args[3]) ? $args[3] : '';

        if ($functionAction == "uploadImage") { //Uploading image...
            $error = "";
            $msg = "";
            $fileElementName = 'ManufacturerLogo';

            if (!empty($_FILES[$fileElementName]['error'])) {

                switch ($_FILES[$fileElementName]['error']) {

                    case '1':
                        $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                        break;
                    case '2':
                        $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                        break;
                    case '3':
                        $error = 'The uploaded file was only partially uploaded';
                        break;
                    case '4':
                        $error = 'No file was uploaded.';
                        break;

                    case '6':
                        $error = 'Missing a temporary folder';
                        break;
                    case '7':
                        $error = 'Failed to write file to disk';
                        break;
                    case '8':
                        $error = 'File upload stopped by extension';
                        break;
                    case '999':
                    default:
                        $error = 'No error code avaiable';
                }
            } else if (empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {

                $error = 'No file was uploaded..';
            } else {

                $manufacturersLogosDirPath = APPLICATION_PATH . "/../" . $this->config['Path']['manufacturerLogosPath'];

                $tmp_name = $_FILES[$fileElementName]["tmp_name"];
                $imgName = $_FILES[$fileElementName]["name"];

                move_uploaded_file($tmp_name, $manufacturersLogosDirPath . $imgName);

                $imgNameArray = explode(".", $_FILES[$fileElementName]["name"]);
                $imgExt = strtolower($imgNameArray[count($imgNameArray) - 1]);

                $imgNewName = $selectedRowId . "." . $imgExt;

                @rename($manufacturersLogosDirPath . $imgName, $manufacturersLogosDirPath . $imgNewName);

                $msg .= $imgName;

                // $msg .= " File Name: " . $_FILES[$fileElementName]['name'] . ", ";
                // $msg .= " File Size: " . @filesize($_FILES[$fileElementName]['tmp_name']);
                //for security reason, we force to remove all uploaded file
                //@unlink($_FILES['fileToUpload']);		
            }

            echo "{";
            echo "error: '" . $error . "',\n";
            echo "msg: '" . $msg . "'\n";
            echo "}";
        } else if ($functionAction == "getAssigned") { //This is used for to get network id status for selected Manufacturer.
            $ManufacturerID = $selectedRowId;

            if ($ManufacturerID) {
                $Skyline = $this->loadModel('Skyline');

                if ($fourthParam != '') {

                    $manuClientStatus = $Skyline->getManufacturerClientStatus($ManufacturerID, $fourthParam);
                    echo json_encode($manuClientStatus);
                } else {
                    $manuNWStatus = $Skyline->getManufacturerNetworkStatus($ManufacturerID, $NetworkID);
                    echo json_encode($manuNWStatus);
                }
            }
        } else if ($functionAction == "saveChanges") {  //This is used for to assigned checked Manufacturers to selected Network/Client.
            if ($thirdParam) {

                $ClientID = $thirdParam;

                if ($ClientID) {

                    $assignedManufacturers = isset($_POST['assignedManufacturers']) ? $_POST['assignedManufacturers'] : '';
                    $sltMnfr = isset($_POST['sltMnfr']) ? $_POST['sltMnfr'] : '';

                    $Manufacturers = $this->loadModel('Manufacturers');
                    $result = $Manufacturers->updateClientManufacturers($ClientID, $assignedManufacturers, $sltMnfr);

                    if ($result) {
                        echo json_encode("OK");
                    } else {
                        echo json_encode("FAIL");
                    }
                }
            } else {
                $NetworkID = $selectedRowId;

                if ($NetworkID) {

                    $assignedManufacturers = isset($_POST['assignedManufacturers']) ? $_POST['assignedManufacturers'] : '';
                    $sltMnfr = isset($_POST['sltMnfr']) ? $_POST['sltMnfr'] : '';

                    $Manufacturers = $this->loadModel('Manufacturers');
                    $result = $Manufacturers->updateNetworkManufacturers($NetworkID, $assignedManufacturers, $sltMnfr);

                    if ($result) {
                        echo json_encode("OK");
                    } else {
                        echo json_encode("FAIL");
                    }
                }
            }
        } else {

            $Skyline = $this->loadModel('Skyline');
            $networks = $Skyline->getNetworks();

            $service_providers_model = $this->loadModel('ServiceProviders');

            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('networks', $networks);

            $this->page = $this->messages->getPage('manufacturers', $this->lang);
            $this->smarty->assign('page', $this->page);

            $this->smarty->assign('UploadedManufacturerLogo', rand(100, 10000));
            $this->smarty->assign('manufacturerLogosPath', $this->config['Path']['manufacturerLogosPath']);

            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

            $accessErrorFlag = false;

            switch ($functionAction) {

                case 'insert': {

                        //We are fetching data for the selected row where user clicks on copy button
                        $datarow = [
                            'ManufacturerID' => '',
                            'ManufacturerName' => '',
                            'Acronym' => '',
                            'NetworkID' => '',
                            'NetworkName' => '',
                            'AccountNumber' => '',
                            'CostCodeAccountNumber' => '',
                            'AuthorisationRequired' => '',
                            'AuthReqChangedDate' => '',
                            'AuthorisationManager' => '',
                            'AuthorisationManagerEmail' => '',
                            'ManufacturerLogo' => '',
                            'Status' => 'Active'
                        ];

                        //Checking user permissons to display the page.
                        if (!$this->user->SuperAdmin) {
                            $accessErrorFlag = true;
                        }

                        $service_providers = $service_providers_model->authServiceProvidersList();

                        $this->smarty->assign('datarow', $datarow);
                        $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                        $this->smarty->assign('service_providers', $service_providers);

                        $htmlCode = $this->smarty->fetch('systemAdmin/manufacturersForm.tpl');

                        echo $htmlCode;

                        break;
                    }

                case 'update': {

                        $args['ManufacturerID'] = isset($args[2]) ? $args[2] : '';
                        $args['NetworkID'] = isset($args[1]) ? $args[1] : '';

                        $model = $this->loadModel('Manufacturers');

                        $datarow = $model->fetchRow($args);

                        $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                        $this->smarty->assign('datarow', $datarow);

                        //Checking user permissons to display the page.
                        if (!$this->user->SuperAdmin && !$args['NetworkID']) {
                            $accessErrorFlag = true;
                        }

                        $service_providers = $service_providers_model->authServiceProvidersList($args['ManufacturerID']);

                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                        $this->smarty->assign('service_providers', $service_providers);

                        $htmlCode = $this->smarty->fetch('systemAdmin/manufacturersForm.tpl');

                        echo $htmlCode;

                        break;
                    }

                default: {

                        $this->smarty->assign('nId', $functionAction);
                        $this->smarty->assign('showAssigned', $selectedRowId);
                        $this->smarty->assign('cId', $thirdParam);

                        $clients = array();
                        if ($functionAction) {
                            $clients = $Skyline->getNetworkClients($functionAction);
                        }

                        $this->smarty->assign('clients', $clients);

                        $this->smarty->display('systemAdmin/manufacturers.tpl');
                    }
            }
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Unit Types Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function unitTypesAction($args) {

        //$this->log("ARGS: " . var_export($args,true));

        $firstArg = isset($args[0]) ? $args[0] : '';
        $secondArg = isset($args[1]) ? $args[1] : '';
        $thirdArg = isset($args[2]) ? $args[2] : '';
        $fourthArg = isset($args[3]) ? $args[3] : '';

        if (isset($args[0]) && isset($args[1])) {
            $this->smarty->assign("turnaround", true);
        }

        if ($firstArg == "getAssigned") {//This is used for to get client id status for selected Unit Type.
            $UnitTypeID = $secondArg;

            if ($UnitTypeID) {
                $Skyline = $this->loadModel('Skyline');
                $utClStatus = $Skyline->getUnitTypeClientStatus($UnitTypeID, $fourthArg);
                echo json_encode($utClStatus);
            }
        } else if ($firstArg == "saveChanges") {//This is used for to assigned checked Unit Types to selected Client.
            $ClientID = $thirdArg;

            if ($ClientID) {

                $assignedUnitTypes = isset($_POST['assignedUnitTypes']) ? $_POST['assignedUnitTypes'] : '';
                $sltUnitTypes = isset($_POST['sltUnitTypes']) ? $_POST['sltUnitTypes'] : '';

                $UnitTypes = $this->loadModel('UnitTypes');
                $result = $UnitTypes->updateClientUnitTypes($ClientID, $assignedUnitTypes, $sltUnitTypes);

                if ($result) {
                    echo json_encode("OK");
                } else {
                    echo json_encode("FAIL");
                }
            }
        } else {


            $Skyline = $this->loadModel('Skyline');


            $jobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);
            $jobTypes = isset($jobTypes[$this->user->DefaultBrandID]) ? $jobTypes[$this->user->DefaultBrandID] : array();
            $repairSkills = $Skyline->getRepairSkills();


            $AccessoriesModel = $this->loadModel('Accessories');
            $AccessoriesList = $AccessoriesModel->fetchAll();


            $this->smarty->assign('statuses', $this->statuses);

            $this->smarty->assign('repairSkills', $repairSkills);

            $this->smarty->assign('AccessoriesList', $AccessoriesList);
            $this->smarty->assign('jobTypes', $jobTypes);


            $this->page = $this->messages->getPage('unitTypes', $this->lang);
            
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('unitTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
            $this->smarty->assign('page', $this->page);


            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

            if ($this->user->SuperAdmin) {
                $networks = $Skyline->getNetworks();
                $this->smarty->assign('networks', $networks);

                $nId = $firstArg;
                $NetworkUser = false; //These user values should be from database 
                $ClientUser = false;
            } else {

                $nId = $this->user->NetworkID;
                $NetworkUser = true; //These user values should be from database 
                $ClientUser = false;
            }

            $this->smarty->assign('NetworkUser', $NetworkUser);
            $this->smarty->assign('ClientUser', $ClientUser);

            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

            $accessErrorFlag = false;


            switch ($firstArg) {

                case 'insert':

                    //We are fetching data for the selected row where user clicks on copy button
                    $datarow = array(
                        'UnitTypeID' => '',
                        'UnitTypeName' => '',
                        'JobTypeID' => '',
                        'RepairSkillID' => '',
                        'Status' => 'Active',
                        "ImeiRequired" => "No",
			"IMEILengthFrom" => "",
			"IMEILengthTo" => "",
                        'DOPWarrantyPeriod' => '',
                        'ProductionDateWarrantyPeriod' => '',
                        "AccessoryID" => array()
                    );

                    if (!$this->user->SuperAdmin) {
                        $accessErrorFlag = true;
                    }



                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('unitTypesForm.tpl');

                    echo $htmlCode;

                    break;


                case 'update':

                    $args['UnitTypeID'] = $thirdArg;

                    $model = $this->loadModel('UnitTypes');

                    $datarow = $model->fetchRow($args);


                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    $this->smarty->assign('datarow', $datarow);

                    if (!$this->user->SuperAdmin) {
                        $accessErrorFlag = true;
                    }

                    if ($secondArg == "") {
                        $this->smarty->assign("displayImei", true);
                    } else {
                        $this->smarty->assign("displayImei", false);
                    }

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('unitTypesForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $clients = array();
                    if ($nId) {
                        $clients = $Skyline->getNetworkClients($nId);
                    }

                    // $this->log(var_export($secondArg, true));


                    $this->smarty->assign('nId', $nId);
                    $this->smarty->assign('cId', $secondArg);
                    $this->smarty->assign('clients', $clients);
                    $this->smarty->assign('showAssigned', $thirdArg);
                    $this->smarty->display('unitTypes.tpl');
            }
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Models Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function modelsAction($args) {


        $firstArg = isset($args[0]) ? $args[0] : '';
        $secondArg = isset($args[1]) ? $args[1] : '';
        $thirdArg = isset($args[2]) ? $args[2] : '';
        $fourthArg = isset($args[3]) ? $args[3] : '';



        $Skyline = $this->loadModel('Skyline');
        $models_model = $this->loadModel('Models');


        $manufacturers = $Skyline->getManufacturer();

        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('manufacturers', $manufacturers);
        if ($this->user->SuperAdmin == 0) {
            if (array_key_exists("AP8001", $this->user->Permissions)) {
                $this->session->activeUser = "ServiveProvider";
                $this->session->mainTable = "service_provider_model";
                $this->session->mainPage = "models";
                $this->session->secondaryTable = "model";
            }
        } else {
            $this->session->activeUser = "SuperAdmin";
            $this->session->mainTable = "model";
            $this->session->mainPage = "models";
            $this->session->secondaryTable = "service_provider_model";
        }


        $this->smarty->assign('activeUser', $this->session->activeUser);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('models');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        
        $this->smarty->assign('page', $this->page);




        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

        if ($this->user->SuperAdmin) {
            $networks = $Skyline->getNetworks();
            $this->smarty->assign('networks', $networks);

            $nId = $firstArg;
            $NetworkUser = false; //These user values should be from database 
            $ClientUser = false;
        } else {

            $nId = $this->user->NetworkID;
            $NetworkUser = true; //These user values should be from database 
            $ClientUser = false;
        }




        $this->smarty->assign('NetworkUser', $NetworkUser);
        $this->smarty->assign('ClientUser', $ClientUser);

        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

        $accessErrorFlag = false;

        $countUnmodified=0;
 $showSpUnmodified=false;
        if(!isset($args['showSpUnmodified']))
        {
        if ($this->user->SuperAdmin == 0) {
            //if service Provider getting list of service provider manufacturers with created date 0000, if count > 0 displaying those records
             $countUnmodified=$models_model->countSPUnmodifiedRecords($this->user->ServiceProviderID);
            
        }
        //if 
        if($countUnmodified>0){
            $showSpUnmodified=true;
        }
        }
        $this->smarty->assign('showSpUnmodified',$showSpUnmodified);


        $clients = array();
        if ($nId) {
            $clients = $Skyline->getNetworkClients($nId);
        }


        $unitTypes = $Skyline->getUnitTypes(null, $secondArg);
        $this->smarty->assign('unitTypes', $unitTypes);


        $this->smarty->assign('nId', $nId);
        $this->smarty->assign('cId', $secondArg);
        $this->smarty->assign('clients', $clients);
        $this->smarty->assign('uId', $thirdArg);
        $this->smarty->assign('mId', $fourthArg);





        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

        $datatable = $this->loadModel('DataTable');
        //  $data=$model->manufacturersac($nid,$cid);
        // $this->smarty->assign('data', $data);
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);
        if (isset($keys_data[1])) {
            $keys = $keys_data[1];
        } else {
            $keys = array();
        }



        $this->smarty->assign('data_keys', $keys);

        if ($this->session->activeUser == "SuperAdmin") {
            $pendingNo = $datatable->getPendingCount($this->session->mainTable);
            $this->smarty->assign('pendingNo', $pendingNo);
            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }

        $this->smarty->display('systemAdmin/models.tpl');
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Pricing Structure Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function pricingStructureAction($args) {


        $firstArg = isset($args[0]) ? $args[0] : '';
        $secondArg = isset($args[1]) ? $args[1] : '';
        $thirdArg = isset($args[2]) ? $args[2] : '';
        $fourthArg = isset($args[3]) ? $args[3] : '';


        if ($firstArg == "getClients") {//This is used for to get clients for given network id.
            $NetworkID = $secondArg;

            if ($NetworkID) {
                $Skyline = $this->loadModel('Skyline');
                $clients = $Skyline->getNetworkClients($NetworkID);
                echo json_encode($clients);
            }
        } else if ($firstArg == "getUnitTypes") {//This is used for to get unit types for given client id.
            $ClientID = $secondArg;

            if ($ClientID) {
                $Skyline = $this->loadModel('Skyline');
                $unitTypes = $Skyline->getUnitTypes('', $ClientID);
                echo json_encode($unitTypes);
            }
        } else if ($firstArg == "getManufacturers") {//This is used for to get manufacturers for given network id.
            $NetworkID = $secondArg;

            if ($NetworkID) {
                $Skyline = $this->loadModel('Skyline');
                $manufacturers = $Skyline->getManufacturer('', $NetworkID);
                echo json_encode($manufacturers);
            }
        } else if ($firstArg == "getCompletionStatuses") {//This is used for to get Completion Statuses for given network id.
            $NetworkID = $secondArg;

            if ($NetworkID) {
                $Skyline = $this->loadModel('Skyline');
                $completionStatuses = $Skyline->getCompletionStatuses($NetworkID);
                echo json_encode($completionStatuses);
            }
        } else {


            $Skyline = $this->loadModel('Skyline');


            $jobSites = $Skyline->getJobSites();
            $this->smarty->assign('jobSites', $jobSites);

            $this->smarty->assign('statuses', $this->statuses);



            $this->page = $this->messages->getPage('pricingStructure', $this->lang);
            
                        //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('pricingStructure');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
            $this->smarty->assign('page', $this->page);


            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

            if ($this->user->SuperAdmin) {
                $networks = $Skyline->getNetworks();
                $this->smarty->assign('networks', $networks);

                $nId = $firstArg;
                $NetworkUser = false; //These user values should be from database 
                $ClientUser = false;
            } else {

                $nId = $this->user->NetworkID;
                $NetworkUser = true; //These user values should be from database 
                $ClientUser = false;
            }





            $this->smarty->assign('NetworkUser', $NetworkUser);
            $this->smarty->assign('ClientUser', $ClientUser);

            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

            $accessErrorFlag = false;


            switch ($firstArg) {

                case 'insert':

                    //We are fetching data for the selected row where user clicks on copy button
                    $datarow = array(
                        'UnitPricingStructureID' => '',
                        'NetworkID' => $secondArg,
                        'NetworkName' => $Skyline->getNetworkName($secondArg),
                        'ClientID' => $thirdArg,
                        'ClientName' => $Skyline->getClientName($thirdArg),
                        'UnitTypeID' => '',
                        'ManufacturerID' => '',
                        'JobSite' => '',
                        'CompletionStatusID' => '',
                        'ServiceRate' => '',
                        'ReferralFee' => '',
                        'Status' => 'Active'
                    );



                    if ($secondArg) {
                        $clients = $Skyline->getNetworkClients($secondArg);
                        $manufacturers = $Skyline->getManufacturer('', $secondArg);
                        $completionStatuses = $Skyline->getCompletionStatuses($secondArg);
                    } else {
                        $clients = array();
                        $manufacturers = array();
                        $completionStatuses = array();
                    }

                    if ($thirdArg) {
                        $unitTypes = $Skyline->getUnitTypes('', $thirdArg);
                    } else {
                        $unitTypes = array();
                    }






                    $this->smarty->assign('clients', $clients);
                    $this->smarty->assign('unitTypes', $unitTypes);
                    $this->smarty->assign('manufacturers', $manufacturers);
                    $this->smarty->assign('completionStatuses', $completionStatuses);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('pricingStructureForm.tpl');

                    echo $htmlCode;

                    break;


                case 'update':

                    $args['UnitPricingStructureID'] = $secondArg;

                    $model = $this->loadModel('PricingStructure');

                    $datarow = $model->fetchRow($args);

                    if (isset($datarow['NetworkID']) && $datarow['NetworkID']) {
                        $clients = $Skyline->getNetworkClients($datarow['NetworkID']);
                        $manufacturers = $Skyline->getManufacturer('', $datarow['NetworkID']);
                        $completionStatuses = $Skyline->getCompletionStatuses($datarow['NetworkID']);
                    } else {
                        $clients = array();
                        $manufacturers = array();
                        $completionStatuses = array();
                    }

                    if (isset($datarow['ClientID']) && $datarow['ClientID']) {
                        $unitTypes = $Skyline->getUnitTypes('', $datarow['ClientID']);
                    } else {
                        $unitTypes = array();
                    }

                    $this->smarty->assign('clients', $clients);


                    $this->smarty->assign('unitTypes', $unitTypes);

                    $this->smarty->assign('manufacturers', $manufacturers);
                    $this->smarty->assign('completionStatuses', $completionStatuses);

                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('pricingStructureForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $clients = array();
                    if ($nId) {
                        $clients = $Skyline->getNetworkClients($nId);
                    }




                    $this->smarty->assign('nId', $nId);
                    $this->smarty->assign('cId', $secondArg);
                    $this->smarty->assign('clients', $clients);

                    $this->smarty->display('pricingStructure.tpl');
            }
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Product Numbers Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function productNumbersAction($args) {


        $firstArg = isset($args[0]) ? $args[0] : '';
        $secondArg = isset($args[1]) ? $args[1] : '';
        $thirdArg = isset($args[2]) ? $args[2] : '';
        $fourthArg = isset($args[3]) ? $args[3] : '';


        if ($firstArg == "getClients") {//This is used for to get clients for given network id.
            $NetworkID = $secondArg;

            if ($NetworkID) {
                $Skyline = $this->loadModel('Skyline');
                $clients = $Skyline->getNetworkClients($NetworkID);
                echo json_encode($clients);
            }
        } else if ($firstArg == "getUnitTypes") {//This is used for to get unit types for given client id.
            $ClientID = $secondArg;

            if ($ClientID) {
                $Skyline = $this->loadModel('Skyline');
                $unitTypes = $Skyline->getUnitTypes('', $ClientID);
                echo json_encode($unitTypes);
            }
        } else if ($firstArg == "getManufacturers") {//This is used for to get manufacturers for given network id.
            $NetworkID = $secondArg;

            if ($NetworkID) {
                $Skyline = $this->loadModel('Skyline');
                $manufacturers = $Skyline->getManufacturer('', $NetworkID);
                echo json_encode($manufacturers);
            }
        } else if ($firstArg == "getModelDetails") {//This is used for to get model details for given model number.
            $NetworkID = $secondArg;
            $ManufacturerID = $thirdArg;
            $ModelNumber = $fourthArg;

            if ($ModelNumber) {
                $Skyline = $this->loadModel('Skyline');
                $modelDetails = $Skyline->getModelDetails($ModelNumber, $NetworkID, $ManufacturerID);
                echo json_encode($modelDetails);
            }
        } else if ($firstArg == "getModels") {//This is used for to get list of Models for given text.
            $NetworkID = $secondArg;
            $ManufacturerID = $thirdArg;
            $modeStr = isset($args['?q']) ? $args['?q'] : '';

            $q = strtolower($modeStr);

            if (!$q)
                return;


            $Skyline = $this->loadModel('Skyline');
            $models = $Skyline->getModelsManufacturer($modeStr, $NetworkID, $ManufacturerID);

            /*
              $models = array(
              "Great Bittern"=>array(2, 'Tes11t1 M211'),
              "Nag1 Grebe"=>array(2, 'Test M211'),
              "Black-necked Grebe"=>array(2, 'Test M211'),
              "Little Bittern"=>array(3, 'Test M211'),
              "Heuglin's Gull"=>array(2, 'Test M211')
              ); */

            foreach ($models as $key => $value) {
                if (strpos(strtolower($key), $q) !== false) {
                    echo "$key|$value[0]|$value[1]\n";
                }
            }
        } else {


            $Skyline = $this->loadModel('Skyline');


            $paymentRoutes = $Skyline->getPaymentRoutes();
            $this->smarty->assign('paymentRoutes', $paymentRoutes);

            $this->smarty->assign('statuses', $this->statuses);



            $this->page = $this->messages->getPage('productNumbers', $this->lang);
            
                                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('productNumbers');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
            $this->smarty->assign('page', $this->page);


            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

            if ($this->user->SuperAdmin) {
                $networks = $Skyline->getNetworks();
                $this->smarty->assign('networks', $networks);

                $nId = $firstArg;
                $NetworkUser = false; //These user values should be from database 
                $ClientUser = false;
            } else {

                $nId = $this->user->NetworkID;
                $NetworkUser = true; //These user values should be from database 
                $ClientUser = false;
            }





            $this->smarty->assign('NetworkUser', $NetworkUser);
            $this->smarty->assign('ClientUser', $ClientUser);

            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

            $accessErrorFlag = false;


            switch ($firstArg) {

                case 'insert':

                    //We are fetching data for the selected row where user clicks on copy button
                    $datarow = array(
                        'ProductID' => '',
                        'NetworkID' => $secondArg,
                        'NetworkName' => $Skyline->getNetworkName($secondArg),
                        'ClientID' => $thirdArg,
                        'ClientName' => $Skyline->getClientName($thirdArg),
                        'ProductNo' => '',
                        'ModelID' => '',
                        'ModelDescription' => '',
                        'ModelNumber' => '',
                        'ActualSellingPrice' => '',
                        'AuthorityLimit' => '',
                        'PaymentRoute' => '',
                        'UnitTypeID' => '',
                        'ManufacturerID' => '',
                        'Status' => 'Active',
                    );



                    if ($secondArg) {
                        $clients = $Skyline->getNetworkClients($secondArg);
                        $manufacturers = $Skyline->getManufacturer('', $secondArg);
                    } else {
                        $clients = array();
                        $manufacturers = array();
                    }

                    if ($thirdArg) {
                        $unitTypes = $Skyline->getUnitTypes('', $thirdArg);
                    } else {
                        $unitTypes = array();
                    }




                    $this->smarty->assign('clients', $clients);
                    $this->smarty->assign('unitTypes', $unitTypes);
                    $this->smarty->assign('manufacturers', $manufacturers);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('productNumbersForm.tpl');

                    echo $htmlCode;

                    break;


                case 'update':

                    $args['ProductID'] = $secondArg;

                    $model = $this->loadModel('ProductNumbers');

                    $datarow = $model->fetchRow($args);

                    if (isset($datarow['NetworkID']) && $datarow['NetworkID']) {
                        $clients = $Skyline->getNetworkClients($datarow['NetworkID']);
                        $manufacturers = $Skyline->getManufacturer('', $datarow['NetworkID']);
                    } else {
                        $clients = array();
                        $manufacturers = array();
                    }

                    if (isset($datarow['ClientID']) && $datarow['ClientID']) {
                        $unitTypes = $Skyline->getUnitTypes('', $datarow['ClientID']);
                    } else {
                        $unitTypes = array();
                    }

                    $this->smarty->assign('clients', $clients);

                    //$this->log(var_export($datarow, true));

                    $this->smarty->assign('unitTypes', $unitTypes);

                    $this->smarty->assign('manufacturers', $manufacturers);

                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('productNumbersForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $clients = array();
                    if ($nId) {
                        $clients = $Skyline->getNetworkClients($nId);
                    }




                    $this->smarty->assign('nId', $nId);
                    $this->smarty->assign('cId', $secondArg);
                    $this->smarty->assign('clients', $clients);

                    $this->smarty->display('productNumbers.tpl');
            }
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Client Service Types Page.
     * 
     * @param array $args It contains action type as first element i.e getAssigned/saveChanges etc.
     * 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function clientServiceTypesAction($args) {


        $firstArg = isset($args[0]) ? $args[0] : '';
        $secondArg = isset($args[1]) ? $args[1] : '';
        $thirdArg = isset($args[2]) ? $args[2] : '';
        $fourthArg = isset($args[3]) ? $args[3] : '';


        if ($firstArg == "getAssigned") {//This is used for to get client id status for selected Unit Type.
            $ServiceTypeID = $secondArg;

            if ($ServiceTypeID) {
                $Skyline = $this->loadModel('Skyline');
                $utClStatus = $Skyline->getClientServiceTypesStatus($ServiceTypeID, $thirdArg, $fourthArg);
                echo json_encode($utClStatus);
            }
        } else if ($firstArg == "saveChanges") {//This is used for to assigned checked Service Types to selected Client.
            $NetworkID = $secondArg;
            $ClientID = $thirdArg;

            if ($ClientID) {

                $assignedClientServiceTypes = isset($_POST['assignedClientServiceTypes']) ? $_POST['assignedClientServiceTypes'] : '';
                $sltClientServiceTypes = isset($_POST['sltClientServiceTypes']) ? $_POST['sltClientServiceTypes'] : '';

                $ClientServiceTypes = $this->loadModel('ClientServiceTypes');
                $result = $ClientServiceTypes->updateClientServiceTypes($NetworkID, $ClientID, $assignedClientServiceTypes, $sltClientServiceTypes);

                if ($result) {
                    echo json_encode("OK");
                } else {
                    echo json_encode("FAIL");
                }
            }
        } else {
            $this->page = $this->messages->getPage('clientServiceTypes', $this->lang);
            
                                                //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('clientServiceTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
            $this->smarty->assign('page', $this->page);

            if ($firstArg == "update") {


                $accessErrorFlag = false;

                $NetworkID = $secondArg;
                $ClientID = $thirdArg;

                if ($this->user->SuperAdmin) {
                    
                } else if ($this->user->NetworkID && $this->user->ClientID) {
                    $NetworkID = $this->user->NetworkID;
                    $ClientID = $this->user->ClientID;
                } else if ($this->user->NetworkID) {

                    $NetworkID = $this->user->NetworkID;
                } else {
                    $accessErrorFlag = true;
                }


                $ClientServiceTypesModel = $this->loadModel('ClientServiceTypes');
                $datarow = $ClientServiceTypesModel->isClientExists($NetworkID, $ClientID, $fourthArg, true);

                if (!is_array($datarow)) {
                    $datarow = array(
                        'ServiceTypeAliasID' => '',
                        'NetworkID' => $NetworkID,
                        'ClientID' => $ClientID,
                        'ServiceTypeID' => $fourthArg,
                        'ServiceTypeMask' => '',
                        'Status' => 'In-active'
                    );
                }

                $SkylineModel = $this->loadModel('Skyline');

                $JobTypeID = null;
                $BrandID = null;

                //$this->log($datarow);

                if (isset($datarow['ServiceTypeID']) && $datarow['ServiceTypeID']) {
                    $ServiceTypesModel = $this->loadModel('ServiceTypes');
                    $ServiceTypesDetails = $ServiceTypesModel->fetchRow(array('ServiceTypeID' => $datarow['ServiceTypeID']));

                    //  $this->log($ServiceTypesDetails);

                    if (isset($ServiceTypesDetails['JobTypeID'])) {
                        $JobTypeID = $ServiceTypesDetails['JobTypeID'];
                        $BrandID = $ServiceTypesDetails['BrandID'];
                    }
                }

                // $this->log('--->'.$BrandID);
                // $this->log('1--->'.$JobTypeID);

                $serviceTypes = $SkylineModel->getServiceTypes($BrandID, $JobTypeID);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('serviceTypes', $serviceTypes);


                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

                $htmlCode = $this->smarty->fetch('clientServiceTypesForm.tpl');

                echo $htmlCode;
            } else {

                $Skyline = $this->loadModel('Skyline');

                $this->smarty->assign('statuses', $this->statuses);




                $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

                if ($this->user->SuperAdmin) {
                    $networks = $Skyline->getNetworks();
                    $this->smarty->assign('networks', $networks);

                    $nId = $firstArg;
                    $NetworkUser = false;
                    $ClientUser = false;
                } else if ($this->user->NetworkID && $this->user->ClientID) {
                    $nId = $this->user->NetworkID;
                    $secondArg = $this->user->ClientID;

                    $NetworkUser = false;
                    $ClientUser = true;
                } else if ($this->user->NetworkID) {

                    $nId = $this->user->NetworkID;

                    $NetworkUser = true;
                    $ClientUser = false;
                }

                $this->smarty->assign('NetworkUser', $NetworkUser);
                $this->smarty->assign('ClientUser', $ClientUser);

                $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss



                $clients = array();
                if ($nId) {
                    $clients = $Skyline->getNetworkClients($nId);
                }

                $this->smarty->assign('nId', $nId);
                $this->smarty->assign('cId', $secondArg);
                $this->smarty->assign('clients', $clients);
                $this->smarty->assign('showAssigned', $thirdArg);
                $this->smarty->display('clientServiceTypes.tpl');
            }
        }
    }

    public function setClientProductTurnaroundAction() {

        $productModel = $this->loadModel("Product");
        $productModel->setClientProductTurnaround($_POST["days"], $_POST["unitID"], $_POST["clientID"]);
    }

    public function UpdateServiceProviderAuthAction(/* $args */) {

        $ManufacturerID = $_POST['mfg'] == '' ? null : $_POST['mfg'];
        $auths = isset($_POST['auth']) ? $_POST['auth'] : array();
        ;
        $checks = isset($_POST['check']) ? $_POST['check'] : array();

        $model = $this->loadModel('ManufacturerServiceProvider');
        $list = $model->getServiceProvidersByManufacturer($ManufacturerID);

        // set default DELETE action for each row to delete
        // remember original settings for Authorised and DataChecked
        // & unset Authorised and DataChecked values
        // because html form checkboxes only contain checked values
        foreach ($list as &$row) {
            $row['mode'] = Constants::DELETE;
            $row['Authorised_orig'] = $row['Authorised'];
            $row['DataChecked_orig'] = $row['DataChecked'];
            $row['Authorised'] = 'No';
            $row['DataChecked'] = 'No';
        }

        // Update list with Authorised checkboxes
        foreach ($auths as $auth) {
            $isFound = false;
            foreach ($list as &$row) {
                if ($row['ServiceProviderID'] == $auth) {
                    $isFound = true;
                    $row['Authorised'] = 'Yes';
                    $row['mode'] = Constants::UPDATE;
                    break;
                }
            }
            if (!$isFound) {
                $list[] = array('ManufacturerID' => $ManufacturerID,
                    'ServiceProviderID' => $auth,
                    'Authorised' => 'Yes',
                    'DataChecked' => 'No',
                    'mode' => Constants::CREATE
                );
            }
        }

        // Update list with DataChecked checkboxes
        foreach ($checks as $check) {
            $isFound = false;
            foreach ($list as &$row) {
                if ($row['ServiceProviderID'] == $check) {
                    $isFound = true;
                    $row['DataChecked'] = 'Yes';
                    // Do not overwrite mode if new INSERT as a result of Authorised update check above.
                    if ($row['mode'] == Constants::DELETE)
                        $row['mode'] = Constants::UPDATE;
                    break;
                }
            }
            if (!$isFound) {
                $list[] = array('ManufacturerID' => $ManufacturerID,
                    'ServiceProviderID' => $check,
                    'Authorised' => 'No',
                    'DataChecked' => 'Yes',
                    'mode' => Constants::CREATE
                );
            }
        }

        // Now check for updated rows that haven't changed and mark as no update required
        foreach ($list as &$row) {
            if ($row['mode'] == Constants::UPDATE
                    && $row['Authorised'] == $row['Authorised_orig']
                    && $row['DataChecked'] == $row['DataChecked_orig']) {
                $row['mode'] = Constants::NONE;
            }
        }

        $isError = $model->update($list);

        if ($isError) {
            $result = ['status' => 'OK',
                'message' => 'Data Updated OK'];
        } else {
            $result = ['status' => 'ERROR',
                'message' => 'Errors in Update'];
        }

        echo json_encode($result);
    }

    /**
     * productGroups
     * 
     * Allow editing of Manufacturer's product groups
     * 
     * @param array $args
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     * ************************************************************************ */
    public function unitTypeManufacturerAction($args) {
        $manufacturers_model = $this->loadModel('Manufacturers');
        $unit_types_model = $this->loadModel('UnitTypes');
        $unit_type_manufacturer_model = $this->loadModel('UnitTypeManufacturer');

        $action = isset($args[0]) ? $args[0] : '';
        $id = isset($args[1]) ? $args[1] : '';
        $args[2] = isset($args[2]) ? $args[2] : '';

        if ($action == "saveChanges") {//This is used for to assigned checked Unit Types to selected Client.
            $ManufacturerID = $args[2];

            if ($ManufacturerID) {

                /* $assignedUnitTypes = isset($_POST['assignedUnitTypes'])?$_POST['assignedUnitTypes']:''; 
                  $sltUnitTypes      = isset($_POST['sltUnitTypes'])?$_POST['sltUnitTypes']:'';

                  $UnitTypes   =  $this->loadModel('UnitTypes');
                  $result      =  $UnitTypes->updateClientUnitTypes($ClientID, $assignedUnitTypes, $sltUnitTypes); */

                if ($args['UnitTypeManufacturerID'])
                    $result = $unit_type_manufacturer_model->create($args);

                if ($result) {
                    echo json_encode("OK");
                } else {
                    echo json_encode("FAIL");
                }
            }
        } else {
            $manufacturers = $manufacturers_model->getAllManufacturers();
            $unitTypes = $unit_types_model->getAllUnitTypes();

            $this->smarty->assign('statuses', $this->statuses);

            $this->smarty->assign('unitTypes', $unitTypes);
            $this->smarty->assign('manufacturers', $manufacturers);


            $this->page = $this->messages->getPage('unitTypeManufacturer', $this->lang);
            
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('unitTypeManufacturer');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
            $this->smarty->assign('page', $this->page);


            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

            $mId = $action;

            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

            $accessErrorFlag = false;


            switch ($action) {

                case 'insert':
                    //We are fetching data for the selected row where user clicks on copy button
                    $datarow = array(
                        'UnitTypeManufacturerID' => 0,
                        'ManufacturerID' => '',
                        'UnitTypeID' => '',
                        'ProductGroup' => '',
                        'Status' => 'Active'
                    );

                    if (!$this->user->SuperAdmin) {
                        $accessErrorFlag = true;
                    }

                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('unitTypeManufacturerForm.tpl');

                    echo $htmlCode;

                    break;


                case 'update':

                    $args['UnitTypeManufacturerID'] = $args[2];

                    $datarow = $unit_type_manufacturer_model->fetchRow($args);


                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    $this->smarty->assign('datarow', $datarow);

                    if (!$this->user->SuperAdmin) {
                        $accessErrorFlag = true;
                    }



                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('unitTypeManufacturerForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $clients = array();

                    // $this->log(var_export($id, true));


                    $this->smarty->assign('mId', $mId);
                    $this->smarty->display('unitTypeManufacturer.tpl');
            }
        }
    }

    public function tableDisplayPreferenceSetupAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('admin', $this->user->SuperAdmin);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Manufacturers');
        $datatable = $this->loadModel('DataTable');

        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'user');
        $columnStringsSA = $datatable->getColumnStrings(1, $this->page['config']['pageID'], 'sa');


        $keys = $datatable->getAllFieldNames($args['table']);
        if (is_array($keys[1])) {
            $this->smarty->assign('data_keys', $keys[1]);
        } else {
            $this->smarty->assign('data_keys', $keys);
        }

        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = explode(',', $columnStrings['ColumnOrderString']);
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
        }

        if ($columnStringsSA) {
            $columnDisplayStringSA = explode(",", $columnStringsSA['ColumnDisplayString']);
            $columnOrderStringSA = explode(',', $columnStringsSA['ColumnOrderString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
            $columnStatusStringSA = explode(",", $columnStringsSA['ColumnStatusString']);
            $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        } else {
            $columnDisplayStringSA = array();
            $columnOrderStringSA = "";
            $columnNameStringSA = array();
        }
//       echo"<pre>";
//       print_r($columnNameString);
//       echo"</pre>";
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('controller', "ProductSetup");
        $this->smarty->assign('typeAction', $args['page']);

        $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');
    }

    public function manufacturersAction($args) {
        if (isset($args[0])) {
            $nid = $args[0];
        } else {
            $nid = false;
        }
        if (isset($args[1])) {
            $cid = $args[1];
        } else {
            $cid = false;
        }

        $this->session->mainPage = "manufacturers";
        $this->session->mainController = "ProductSetupController";
        //set table and user based on permissions

        if ($this->user->SuperAdmin == 0) {
            if (array_key_exists("AP8001", $this->user->Permissions)) {
                $this->session->activeUser = "ServiveProvider";
                $this->session->mainTable = "service_provider_manufacturer";
                $this->session->secondaryTable = "manufacturer";
            } else {
                //if no permison redirect to home page
                $this->redirect("DefaultController", 'IndexAction');
            }
        } else {

            $this->session->activeUser = "SuperAdmin";
            $this->session->mainTable = "manufacturer";
            $this->session->secondaryTable = "service_provider_manufacturer";
        }


        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('manufacturers');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Manufacturers');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->ServiceProviderID != '') {
            $spid = $this->user->ServiceProviderID;
        } else {
            $spid = false;
        }

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID'], $spid);

        if (isset($keys_data[1])) {
            $keys = $keys_data[1];
        } else {
            $keys = array();
        }

        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('data_keys1', $keys_data[0]);
        if (isset($args['aprove'])) {
            $aprove = $args['aprove'];
        } else {
            $aprove = "";
        }

        if (isset($args['status'])) {
            $status = $args['status'];
        } else {
            $status = "Active";
        }
        $this->smarty->assign('args', $args);
$countUnmodified=0;
 $showSpUnmodified=false;
        if(!isset($args['showSpUnmodified']))
        {
        if ($this->user->SuperAdmin == 0) {
            //if service Provider getting list of service provider manufacturers with created date 0000, if count > 0 displaying those records
             $countUnmodified=$model->countSPUnmodifiedRecords($this->user->ServiceProviderID);
            
        }
        //if 
        if($countUnmodified>0){
            $showSpUnmodified=true;
        }
        }
        $this->smarty->assign('showSpUnmodified',$showSpUnmodified);
        //manufactuer ttable do not use serverside datatable so we getting all the manufacturers data

        $data = $model->getManufacturerData($nid, $cid, $keys_data[0], $this->session->mainTable, $this->user->ServiceProviderID, $aprove, $status,$showSpUnmodified);





        $this->smarty->assign('data', $data);

        //to use this function we need provide pageid, it can be set in language config file



        $Skyline = $this->loadModel('Skyline');
        $networks = $Skyline->getNetworks();
        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';
        $NetworkID = $thirdParam = isset($args[2]) ? $args[2] : '';
        $service_providers_model = $this->loadModel('ServiceProviders');
        $this->smarty->assign('nId', $nid);
        $this->smarty->assign('cId', $cid);

        $clients = array();
        if ($functionAction) {
            $clients = $Skyline->getNetworkClients($functionAction);
        }

        $this->smarty->assign('clients', $clients);
        $this->smarty->assign('showAssigned', false);

        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('networks', $networks);
        $this->smarty->assign('UploadedManufacturerLogo', rand(100, 10000));
        $this->smarty->assign('manufacturerLogosPath', $this->config['Path']['manufacturerLogosPath']);


        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss


        if ($this->session->activeUser == "SuperAdmin") {
            $pendingNo = $datatable->getPendingCount($this->session->mainTable);
            $this->smarty->assign('pendingNo', $pendingNo);
            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }


        $this->smarty->display('systemAdmin/manufacturers.tpl');
    }

    public function saveDisplayPreferencesAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->saveDisplayPreferences($this->user->UserID, $this->page['config']['pageID'], $_POST);
        $this->redirect("ProductSetupController", $this->session->mainPage . 'Action');
        //$t = $this->session->mainPage;
        //$this->log($t);
        
    }

    public function saveAssignedAction($args) {

        $manufacturer_model = $this->loadModel('Manufacturers');
        $manufacturer_model->saveAssignedManufacturers($_POST, $this->user->UserID);
        $this->redirect('ProductSetup/manufacturers/' . $_POST['nid'] . "/" . $_POST['cid']);
    }

    public function processManufacturerAction($args) {


        $manufacturerLogosPath = $this->config['Path']['manufacturerLogosPath'];
        $this->smarty->assign('manufacturerLogosPath', $manufacturerLogosPath);
        $ServiceProviders = $this->loadModel('ServiceProviders');
        $this->smarty->assign('activeUser', $this->session->activeUser);
        $service_providers_model = $this->loadModel('ServiceProviders');

        $this->smarty->assign('statuses', $this->statuses);

        $Skyline = $this->loadModel('Skyline');
        $countries = $Skyline->getCountriesCounties();

        $this->smarty->assign('countries', $countries);
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        if (!$this->user->SuperAdmin) {
            $accessErrorFlag = true;
        } else {
            $accessErrorFlag = false;
        }

        $service_providers = $service_providers_model->authServiceProvidersList();
        $Suppliers_model = $this->loadModel('Suppliers');
        $manufacturer_model = $this->loadModel('Manufacturers');
        $model_model = $this->loadModel('Models');	

        if (!isset($args['id'])) {
            $mid = 0;
        } else {
            $mid = $args['id'];
        }
        $suppliers = $Suppliers_model->getAllSuppliersData();
        $manufacturers = $manufacturer_model->getManufacturerData();
	$systemstatus = $Skyline->getSystemStatus(); /* Updated By Praveen Kumar N */
//                    $models=$model_model->getManufacturerModels($mid);
//                    $this->smarty->assign('models',  $models);
        $this->smarty->assign('manufacturers', $manufacturers);
        $this->smarty->assign('suppliers', $suppliers);
        $this->smarty->assign('ForceStatus', array()); ////need status table TO DO need spec
	$this->smarty->assign('systemstatus', $systemstatus); /* Updated By Praveen Kumar N */
        $this->smarty->assign('BouncerPeriodBasedOn', array('BookedDate', 'CompletedDate', 'DespatchedDates'));
        $this->smarty->assign('EDIExportFormat', array());
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('UploadedManufacturerLogo', rand(100, 10000));
        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
        $this->smarty->assign('service_providers', $service_providers);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('Action', "manufacturers");



        if (!isset($args['id'])) {
            $datarow = [
                'ManufacturerID' => '',
                'ManufacturerName' => '',
                'Acronym' => '',
                'NetworkID' => '',
                'NetworkName' => '',
                'AccountNumber' => '',
                'CostCodeAccountNumber' => '',
                'AuthorisationRequired' => 'No',
                'AuthReqChangedDate' => '',
                'AuthorisationManager' => '',
                'AuthorisationManagerEmail' => '',
                'ManufacturerLogo' => '',
                'AccountNo' => '',
                'Status' => 'Active',
                'BuildingNameNumber' => '',
                'Street' => '',
                'LocalArea' => '',
                'TownCity' => '',
                'CountryID' => '',
                'TelNoSwitchboard' => '',
                'TelNoSpares' => '',
                'TelNoWarranty' => '',
                'TelNoTechnical' => '',
                'FaxNo' => '',
                'EmailAddress' => '',
                'WarrantyEmailAddress' => '',
                'SparesEmailAddress' => '',
                'ContactName' => '',
                'PrimarySupplierID' => '',
                'StartClaimNo' => '',
                'VATNo' => '',
                'IRISManufacturer' => '',
                'FaultCodesReq' => '',
                'ExtendedGuaranteeNoReq' => '',
                'PolicyNoReq' => '',
                'InvoiceNoReq' => '',
                'CircuitRefReq' => '',
                'PartNoReq' => '',
                'OriginalRetailerReq' => '',
                'ReferralNoReq' => '',
                'SerialNoReq' => '',
                'FirmwareReq' => '',
                'WarrantyLabourRateReqOn' => 'None',
                'SageAccountNo' => '',
                'NoWarrantyInvoices' => '',
                'UseProductCode' => '',
                'UseMSNNo' => '',
                'UseMSNFormat' => '',
                'MSNFormat' => '',
                'ValidateDateCode' => '',
                'ForceStatusID' => '',
                'POPPeriod' => '',
                'ClaimPeriod' => '',
                'UseQA' => '',
                'UsePreQA' => '',
                'QAExchangeUnits' => '',
                'CompleteOnQAPass' => '',
                'ValidateNetworkQA' => '',
                'ExcludeFromBouncerTable' => '',
                'BouncerPeriod' => '',
                'CreateTechnicalReport' => '',
                'CreateServiceHistoryReport' => '',
                'IncludeOutOfWarrantyJobs' => '',
                'WarrantyAccountNo' => '',
                'WarrantyPeriodFromDOP' => '',
                'TechnicalReportIncludeAdj' => '',
                'PartNoReqForWarrantyAdj' => '',
                'ForceAdjOnNoParts' => '',
                'ForceKeyRepairPart' => '',
                'ForceOutOfWarrantyFaultCodes' => '',
                'WarrantyExchangeFree' => '',
                'UseFaultCodesForOBFJobs' => '',
                'AutoFinalRejectClaimsNotResubmitted' => '',
                'DaysToResubmit' => '',
                'LimitResubmissions' => '',
                'LimitResubmissionsTo' => '',
                'CopyDOPFromBouncerJob' => '',
                'JobFaultCodeID' => '',
                'PartFaultCodeID' => '',
                'ModelID' => '',
                'BouncerPeriodBasedOn' => 'BookedDate',
                'DisplaySectionCodes' => '',
                'DisplayTwoConditionCodes' => '',
                'DisplayJobPriceStructure' => '',
                'DisplayTelNos' => '',
                'DisplayPartCost' => '',
                'DisplayRepairTime' => '',
                'DisplayAuthNo' => '',
                'DisplayOtherCosts' => '',
                'DisplayAdditionalFreight' => '',
                'NoOfClaimPrints' => '',
                'EDIClaimNo' => '',
                'EDIClaimFileName' => '',
                'EDIExportFormatID' => '',
            ];
            $this->smarty->assign('datarow', $datarow);
        }



        //edit,copy
        if (isset($args['id'])) {
            $id = "";
            $model = $this->loadModel('Manufacturers');


            if (isset($data['ManufacturerID'])) {
                $id = $data['ManufacturerID'];
            }
            if (isset($data['ServiceProviderManufacturerID'])) {
                $id = $data['ServiceProviderManufacturerID'];
            }
            $this->smarty->assign('id', "$id");
            if (isset($args['copy'])) {
                $data = $model->getData($args['id'], $this->session->secondaryTable);
                if (isset($data['ManufacturerID'])) {
                    $id = $data['ManufacturerID'];
                }

                $this->smarty->assign('mode', "copyNew");
            } else {
                $data = $model->getData($args['id'], $this->session->mainTable);
                if (isset($data['ManufacturerID'])) {
                    $id = $data['ManufacturerID'];
                }
                if (isset($data['ServiceProviderManufacturerID'])) {
                    $id = $data['ServiceProviderManufacturerID'];
                }
                $this->smarty->assign('mode', "update");
            }
            $this->smarty->assign('id', "$id");
            if (isset($args['nid'])) {
                $datarow['NetworkID'] = $args['nid'];
            } else {
                $datarow['NetworkID'] = 0;
            }
            $this->smarty->assign('datarow', $data);

            if ($this->user->ServiceProviderID == "" && isset($data)) {

                $spid = $model->getIDFromMain($data['ManufacturerID']);

                if ($spid) {
                    $ancronym = $ServiceProviders->getServiceCentreAncronym($spid);
                    $this->smarty->assign('ancronym', $ancronym);
                } else {
                    $this->smarty->assign('ancronym', "");
                }
            }
            $this->smarty->assign('activeUser', $this->session->activeUser);

            $this->smarty->display('systemAdmin/manufacturersForm.tpl');
        } else {
            if ($this->session->activeUser != "SuperAdmin") {
                //service provider inserting
                if (isset($args['new'])) {


                    $this->smarty->assign('id', null);
                    $this->smarty->assign('mode', "New");
                    $this->smarty->display('systemAdmin/manufacturersForm.tpl');
                } else {
                    //if no distinct item is left  in global list show form instead
                    $count = $manufacturer_model->getMainTableDistinctRecords($this->user->ServiceProviderID);

                    if ($count > 0) {

                        $this->smarty->assign('action', "manufacturers");
                        $this->smarty->assign('controller', "ProductSetup");
                        $this->smarty->display('systemAdmin/insertNewSelector.tpl');
                    } else {
                        $this->smarty->assign('id', null);
                        $this->smarty->assign('mode', "New");
                        $this->smarty->display('systemAdmin/manufacturersForm.tpl');
                    }
                }
            } else {
                $this->smarty->assign('mode', "insert");
                $this->smarty->display('systemAdmin/manufacturersForm.tpl');
            }
        }



        if (!isset($args['id'])) {
            $datarow = [
                'ManufacturerID' => '',
                'ManufacturerName' => '',
                'Acronym' => '',
                'NetworkID' => '',
                'NetworkName' => '',
                'AccountNumber' => '',
                'CostCodeAccountNumber' => '',
                'AuthorisationRequired' => 'No',
                'AuthReqChangedDate' => '',
                'AuthorisationManager' => '',
                'AuthorisationManagerEmail' => '',
                'ManufacturerLogo' => '',
                'AccountNo' => '',
                'Status' => 'Active',
                'BuildingNameNumber' => '',
                'Street' => '',
                'LocalArea' => '',
                'TownCity' => '',
                'CountryID' => '',
                'TelNoSwitchboard' => '',
                'TelNoSpares' => '',
                'TelNoWarranty' => '',
                'TelNoTechnical' => '',
                'FaxNo' => '',
                'EmailAddress' => '',
                'WarrantyEmailAddress' => '',
                'SparesEmailAddress' => '',
                'ContactName' => '',
                'PrimarySupplierID' => '',
                'StartClaimNo' => '',
                'VATNo' => '',
                'IRISManufacturer' => '',
                'FaultCodesReq' => '',
                'ExtendedGuaranteeNoReq' => '',
                'PolicyNoReq' => '',
                'InvoiceNoReq' => '',
                'CircuitRefReq' => '',
                'PartNoReq' => '',
                'OriginalRetailerReq' => '',
                'ReferralNoReq' => '',
                'SerialNoReq' => '',
                'FirmwareReq' => '',
                'WarrantyLabourRateReqOn' => 'None',
                'SageAccountNo' => '',
                'NoWarrantyInvoices' => '',
                'UseProductCode' => '',
                'UseMSNNo' => '',
                'UseMSNFormat' => '',
                'MSNFormat' => '',
                'ValidateDateCode' => '',
                'ForceStatusID' => '',
                'POPPeriod' => '',
                'ClaimPeriod' => '',
                'UseQA' => '',
                'UsePreQA' => '',
                'QAExchangeUnits' => '',
                'CompleteOnQAPass' => '',
                'ValidateNetworkQA' => '',
                'ExcludeFromBouncerTable' => '',
                'BouncerPeriod' => '',
                'CreateTechnicalReport' => '',
                'CreateServiceHistoryReport' => '',
                'IncludeOutOfWarrantyJobs' => '',
                'WarrantyAccountNo' => '',
                'WarrantyPeriodFromDOP' => '',
                'TechnicalReportIncludeAdj' => '',
                'PartNoReqForWarrantyAdj' => '',
                'ForceAdjOnNoParts' => '',
                'ForceKeyRepairPart' => '',
                'ForceOutOfWarrantyFaultCodes' => '',
                'WarrantyExchangeFree' => '',
                'UseFaultCodesForOBFJobs' => '',
                'AutoFinalRejectClaimsNotResubmitted' => '',
                'DaysToResubmit' => '',
                'LimitResubmissions' => '',
                'LimitResubmissionsTo' => '',
                'CopyDOPFromBouncerJob' => '',
                'JobFaultCodeID' => '',
                'PartFaultCodeID' => '',
                'ModelID' => '',
                'BouncerPeriodBasedOn' => 'BookedDate',
                'DisplaySectionCodes' => '',
                'DisplayTwoConditionCodes' => '',
                'DisplayJobPriceStructure' => '',
                'DisplayTelNos' => '',
                'DisplayPartCost' => '',
                'DisplayRepairTime' => '',
                'DisplayAuthNo' => '',
                'DisplayOtherCosts' => '',
                'DisplayAdditionalFreight' => '',
                'NoOfClaimPrints' => '',
                'EDIClaimNo' => '',
                'EDIClaimFileName' => '',
                'EDIExportFormatID' => '',
            ];
        } else {


            $datarow = $manufacturer_model->getAllManufacturers($args['id']);
            if (isset($args['nid'])) {
                $datarow['NetworkID'] = $args['nid'];
            }
        }
        $this->smarty->assign('datarow', $datarow);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('UploadedManufacturerLogo', rand(100, 10000));

        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
        $this->smarty->assign('service_providers', $service_providers);
    }

    public function saveManufacturerAction($args) {
        //Uploading image...

        $manName = $_POST['ManufacturerName'] . "_" . date("U");

        $error = "No error";
        $msg = "";
        $fileElementName = 'ManufacturerLogo';
//            echo"<pre>";
//            print_r($_FILES);
//            echo"</pre>";
        if (!empty($_FILES[$fileElementName]['error'])) {

            switch ($_FILES[$fileElementName]['error']) {

                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder';
                    break;
                case '7':
                    $error = 'Failed to write file to disk';
                    break;
                case '8':
                    $error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $error = 'No error code avaiable';
            }
        } else if (empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {

            $error = 'No file was uploaded..';
        } else {

            $manufacturersLogosDirPath = APPLICATION_PATH . "/../" . $this->config['Path']['manufacturerLogosPath'];

            $tmp_name = $_FILES[$fileElementName]["tmp_name"];
            $imgName = $_FILES[$fileElementName]["name"];

            move_uploaded_file($tmp_name, $manufacturersLogosDirPath . $imgName);

            $imgNameArray = explode(".", $_FILES[$fileElementName]["name"]);
            $imgExt = strtolower($imgNameArray[count($imgNameArray) - 1]);

            $imgNewName = $manName . "." . $imgExt;

            @rename($manufacturersLogosDirPath . $imgName, $manufacturersLogosDirPath . $imgNewName);

            $msg .= $imgName;

            // $msg .= " File Name: " . $_FILES[$fileElementName]['name'] . ", ";
            // $msg .= " File Size: " . @filesize($_FILES[$fileElementName]['tmp_name']);
            //for security reason, we force to remove all uploaded file
            //@unlink($_FILES['fileToUpload']);		
            $_POST['ManufacturerLogo'] = $imgNewName;
        }

        $this->log($error, "imgUpload---");
        $model = $this->loadModel('Manufacturers');
        $model->saveManufacturer($_POST, $this->session->mainTable, $this->session->secondaryTable);
        $this->redirect('ProductSetupController', 'manufacturersAction');
    }

    public function deleteManufacturerAction($args) {
        $model = $this->loadModel('Manufacturers');
        $model->deleteManufacturer($args['id'], $this->session->mainTable);
        $this->redirect('ProductSetupController', 'manufacturersAction');
    }

    public function addAssocManufacturerAction($args) {
        $model = $this->loadModel('Manufacturers');
        $model->addAssocManufacturer($_POST['PID'], $_POST['CID']);
    }

    public function loadAssocManufacturerAction($args) {
        $model = $this->loadModel('Manufacturers');
        $data = $model->loadAssocManufacturer($_POST['PID']);
        echo json_encode($data);
    }

    public function delAssocManufacturerAction($args) {
        $model = $this->loadModel('Manufacturers');
        $model->delAssocManufacturer($_POST['PID'], $_POST['CID']);
    }

    public function saveModelAction($args) {
        $model = $this->loadModel('Models');
        $model->saveModel($_POST, $this->session->mainTable, $this->session->secondaryTable);
        $this->redirect('ProductSetupController', 'modelsAction');
    }

    public function deleteModelAction($args) {
        $model = $this->loadModel('Models');
        $model->deleteModel($args['id'], $this->session->mainTable);
        $this->redirect('ProductSetupController', 'modelsAction');
    }

////this functions used to deal with accessory to model linking
    public function addAccessoryAction($args) {
        $model = $this->loadModel('Models');
        $model->addAccessory($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

    public function loadAccessoryAction($args) {
        $model = $this->loadModel('Models');
        $data = $model->loadAccessory($_POST['ServiceProviderModelID']);
        echo json_encode($data);
    }

    public function delAccessoryAction($args) {
        $model = $this->loadModel('Models');
        $model->delAccessory($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

////this functions used to deal with accessory to model linking END



    public function addcolourAction($args) {
        $model = $this->loadModel('Models');
        $model->addcolour($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

    public function loadcolourAction($args) {
        $model = $this->loadModel('Models');
        $data = $model->loadcolour($_POST['ServiceProviderModelID']);
        echo json_encode($data);
    }

    public function delcolourAction($args) {
        $model = $this->loadModel('Models');
        $model->delcolour($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

    public function addProductCodeAction($args) {
        $model = $this->loadModel('Models');
        $model->addProductCode($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

    public function loadProductCodeAction($args) {
        $model = $this->loadModel('Models');
        $data = $model->loadProductCode($_POST['ServiceProviderModelID']);
        echo json_encode($data);
    }

    public function delProductCodeAction($args) {
        $model = $this->loadModel('Models');
        $model->delProductCode($_POST['ServiceProviderModelID'], $_POST['ACCID']);
    }

    public function resetDisplayPreferencesAction($args) {
        $typeAction = $args['typeAction'];
        $this->page = $this->messages->getPage($typeAction, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->resetDisplayPreferences($this->user->UserID, $this->page['config']['pageID']);
        $this->redirect('ProductSetupController', $typeAction . "Action");
    }

    //this is used only for new supplier insert
    public function loadManufacturerListAction($args) {

        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');




        $columns = array("ManufacturerID", "ManufacturerName");

        $extraCol = array("<input type='checkbox'>");
        $t = $this->session->secondaryTable;
        //$joins="";
        $spid = $this->user->ServiceProviderID;
        $where = " and $t.ManufacturerID not in(select ManufacturerID from service_provider_manufacturer where ServiceProviderID=$spid)";
        $data = $datatable->datatableSS($t, $columns, $dd, "", $extraCol, 0, false, false, false, "Approved", "", $where);
        $this->log($data, "datatableData");
        echo json_encode($data);
    }

    public function loadModelsTableAction($args) {

        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');
        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
        $table = $this->session->mainTable;
        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
            $aproved = "";
            $spp = "ServiceProvider";
            $pref="service_provider_";
        } else {
            $sp = false;
            $aproved = "Approved";
            $spp = "";
            $pref="";
           
        }
       
        if (isset($args['unaproved'])) {
            $aproved = "NotApproved";
        }
        if (isset($args['pending'])) {
            $aproved = "Pending";
        }
          $where='';
        if (isset($args['showSpUnmodified'])) {
            
            $where.=" and $table.CreatedDate='0000-00-00 00:00:00'";
        }
        
        if (isset($args['modelid'])) {
            $sp = $args['modelid'];
            $table = $this->session->secondaryTable;
        }

        $keys_data = $datatable->getAllFieldNames($table, $this->user->UserID, $this->page['config']['pageID']);
        $columns = $keys_data[0];


        // $columns=$datatable->getAllFieldNames('currency');
        //$columns="";
//       $joins=" left join country on  country.CountryID=".$table.".CountryID
//                left join currency cu on cu.CurrencyID=".$table.".DefaultCurrencyID";
        $joins = "";
        $this->log($args, "datatableData_args");
        //unit type filter
        if (isset($args[2]) && $args[2] != '') {
            $joins.=" join unit_type ut on ut.UnitTypeID=$table.UnitTypeID and ut.UnitTypeID=" . $args[2];
        } else {
            $joins.=" join unit_type ut on ut.UnitTypeID=$table.UnitTypeID ";
        }
        //manufacturer filter
        if (isset($args[3]) && $args[3] != '') {
            $joins.=" join $pref"."manufacturer mm on mm." . $spp . "ManufacturerID=$table." . $spp . "ManufacturerID and mm.ManufacturerID=" . $args[3];
        } else {
            $joins.=" join $pref"."manufacturer mm on mm." . $spp . "ManufacturerID=$table." . $spp . "ManufacturerID ";
        }

        

        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns = array('<input type="checkbox">'), 0, false, $inactive, $sp, $aproved,'',$where);
        //$this->log($data,"datatableData");
        echo json_encode($data);
    }

    public function processModelAction($args) {

        $model = $this->loadModel('Models');
        $Skyline = $this->loadModel('Skyline');
        $manufacturer = $this->loadModel('Manufacturers');
        $Currency = $this->loadModel('Currency');
        $ServiceProviders = $this->loadModel('ServiceProviders');
        $accessory_model = $this->loadModel('Accessories');
        $colour_model = $this->loadModel('Colours');
        $productCode_model = $this->loadModel('ProductCodes');

        $countries = $Skyline->getCountriesCounties();
        $currencylist = $Currency->getCurrencyList();
        $manufacturers = $Skyline->getManufacturer();
        $unitTypes = $Skyline->getUnitTypes();
        $accessories = $accessory_model->getAccessoriesList();
        $colours = $colour_model->getColourList();
        $productCodes = $productCode_model->getProductCodeList();
        if ($this->user->ServiceProviderID != "") {

            $manufacturers = $manufacturer->getAllSpManufacturers($this->user->ServiceProviderID);
        }

        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('countries', $countries);
        $this->smarty->assign('currencylist', $currencylist);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('unitTypes', $unitTypes);
        $this->smarty->assign('manufacturers', $manufacturers);
        $this->smarty->assign('accessories', $accessories);
        $this->smarty->assign('colours', $colours);
        $this->smarty->assign('productCodes', $productCodes);
        $this->smarty->assign('insert_page_legend', "Insert Model");
        $this->smarty->assign('form_legend', "Model");
        $this->smarty->assign('activeUser', $this->session->activeUser);






        //edit,copy
        if (isset($args['id'])) {


            if (isset($args['copy'])) {

                $this->smarty->assign('mode', "copyNew");
                $data = $model->getData($args['id'], $this->session->secondaryTable);
                $data['ServiceProviderManufacturerID'] = $manufacturer->getSpManFromMainManufacturerID($data['ManufacturerID']);
            } else {
                $this->smarty->assign('mode', "update");
                $this->smarty->assign('form_legend', "Edit Model");
                $data = $model->getData($args['id'], $this->session->mainTable);
            }


            $this->smarty->assign('data', $data);
            if ($this->user->ServiceProviderID == "" && isset($data)) {

                $spid = $model->getIDFromMain($data['ModelID']);

                if ($spid) {
                    $ancronym = $ServiceProviders->getServiceCentreAncronym($spid);
                    $this->smarty->assign('ancronym', $ancronym);
                } else {
                    $this->smarty->assign('ancronym', "");
                }
            }
            $this->smarty->assign('activeUser', $this->session->activeUser);

            $this->smarty->display('systemAdmin/modelsForm.tpl');
        } else {
            if ($this->session->activeUser != "SuperAdmin") {
                if (isset($args['new'])) {
                    $this->smarty->assign('mode', "New");

                    $this->smarty->display('systemAdmin/modelsForm.tpl');
                } else {
                     //if no distinct item is left  in global list show form instead
                    $count = $model->getMainTableDistinctRecords($this->user->ServiceProviderID);

                    if ($count > 0) {

                        $this->smarty->assign('action', "models");
                        $this->smarty->assign('controller', "ProductSetup");
                        $this->smarty->display('systemAdmin/insertNewSelector.tpl');
                    } else {
                        $this->smarty->assign('id', null);
                        $this->smarty->assign('mode', "New");
                        $this->smarty->display('systemAdmin/manufacturersForm.tpl');
                    }
                    
                    
                }
            } else {

                $this->smarty->assign('form_legend', "New Model");
                $this->smarty->assign('mode', "insert");
                $this->smarty->display('systemAdmin/modelsForm.tpl');
            }
        }
    }

    //this is used only for new model insert
    public function loadModelListAction($args) {

        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');




        $columns = array("ModelID", "ModelNumber");


$t=$this->session->secondaryTable;
        //$joins="";
$spid=$this->user->ServiceProviderID;
$extraCol = array("<input type='checkbox'>");
$where = " and $t.ModelID not in (select ModelID from service_provider_model where ServiceProviderID=$spid)
    and ManufacturerID in (select spm.ManufacturerID from service_provider_manufacturer spm where ServiceProviderID=$spid)
";
        
        $data = $datatable->datatableSS($t, $columns, $dd, "", $extraCol, 0, false, false, false, "Approved", "", $where);
        $this->log($data, "datatableData");
        echo json_encode($data);
    }
    
    
    
     public function accessoryAction($args) {
         
        $this->page = $this->messages->getPage('accessory', $this->lang);
        $this->session->mainPage="accessory";
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('accessory');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Accessories');
        $datatable = $this->loadModel('DataTable');
        if (!isset($args[0])) {
            $ut = false;
        } else {
            $ut = $args[0];
        }
        $data = $model->getAccessoriesList($ut);
        $this->smarty->assign('data', $data);
        $keys_data = $datatable->getAllFieldNames($this->session->mainPage,$this->user->UserID,$this->page['config']['pageID']);

        if(isset($keys_data[1])){
//        $keys=$keys_data[1];
            $keys=  array_values($keys_data[1]);
        }else{
        $keys=array();
        } 
        
        $this->smarty->assign('data_keys', $keys);


        //to use this function we need provide pageid, it can be set in language config file
        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID']);
        $columnStringsSA = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'sa');


        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = $columnStrings['ColumnOrderString'];
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
            $columnNameStringSA = array();
        }

        $Skyline = $this->loadModel('Skyline');
        $unitTypes = $Skyline->getUnitTypes();

        $this->smarty->assign('unitTypes', $unitTypes);

        $this->smarty->assign('uId', $ut);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('nId', 0);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/accessory.tpl');
    }

    public function processAccessoryAction($args) {
        $this->page = $this->messages->getPage('accessoryForm', $this->lang);
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('Accessories');
        if (isset($args['id'])) {
            $datarow = $model->getAccessoryData($args['id']);
        } else {
            $datarow = array(
                'AccessoryName' => '',
                'AccessoryID' => '',
                'Status' => 'Active',
                'UnitTypeID' => '',
            );
        }
        $Skyline = $this->loadModel('Skyline');
        $unitTypes = $Skyline->getUnitTypes();

        $this->smarty->assign('unitTypes', $unitTypes);
        $this->smarty->assign('unitTypes', $unitTypes);
        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/accessoryForm.tpl');
    }

    public function saveAccessoryAction($args) {        
        $model = $this->loadModel('Accessories');
        if ($_POST['AccessoryID'] != '') {              
            $model->updateAccessory($_POST);           
        } else {
            $model->insertAccessory($_POST);
        }
        //commented by srinvas
        //$this->redirect('ProductSetup/accessory/' . $_POST['UnitTypeID']);
        exit;
    }

    public function deleteAccessoryAction($args) {
        $model = $this->loadModel('Accessories');

        $model->deleteAccessory($args['id']);



        $this->redirect('ProductSetupController', 'accessoryAction');
    }   
    
    public function loadAccessoryTableAction($args)
            {
     
       $dd=$_GET;

      $this->page =  $this->messages->getPage('accessory', $this->lang);
      
      
        $datatable= $this->loadModel('DataTable');
        if(isset($args['inactive'])){
           $inactive=true;
       }else{
           $inactive=false;
       }
      // $table=$this->session->mainTable;
      // $this->log($table);
       $table='accessory';
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
           $aproved="";
       }else{
           $sp=false;
           $aproved="Approved";
       }
       if(isset($args['unaproved'])){
           $aproved="NotApproved";
       }

       if(isset($args['accessoryid'])){
           $sp=$args['accessoryid'];
           $table='accessory';
       }
      $t = $this->page['config']['pageID'];
      $this->log($t);
        $keys_data=$datatable->getAllFieldNames($table,$this->user->UserID,$this->page['config']['pageID']);
        $columns=$keys_data[0];

       
       $joins=" left join unit_type on  unit_type.UnitTypeID=".$table.".UnitTypeID";
        
      // $joins="";
       
       
        $data=$datatable->datatableSS($table,$columns,$dd,$joins,$extraColumns=array('<input type="checkbox">'),0,false,$inactive,$sp,$aproved);
      $this->log($data,"datatableData");
        echo json_encode($data);
    }

}

?>
