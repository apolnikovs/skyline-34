<?php

/**
 * Short Description of JobController.
 * 
 * Long description of JobController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.68
 *   
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 * ??/??/????  1.00A   Nageswara Rao Kanteti 
 * ??/??/????  1.01    Brian Etherington     Job create, Open and over dues job actions handling.
 * 22/08/2012  1.02    Nageswara Rao Kanteti Written networkClientPopUp action.
 * 23/08/2012  1.1     Brian Etherington     #59 Resolved issue with loading Manufacturer model in Job Booking (ProcessData method)
 * 31/08/2012  1.11    Andrew J. Williams    Test Report 103 - Graph Formating for Open and Overdue Jobs
 * 17/09/2012  1.12    Andrew J. Williams    Issue #69 - Smasung API calling issues
 * 09/10/2012  1.13    Brian Etherington     Issue #91 Add RepairType to Create Job
 * 23/10/2012  1.14    Brian Etherington     Fix problem with batchID in job booking - make it OPTIONAL!!
 * 26/10/2012  1.15    Nageswara Rao Kanteti Written raJobs action. 
 * 25/01/2013  1.16    Brian Etherington     Fix case of jobDetailsContactHistoryForm.tpl
 * 29/01/2013  1.17    Brian Etherington     Fix case of jobDetails.ini
 * 11/02/2013  1.18    Vykintas Rutkunas     Service Apprisal
 * 12/02/2013  1.19    Nageswara Rao Kanteti Samsung Experience
 * 15/02/2013  1.20    Andrew J. Williams    Changes to customner receipt, changes as requested by Joe
 * 18/02/2013  1.21    Andrew J. Williams    Changes as required by Job Card
 * 18/02/2013  1.22    Andrew J. Williams    Added Service Report 
 * 20/02/2013  1.23    Andrew J. Williams    Issue 225 - Small Corrections to Service Report
 * 21/02/2013  1.24    Andrew J. Williams    Issue 228 - Add second logo to paperwork for Samsung Experience
 * 22/02/2013  1.25    Vykintas Rutkunas     Job Service Provider allocation changes
 * 22/02/2013  1.26    Nageswara Rao Kanteti Cancel job email  and Branch Current Location of Product are done.
 * 27/02/2013  1.27    Nageswara Rao Kanteti Preferential Service Providers section has been added on Open and Overdue jobs page.
 * 01/03/2013  1.28    Nageswara Rao Kanteti Samsung Experience Enhancements all tasks are done which are assigned to Nag.
 * 07/03/2013  1.29    Nageswara Rao Kanteti Email Service Instruction email sending to both SP Service Manager and Admin Supervisor
 * 08/03/2013  1.30    Vykintas Rutkunas     Job Booking SP allocation fix
 * 12/03/2013  1.31    Vykintas Rutkunas     Job Allocation functionality fix
 * 13/03/2013  1.32    Nageswara Rao Kanteti Diary wall board enhancements and open, overdue jobs cosmetic changes..
 * 18/03/2013  1.33    Vykintas Rutkunas     Closed Jobs completed
 * 19/03/2013  1.34    Vykintas Rutkunas     Job Controller fix
 * 21/03/2013  1.35    Vykintas Rutkunas     Rapid Despatch functional
 * 22/03/2013  1.36    Andrew J. Williams    Barcodes on paperwork
 * 22/03/2013  1.37    Vykintas Rutkunas     Job Controller - sending emails change
 * 26/03/2013  1.38    Vykintas Rutkunas     Closed Jobs changes
 * 26/03/2013  1.39    Vykintas Rutkunas     Various fixes
 * 26/03/2013  1.40    Vykintas Rutkunas     Home page fix and Rapid Process printing finished
 * 26/03/2013  1.41    Brian Etherington     WIP Courier Coolection Booking
 * 27/03/2013  1.42    Nageswara Rao Kanteti Samsung Experience Enhancements V5 - in progress
 * 29/03/2013  1.43    Nageswara Rao Kanteti Overdue and open jobs TAT task done.
 * 02/04/2013  1.44    Vykintas Rutkunas     Open Jobs changes
 * 03/04/2013  1.45    Vykintas Rutkunas     Open and Overdue jobs changes
 * 03/04/2013  1.46    Brian Etherington     Prevent Courier Collection form from updating Collection address during Job Booking.
 * 03/04/2013  1.47    Vykintas Rutkunas     Job class fixes
 * 03/04/2013  1.48    Nageswara Rao Kanteti Email project is done except one email.
 * 04/04/2013  1.49    Andrew J. Williams    Open Jobs Screen & Graphical Analysis use the Same definition of Open Job
 * 04/04/2013  1.50    Nageswara Rao Kanteti Email project latest changes are done.
 * 08/04/2013  1.51    Andrew J. Williams    Dials addes to Closed Jobs Screen
 * 19/04/2013  1.52    Andris Polnikovs      Table Display Preference functions addet
 * 22/04/2013  1.53    Andrew J. Williams    Data Integrity Check - Servicebase Refresh
 * 26/04/2013  1.54    Nageswara Rao Kanteti 3.2  sms administration  has been done (1304 Skyline Issues April (Nag).docx)
 * 26/04/2013  1.55    Vykintas Rutkunas     Service Base work in progress
 * 30/04/2013  1.56    Andrew J. Williams    Issue 339 - Refresh Dialogue Layout (Request from Chris Berry)
 * 30/04/2013  1.57    Vykintas Rutkunas     Service Action Tracking work in progress
 * 01/05/2013  1.58    Nageswara Rao Kanteti Booking performance enhancements are  done
 * 01/05/2013  1.59    Vykintas Rutkunas     Service Action Tracking finished
 * 03/05/2013  1.60    Vykintas Rutkunas     Various changes/fixes, Java initial code submission
 * 03/05/2013  1.61    Andris Polnikovs      Signed-off-by: a.polnikovs <a.polnikovs@pccsuk.com>
 * 07/05/2013  1.62    Nageswara Rao Kanteti sms enhancements are done except sending sms to users.
 * 07/05/2013  1.63    Vykintas Rutkunas     Changes and fixes
 * 07/05/2013  1.64    Andris Polnikovs      Signed-off-by: a.polnikovs <a.polnikovs@pccsuk.com>
 * 09/05/2013  1.65    Andrew J. Wiilliams   Add servicebase refresh to Closed Jobs
 * 10/05/2013  1.66    Andris Polnikovs      Closed Jobs fullscreen and display preferences
 * 05/05/2013  1.67    Andrew J. Williams    Added Servicebase Bulk Refresh
 * 06/06/2013  1.68    Andris Polnikovs      Fullscreen and display preferences for overdue page
 ******************************************************************************/

require_once('CustomSmartyController.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');
include_once(APPLICATION_PATH.'/models/AuditTrailActions.class.php');
include_once(APPLICATION_PATH.'/controllers/SamsungClientController.class.php');    /* Required for interfacing with Samsung */


class JobController extends CustomSmartyController {
    
    public $smarty;
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $user = null;
    private $lang = 'en';
    public $debug = false;
    public $statuses  = array(
                     
                     0=>array('Name'=>'Active', 'Code'=>'Active'),
                     1=>array('Name'=>'In-Active', 'Code'=>'In-active')
                    
                     );
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
        
        if(isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
	    $this->smarty->assign('loggedin_user', $this->user);
	    
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            
            //$this->log(var_export($this->user, true));
            

        } else {

            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('_theme', 'skyline');
           // $this->smarty->assign('name','');
           // $this->smarty->assign('last_logged_in','');
           // $this->smarty->assign('logged_in_branch_name', '');
            $this->redirect('index',null, null);
            

        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        
        $this->smarty->assign('showTopLogoBlankBox', false);
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
	
	//Access permissions and smarty variables are initiated/assigned here.	
	//fix applied, checking if user is logged in first
	if(isset($_SESSION["UserID"])) {
	    $user_model->initiatePermissions($this);
	}
	
    }
    
    
    
    public function AddContactNoteAction($args) {
               
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            //Request Method is POST so action the form request
            
            $model = $this->loadModel('ContactHistory');
            
            $data = [   'JobID' => $_POST['JobID'],
                        'ContactHistoryActionID' => $_POST['ContactHistoryActionID'],
                        'BrandID' => $this->user->DefaultBrandID,
                        'ContactDate' => date('Y-m-d'), 
                        'ContactTime' => date('H:i'),
                        'UserCode' => $this->user->UserID,
                        'Note' => $_POST['Note'] 
                    ];
            
            $result = $model->create($data);
            
            if ($result['status'] == 'SUCCESS') $result['status'] = 'OK';
            
            $result['count'] = $model->getNumberForJob( $_POST['JobID'] );
            
            echo json_encode( $result );
        
        } else {
            
            //Request Method is GET so return Form HTML
        
            if (!isset($args[0])) throw new Exception('AddContactNoteAction: JobId Missing');
        
            $model = $this->loadModel('ContactHistoryActions');
        
            $actions = $model->fetchBrandContactActions($this->user->DefaultBrandID);
        
            $page = $this->messages->getPage('jobDetails', $this->lang);

            $this->smarty->assign('JobID', $args[0]);
            $this->smarty->assign('contact_history_actions', $actions);
            $this->smarty->assign('accessErrorFlag', false);
            $this->smarty->assign('page', $page);
        
            $htmlCode = $this->smarty->fetch('jobDetailsContactHistoryForm.tpl');
            echo $htmlCode;
            
        }
        
    }
       
    
    
    public function overdueJobsResultAction($args) { 
          $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
	$Job = $this->loadModel('Job');

	$params = array_merge($args, $_REQUEST);
	
	$output     = $Job->fetchAllOverdue($params,$this->user->UserID,$this->page['config']['pageID']);
       
	echo json_encode( $output );
         
    }
     
     
    public function overdueJobsSummaryResultAction($args) { 
     
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        
        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'OpenJobs'));
        
	$Job = $this->loadModel('Job');

	$params = array_merge($args, $_REQUEST);
	
	$output     = $Job->fetchAllOverdueSummary($params,$this->user->UserID,$this->page,$TatResult);
       
	echo json_encode( $output );
         
    }
     
     
     
     /**
     * bookingPerformanceResultAction
     * This will get a wall board details all jobs booked per network.
     * 
     * @param  array $args
     * @return void
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     ********************************************/
      public function bookingPerformanceResultAction( $args ) { 
         
            $Job = $this->loadModel('Job');

            $output     = $Job->bookingPerformance($args);
            echo json_encode( $output );
         
     }
     
     
    
    
    
    
//     public function postcodeAction( $args ) { 
//         
//         $this->smarty->assign('page', $this->messages->getPage('PostCodeNotListed', $this->lang));
//         
//         $firstArg = isset($args[0]) ? $args[0] : '';
//
//         $this->smarty->assign('PostalCode', $firstArg);
//               
//          $html_code =   $this->smarty->fetch('PostCodeNotListed.tpl'); 
//          echo $html_code;   
//         
//     }
    
     /*
     public function stockCodeResultAction( $args ) { 
     
            $firstArg = isset($args[0]) ? $args[0] : '';
     
            $_POST['ModelNoSearchStr'] = '';
            $_POST['StockCodeSearchStr'] = $firstArg;

            $Job = $this->loadModel('Job');
            $output     = $Job->fetchProductResult($_POST);
            echo json_encode( $output );
   
     }
     */
     
     
     
      public function stockCodeSearchAction( /*$args */ ) { 
     
            $page = $this->messages->getPage('ProductSearch', $this->lang);
           
            $afn_product = $this->messages->getAFN('AFN-PRODUCT');
            $afn_stock_code = $this->messages->getAFN('AFN-STOCK CODE');
             
            $page['Text']['product_description'] = str_replace("%%afn-product%%", $afn_product, $page['Text']['product_description']); 
            $page['Text']['product_type'] = str_replace("%%afn-product%%", $afn_product, $page['Text']['product_type']); 
            $page['Text']['stock_code'] = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Text']['stock_code']); 
            $page['Text']['stock_code_lookup'] = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Text']['stock_code_lookup']); 
            
            $this->smarty->assign('page', $page);
            $this->smarty->assign('search_form_title', $page['Text']['stock_code_lookup']);
                        
            $html_code =   $this->smarty->fetch('ProductSearch.tpl'); 
            echo $html_code;

     }
     
     /******
     public function modelNoResultAction( $args ) { 
     
            $firstArg = isset($args[0]) ? $args[0] : '';
            
            $_POST['StockCodeSearchStr'] = ''; 
            $_POST['ModelNoSearchStr'] = $firstArg;

            $Job = $this->loadModel('Job');
            //$output     = $Job->fetchProductResult($_POST);
            $output     = $Job->fetchModelResult($_POST);
            

            echo json_encode( $output );

     }
     *****/
     
     
     public function modelNoSearchAction( /*$args */ ) { 
         
             $page = $this->messages->getPage('ProductSearch', $this->lang);
             $afn_product = $this->messages->getAFN('AFN-PRODUCT');
             $afn_stock_code = $this->messages->getAFN('AFN-STOCK CODE');
             
             $page['Text']['product_description'] = str_replace("%%afn-product%%", $afn_product, $page['Text']['product_description']); 
             $page['Text']['product_type'] = str_replace("%%afn-product%%", $afn_product, $page['Text']['product_type']); 
             $page['Text']['stock_code'] = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Text']['stock_code']); 
             $page['Text']['stock_code_lookup'] = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Text']['stock_code_lookup']); 
             
             $this->smarty->assign('page', $page);
             $this->smarty->assign('search_form_title', $page['Text']['model_number_lookup']);

             $html_code =   $this->smarty->fetch('ProductSearch.tpl'); 
             echo $html_code;

            return;
     }
     
     
     
     
     public function noEmailPageAction( /*$args */ ) { 
         
             $page = $this->messages->getPage('noEmailPage', $this->lang);
             
             
             $this->smarty->assign('page', $page);
             

             $html_code =   $this->smarty->fetch('noEmailPage.tpl'); 
             echo $html_code;

            return;
     }
     
     
     
     public function previewAction( /*$args */ ) { 
         
               // $this->log("PreviewJob:");
               // $this->log($_POST);
         
         
         
         
         
               $jb_datarow = array(
                        'NetworkID', 
                        'ClientID',
                        'BranchID',
                        'DefaultBranchID', 
                        'BrandID', 
                        'CustomerType',
                        'CompanyName',
                        'CustomerTitleID', 
                        'ContactFirstName',
                        'ContactLastName',
                        'PostalCode', 
                        'BuildingName',
                        'Street',
                        'LocalArea',
                        'City',
                        'CountyID',
                        'CountryID',
                        'ContactEmail',
                        'ContactHomePhone',
                        'ContactHomePhoneExt',
                        'ContactMobile',
                        'JobTypeID',
                        'ServiceTypeID',
                        //'RepairTypeID'=>'1',
                        'LocateBy',
                        'StockCode',
                        'ModelName',
                        'ProductDescription',
                        'UnitTypeID',
                        'ManufacturerID',
                        'SerialNo',
                        'ReportedFault',
                        'ProductLocation',
                        'OriginalRetailerPart1',
                        'OriginalRetailerPart2',
                        'OriginalRetailerPart3',
                        'DateOfPurchase',
                        'ReceiptNo',
                        'Insurer',
                        'PolicyNo',
                        'AuthorisationNo',
                        'Notes',
                        'ReferralNo',
                        'PostcodeLookup',
                        'BrandCountry',
                        'CountryMandatory',
                        'ShowStockCode',
                        'ReferralNumberRequired',
                        'JobTypeCode',
                        'data_protection_act',
                        'ColAddPostcode',
                        'ColAddCompanyName',
                        'ColAddBuildingNameNumber',
                        'ColAddStreet',
                        'ColAddLocalArea',
                        'ColAddCity',
                        'ColAddCountyID',
                        'ColAddCountryID',
                        'ColAddEmail',
                        'ColAddPhone',
                        'ColAddPhoneExt',
                        'AutoPopulateBranchID',
                        'JobSourceID',
                        'Accessories',
                        'UnitCondition',
                        'OriginalRetailerFormElement',
                        'DisplayCompulsory',
                        'MobilePhoneNetworkID',
                        'SendRepairCompleteTextMessage',
                        'ImeiNo',
						'AccessoriesRadio',
						'BranchIDAddress',
						'BuildingNameNumber',
						'ContactAltMobile',
						'DateOfPurchaseDay',
						'DateOfPurchaseMonth',
						'DateOfPurchaseYear',
						'HiddenAccessoriesFlag',
						'HiddenStockCode',
						'OriginalRetailerFullPart',
						'RequestNewUnitTypeText',
						'SMSPopupPage',
						'SendServiceCompletionText',
						'jb_data_protection_act'

                        );
         
                foreach($jb_datarow as $jbd)
                {
                    if(!isset($_POST[$jbd]))
                    {    
                        
                        $_POST[$jbd]  = NULL;
                    }
                }    
         
         
         
         
         
         
         
                //Storing Retailer Name if user has entered it in dropdown/autocomplete text box..
                if(isset($_POST['OriginalRetailerFullPart']))
                {
                    
                    if($_POST['OriginalRetailerFullPart']!='')
                    {
                        $RetailersModel          = $this->loadModel('Retailers');
                        
                        $retailerDetails  = $RetailersModel->fetchRow(array('BranchID'=>$_POST['BranchID'], "RetailerName"=>$_POST['OriginalRetailerFullPart']));
            
                        if(isset($retailerDetails['RetailerID']))
                        {
                            $RetailersModel->processData(array('RetailerID'=>$retailerDetails['RetailerID'], 'RetailerName'=>$_POST['OriginalRetailerFullPart'], 'BranchID'=>$_POST['BranchID'], 'Status'=>'Active'));
                        }
                        else
                        {
                          $RetailersModel->processData(array('RetailerName'=>$_POST['OriginalRetailerFullPart'], 'BranchID'=>$_POST['BranchID'], 'Status'=>'Active'));
                        }
                    }
                    
                    $_POST['OriginalRetailerPart1'] = $_POST['OriginalRetailerFullPart'];    
                    $_POST['OriginalRetailerPart2'] = '';    
                    $_POST['OriginalRetailerPart3'] = '';        
                    
                }    
         
         
         
     
                $Skyline = $this->loadModel('Skyline');
                $afn_job     = $this->messages->getAFN('AFN-JOB');
                $afn_product = $this->messages->getAFN('AFN-PRODUCT');
                $page = $this->messages->getPage('jobConfirmation', $this->lang);
                
                $page['Labels']['job_type'] = str_replace("%%afn-job%%", $afn_job, $page['Labels']['job_type']);
                $page['Buttons']['cancel_job_booking'] = str_replace("%%afn-job%%", $afn_job, $page['Buttons']['cancel_job_booking']);
                $page['Text']['jcr_legend'] = str_replace("%%afn-job%%", $afn_job, $page['Text']['jcr_legend']);
                $page['Text']['email'] = str_replace("%%afn-job%%", $afn_job, $page['Text']['email']);
                $page['Text']['mobile'] = str_replace("%%afn-job%%", $afn_job, $page['Text']['mobile']);
                $page['Text']['jcr_info_top_text'] = str_replace("%%afn-job%%", $afn_job, $page['Text']['jcr_info_top_text']);

                $page['Labels']['product_type'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_type']); 
                $page['Labels']['product_location'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_location']); 
  
                $this->smarty->assign('page', $page);
         
                if ($_SERVER['REQUEST_METHOD'] != 'POST') {
                    throw new Exception('jobConfirmation not POST');
                }

                $jb_confirm  = array();
                
                
                
               // $NetworkID = ($this->user->NetworkID)?$this->user->NetworkID:NULL;
               // $ClientID  = ($this->user->ClientID)?$this->user->ClientID:NULL;
                $NetworkID   = $_POST['NetworkID'];
                $ClientID    = $_POST['ClientID'];
                
                //Making data into uppder case..
                $_POST['ModelName'] = strtoupper($_POST['ModelName']);
                $_POST['SerialNo'] = isset($_POST['SerialNo'])?strtoupper($_POST['SerialNo']):NUll;
                $_POST['PolicyNo'] = strtoupper($_POST['PolicyNo']);
                $_POST['AuthorisationNo'] = strtoupper($_POST['AuthorisationNo']);
                $_POST['ReferralNo'] = strtoupper($_POST['ReferralNo']);
                $_POST['ReceiptNo'] = strtoupper($_POST['ReceiptNo']);
                
                //Uppercase the first character of each word in a string 
                $_POST['CompanyName'] = isset($_POST['CompanyName'])?ucwords(strtolower($_POST['CompanyName'])):NULL;
                $_POST['ContactFirstName'] = ucwords(strtolower($_POST['ContactFirstName']));
                $_POST['ContactLastName'] = ucwords(strtolower($_POST['ContactLastName']));
                $_POST['BuildingNameNumber'] = ucwords(strtolower($_POST['BuildingNameNumber']));
                $_POST['Street'] = ucwords(strtolower($_POST['Street']));
                $_POST['LocalArea'] = ucwords(strtolower($_POST['LocalArea']));
                $_POST['City'] = ucwords(strtolower($_POST['City']));
                $_POST['OriginalRetailerPart1'] = ucwords(strtolower($_POST['OriginalRetailerPart1']));
                $_POST['OriginalRetailerPart2'] = ucwords(strtolower($_POST['OriginalRetailerPart2']));
                $_POST['OriginalRetailerPart3'] = ucwords(strtolower($_POST['OriginalRetailerPart3']));
                $_POST['Insurer'] = ucwords(strtolower($_POST['Insurer']));
                
                
                
                
                

                //Need correction.
                $ModelID    =  0;
                $UnitTypeID =  $_POST['UnitTypeID'];
                
               // $this->log(var_export("first:".$UnitTypeID, true));
               // $this->log(var_export("first:".$ModelID, true));

                if(isset($_POST['ModelName']) && $_POST['ModelName'])
                {
                    //Getting Model ID for given Model Number/Name.
                    $modelModel = $this->loadModel('Models');
                    $ModelDetails = $modelModel->isValid(0, $_POST['ModelName'], $_POST['ManufacturerID'], true);
                    
                    if(is_array($ModelDetails))
                    {
                        $ModelID     = isset($ModelDetails['ModelID'])?$ModelDetails['ModelID']:$ModelID;
                        
                       // if(isset($this->user->Permissions['AP7040']) && isset($ModelDetails['UnitTypeID']) && $ModelDetails['UnitTypeID']==1 && $UnitTypeID)
                        
                        if(isset($ModelDetails['UnitTypeID']) && $ModelDetails['UnitTypeID']==1 && $UnitTypeID)//we have removed access permission to change model's unit type if it has unknow unit type.
                        {
                             $_POST['UpdateModelUnitType'] = $UnitTypeID;
                        }
                        else
                        {
                            $UnitTypeID  = isset($ModelDetails['UnitTypeID'])?$ModelDetails['UnitTypeID']:$UnitTypeID;
                            $_POST['UpdateModelUnitType'] = false;
                        }
                    }
                }
                
              //  $this->log(var_export("second:".$UnitTypeID, true));
             //   $this->log(var_export("second:".$ModelID, true));


                $ProductID =  0;
                
                
                if(isset($_POST['HiddenStockCode']) && $_POST['HiddenStockCode'])
                {
                    //Getting Product ID for given Stock Code.
                    $ProductNumbersModel = $this->loadModel('ProductNumbers');
                    $ProductDetails = $ProductNumbersModel->isValid($NetworkID, $ClientID, $_POST['HiddenStockCode'], 0, true);
                    if(is_array($ProductDetails))
                    { 
                        $ProductID = $ProductDetails['ProductID'];
                        $ModelID   = $ProductDetails['ModelID'];
                        $UnitTypeID  = isset($ProductDetails['UnitTypeID'])?$ProductDetails['UnitTypeID']:$UnitTypeID;
                    }
                }
                
               // $this->log(var_export("third:".$UnitTypeID, true));
              //  $this->log(var_export("third:".$ModelID, true));
                
               // $this->log(var_export("third:".$ProductID, true));
                
                
                $_POST['ModelID']   =  $ModelID;
                $_POST['ProductID'] =  $ProductID;
                $_POST['UnitTypeID'] =  $UnitTypeID;
                
                
              //  $this->log("tessss");
              //  $this->log(var_export($_POST['UnitTypeID'], true));
              //  $this->log(var_export($UnitTypeID, true));
                
                $_POST['ServiceBaseUnitType'] = '';
                if($_POST['UnitTypeID'])
                {
                    $UnitTypesModel   = $this->loadModel('UnitTypes');
                    $UnitTypesDetails =  $UnitTypesModel->fetchRow(array('UnitTypeID'=>$_POST['UnitTypeID']));
                    
                    if(is_array($UnitTypesDetails))
                    {
                        
                        $_POST['ServiceBaseUnitType'] = $UnitTypesDetails['UnitTypeName'];
                       // $_POST['UnitTypeID'] = 0;
                    }
                    
                }
                

                //Bought out guarantee checking..
                $bogData = array(
                                    "ServiceTypeID"=>$_POST['ServiceTypeID'],
                                    "NetworkID"=>$NetworkID,
                                    "ClientID"=>$ClientID,
                                    "ManufacturerID"=>$_POST['ManufacturerID'],
                                    "ModelID"=>$ModelID,
                                    "ProductID"=>$ProductID,
                                    "UnitTypeID"=>$_POST['UnitTypeID']

                                );
                $_POST['ServiceTypeID']  = $Skyline->boughtOutGuarantee($bogData);

                
                //Finding service service of clicent specific provider for this job.
                
		$ServiceProviderID = false;
                
		/*
                if($_POST['BranchID']  && !$ServiceProviderID)
                {    
                    
                    $BranchesModel     =  $this->loadModel('Branches');
                    $BranchDetails     =  $BranchesModel->fetchRow(array('BranchID'=>$_POST['BranchID']));
                    
                    if(isset($BranchDetails['DefaultServiceProvider']) && $BranchDetails['DefaultServiceProvider'])
                    {
                        $ServiceProviderID = $BranchDetails['DefaultServiceProvider'];
                    }
                }
		*/
                
                
                
                if($ClientID  && !$ServiceProviderID)
                {    
                    
                    $ClientsModel     =  $this->loadModel('Clients');
                    $ClientDetails    =  $ClientsModel->fetchRow(array('ClientID'=>$ClientID));
                    
                    if(isset($ClientDetails['ServiceProviderID']) && $ClientDetails['ServiceProviderID'])
                    {
                        $ServiceProviderID = $ClientDetails['ServiceProviderID'];
                    }
                }
                
                //Finding service service for this job if there is no specific service centre for the client.
                if(!$ServiceProviderID)
                {
                    $ServiceProviderID  = $Skyline->findServiceCentre($_POST);
                }
               
              
                
                $ServiceProviderDetails = array();
                if($ServiceProviderID)
                {    
                    $ServiceProviderDetails = $Skyline->getServiceCenterDetails($ServiceProviderID);
                }
                
                $BrandEmailSendFlag  = true;
                $ShowCRMEmailTextFlag  = true;
                
                if(isset($_POST['BranchID'])) {
		    
                   $jobBrandsDetails = $Skyline->getBrands(false, false, $_POST['BranchID']);
                   
                   if(isset($jobBrandsDetails[0]['AutoSendEmails']) && $jobBrandsDetails[0]['AutoSendEmails']=='0') {
                       $BrandEmailSendFlag  = false;
                   }
                   
                   if(isset($jobBrandsDetails[0]['EmailType']) && $jobBrandsDetails[0]['EmailType']=='Generic') {
                       $ShowCRMEmailTextFlag  = false;
                   }
		   
                }
                
                
                $this->smarty->assign('ShowCRMEmailTextFlag', $ShowCRMEmailTextFlag);
                $this->smarty->assign('BrandEmailSendFlag', $BrandEmailSendFlag);
                
                
                if(isset($_POST['AccessoriesCB']) && is_array($_POST['AccessoriesCB']))
                {
                    $_POST['AccessoriesCB'] = implode(",", $_POST['AccessoriesCB']);
                }    
                
                $jb_confirm  = $_POST; 

                $this->smarty->assign('jb_confirm', $jb_confirm);

                $jb_title_name = '';
                if(isset($_POST['CustomerTitleID']))
                {
                        $jb_title_name   = $Skyline->getTitleName($_POST['CustomerTitleID']);
                }

                $this->smarty->assign('jb_title_name',  $jb_title_name); 

                $jb_job_type_name = '';
                if(isset($_POST['JobTypeID']))
                {
                        $jb_job_type_name   = $Skyline->getJobTypeName($_POST['JobTypeID']);
                }

                $this->smarty->assign('jb_job_type_name', $jb_job_type_name);  

                $jb_product_type_name = '';
                if($_POST['ServiceBaseUnitType'])
                {
                    $jb_product_type_name = $_POST['ServiceBaseUnitType'];
                } 
                else if(isset($_POST['UnitTypeID']) && $_POST['UnitTypeID']=="-1")
                {
                    $jb_product_type_name   = "NOT LISTED";
                }
                else if(isset($_POST['UnitTypeID']))
                {
                    $jb_product_type_name   = $Skyline->getUnitTypeName($_POST['UnitTypeID']);
                }

                $this->smarty->assign('jb_product_type_name', $jb_product_type_name);  

                $jb_service_type_name = '';
                if(isset($_POST['ServiceTypeID']))
                {
                    //$jb_service_type_name   = $Skyline->getServiceTypeName($_POST['ServiceTypeID']);
                    $ServiceTypesModel  = $this->loadModel('ServiceTypes');
        
                    $jb_service_type_name  = $ServiceTypesModel->fetchMask($_POST['NetworkID'], $_POST['ClientID'], $_POST['ServiceTypeID']);
        
                }

                $this->smarty->assign('jb_service_type_name', $jb_service_type_name);  

                $product_location_name = '';
                if(isset($_POST['ProductLocation']))
                {
                    //$product_location_name   = $Skyline->getProductLocationName($_POST['ProductLocation']);
                    
                    if($_POST['ProductLocation']=='Branch' || $_POST['ProductLocation']=='Other')
                    {
                        
                        
                        
                        $product_location_name = trim($_POST['ColAddCompanyName']);
                   
                           
                        
                        if(trim($_POST['ColAddBuildingNameNumber']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            } 
                            
                            $product_location_name .= trim($_POST['ColAddBuildingNameNumber']);

                        }    
                            
                        
                        
                        if(trim($_POST['ColAddStreet']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                         
                            $product_location_name .= trim($_POST['ColAddStreet']);
                        }

                        if(trim($_POST['ColAddLocalArea']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                            $product_location_name .= trim($_POST['ColAddLocalArea']);
                        }
                        
                        if(trim($_POST['ColAddCity']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }

                            $product_location_name .= trim($_POST['ColAddCity']);
                        }

                        if(trim($_POST['ColAddPostcode']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                            $product_location_name .= strtoupper(trim($_POST['ColAddPostcode']));
                        }
                        
                        
                    }   
                    else if($_POST['ProductLocation']=='Customer')
                    {
                        $product_location_name = trim($_POST['CompanyName']);
                   
                           
                        
                        if(trim($_POST['BuildingNameNumber']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            } 
                            
                            $product_location_name .= trim($_POST['BuildingNameNumber']);

                        }    
                            
                        
                        
                        if(trim($_POST['Street']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                         
                            $product_location_name .= trim($_POST['Street']);
                        }

                        if(trim($_POST['LocalArea']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                            $product_location_name .= trim($_POST['LocalArea']);
                        }
                        
                        if(trim($_POST['City']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }

                            $product_location_name .= trim($_POST['City']);
                        }

                        if(trim($_POST['PostalCode']))
                        {    
                            if($product_location_name)
                            {
                                $product_location_name .= ", ";
                            }
                            $product_location_name .= strtoupper(trim($_POST['PostalCode']));
                        }
                    } 
                    else if($_POST['ProductLocation']=='Service Provider' && $ServiceProviderID)
                    {
                       $product_location_name = isset($ServiceProviderDetails['FullAddress'])?$ServiceProviderDetails['FullAddress']:'';
                    }
                    
                   
                    
                    
                }

                $this->smarty->assign('product_location_name', $product_location_name); 
                
                 
                $this->smarty->assign('jb_service_provider_details', isset($ServiceProviderDetails['CompanyName'])?$ServiceProviderDetails['CompanyName']:$page['Text']['no_service_provider_text']); 
               
                
                $this->smarty->assign('ServiceProviderID', $ServiceProviderID); 

                
                //checking for permisson of changing Service Provider
                $displaySPChangeLink = false;
                
                if (isset($this->user->Permissions['AP7006']) || $this->user->SuperAdmin)
                {
                    $displaySPChangeLink = true;
                }
                
                
                $this->smarty->assign('displaySPChangeLink', $displaySPChangeLink);
                 
                $tableRandNumber  = rand(0, 1000);
                $this->smarty->assign('tableRandNumber', $tableRandNumber);
                
                $html_code =   $this->smarty->fetch('jobConfirmation.tpl'); 
                echo $html_code;


            return;
     }
     
     
     
     public function processDataAction() { 
          
        if($this->user == null) {
	    
            echo json_encode([	'status' => 'FAIL',
				'jobId' => 0,
                                'message' => 'LOGGED OUT'
			    ]); 
	    
        } else {
         
            $page = $this->messages->getPage('Job', $this->lang);
		
	    $emailModel = $this->loadModel('Email'); 

	    
            try {
            
		// $this->log("-----test----");
		// $this->log($_POST);
	    
		$jobModel = $this->loadModel('Job'); 
		$skyline = $this->loadModel('Skyline');

		$raRequired = $jobModel->checkRARequired($_POST['ServiceTypeID'], $_POST['ManufacturerID'], $_POST['AuthorisationNo']);

		if($_POST['ServiceProviderID'] && $_POST['ServiceProviderID'] != '' && !$raRequired) {
		    $statusID = $skyline->getStatusID('02 ALLOCATED TO AGENT');
		} else if($raRequired) {
		    $statusID = $skyline->getStatusID('00 REPAIR AUTHORISATION');
		} else {
		    $statusID = $skyline->getStatusID('01 UNALLOCATED JOB');
		}

		//Override $statusID if Service Appraisal Required for this branch is Yes. 
		//NOTE: It's absolutely crucial that $_POST['BranchID'] is set for this functionality to work
		if($jobModel->isServiceAppraisalRequired($_POST['BranchID'])) {
		    $statusID = 44;
		    $appraisalRequired = true;
		} else {
		    $appraisalRequired = false;
		}
		
		$audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::JOB_CREATED);            
		$repair_type = $skyline->getRepairTypeFromJobTypeID($_POST['JobTypeID']);

		//Create Customer Record          
		$customer = $this->loadModel('Customer');
		$cust_data = [
		    'CustomerTitleID' => ($_POST['CustomerTitleID'] == '') ? NULL : $_POST['CustomerTitleID'],
		    'CompanyName' => $_POST['CompanyName'],
		    'ContactFirstName' => $_POST['ContactFirstName'],
		    'ContactLastName' => $_POST['ContactLastName'],
		    'PostalCode' => strtoupper($_POST['PostalCode']), 
		    'BuildingNameNumber' => $_POST['BuildingNameNumber'],
		    'Street' => $_POST['Street'],
		    'LocalArea' => $_POST['LocalArea'],
		    'TownCity' => $_POST['City'],               
		    'CountyID' => ($_POST['CountyID'] == '') ? NULL : $_POST['CountyID'],
		    'CountryID' => ($_POST['CountryID'] == '') ? NULL : $_POST['CountryID'],
		    'ContactEmail' => $_POST['ContactEmail'],
		    'ContactHomePhone' => $_POST['ContactHomePhone'], 
                    'ContactWorkPhoneExt' => isset($_POST['ContactHomePhoneExt'])?$_POST['ContactHomePhoneExt']:'',
                    'ContactMobile' => $_POST['ContactMobile'],
		    'DataProtection' => ($_POST['data_protection_act'] == "1") ? 'Yes' : 'No',
		    'DataProtectionEmail' => ($_POST['contact_method_mail'] == "1") ? '1' : '0',
		    'DataProtectionLetter' => ($_POST['contact_method_letter'] == "1") ? '1' : '0',
		    'DataProtectionSMS' => ($_POST['contact_method_sms'] == "1") ? '1' : '0',
                    'SendServiceCompletionText' => ( isset($_POST['SendServiceCompletionText']) && $_POST['SendServiceCompletionText']!='' )?$_POST['SendServiceCompletionText']:'MobileNo',
                    'ContactAltMobile' => (isset($_POST['SendServiceCompletionText']) && $_POST['SendServiceCompletionText']=='AlternativeMobileNo' && isset($_POST['ContactAltMobile']))?$_POST['ContactAltMobile']:NULL
                    
		];

		$custID = null;

		//Requested by Joe, so Nag has commented this.
		//if($_POST['ContactEmail'])
		//{
		//    $custID = $customer->isExists($_POST['ContactEmail']);
		//}

		if($custID) {    
		    $customer->updateByID($cust_data, $custID);
		} else {
		    $cust = $customer->create($cust_data);
		    if($cust['status'] == 'OK')
		    $custID = $cust['id'];
		}

		//$NetworkID = ($this->user->NetworkID)?$this->user->NetworkID:NULL;
		//$ClientID  = ($this->user->ClientID)?$this->user->ClientID:NULL;

		//Need correction.
		$ModelID = isset($_POST['ModelID']) ? $_POST['ModelID'] : 0;
		$ProductID = isset($_POST['ProductID']) ? $_POST['ProductID'] : 0;

		//if(isset($_POST['ModelName']) && $_POST['ModelName'])
		//{
		//   //Getting Model ID for given Model Number/Name.
		//   $modelModel = $this->loadModel('Models');
		//	 $ModelID = $modelModel->isValid(0, $_POST['ModelName'], $_POST['ManufacturerID'], true);
		//}


		//$ProductID =  0;
		//if(isset($_POST['StockCode']) && $_POST['StockCode'])
		//{
		//   //Getting Product ID for given Stock Code.
		//   $ProductNumbersModel = $this->loadModel('ProductNumbers');
		//   $ProductDetails = $ProductNumbersModel->isValid($NetworkID, $ClientID, $_POST['StockCode'], 0, true);
		//   if($ProductDetails) { 
		//   $ProductID = $ProductDetails['ProductID'];
		//       $ModelID   = $ProductDetails['ModelID'];
		//   }
		//}


		//Bought out guarantee checking..
		//$bogData = array(
		//             "ServiceTypeID"=>$_POST['ServiceTypeID'],
		//             "NetworkID"=>$NetworkID,
		//             "ClientID"=>$ClientID,
		//             "ManufacturerID"=>$_POST['ManufacturerID'],
		//             "ModelID"=>$ModelID,
		//             "ProductID"=>$ProductID
		//          );
		//$_POST['ServiceTypeID']  = $skyline->boughtOutGuarantee($bogData);
		//            
		//if($this->user->PreferredServiceProviderID)
		//{    
		//   $serviceCenterID  = $this->user->PreferredServiceProviderID;
		//}
		//else
		//{
		//   $serviceCenterID  = $skyline->findServiceCentre($_POST);
		//}


		$_POST['ServiceBaseModel'] = NULL;
		//We are setting ServiceBaseModel value if model name doesn't exist in the database.
		if(!$ModelID && $_POST['ModelName']) {
		    $_POST['ServiceBaseModel'] = $_POST['ModelName'];
		}
		/* else if(!$ProductID && $_POST['StockCode'])//We are setting ServiceBaseModel value if model name and stock code don't exist in the database.
		{
		    $_POST['ServiceBaseModel'] = $_POST['StockCode'];
		}*/
		else if($_POST['ProductDescription'] && $_POST['ProductDescription'] != '' && !$ModelID) { //We are setting ServiceBaseModel value if model name and stock code don't exist in the database.
		    $_POST['ServiceBaseModel'] = $_POST['ProductDescription'];
		}


		$branch_id = $_POST['BranchID'];
		if(!$branch_id && $_POST['DefaultBranchID']) {
		    $branch_id = $_POST['DefaultBranchID'];
		}

		if(!$branch_id) {
		    $branch_id  = NULL;
		}
                
                
                $NetworkName =  strtoupper($skyline->getNetworkName($_POST['NetworkID']));
                $ClientName  =  strtoupper($skyline->getClientName($_POST['ClientID']));

                $NotesExtraStr = '';
                
                if($NetworkName)
                {
                    $NotesExtraStr = 'Network: '.$NetworkName.' ';
                } 
                
                if($ClientName)
                {    
                    $NotesExtraStr .= 'Client: '.$ClientName;
                }
                
                if($NotesExtraStr && trim($_POST['Notes']))
                {
                    $NotesExtraStr = "\r\n\r\n".$NotesExtraStr;
                }    
                
                $_POST['Notes'] = trim($_POST['Notes']).$NotesExtraStr;
                
                
                if($ProductID)
                {
                   $ProductNumbersModel = $this->loadModel('ProductNumbers');
                   $productDetails = $ProductNumbersModel->fetchRow(array('ProductID'=>$ProductID));
                   
                   if(isset($productDetails['AuthorityLimit']))
                   {
                       if($productDetails['AuthorityLimit'] && $productDetails['AuthorityLimit']!='0.00')
                       {
                           $_POST['Notes'] = $_POST['Notes']."\r\n\r\n".$page['Text']['repair_limit'].": £".$productDetails['AuthorityLimit']; 
                       }
                       else
                       {
                           $_POST['Notes'] = $_POST['Notes']."\r\n\r\n".$page['Text']['repair_limit'].": ".$page['Text']['no_limit']; 
                       }
                   }
                    
                } 
		$auditMsg  = '';
                if(!empty($_POST['repJobId']) && !empty($_POST['repType']))
                {
                    if($_POST['repType'] == "Both")
                        $auditMsg = "The Customer & Job details from Job: ".$_POST['repJobId']." were replicated to this job";
                    else
                        $auditMsg = "The Customer Details Only from Job: ".$_POST['repJobId']." were replicated to this job";
                }
		// Create Job Record
		$job = [
		    'NetworkID' => $_POST['NetworkID'],
		    'ClientID' => $_POST['ClientID'],
		    'BranchID' => $branch_id,
		    'ServiceProviderID' => ($_POST['ServiceProviderID'] == '' || !$_POST['ServiceProviderID']) ? NULL : $_POST['ServiceProviderID'],
		    'CustomerID' => ($custID == '') ? NULL : $custID,
		    'JobTypeID' => ($_POST['JobTypeID'] == '') ? NULL : $_POST['JobTypeID'],
		    'ServiceTypeID' => ($_POST['ServiceTypeID'] == '') ? NULL : $_POST['ServiceTypeID'],
                    'MobilePhoneNetworkID' => empty($_POST['MobilePhoneNetworkID']) ?  NULL : $_POST['MobilePhoneNetworkID'],
                    'ServiceBaseUnitType' => ($_POST['ServiceBaseUnitType'] && !$ModelID) ? $_POST['ServiceBaseUnitType'] : NULL,
		    'ServiceBaseModel' => ($_POST['ServiceBaseModel'] && !$ModelID)?$_POST['ServiceBaseModel']:NULL,
		    'ManufacturerID' => ($_POST['ManufacturerID'] == '') ? NULL : $_POST['ManufacturerID'],
		    'ModelID' => ($ModelID == 0) ? NULL : $ModelID,
		    'SerialNo' => $_POST['SerialNo'],
		    'ReportedFault' => $_POST['ReportedFault'],
		    'ProductLocation' => $_POST['ProductLocation'],
		    'OriginalRetailer' => trim($_POST['OriginalRetailerPart1'] . " " . $_POST['OriginalRetailerPart2'] . " " . $_POST['OriginalRetailerPart3']),
		    'Insurer' => $_POST['Insurer'],
		    'PolicyNo' => $_POST['PolicyNo'],
		    'AuthorisationNo' => $_POST['AuthorisationNo'],
		    'Notes' => $_POST['Notes'],
                    'JobSourceID' => isset($_POST['JobSourceID']) ? $_POST['JobSourceID'] : NULL,
                    'AgentRefNo' => $_POST['ReferralNo'],
		    'StockCode' => isset($_POST['HiddenStockCode']) ? $_POST['HiddenStockCode'] : NULL,
		    'ProductID' => ($ProductID==0) ? NULL : $ProductID,
		    'ReceiptNo' => isset($_POST['ReceiptNo']) ? $_POST['ReceiptNo'] : NULL,
		    'ColAddCompanyName' => isset($_POST['ColAddCompanyName']) ? $_POST['ColAddCompanyName'] : NULL,
		    'ColAddBuildingNameNumber' => isset($_POST['ColAddBuildingNameNumber']) ? $_POST['ColAddBuildingNameNumber'] : NULL,
		    'ColAddStreet' => (isset($_POST['ColAddStreet']) && $_POST['ColAddStreet'] != '') ? $_POST['ColAddStreet'] : NULL,
		    'ColAddLocalArea' => isset($_POST['ColAddLocalArea']) ? $_POST['ColAddLocalArea'] : NULL,
		    'ColAddTownCity' => isset($_POST['ColAddCity']) ? $_POST['ColAddCity'] : NULL,
		    'ColAddCountyID' => (isset($_POST['ColAddCountyID']) && $_POST['ColAddCountyID'] != '') ? $_POST['ColAddCountyID'] : NULL,
		    'ColAddCountryID' => (isset($_POST['ColAddCountryID']) && $_POST['ColAddCountryID'] != '') ? $_POST['ColAddCountryID'] : NULL,
		    'ColAddPostcode' => isset($_POST['ColAddPostcode']) ? strtoupper($_POST['ColAddPostcode']) : NULL,
		    'ColAddEmail' => isset($_POST['ColAddEmail']) ? $_POST['ColAddEmail'] : NULL,
		    'ColAddPhone' => isset($_POST['ColAddPhone']) ? $_POST['ColAddPhone'] : NULL,
		    'ColAddPhoneExt' => isset($_POST['ColAddPhoneExt']) ? $_POST['ColAddPhoneExt'] : NULL,
		    'DateBooked' => date('Y-m-d'),
		    'TimeBooked' => date('H:i:s'),
		    'StatusID' => ($statusID == '') ? NULL : $statusID,
		    'BookedBy' => ($this->user->UserID) ? $this->user->UserID : NULL,
		    'ModifiedUserID' => ($this->user->UserID) ? $this->user->UserID : NULL,
		    'ModifiedDate' => date('Y-m-d H:i:s'),
		    "batchID" => isset($_POST["batchID"]) ? $_POST["batchID"] : null,
		    'RepairType' => $repair_type,
		    "OpenJobStatus" => ((isset($_POST["isProductAtBranch"]) && $_POST["isProductAtBranch"] == "Yes") || $_POST["ProductLocation"] == "Branch") ? "in_store" : null,
		    "ImeiNo" => (isset($_POST["ImeiNo"]) ? $_POST["ImeiNo"] : null),
                    'FromJobId' => empty($_POST['repJobId']) ?  NULL : $_POST['repJobId'],
                    'ReplicateJob' => $auditMsg
		];

		
		if ($this->debug) $this->log($job);
		
                
                if($_POST['ProductLocation'] == "Service Provider" || $_POST['ProductLocation'] == "Branch") {
                    
                     if(!isset($_POST['Accessories']))
                     {
                         $_POST['Accessories'] = NULL;
                     }
                     
                     if(isset($_POST['AccessoriesRadio']) && $_POST['AccessoriesRadio']=='-1')//If None is selected.
                     {
                         $_POST['Accessories'] = NULL;
                     }
                    
                     $job['Accessories'] = isset($_POST["Accessories"]) ? $_POST["Accessories"] : NULL;
                     $job['UnitCondition'] = isset($_POST["UnitCondition"]) ? $_POST["UnitCondition"] : NULL;
                } else {
                     $job['Accessories'] = NULL;
                     $job['UnitCondition'] = NULL;
                }
                
                
		if($_POST['ProductLocation'] == "Service Provider" && $_POST['ServiceProviderID']) {
		    $spModel = $this->loadModel("ServiceProviders");
		    $sp = $spModel->getServiceProvider($_POST['ServiceProviderID']);
		    $job['ColAddCompanyName'] = isset($sp["CompanyName"]) ? $sp["CompanyName"] : NULL;
		    $job['ColAddBuildingNameNumber'] = isset($sp["BuildingNameNumber"]) ? $sp["BuildingNameNumber"] : NULL;
		    $job['ColAddStreet'] = isset($sp["Street"]) ? $sp["Street"] : NULL;
		    $job['ColAddLocalArea'] = isset($sp["LocalArea"]) ? $sp["LocalArea"] : NULL;
		    $job['ColAddTownCity'] = isset($sp["TownCity"]) ? $sp["TownCity"] : NULL;
		    $job['ColAddCountyID'] = isset($sp["CountyID"]) ? $sp["CountyID"] : NULL;
		    $job['ColAddCountryID'] = isset($sp["CountryID"]) ? $sp["CountryID"] : NULL;
		    $job['ColAddPostcode'] = isset($sp["PostalCode"]) ? $sp["PostalCode"] : NULL;
		    $job['ColAddEmail'] = isset($sp["ContactEmail"]) ? $sp["ContactEmail"] : NULL;
		    $job['ColAddPhone'] = isset($sp["ContactPhone"]) ? $sp["ContactPhone"] : NULL;
		    $job['ColAddPhoneExt'] = isset($sp["ContactPhoneExt"]) ? $sp["ContactPhoneExt"] : NULL;
		}

		if($_POST['ProductLocation'] == "Customer") {
		    $job['ColAddCompanyName'] = isset($_POST['CompanyName']) ? $_POST['CompanyName'] : NULL;
		    $job['ColAddBuildingNameNumber'] = isset($_POST['BuildingNameNumber']) ? $_POST['BuildingNameNumber'] : NULL;
		    $job['ColAddStreet'] = isset($_POST['Street']) ? $_POST['Street'] : NULL;
		    $job['ColAddLocalArea'] = isset($_POST['LocalArea']) ? $_POST['LocalArea'] : NULL;
		    $job['ColAddTownCity'] = isset($_POST['City']) ? $_POST['City'] : NULL;
		    $job['ColAddCountyID'] = (isset($_POST['CountyID']) && $_POST['CountyID'] != "") ? $_POST['CountyID'] : NULL;
		    $job['ColAddCountryID'] = (isset($_POST['CountryID']) && $_POST['CountryID'] != "") ? $_POST['CountryID'] : NULL;
		    $job['ColAddPostcode'] = isset($_POST['PostalCode']) ? strtoupper($_POST['PostalCode']) : NULL;
		    $job['ColAddEmail'] = isset($_POST['ContactEmail']) ? $_POST['ContactEmail'] : NULL;
		    $job['ColAddPhone'] = isset($_POST['ContactHomePhone']) ? $_POST['ContactHomePhone'] : NULL;
		    //$job['ColAddPhoneExt'] = isset($_POST['ColAddPhoneExt']) ? $_POST['ColAddPhoneExt'] : NULL;
		}

		if($_POST['DateOfPurchase'] != '' && $_POST['DateOfPurchase'] != 'dd/mm/yyyy') {    
		    $dateArray = explode("/", $_POST['DateOfPurchase']);
		    if(count($dateArray) == 3) {    
			$job['DateOfPurchase'] = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
		    }
		}

		if($appraisalRequired || $raRequired) {
		    $branchModel = $this->loadModel("Branches");
		    $job['ServiceProviderID'] = $branchModel->getBranchServiceProvider($_POST["BranchID"]);
		}

		$result = $jobModel->create($job);

            } catch (Exception $e) {
                
                $this->log($page['Errors']['job_booking_subject'] . ":");
                $this->log(var_export($e->getMessage(), true));
		
                $emailModel->Send(  $this->config['SMTP']['FromName'], 
				    $this->config['SMTP']['From'], 
				    $this->config['General']['skyline_issue_email'], 
				    $page['Errors']['job_booking_subject'], 
				    $e->getMessage()
				 );
		
                $result = [ 
		    'status' => 'FAIL',
		    'jobId' => 0,
		    'message' => $page['Errors']['job_booking']
		];
                
            }    

	    
	    //Job is created, now sending emails and comunicating with Service Center, RMA Tracker, Service Base.	    
	    
	    
	    if($result['status'] == 'SUCCESS') {
                
                
                //Insering  Accessories
                if(isset($_POST['AccessoriesCB']) && $_POST['AccessoriesCB']!='')
                {
                    $_POST['AccessoriesCB'] = explode(",", $_POST['AccessoriesCB']);
                    $accessories_model = $this->loadModel('Accessories');
                    
                    $accessories_model->processJobAccessories($_POST['AccessoriesCB'], $result['jobId']);
                }    
                
                
                
                if (isset($this->session->BookCollectionFormData)) {
                    $bcf = $this->session->BookCollectionFormData;
                    $bcf['JobID'] = $result['jobId'];
                    $courier_business_model = $this->loadModel('CourierBusinessModel');
                    // Book Courier Collection but do NOT update Collection address
                    $courier_business_model->BookCourierCollection($bcf, false);
                    unset($this->session->BookCollectionFormData);
                }
		
                try {
		
		    //Updating branch address starts here ...
		    if(isset($this->user->Permissions['AP7007']) || $this->user->SuperAdmin) {
		       if(isset($_POST['BranchID']) && $_POST['BranchID'] && $_POST['ProductLocation'] == "Branch") {
			    $branchAddress = [
				'BuildingNameNumber' => $_POST['ColAddBuildingNameNumber'], 
				'Street' => $_POST['ColAddStreet'],
				'LocalArea' => $_POST['ColAddLocalArea'],
				'TownCity' => $_POST['ColAddCity'],
				'CountyID' => ($_POST['ColAddCountyID'] == '') ? NULL : $_POST['CountyID'],
				'CountryID' => ($_POST['ColAddCountryID'] == '') ? NULL : $_POST['CountryID'],
				'PostalCode' => isset($_POST['ColAddPostcode']) ? $_POST['ColAddPostcode'] : null,
				'ContactEmail' => isset($_POST['ColAddEmail']) ? $_POST['ColAddEmail'] : null,
				'ContactPhoneExt' => isset($_POST['ColAddPhoneExt']) ? $_POST['ColAddPhoneExt'] : null,
				'ContactPhone' => isset($_POST['ColAddPhone']) ? $_POST['ColAddPhone'] : null,
				'BranchID' => $_POST['BranchID']
			    ];
			    $branches_model = $this->loadModel('Branches');
			    $branches_model->updateAddress($branchAddress);
			}    
		    }
		    //Updating branch address ends here ...

                } catch (Exception $e) {
                
                    $this->log($page['Errors']['job_booking_branch_subject'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					$page['Errors']['job_booking_branch_subject'], 
					$e->getMessage()
				     );

                }
		    

                try {
		
		    //Updating Unit type for given model number..-starts here..
		    //if (isset($this->user->Permissions['AP7040']) && isset($_POST['UpdateModelUnitType']) && $_POST['UpdateModelUnitType']) {  
		    if(isset($_POST['UpdateModelUnitType']) && $_POST['UpdateModelUnitType']) {  
                        
			$models_model = $this->loadModel('Models');
			$um = $models_model->UpdateModel(array("ModelID"=>$ModelID, "UnitTypeID"=>$_POST['UpdateModelUnitType']));
		    
                        if(!isset($this->user->Permissions['AP7040'])) {
                            
                            //Sending email to admin if unit type has been assigned to model no.
                            if ($this->debug) $this->log("New Unit Type has been assigned to the Model Number:");
                            
                            $m_model_result = $models_model->fetchRow(array('ModelID'=>$ModelID));
                            
                            $m_model_number = (isset($m_model_result['ModelNumber']))?$m_model_result['ModelNumber']:'';
                            
                            $UnitTypesModel     = $this->loadModel('UnitTypes');
                            $m_unit_type_result = $UnitTypesModel->fetchRow(array('UnitTypeID'=>$_POST['UpdateModelUnitType'])); 
                            $m_unit_type = (isset($m_unit_type_result['UnitTypeName']))?$m_unit_type_result['UnitTypeName']:'';
                            
                            $m_message = "Change Notification ModelID: ".$ModelID." Model Number: ".$m_model_number." UnitTypeID: ".$_POST['UpdateModelUnitType']."  UnitType: ".$m_unit_type." Job being booked: SL".$result['jobId']."  Please correct it if it is wrong.";
                            
                            if ($this->debug) $this->log($m_message);
                            
                            $emailModel->Send(	$this->config['SMTP']['FromName'], 
                                                $this->config['SMTP']['From'], 
                                                $this->config['General']['skyline_issue_email'], 
                                                "New Unit Type has been assigned to the Model Number", 
                                                $m_message
                                             );
                            
                        }
		    }   
		    //Updating Unit type for given model number..-ends here..

                } catch (Exception $e) {
                
                    $this->log($page['Errors']['update_model_unittype_subject'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					$page['Errors']['update_model_unittype_subject'], 
					$e->getMessage()
				     );

                }
                
                
                //Requesting new unit type name. - starts here..
		try {
		   
		    if(isset($_POST['RequestNewUnitTypeText']) && $_POST['RequestNewUnitTypeText']) {  
                        
			//Sending email to admin for New Unit type needed in Skyline.
			if ($this->debug) $this->log("New Unit type needed in Skyline");

			$NetworkName = $skyline->getNetworkName($_POST['NetworkID']);

			$NetworkName = $NetworkName.' (NetworkID: '.$_POST['NetworkID'].')';

			$ClientName  =  $skyline->getClientName($_POST['ClientID']);

			$ClientName  = $ClientName.' (ClientID: '.$_POST['ClientID'].')';

			$RequestNewUnitTypeMailBody = 'User <b>'.$this->session->UserID.'</b> from <b>'.$NetworkName.'</b> - <b>'.$ClientName.'</b> has requested the following unit type to be added to Skyline.<br>

			    New Unit Type requested: <b>'.$_POST['RequestNewUnitTypeText'].'</b><br>

			    Job: <b>'.$result['jobId'].'</b>';

			if ($this->debug) $this->log($RequestNewUnitTypeMailBody);

			$emailModel->Send(  $this->config['SMTP']['FromName'], 
					    $this->config['SMTP']['From'], 
					    $this->config['General']['skyline_issue_email'], 
					    'New Unit type needed in Skyline', 
					    $RequestNewUnitTypeMailBody
					 );
                            
		    }   

                } catch (Exception $e) {
                
                    $this->log("Error in New Unit type needed in Skyline:");
                    $this->log(var_export($e->getMessage(), true));
                    
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					"Error in New Unit type needed in Skyline", 
					$e->getMessage()
				     );

                }
		//Requesting new unit type name. - ends here..

		
                try {
		
		    if(!$branch_id) {
			$mail_subject = "ClientID: " . $_POST['ClientID'] . " missing Default Assigned Branch. Job No: SL" . $result['jobId'];
			//@mail($this->config['General']['skyline_issue_email'], $mail_subject, $mail_subject);
                        $emailModel->Send($this->config['SMTP']['FromName'], $this->config['SMTP']['From'], $this->config['General']['skyline_issue_email'], $mail_subject, $mail_subject);
			if ($this->debug) $this->log("Email Sent:");
			if ($this->debug) $this->log(var_export($this->config['General']['skyline_issue_email'], true));
			if ($this->debug) $this->log(var_export($mail_subject, true));
		    }

		    $jobId = $result['jobId'];

		    //Create Status history Record
		    $status_history = $this->loadModel('StatusHistory');
		    $status_history->create([   'JobID' => $jobId,
						'StatusID' => $statusID,
						'UserID' => $this->user->UserID,
						'Date' => date('Y-m-d H:i:s') 
					    ]);

		    //Create Audit Record
		    $audit = $this->loadModel('Audit');
                    $audit->create(['JobID' => $jobId,
                                        'AuditTrailActionID' => $audit_actionID,
                                        'Description' => ''
                                       ]);
                    /* Tracker Base Log 541 changes added By Raju on 4th July 2013 Starts Here */
                    if(!empty($_POST['repJobId']) && !empty($_POST['repType']))
                    {
                        if($_POST['repType'] == "Both")
                            $parentAuditMsg = "The Customer & Job Details have been replicated to Job: ".$jobId;
                        else
                            $parentAuditMsg = "The Customer Details Only have been replicated to Job: ".$jobId;
                        $arguments = [
                            "JobID" => $_POST['repJobId'],
                            "ToJobId" => $jobId,
                            "ReplicateJob" => $parentAuditMsg
                        ];
                        $jobModel->update($arguments);
                    }
                    /*if(!empty($_POST['repJobId']) && !empty($_POST['repType']))
                    {
                        $audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::REPLICATE_JOB);
                        if($_POST['repType'] == "Both")
                            $auditMsg = "The Customer & Job details from Job: ".$_POST['repJobId']." were replicated to this job";
                        else
                            $auditMsg = "The Customer Details Only from Job: ".$_POST['repJobId']." were replicated to this job";
                        $audit->create(['JobID' => $jobId,
				    'AuditTrailActionID' => $audit_actionID,
				    'Description' => $auditMsg
				   ]);
                    }
                    else
                    {
                        $audit->create(['JobID' => $jobId,
                                        'AuditTrailActionID' => $audit_actionID,
                                        'Description' => ''
                                       ]);
                    }
                    if(!empty($_POST['repJobId']) && !empty($_POST['repType']))
                    {
                        $arguments = [
                            "JobID" => $_POST['repJobId'],
                            "ToJobId" => $jobId,
                            "ReplicateType" => $_POST['repType']
                        ];
                        $jobModel->update($arguments);
                        $audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::REPLICATE_JOB);
                        if($_POST['repType'] == "Both")
                            $auditMsg = "The Customer & Job Details have been replicated to Job: ".$jobId;
                        else
                            $auditMsg = "The Customer Details Only have been replicated to Job: ".$jobId;
                        $audit->create(['JobID' => $_POST['repJobId'],
				    'AuditTrailActionID' => $audit_actionID,
				    'Description' => $auditMsg
				   ]);
                    }
                    Tracker Base Log 541 changes added By Raju on 4th July 2013 Ends Here*/
		    //Credit Deduction
		    $network = $this->loadModel('ServiceNetworks');
		    $CreditDetails = $network->CreditDeduction($_POST['NetworkID']);

		    if(is_array($CreditDetails)) {

			if(isset($CreditDetails['CreditLevelSendMail']) && $CreditDetails['CreditLevelSendMail'] == 1) {
			    //To do: Send mail to $CreditDetails['ContactEmail'] if it is not already sent.
			    if(APPLICATION_PATH != "C:\wamp\www\skyline\public\application") {
				$this->log("Send mail: credit level is less than record level");
			    }
			}

			if($CreditDetails['CreditLevel'] < 0) {
			    //To do: Send mail to $CreditDetails['ContactEmail'] always.
			    if(APPLICATION_PATH != "C:\wamp\www\skyline\public\application") {
				$this->log("Send mail: credit level is less than zero");
			    }
			}

		    }

                } catch (Exception $e) {
                
                    $this->log($page['Errors']['job_booking_credit_level'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send($this->config['SMTP']['FromName'], $this->config['SMTP']['From'], $this->config['General']['skyline_issue_email'], $page['Errors']['job_booking_credit_level'], $e->getMessage());

                }
		

		try {
		
		    $jobModel->fetch($jobId);

		    $cur = $jobModel->current_record;

		    //Checking if RA required and if yes - setting job.RepairAuthRequired = Yes

		    if($raRequired) {

			$jobModel->setRAStatus($jobModel->current_record["JobID"], "required");
			$jobModel->setRAType($jobModel->current_record, "repair");
			$brandModel = $this->loadModel("Brands");
			$brand = $brandModel->getBrandByBranchID($jobModel->current_record["BranchID"]);
			$jobModel->setRAStatusID($jobModel->current_record, "Authorisation Required", $brand["BrandID"]);

		    } else if(isset($cur["AuthorisationNo"]) && $cur["AuthorisationNo"] != null && $cur["AuthorisationNo"] != "") {

			$jobModel->setRAStatus($jobModel->current_record["JobID"], "confirmed");
			$raRequired = false;

		    } else {

			$jobModel->setRAStatus($jobModel->current_record["JobID"], "not_required");

		    }
		
		} catch (Exception $e) {
                
                    $this->log($page['Errors']['job_booking_ra'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					$page['Errors']['job_booking_ra'], 
					$e->getMessage()
				     );
                    
                }

		
		if($appraisalRequired) {
		    //SSSS email needs to be sent here
		    //skipping RA sending emails, etc.
		    goto endOfProcessDataAction;
		}
		
		
                try {
		
		    //Sending service request details mail to customer based on auto send email flag set for brand - starts here...

		    //Setting up email templates to send multiple emails.
		    $emails = [];

		    if($raRequired) {

			if($jobModel->current_record['AutoSendEmails'] == "1") {
			
			    //Notification about RA Request is sent to client if client email address and SP ID are present
			    if($jobModel->current_record['ContactEmail'] && $_POST['ServiceProviderID']) {
				$emails[] = $emailModel->fetchRow(['EmailCode' => "ra_required_client"]);
				end($emails);
				$key = key($emails);
				$emails[$key]["toEmail"] = $cur["ContactEmail"];
				$emails[$key]["fromName"] = "Skyline";
				$emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
				$emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
				$emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
				$emails[$key]["ccEmails"] = false;
			    }
			    
			}

			//Notification about RA Request is sent to manufacturer
			$emails[] = $emailModel->fetchRow(['EmailCode' => "ra_required_manufacturer"]);
			$manufacturerModel = $this->loadModel("Manufacturers");
			$manufacturerEmail = $manufacturerModel->getManufacturer($cur["ManufacturerID"])["AuthorisationManagerEmail"];
			$networkModel = $this->loadModel("ServiceNetworks");
			$network = $networkModel->fetchRow(["NetworkID" => $cur["NetworkID"]]);
			end($emails);
			$key = key($emails);
			$emails[$key]["toEmail"] = $manufacturerEmail;
			$emails[$key]["fromName"] = "Skyline";
			$emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
			$emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
			$emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
			$emails[$key]["ccEmails"] = $network["NetworkManagerEmail"] . ":j.berry@pccsuk.com";

		    } else {

			if($jobModel->current_record['AutoSendEmails'] == "1") {
			
			    //RA is not required, send a standard job booking email to client
			    //Notification about RA Request is sent to client if client email address and SP ID are present
			    if($jobModel->current_record['ContactEmail'] && $_POST['ServiceProviderID']) {
				
                                if($jobModel->current_record['EmailType']=='CRM')
                                {    
                                    $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking_crm"]);
                                }
                                else if($jobModel->current_record['EmailType']=='Tracker')
                                {    
                                    $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking_smart_service_tracker"]);
                                }
                                else
                                {
                                    $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking"]);
                                }    
                                
				end($emails);
				$key = key($emails);
				$emails[$key]["toEmail"] = $cur["ContactEmail"];
				$emails[$key]["fromName"] = "Skyline";
				$emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
				$emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
				$emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
				$emails[$key]["ccEmails"] = false;
			    }
			    
			}

			//Sending Service Instruction email to service provider's admin supervisor - starts here..
			if($jobModel->current_record['SPEmailServiceInstruction']=='Active' && ($jobModel->current_record['SPAdminSupervisorEmail'] || $jobModel->current_record['SPServiceManagerEmail']) && $_POST['ServiceProviderID']) {

			    $email_service_instruction  =  $emailModel->fetchRow(['EmailCode' => "email_service_instruction"]);

			    //$this->log($email_service_instruction);

			    if(is_array($email_service_instruction) && count($email_service_instruction)>0)
			    {    
				$emails[] = $email_service_instruction;
				end($emails);
				$key = key($emails);


				$sp_send_email1 = ($jobModel->current_record['SPAdminSupervisorEmail'])?$jobModel->current_record['SPAdminSupervisorEmail']:false;
				$sp_send_email2 = false;
				if(!$sp_send_email1)
				{
				  $sp_send_email1 =  $jobModel->current_record['SPServiceManagerEmail']; 
				}
				else if($jobModel->current_record['SPServiceManagerEmail'])
				{
				    $sp_send_email2 = $jobModel->current_record['SPServiceManagerEmail'];
				}    

				$emails[$key]["toEmail"] = $sp_send_email1;
				$emails[$key]["fromName"] = "Skyline";
				$emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
				$emails[$key]["replyEmail"] = false;
				$emails[$key]["replyName"] = false;
				$emails[$key]["ccEmails"] = $sp_send_email2;

			   }
			}
			//Sending Service Instruction email to service provider's admin supervisor - ends here..
		    }


		    foreach($emails as $email) {

			$emailModel->processEmail($email, $cur);

		    }

                    
                    //Sending email to admin if the Job is not assigned to service center  - starts here...
                    if(($_POST['ServiceProviderID'] == '' || !$_POST['ServiceProviderID']) && !$appraisalRequired && !$raRequired) {
                        
                        $UnallocatedJobEmailSubject = 'New Skyline Unallocated job ' . $jobModel->current_record["JobID"];
                        
                        $UnallocatedJobEmailBody = 'A new job (' . $jobModel->current_record["JobID"] . ') has been booked onto Skyline which is currently unallocated, please visit the Unallocated jobs page to allocate it manually.';
                        
                        $emailModel->Send(  $this->config['SMTP']['FromName'], 
                                            $this->config['SMTP']['From'], 
                                            $this->config['General']['skyline_issue_email'], 
                                            $UnallocatedJobEmailSubject, 
                                            $UnallocatedJobEmailBody,
                                            $this->config['General']['unallocated_jobs_email']
                                         );
                    
                    }
                    //Sending email to admin if the Job is not assigned to service center  - ends here...

		    //Sending service request details mail to customer based on auto send email flag set for brand - ends here...

                } catch (Exception $e) {
                
                    $this->log($page['Errors']['job_booking_sendemail'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					$page['Errors']['job_booking_sendemail'], $e->getMessage()
				     );

                }            
		
		
                try {
                
		    //Finally transmit allocated job to Service Centre.....
		    //If RA is not required then transmit, otherwise not.

		    if(!$raRequired) {

			$jobModel->pushJobToSC($jobId);			
			
		    }
                
                } catch (Exception $e) {
                
                    $this->log($page['Errors']['job_booking_api'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    $emailModel->Send(	$this->config['SMTP']['FromName'], 
					$this->config['SMTP']['From'], 
					$this->config['General']['skyline_issue_email'], 
					$page['Errors']['job_booking_api'], $e->getMessage()
				     );

                }
		
		
                /* TODO: If Job didn't come from RMA tracker need to send to RAM tracker */

                //$manufacturers_model = $this->loadModel('Manufacturers');  // Issue #59
                /* Issue #69 - Samsuung API shoudl not be called on create */
                //if ($job['ManufacturerID'] == $manufacturers_model->getManufacturerId('SAMSUNG')) {     /* Find out if Manufacturer is Samsung */
                //    if ($_POST['ServiceProviderID']) {
                //        $samsungApi = new SamsungClientController();
                //        $samsungApi->putServiceRequestAction(array($jobId));                    /* Yes so call Samsung API */ 
                //    }
                //}
                
            }

	    endOfProcessDataAction:
		
	    
	    if(!isset($_POST["csvAjax"])) {
		echo json_encode($result);
	    }
	    
	}

    }

      
      
      
    /**
    * printJobRecordAction
    * It prints job record.
    * 
    * @param  array $args
    * @return void
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
    * Additionaol modfications by Andrew Williams <a.williams@pccsuk.com>
    ********************************************/
    
    public function printJobRecordAction($args) { 
        
	ini_set("display_errors", 1);
	    
	$part_model= $this->loadModel('Part');
        $product_model = $this->loadModel('Product');
        
        $job_details_page = $this->messages->getPage('jobDetails', $this->lang);   
         
	$jobID = isset($args[0]) ? $args[0] : false;
	$PrintType = isset($args[1]) ? $args[1] : false;
      
	$pdf = isset($_GET["pdf"]);
	$this->smarty->assign("pdf", $pdf);	

	if ($pdf) {
	    include('libraries/MPDF56/mpdf.php');
	    $mpdf = new mPDF();
	    $mpdf->SetDisplayPreferences('/NoPrintScaling');
	}
	
	
	$jobs = [];
	
	if(isset($_GET["jobs"])) {
	    //multiple jobs
	    $data = json_decode($_GET["jobs"]);
	    foreach($data as $job) {
		$jobs[] = $job[0];
	    }
	} else if($jobID) {
	    //single job
	    $jobs[] = $jobID;
	}
	
	
	foreach($jobs as $JobID) {
	    
	    $Job = $this->loadModel('Job');
	    $Job->fetch($JobID);

	    if(isset($Job->current_record['JobID'])) {
		
		$printJob  = $Job->current_record;
                if($printJob['Accessories'] == -1)
                    $printJob['Accessories'] = 0;
                $brandId = $this->user->DefaultBrandID;
                $this->smarty->assign('brandId', $brandId);
                
                $parts = $part_model->getByJobId($printJob['JobID']);
                //$this->log($parts);
                $InvoiceCosts = $Job->getInvoiceCosts($printJob['JobID']);
                $VATRatesModel = $this->loadModel("VATRates");
                $vatRateReult = $VATRatesModel->fetchVatRate("T1");
                $InvoiceCosts["VATRate"] = $vatRateReult;
                $this->smarty->assign("InvoiceCosts", $InvoiceCosts);
                $row = 1;
                $col = 1;
                $partsbox = '';
                $partsbox1 = '';
                if (! is_null($parts)) {
                    foreach ($parts as $part) {                                 /* Loop each part */
                        if ( $row == 2 && $col == 1) {
                            $partsbox1 = $partsbox;                             /* Need to split the parts boxes into two allow addition of billing details */
                            $partsbox = '';
                        }
                        $partsbox .= '<td style="border: 1px solid #336699;width:136px;">';
                        $partsbox .= "{$part['PartDescription']}</td>";
                        $col++;                                                 /* Oncrement the column count */
                        if ($col >= 4) {                                        /* If it is greater than 4 (we want 3 cols) */
                            if ($row != 1) $partsbox .= "</tr>";                /* then end row if not first row; */
                            $row++;                                             /* Increment row */
                            $col = 1;                                           /* Reset column count */
                        }
                    } 
                    for ($n = 1; $n <= (4 - $col ); $n++) {
                        $partsbox .= '<td style="border: 1px solid #336699;">&nbsp;</td>';
                    }
                    if ($row == 1) {                                            /* If only one row then we waqnt blank second row */
                        $partsbox1 = $partsbox;
                        $partsbox = '<tr><td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td></tr>';
                        $row = 2;
                    }
                    $this->smarty->assign('numparts',count($parts));
                } else {                                                        /* No parts so leave two rows of blank */
                    $partsbox1 .= '<td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td>';
                    $partsbox .= '<tr><td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td><td style="border: 1px solid #336699;">&nbsp;</td></tr>';
                    $row = 2;
                    $this->smarty->assign('numparts',0);
                }
                    
                $this->smarty->assign('partsbox',$partsbox);
                $this->smarty->assign('partsbox1',$partsbox1);
                $this->smarty->assign('partsboxcol',$row+1);
                
                //$this->log($printJob);

		if (defined('SUB_DOMAIN')) {
		    $sub_domain = SUB_DOMAIN;
		} else {
		    $sub_domain = '';
		}

		if (isset($_SERVER['HTTPS'])) { 
		    $domain_name = 'https://' . $_SERVER["HTTP_HOST"] . $sub_domain . "/";
		} else { 
		    $domain_name = 'http://' . $_SERVER["HTTP_HOST"] . $sub_domain . "/";
		} 
                    
		$brandLogo = $domain_name.$this->config['Path']['brandLogosPath'];
                $this->smarty->assign('img_path', APPLICATION_PATH . "/../");
		if($printJob['BrandLogo']) {    
		    if(!file_exists(APPLICATION_PATH . "/../" . $this->config['Path']['brandLogosPath'] . $printJob['BrandLogo'])) {
			$printJob['BrandLogo'] = 'default_logo.png';
		    }        
		    $brandLogo = $brandLogo . $printJob['BrandLogo'];
		} else {
		    $brandLogo = $brandLogo . 'default_logo.png';
		}

		$bradLogoSize = getimagesize($brandLogo);

		$this->smarty->assign('brandLogo', $brandLogo);
		$this->smarty->assign('brandLogoWidth', $bradLogoSize[0]);
		$this->smarty->assign('brandLogoHeight', $bradLogoSize[1]);
                
                $this->smarty->assign('ReferralNo', $job_details_page['Labels']['referral_no']);
                    
		if($PrintType == 2) {
		    $printJob['PageTitle'] = 'Customer Receipt';
                } elseif($PrintType == 3) {
		    $printJob['PageTitle'] = 'Service Report';
		} else {
		    $printJob['PageTitle'] = 'Job Card';
		}
                
                $utm = $Job->getUnitModelType($printJob['JobID']);              /* Get the unit type and the model */
                $printJob['pModel'] = $utm['Model'];                            /* Set processed model */
                $printJob['pUnitType'] = $utm['UnitType'];		
		
		// Get ImeiRequired Value :: Done by Praveen Kumar N [04/07/2013] [START]//
		$imr = $Job->getImeiRequired($printJob['UnitTypeID']);  
		$printJob['ImeiRequired'] = $imr['ImeiRequired'];   /* Set ImeiRequired */
		$printJob['ImeiNo']	  = $printJob['ImeiNo'];  /* Get the value of  ImeiNo */
		/*------ end--------------- [END]*/			 
		
                
                if ($printJob['ProductID']) {
                    $printJob['ProductNo'] = $product_model->getProductNoFromId($printJob['ProductID']);
                } else {
                    $printJob['ProductNo'] = '';
                }		
		$printJob['DateBooked'] = date("d/m/Y", strtotime($printJob['DateBooked']));
                if($printJob['DateOfPurchase'] != "")
                    $printJob['DateOfPurchase'] = date("d/m/Y", strtotime($printJob['DateOfPurchase']));
                else
                    $printJob['DateOfPurchase'] = "";
		$bFNo = 1;
                    
		if($printJob['BranchBuildingNameNumber']) {    
		    $printJob['BranchDetails' . $bFNo] = $printJob['BranchBuildingNameNumber'];
		    $bFNo++;
		}
		if($printJob['BranchStreet']) {    
		    $printJob['BranchDetails' . $bFNo] = $printJob['BranchStreet'];
		    $bFNo++;
		}
		if($printJob['BranchLocalArea']) {    
		    $printJob['BranchDetails' . $bFNo] = $printJob['BranchLocalArea'];
		    $bFNo++;
		}
		if($printJob['BranchTownCity']) {    
		    $printJob['BranchDetails' . $bFNo] = $printJob['BranchTownCity'];
		    $bFNo++;
		}
		if($printJob['BranchPostalCode']) {    
		    $printJob['BranchDetails' . $bFNo] = $printJob['BranchPostalCode'];
		    $bFNo++;
		}
		
		// if($printJob['BranchContactPhone'])
		// {    
		    if(!$printJob['BranchContactPhone']) {
			$printJob['BranchContactPhone'] = " -- ";
		    }    

		    $printJob['BranchDetails' . $bFNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(T) </span>' . $printJob['BranchContactPhone'];

		    if($printJob['BranchContactPhoneExt']) {
			$printJob['BranchDetails' . $bFNo] = $printJob['BranchDetails' . $bFNo] . ' &nbsp;&nbsp;&nbsp;<span style="color:#666;font-weight:bold;margin-right:5px;">Ex: </span>' . $printJob['BranchContactPhoneExt'];
		    }

		    $bFNo++;
		// }
                   
		if(!$printJob['BranchContactEmail'] || strtolower($printJob['BranchContactEmail']) == "<null>") {
		    $printJob['BranchContactEmail'] = " -- ";
		}
                     
		//if($printJob['BranchContactEmail'])
		//{    
		    $printJob['BranchDetails' . $bFNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(E) </span>' . $printJob['BranchContactEmail'];
		    $bFNo++;
		//}
                    
		for($i = $bFNo; $i <= 7; $i++) {
		    $printJob['BranchDetails' . $i] = '';
		}
                    
		$SCAddress   = explode(",", $printJob['ServiceCentreAddress']);

		$sp_address1 = isset($SCAddress[1]) ? $SCAddress[1] : '';

		$sp_address1 = $sp_address1 . " " . (isset($SCAddress[2]) ? $SCAddress[2] : '');
		$sp_address1 = trim($sp_address1);

		$scNo = 1;
		if($sp_address1) {
		    $printJob['SCDetails' . $scNo] = $sp_address1;
		    $scNo++;
		}

		if(isset($SCAddress[3]) && $SCAddress[3] != '') {
		   $printJob['SCDetails' . $scNo] = $SCAddress[3];
		   $scNo++;
		}     
		if(isset($SCAddress[4]) && $SCAddress[4] != '') {
		    $printJob['SCDetails' . $scNo] = $SCAddress[4];
		    $scNo++;
		}
		if(isset($SCAddress[5]) && $SCAddress[5] != '') {
		    $printJob['SCDetails' . $scNo] = $SCAddress[5];
		    $scNo++;
		}
		// if($printJob['ServiceCentreTelephone']!='')
		// {
		    if(!$printJob['ServiceCentreTelephone']) {
			$printJob['ServiceCentreTelephone'] = " -- ";
		    }
		    $printJob['SCDetails' . $scNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(T) </span>' . $printJob['ServiceCentreTelephone'];
		    $scNo++;
		// }
                    
		// if($printJob['ServiceCentreEmail']!='')
		// {
		    if(!$printJob['ServiceCentreEmail']) {
			$printJob['ServiceCentreEmail'] = " -- ";
		    }    
		    $printJob['SCDetails' . $scNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(E) </span>' . $printJob['ServiceCentreEmail'];
		    $scNo++;
		// }

		for($i = $scNo; $i <= 6; $i++) {
		    $printJob['SCDetails' . $i] = '';
		}
                    
		$SkylienModel = $this->loadModel('Skyline');
		$product_location = $SkylienModel->getProductLocationName($printJob['ProductLocation']);
                    
		$this->smarty->assign('product_location', $product_location);

		if($printJob['ClosedDate'] && $printJob['ClosedDate'] != '0000-00-00') {
		    $printJob['ClosedDate'] = date("d/m/Y", strtotime($printJob['ClosedDate']));
		} else {
		    $printJob['ClosedDate'] = "";
		}

		$consumerFullName = trim($printJob['ContactTitle'] . " " . $printJob['ContactFirstName'] . " " . $printJob['ContactLastName']);

		$this->smarty->assign('consumerFullName', $consumerFullName);

		
		$customer_address1 = ($printJob['CompanyName']) ? $printJob['CompanyName'] : '';

		if($customer_address1 != '' && $printJob['BuildingNameNumber'] != '') {
		    $customer_address1 = $customer_address1 . ", " . $printJob['BuildingNameNumber'];
		}

		if($customer_address1 == '') {
		    $customer_address1 = trim($printJob['Street']);
		    $customer_address2 = trim($printJob['LocalArea']);
		} else {
		   $customer_address2 = trim($printJob['Street']);
		   if($customer_address2 != '' && $printJob['LocalArea'] != '') {
		       $customer_address2 = $customer_address2 . ", " . trim($printJob['LocalArea']);
		   }    
		}
                   
		$cNo = 1;
		if($customer_address1) {
		    $printJob['CDetails' . $cNo] = $customer_address1;
		    $cNo++;
		}
		if($customer_address2) {
		    $printJob['CDetails' . $cNo] = $customer_address2;
		    $cNo++;
		}
		if($printJob['TownCity']) {
		    $printJob['CDetails' . $cNo] = $printJob['TownCity'];
		    $cNo++;
		}
		if($printJob['CountryName']) {
		    $printJob['CDetails' . $cNo] = $printJob['CountryName'];
		    $cNo++;
		}
		if($printJob['PostalCode']) {
		    $printJob['CDetails' . $cNo] = $printJob['PostalCode'];
		    $cNo++;
		}
                    
		//  if($printJob['ContactHomePhone']!='')
		//  {
		    if(!$printJob['ContactHomePhone']) {
			$printJob['ContactHomePhone'] = " -- ";
		    }

		    $printJob['CDetails' . $cNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(T) </span>' . $printJob['ContactHomePhone'];

		    if($printJob['ContactWorkPhoneExt']) {
			$printJob['CDetails' . $cNo] =  $printJob['CDetails' . $cNo] . "&nbsp;&nbsp;&nbsp;<span style='color:#666;font-weight:bold;margin-right:5px;'>Ex: </span>" . $printJob['ContactWorkPhoneExt'];
		    }
                    
                    /* Mobile Number */		    
                    $cNo++;
                    
                    if(!$printJob['ContactMobile']) {
			$printJob['ContactMobile'] = " -- ";
		    }
                    
                    $printJob['CDetails' . $cNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(M) </span>' . $printJob['ContactMobile'];

		    $cNo++;
		 // }
                    
		    if(!$printJob['ContactEmail']) {
			$printJob['ContactEmail'] = " -- ";
		    }   
                        
		// if($printJob['ContactEmail']!='')
		// {
		    $printJob['CDetails' . $cNo] = '<span style="color:#666;font-weight:bold;margin-right:5px;">(E) </span>' . $printJob['ContactEmail'];
		    $cNo++;
		// }
                    
		for($i = $cNo; $i <= 8; $i++) {
		    $printJob['CDetails' . $i] = '';
		}
                    

		$printJob['ServiceJobTypeName'] = trim($printJob['ServiceTypeName'] . " " . $printJob['JobTypeName']);
                    
		$this->smarty->assign('printJob', $printJob);

		if($pdf) {
		    
                    if($PrintType == 2) {
                        //Customer Receipt
                        $html_code = $this->smarty->fetch('jobCustomerReceiptPDF.tpl');
                    } else if($PrintType == 3) {                            
                        //Service Report
                        $html_code = $this->smarty->fetch('jobServiceReportPDF.tpl');
                    } else {
                        //Deafult to Job Card
                        $html_code = $this->smarty->fetch('jobRecordPDF.tpl'); 
                    }
		    
		    $mpdf->AddPage();
		    $mpdf->WriteHTML($html_code);
		    
		} else {
		    
		    if ($PrintType == 2) {                                      /* Check tgemplate to be used */
                        /* Customer Receipt */
                        $html_code = $this->smarty->fetch('jobCustomerReceiptPDF.tpl');
                    } elseif ($PrintType == 3) {                            
                        /* Customer Receipt */
                        $html_code = $this->smarty->fetch('jobServiceReportPDF.tpl');
                    } else {
                        $html_code = $this->smarty->fetch('printJobRecord.tpl'); 
                    }
		    echo $html_code;
		    
		}
		
	    }
	    
	}
	
	if($pdf) {
	    $mpdf->Output();
	    exit;
	}
	
    }
      
      
      
      public function confirmedAction(  $args  ) { 
         
            $Skyline = $this->loadModel('Skyline');
            $afn_job = $this->messages->getAFN('AFN-JOB');
            $page    = $this->messages->getPage('bookingProcess', $this->lang);
            
            $page['Buttons']['print_job_record'] = str_replace("%%afn-job%%", $afn_job, $page['Buttons']['print_job_record']);
            $page['Buttons']['open_job_record'] = str_replace("%%afn-job%%", $afn_job, $page['Buttons']['open_job_record']);
            
            
            $this->smarty->assign('page', $page);
            
            $firstArg = isset($args[0]) ? $args[0] : false;
          
            $this->smarty->assign('bp_number', $firstArg); 
                        
           // $args['NetworkID'] = 1;
           // $args['CountryID'] = 2;
           // $args['RepairSkillID'] = 4;
           
            $service_center    = $Skyline->getServiceCenterDetails(0, $firstArg);
          
            $product_address = false;
            
            if($firstArg)
            {
                $Job = $this->loadModel('Job');
                $Job->fetch($firstArg);
                
                if(isset($Job->current_record['JobID']))
                {
                   
                    if($Job->current_record['ProductLocation']=='Branch' || $Job->current_record['ProductLocation']=='Other' || trim($Job->current_record['ColAddPostcode']))
                    {
                           $product_address = trim($Job->current_record['ColAddCompanyName']);
                   
                            if(trim($Job->current_record['ColAddBuildingNameNumber']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ", ";
                                 }    
                                 $product_address .= trim($Job->current_record['ColAddBuildingNameNumber']);
                            }

                            if(trim($Job->current_record['ColAddStreet']))
                            {     
                                 if($product_address)
                                 {
                                     $product_address .= ", ";
                                 }
                                 $product_address .= trim($Job->current_record['ColAddStreet']);
                            } 

                            if(trim($Job->current_record['ColAddLocalArea']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ", ";
                                 }
                                 $product_address .= trim($Job->current_record['ColAddLocalArea']);
                            }

                            if(trim($Job->current_record['ColAddTownCity']))
                            {    
                             if($product_address)
                             {
                                 $product_address .= ", ";
                             }

                             $product_address .= trim($Job->current_record['ColAddTownCity']);
                            }

                            if(trim($Job->current_record['ColAddPostcode']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ", ";
                                 }
                                 $product_address .= strtoupper(trim($Job->current_record['ColAddPostcode']));
                            }
                        
                    }   
                    else if($Job->current_record['ProductLocation']=='Customer')
                    {
                        $product_address = trim($Job->current_record['CompanyName']);
                   
                           
                        
                        if(trim($Job->current_record['BuildingNameNumber']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ", ";
                            } 
                            
                            $product_address .= trim($Job->current_record['BuildingNameNumber']);

                        }    
                            
                        if(trim($Job->current_record['Street']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ", ";
                            }
                         
                            $product_address .= trim($Job->current_record['Street']);
                        }

                        if(trim($Job->current_record['LocalArea']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ", ";
                            }
                            $product_address .= trim($Job->current_record['LocalArea']);
                        }
                        
                        if(trim($Job->current_record['TownCity']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ", ";
                            }

                            $product_address .= trim($Job->current_record['TownCity']);
                        }

                        if(trim($Job->current_record['PostalCode']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ", ";
                            }
                            $product_address .= strtoupper(trim($Job->current_record['PostalCode']));
                        }
                    } 
                    else if($Job->current_record['ProductLocation']=='Service Provider' && $Job->current_record['ServiceProviderID'])
                    {
                       $product_address = isset($Job->current_record['ServiceCentreAddress'])?$Job->current_record['ServiceCentreAddress']:'';
                    }
                    
                   
                }
                else
                {
                    $this->redirect('index',null, null);
                }
            
            }
            else
            {
                $this->redirect('index',null, null);
            }
            
            
            
            $this->smarty->assign('product_address', $product_address); 
            
            
	    //$this->log("JOB: " . var_export($Job->current_record,true));
	    //$this->log("SERVICE CENTER: " . var_export($service_center,true));
	    
	    //$altSCDetails = $Job->getALtSCDetails($Job->current_record["ServiceProviderID"],$Job->current_record["ClientID"]);
	    
            $this->smarty->assign('service_center', $service_center); 
            
            $CollectionDate = '';
            if($Job->current_record['CollectionDate'] && strlen($Job->current_record['CollectionDate'])==10)
            {
                $CollectionDate = date("d/m/Y", strtotime($Job->current_record['CollectionDate']));
            }    
            
            $this->smarty->assign('CollectionDate', $CollectionDate); 
            $this->smarty->assign('CourierID', $Job->current_record['CourierID']); 
            $this->smarty->assign('RMANumber', $Job->current_record['RMANumber']); 
            
            $this->smarty->assign('AP10000', Functions::hasPermission($this->user, 'AP10000'));
                       
            $CouriersModel = $this->loadModel('Couriers');
            $couriers      = $CouriersModel->fetchAll(false, false, $Job->current_record['ServiceProviderID']);
            $this->smarty->assign('couriers', $couriers);
            
            
            $html_code =   $this->smarty->fetch('bookingProcess.tpl'); 
            echo $html_code;
         
            
            return;
     } 
     
     
     /**
     * Description
     * 
     * This method is for to handle job appointments.
     * 
     * @param array $args Its an associative array.
    
     * @return void   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function appointmentsJobAction(  $args  ) { 
         
         if ($_SERVER['REQUEST_METHOD'] == 'POST') 
         {
             
             if(isset($_POST['APT_AppointmentDate']))
             {    
                $dateArray = explode("/", $_POST['APT_AppointmentDate']);
                if(count($dateArray)==3)
                {
                    $_POST['APT_AppointmentDate'] = $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
                }
                else
                {
                    $_POST['APT_AppointmentDate'] = "0000-00-00";
                }
             }

             $AppointmentModel = $this->loadModel("Appointment");
             
             $dataArray = array();
             if(isset($_POST['APT_AppointmentID']) && $_POST['APT_AppointmentID'])
             {
                 
                 if(isset($_POST['APT_Cancelled']))
                 {
                     $dataArray = array(

                        'Cancelled'=>'Yes',
                        'AppointmentID'=>$_POST['APT_AppointmentID']
                        
                        );
                 }
                 else
                 {
                 
                    $dataArray = array(

                        'JobID'=>$_POST['APT_JobID'],
                        'UserID'=>$this->user->UserID,
                        'AppointmentID'=>$_POST['APT_AppointmentID'],
                        'AppointmentDate'=>$_POST['APT_AppointmentDate'],
                        'AppointmentTime'=>$_POST['APT_AppointmentTime'],
                        'AppointmentType'=>$_POST['APT_AppointmentType'],
                        'Notes'=>$_POST['APT_Notes'],
                        'ServiceProviderID'=>$_POST['APT_ServiceProviderID'],
                        'ServiceProviderEngineerID'=>$_POST['APT_ServiceProviderEngineerID']

                        );
                 }
                 
                 $result = $AppointmentModel->update($dataArray);
                 
             }
             else
             {
                 $dataArray = array(
                     
                     'JobID'=>$_POST['APT_JobID'],
                     'UserID'=>$this->user->UserID,
                     'AppointmentDate'=>$_POST['APT_AppointmentDate'],
                     'AppointmentTime'=>$_POST['APT_AppointmentTime'],
                     'AppointmentType'=>$_POST['APT_AppointmentType'],
                     'Notes'=>$_POST['APT_Notes'],
                     'ServiceProviderID'=>$_POST['APT_ServiceProviderID'],
                     'ServiceProviderEngineerID'=>$_POST['APT_ServiceProviderEngineerID']
                     
                     );
                 
                 $result = $AppointmentModel->create($dataArray);
                 
             }
             
             
             if(is_array($result))
             {
                 if($result['status']=='SUCCESS')
                 {
                     $result['status'] = 'OK';
                 }
                 else if($result['status']=='FAIL')
                 {
                     $result['status'] = 'ERROR';
                 }
             }
             else
             {
                 $result['status'] = 'ERROR';
             }
             
             echo json_encode( $result );
             
         }
         else
         {
           
          $functionAction       = isset($args[0])?$args[0]:false;
          $selectedRowId        = isset($args[1])?$args[1]:false;
          
          if(!$selectedRowId || (!$this->user->SuperAdmin && !$this->user->ServiceProviderID))
          {
             $this->redirect('index', null, null); 
          }    
          
          $this->page      = $this->messages->getPage('jobDetails', $this->lang);
          $this->smarty->assign('page', $this->page);

          $SkylineModel     =  $this->loadModel('Skyline');
          $AppointmentTypes =  $SkylineModel->getAppointmentTypes();
          
          $this->smarty->assign('AppointmentTypes', $AppointmentTypes);
          $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
          $EngineersList  = array();
          $EngineersModel     =  $this->loadModel('Engineers');
          
          if($this->user->SuperAdmin)
          {    
              $ServiceProviders   = $SkylineModel->getServiceProviders();
              $this->smarty->assign('ServiceProviders', $ServiceProviders);
              
              
          }
          else if($this->user->ServiceProviderID)
          {
              $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
              
              
              $EngineersList = $EngineersModel->fetchAll($this->user->ServiceProviderID, 'Active');
          }
          
           $CancelPage = false;
          
          
            switch( $functionAction) {

                case 'insert':

                    
                        $datarow = array('AppointmentID'=>'', 'JobID'=>$selectedRowId, 'AppointmentDate'=>'', 'AppointmentTime'=>'ANY', 'AppointmentType'=>'', 'ServiceProviderID'=>'', 'ServiceProviderEngineerID'=>'', 'Notes'=>'');
                       
                        $this->smarty->assign('datarow', $datarow);
                        $this->smarty->assign('form_legend', $this->page['Text']['insert_app_page_legend']);
                        
                        $this->smarty->assign('EngineersList', $EngineersList);
                        
                        $this->smarty->assign('CancelPage', $CancelPage);
                        
                        
                        $htmlCode = $this->smarty->fetch('appointmentsJob.tpl');

                        
                        
                        echo $htmlCode;

                    break;


                case 'update':


                        $args['AppointmentID'] = $selectedRowId;
                        $model     = $this->loadModel('Appointment');

                        
                        $datarow   = $model->fetchRow($args);

                        
                        if(isset($datarow['AppointmentDate']) && $datarow['AppointmentDate'])
                        {    
                            $datarow['AppointmentDate'] = date("d/m/Y", strtotime($datarow['AppointmentDate']));
                            
                        }

                        $this->smarty->assign('form_legend', $this->page['Text']['update_apt_page_legend']);

                       
                        $this->smarty->assign('datarow', $datarow);
                        
                        if(isset($datarow['ServiceProviderID']) && $datarow['ServiceProviderID'] && $this->user->SuperAdmin)
                        {    
                            $EngineersList = $EngineersModel->fetchAll($datarow['ServiceProviderID'], 'Active');
                            
                        }    
                        
                        $this->smarty->assign('EngineersList', $EngineersList);

                        $this->smarty->assign('CancelPage', $CancelPage);
                        
                        $htmlCode = $this->smarty->fetch('appointmentsJob.tpl');

                        echo $htmlCode;    

                    break;

                    
                 case 'delete':

                        $this->smarty->assign('form_legend', $this->page['Text']['delete_apt_page_legend']);
                     
                        $this->smarty->assign('CancelPage', true);
                        $this->smarty->assign('AppointmentID', $selectedRowId);
                        
                        
                        
                        
                        $htmlCode = $this->smarty->fetch('appointmentsJob.tpl');

                        echo $htmlCode;    

                    break;   
                    
                    
                default:

                   

                }  
         }
            
     }
     
     
     
     /**
     * Description
     * 
     * This method is for to display diary system.
     * 
     * @param array $args Its an associative array.
    
     * @return void   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function bookAppointmentAction(  $args  ) { 
         
            
            $page    = $this->messages->getPage('jobBookingDiary', $this->lang);
            
            $Skyline = $this->loadModel('Skyline');
           
            $countries  = $Skyline->getCountriesCounties();
            
            $this->smarty->assign('page', $page);
            
            $JobID = isset($args[0]) ? $args[0] : false;
            $product_address = '';
            
            $jb_datarow = array("ColAddCompanyName"=>'', "ColAddBuildingName"=>'', "ColAddStreet"=>'', "ColAddArea"=>'', "ColAddCity"=>'', "ColAddCounty"=>0, "ColAddCountry"=>0, "ColAddPostalCode"=>'', "ContactEmail"=>'', "ContactHomePhone"=>'', "ContactHomePhoneExt"=>'', "ContactMobile"=>'');
            
            if($JobID)
            {
                $Job = $this->loadModel('Job');
                $Job->fetch($JobID);
                
                if(isset($Job->current_record['JobID']))
                {
                   
                    if($Job->current_record['ProductLocation']=='Branch' || $Job->current_record['ProductLocation']=='Other' || trim($Job->current_record['ColAddPostcode']))
                    {
                           $product_address = trim($Job->current_record['ColAddCompanyName']);
                           
                           $jb_datarow['ColAddCompanyName'] = trim($Job->current_record['ColAddCompanyName']);
                   
                            if(trim($Job->current_record['ColAddBuildingNameNumber']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ",<br>";
                                 }    
                                 $product_address .= trim($Job->current_record['ColAddBuildingNameNumber']);
                                 
                                 $jb_datarow['ColAddBuildingName'] = trim($Job->current_record['ColAddBuildingNameNumber']);
                            }

                            if(trim($Job->current_record['ColAddStreet']))
                            {     
                                 if($product_address)
                                 {
                                     $product_address .= ",<br>";
                                 }
                                 $product_address .= trim($Job->current_record['ColAddStreet']);
                                 
                                 $jb_datarow['ColAddStreet'] = trim($Job->current_record['ColAddStreet']);
                            } 

                            if(trim($Job->current_record['ColAddLocalArea']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ",<br>";
                                 }
                                 $product_address .= trim($Job->current_record['ColAddLocalArea']);
                                 
                                 $jb_datarow['ColAddArea'] = trim($Job->current_record['ColAddLocalArea']);
                            }

                            if(trim($Job->current_record['ColAddTownCity']))
                            {    
                             if($product_address)
                             {
                                 $product_address .= ",<br>";
                             }

                             $product_address .= trim($Job->current_record['ColAddTownCity']);
                             
                              $jb_datarow['ColAddCity'] = trim($Job->current_record['ColAddTownCity']);
                            }

                            if(trim($Job->current_record['ColAddPostcode']))
                            {    
                                 if($product_address)
                                 {
                                     $product_address .= ",<br>";
                                 }
                                 $product_address .= strtoupper(trim($Job->current_record['ColAddPostcode']));
                                 
                                 $jb_datarow['ColAddPostalCode'] = strtoupper(trim($Job->current_record['ColAddPostcode']));
                            }
                          
                            $jb_datarow['ColAddCounty'] = $Job->current_record['ColAddCountyID'];
                            $jb_datarow['ColAddCountry'] = $Job->current_record['ColAddCountryID'];
                        
                           
                    }   
                    else if($Job->current_record['ProductLocation']=='Customer')
                    {
                        $product_address = trim($Job->current_record['CompanyName']);
                   
                        $jb_datarow['ColAddCompanyName'] = trim($Job->current_record['CompanyName']);   
                        
                        
                        if(trim($Job->current_record['BuildingNameNumber']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ",<br>";
                            } 
                            
                            $product_address .= trim($Job->current_record['BuildingNameNumber']);
                            
                            $jb_datarow['ColAddBuildingName'] = trim($Job->current_record['BuildingNameNumber']);
                        }    
                            
                        if(trim($Job->current_record['Street']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ",<br>";
                            }
                         
                            $product_address .= trim($Job->current_record['Street']);
                            
                            $jb_datarow['ColAddStreet'] = trim($Job->current_record['Street']);
                        }

                        if(trim($Job->current_record['LocalArea']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ",<br>";
                            }
                            $product_address .= trim($Job->current_record['LocalArea']);
                            
                            $jb_datarow['ColAddArea'] = trim($Job->current_record['LocalArea']);
                        }
                        
                        if(trim($Job->current_record['TownCity']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ",<br>";
                            }

                            $product_address .= trim($Job->current_record['TownCity']);
                            
                            $jb_datarow['ColAddCity'] = trim($Job->current_record['TownCity']);
                        }

                        if(trim($Job->current_record['PostalCode']))
                        {    
                            if($product_address)
                            {
                                $product_address .= ",<br>";
                            }
                            $product_address .= strtoupper(trim($Job->current_record['PostalCode']));
                            
                            $jb_datarow['ColAddPostalCode'] = strtoupper(trim($Job->current_record['PostalCode']));
                        }
                        
                        $jb_datarow['ColAddCounty'] = $Job->current_record['CountyID'];
                        $jb_datarow['ColAddCountry'] = $Job->current_record['CountryID'];
                        
                    } 
                    else if($Job->current_record['ProductLocation']=='Service Provider' && $Job->current_record['ServiceProviderID'])
                    {
                       $product_address = isset($Job->current_record['ServiceCentreAddress'])?$Job->current_record['ServiceCentreAddress']:'';
                    
                       
                       $product_address = str_replace(",", ",<br>", $product_address);
                       
                       $service_center    = $Skyline->getServiceCenterDetails($Job->current_record['ServiceProviderID']);
                       
                       $jb_datarow['ColAddBuildingName'] = trim($service_center['BuildingNameNumber']);
                       $jb_datarow['ColAddStreet'] = trim($service_center['Street']);
                       $jb_datarow['ColAddArea'] = trim($service_center['LocalArea']);
                       $jb_datarow['ColAddCity'] = trim($service_center['TownCity']);
                       $jb_datarow['ColAddPostalCode'] = trim($service_center['PostalCode']);
                       $jb_datarow['ColAddCounty'] = $service_center['CountyID'];
                       $jb_datarow['ColAddCountry'] = $service_center['CountryID'];
                       
                    }
                    
                   
                    $jb_datarow['ContactEmail']        = $Job->current_record['ContactEmail'];
                    $jb_datarow['ContactHomePhone']    = $Job->current_record['ContactHomePhone'];
                    $jb_datarow['ContactHomePhoneExt'] = $Job->current_record['ContactWorkPhoneExt'];
                    $jb_datarow['ContactMobile']       = $Job->current_record['ContactMobile'];
                    
                }
                else
                {
                    $this->redirect('index',null, null);
                }
                
                if($product_address!='')
                {
                   $product_address = trim($Job->current_record['ContactTitle']." ".$Job->current_record['ContactFirstName']." ".$Job->current_record['ContactLastName']).",<br>".$product_address;   
                }    
            }
            
          
           
            $this->smarty->assign('appointmentAddress', $product_address); 
            $this->smarty->assign('jb_datarow', $jb_datarow);
            $this->smarty->assign('countries', $countries);
            $this->smarty->assign('JobID', $Job->current_record['JobID']);
            $this->smarty->assign('CustomerID', $Job->current_record['CustomerID']);
            
            $this->smarty->assign('CustomerFullName', trim($Job->current_record['ContactTitle']." ".$Job->current_record['ContactFirstName']." ".$Job->current_record['ContactLastName']));
            
            $this->smarty->assign('appointment_booked', "Appointment Booked<br>".date("jS F Y"));
            
            
            
	   
            $html_code =   $this->smarty->fetch('jobBookingDiary.tpl'); 
            echo $html_code;
         
            
            return;
     }
     
     
     
     /**
     * Description
     * 
     * This method is for to display ra jobs in query table.
     * 
     * @param array $args Its an associative array.
     * @return void   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function raJobsResultAction($args) { 
                 
	$jobModel = $this->loadModel('Job');

	$Skyline = $this->loadModel('Skyline');

	$AuthorisationTypes = $Skyline->getAuthorisationTypes();

	$args['RATypes'] = $AuthorisationTypes;

	$output = $jobModel->fetchAllRA($args);

	echo json_encode($output);
   
     }
     
     
     
    public function openJobsResultAction($args) { 

	$params = array_merge($args, $_REQUEST);
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
       
     if ($this->debug) $this->log($_REQUEST);
	$Job = $this->loadModel('Job');
	$output = $Job->fetchAllOpen($params,$this->user->UserID,$this->page['config']['pageID']);
	echo json_encode($output);
   
    }
     
    public function openJobsSummaryResultAction($args) { 
    
	$params = array_merge($args, $_REQUEST);
     
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
       
        if ($this->debug) $this->log($_REQUEST);
     
        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'OpenJobs'));
     
	$Job = $this->loadModel('Job');
	$output = $Job->fetchAllOpenSummary($params,$this->user->UserID,$this->page,$TatResult);
	echo json_encode($output);
    }
     
     public function fetchCallerAction( $args ) { 

         $Job = $this->loadModel('Job');
        $output     = $Job->fetchCallerJobs($args);
            
          //  $this->log($output);
            
            echo json_encode( $output );
   
     }
     
     
     public function openJobsAction($args) { 
        
         //---unset session values when click on show all----------------------------
         if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='s'){
             unset($_SESSION['openjob']['summary']);
         }
         else if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='d'){
             unset($_SESSION['openjob']['detail']);
         }
         else if(isset($args[0]) && strtolower($args[0]) == "showall" ){
             unset($_SESSION['openjob']);
         }
       
        
          /// // $Skyline = $this->loadModel('Skyline');
        $Job = $this->loadModel('Job');
        $users_model = $this->loadModel('Users');
            
        $page    = $this->messages->getPage('openJobs', $this->lang);
            
            
        $this->smarty->assign('AP9000', Functions::hasPermission($this->user, 'AP9000'));     /* Servicebase Refresh */
        $this->smarty->assign('AP9001', Functions::hasPermission($this->user, 'AP9001'));     /* Servicebase Bulk Refresh */
            
            ///table display preferences ses vars
              
        $this->session->mainTable="job";
        $this->session->mainPage="openJobs";
        $this->page =  $page;
      
//        $args['ojvBy'] = isset($args['ojvBy']) ? $args['ojvBy'] : 's';
        //store TAT View in Session
        $args['ojvBy'] = isset($args['ojvBy']) ? $args['ojvBy'] : (isset($_SESSION['openjob']['ojvBy'])?$_SESSION['openjob']['ojvBy']: 's');
        $_SESSION['openjob']['ojvBy'] = $args['ojvBy'];
       
        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'OpenJobs'));
            
       
        
        if($args['ojvBy'] == "s"){
            $keys_data = $Job->getSummaryFields($page,$TatResult);
        }
        else{
        $datatable= $this->loadModel('DataTable');
            $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID'],$args['ojvBy']);
        }
        
        if(isset($keys_data[1])){
//        $keys=$keys_data[1];
            $keys=  array_values($keys_data[1]);
        }else{
        $keys=array();
        }
//      echo"<pre>";
//      print_r($keys);
//      echo"</pre>";
        $this->smarty->assign('data_keys', $keys);
               ////table display preferences ses vars end
             
            
            $SystemStatusesModel   = $this->loadModel('SystemStatuses');
            $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');
            
//            $args['network']          = isset($args['network']) ? $args['network'] : 0;
//            $args['branch']           = isset($args['branch']) ? $args['branch'] : 0; 
//            $args['client']           = isset($args['client']) ? $args['client'] : 0;
//            $args['unitType']         = isset($args['unitType']) ? $args['unitType'] : 0;
//            $args['skillSet']         = isset($args['skillSet']) ? $args['skillSet'] : 0;
//            
//            $args['brand']            = isset($args['brand']) ? $args['brand'] : 0;
//            $args['serviceProvider']  = isset($args['serviceProvider']) ? $args['serviceProvider'] : 0;
//            $args['manufacturer']     = isset($args['manufacturer']) ? $args['manufacturer'] : 0;
//            $args['sType']            = isset($args['sType']) ? $args['sType'] : 0;
//            $args['scType']           = isset($args['scType']) ? $args['scType'] : 0;
//            $args['ojBy']             = isset($args['ojBy']) ? $args['ojBy'] : 'sp';
//            
//            $args['tView']            = isset($args['tView']) ? $args['tView'] : 's';
//            $args['btnName']          = isset($args['btnName']) ? $args['btnName'] : false;
//            $args['btnValue']         = isset($args['btnValue']) ? $args['btnValue'] : false;
//            $args['btnValue2']        = isset($args['btnValue2']) ? $args['btnValue2'] : false;
            
            //----------store filter value in session----------------------------------            
            if($args['ojvBy'] == "s"){
                $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['openjob']['summary']['network'])?$_SESSION['openjob']['summary']['network']: 0);
                $_SESSION['openjob']['summary']['network'] = $args['network'];

                $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['openjob']['summary']['branch'])?$_SESSION['openjob']['summary']['branch']: 0);
                $_SESSION['openjob']['summary']['branch'] = $args['branch'];

                $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['openjob']['summary']['client'])?$_SESSION['openjob']['summary']['client']: 0);
                $_SESSION['openjob']['summary']['client'] = $args['client'];

                $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['openjob']['summary']['unitType'])?$_SESSION['openjob']['summary']['unitType']: 0);
                $_SESSION['openjob']['summary']['unitType'] = $args['unitType'];

                $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['openjob']['summary']['skillSet'])?$_SESSION['openjob']['summary']['skillSet']: 0);
                $_SESSION['openjob']['summary']['skillSet'] = $args['skillSet'];

                $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['openjob']['summary']['brand'])?$_SESSION['openjob']['summary']['brand']: 0);
                $_SESSION['openjob']['summary']['brand'] = $args['brand'];

                $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['openjob']['summary']['serviceProvider'])?$_SESSION['openjob']['summary']['serviceProvider']: 0);
                $_SESSION['openjob']['summary']['serviceProvider'] = $args['serviceProvider'];

                $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['openjob']['summary']['manufacturer'])?$_SESSION['openjob']['summary']['manufacturer']: 0);
                $_SESSION['openjob']['summary']['manufacturer'] = $args['manufacturer'];

                $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['openjob']['summary']['sType'])?$_SESSION['openjob']['summary']['sType']: 0);
                $_SESSION['openjob']['summary']['sType'] = $args['sType'];

                $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['openjob']['summary']['scType'])?$_SESSION['openjob']['summary']['scType']: 0);
                $_SESSION['openjob']['summary']['scType'] = $args['scType'];

                $args['ojBy'] = isset($args['ojBy']) ? $args['ojBy'] : (isset($_SESSION['openjob']['summary']['ojBy'])?$_SESSION['openjob']['summary']['ojBy']: 'sp');
                $_SESSION['openjob']['summary']['ojBy'] = $args['ojBy'];

                $args['tView'] = isset($args['tView']) ? $args['tView'] : (isset($_SESSION['openjob']['summary']['tView'])?$_SESSION['openjob']['summary']['tView']: 's');
                $_SESSION['openjob']['summary']['tView'] = $args['tView'];

                $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['openjob']['summary']['btnName'])?$_SESSION['openjob']['summary']['btnName']: false);
                $_SESSION['openjob']['summary']['btnName'] = $args['btnName'];

                $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['openjob']['summary']['btnValue'])?$_SESSION['openjob']['summary']['btnValue']: false);
                $_SESSION['openjob']['summary']['btnValue'] = $args['btnValue'];

                $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['openjob']['summary']['btnValue2'])?$_SESSION['openjob']['summary']['btnValue2']: false);
                $_SESSION['openjob']['summary']['btnValue2'] = $args['btnValue2'];
            }
            else{
                $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['openjob']['detail']['network'])?$_SESSION['openjob']['detail']['network']: 0);
                $_SESSION['openjob']['detail']['network'] = $args['network'];

                $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['openjob']['detail']['branch'])?$_SESSION['openjob']['detail']['branch']: 0);
                $_SESSION['openjob']['detail']['branch'] = $args['branch'];

                $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['openjob']['detail']['client'])?$_SESSION['openjob']['detail']['client']: 0);
                $_SESSION['openjob']['detail']['client'] = $args['client'];

                $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['openjob']['detail']['unitType'])?$_SESSION['openjob']['detail']['unitType']: 0);
                $_SESSION['openjob']['detail']['unitType'] = $args['unitType'];

                $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['openjob']['detail']['skillSet'])?$_SESSION['openjob']['detail']['skillSet']: 0);
                $_SESSION['openjob']['detail']['skillSet'] = $args['skillSet'];

                $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['openjob']['detail']['brand'])?$_SESSION['openjob']['detail']['brand']: 0);
                $_SESSION['openjob']['detail']['brand'] = $args['brand'];

                $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['openjob']['detail']['serviceProvider'])?$_SESSION['openjob']['detail']['serviceProvider']: 0);
                $_SESSION['openjob']['detail']['serviceProvider'] = $args['serviceProvider'];

                $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['openjob']['detail']['manufacturer'])?$_SESSION['openjob']['detail']['manufacturer']: 0);
                $_SESSION['openjob']['detail']['manufacturer'] = $args['manufacturer'];

                $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['openjob']['detail']['sType'])?$_SESSION['openjob']['detail']['sType']: 0);
                $_SESSION['openjob']['detail']['sType'] = $args['sType'];

                $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['openjob']['detail']['scType'])?$_SESSION['openjob']['detail']['scType']: 0);
                $_SESSION['openjob']['detail']['scType'] = $args['scType'];

                $args['ojBy'] = isset($args['ojBy']) ? $args['ojBy'] : (isset($_SESSION['openjob']['detail']['ojBy'])?$_SESSION['openjob']['detail']['ojBy']: 'sp');
                $_SESSION['openjob']['detail']['ojBy'] = $args['ojBy'];

                $args['tView'] = isset($args['tView']) ? $args['tView'] : (isset($_SESSION['openjob']['detail']['tView'])?$_SESSION['openjob']['detail']['tView']: 's');
                $_SESSION['openjob']['detail']['tView'] = $args['tView'];

                $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['openjob']['detail']['btnName'])?$_SESSION['openjob']['detail']['btnName']: false);
                $_SESSION['openjob']['detail']['btnName'] = $args['btnName'];

                $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['openjob']['detail']['btnValue'])?$_SESSION['openjob']['detail']['btnValue']: false);
                $_SESSION['openjob']['detail']['btnValue'] = $args['btnValue'];

                $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['openjob']['detail']['btnValue2'])?$_SESSION['openjob']['detail']['btnValue2']: false);
                $_SESSION['openjob']['detail']['btnValue2'] = $args['btnValue2'];
                
                if(isset($_SESSION['openjob']['detail']['JobID']) && $_SESSION['openjob']['detail']['JobID']){
                    $this->smarty->assign('selectedJobId',$_SESSION['openjob']['detail']['JobID']);
                }
                else{
                    $this->smarty->assign('selectedJobId',0);
                }
                
            }
            
            
            $statsResult = $Job->fetchAllOpenStats($args);
            
            if(isset($TatResult['TatID']))
            {
                $tempArgs = $args;
                
                $tempArgs['btnName']    = 'b1';
                $tempArgs['btnValue']   = $TatResult['Button1'];
                $tempArgs['btnValue2']  = false;
                
                $button1StatsResult = $Job->fetchAllOpenStats($tempArgs);
                $TatResult['Button1Value'] = 0;
                foreach($button1StatsResult AS $BS)
                {
                    $TatResult['Button1Value'] += $BS[1];
                }    
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button1StatsResult_1 = $Job->fetchAllOpenStats($tempArgs1);
                $b1bValue = 0;
                foreach($button1StatsResult_1 AS $BS)
                {
                    $b1bValue += $BS[1];
                } 
                if($b1bValue)
                {
                    $TatResult['Button1Value'] .= ' ['.$b1bValue.'] ';
                }    
                
                
                $tempArgs['btnName']    = 'b2';
                $tempArgs['btnValue']   = $TatResult['Button1'];
                $tempArgs['btnValue2']  = $TatResult['Button3'];
                
                $button2StatsResult = $Job->fetchAllOpenStats($tempArgs);
                $TatResult['Button2Value'] = 0;
                foreach($button2StatsResult AS $BS)
                {
                    $TatResult['Button2Value'] += $BS[1];
                }
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button2StatsResult_1 = $Job->fetchAllOpenStats($tempArgs1);
                $b2bValue = 0;
                foreach($button2StatsResult_1 AS $BS)
                {
                    $b2bValue += $BS[1];
                } 
                if($b2bValue)
                {
                    $TatResult['Button2Value'] .= ' ['.$b2bValue.'] ';
                }
                
                
                
                $tempArgs['btnName']    = 'b3';
                $tempArgs['btnValue']   = $TatResult['Button3'];
                $tempArgs['btnValue2']  = false;
                
                $button3StatsResult = $Job->fetchAllOpenStats($tempArgs);
                $TatResult['Button3Value'] = 0;
                foreach($button3StatsResult AS $BS)
                {
                    $TatResult['Button3Value'] += $BS[1];
                } 
                
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button3StatsResult_1 = $Job->fetchAllOpenStats($tempArgs1);
                $b3bValue = 0;
                foreach($button3StatsResult_1 AS $BS)
                {
                    $b3bValue += $BS[1];
                } 
                if($b3bValue)
                {
                    $TatResult['Button3Value'] .= ' ['.$b3bValue.'] ';
                }
                
                
                $tempArgs['btnName']    = 'b4';
                $tempArgs['btnValue']   = $TatResult['Button4'];
                $tempArgs['btnValue2']  = false;
                
                $button4StatsResult = $Job->fetchAllOpenStats($tempArgs);
                $TatResult['Button4Value'] = 0;
                foreach($button4StatsResult AS $BS)
                {
                    $TatResult['Button4Value'] += $BS[1];
                }
                
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button4StatsResult_1 = $Job->fetchAllOpenStats($tempArgs1);
                $b4bValue = 0;
                foreach($button4StatsResult_1 AS $BS)
                {
                    $b4bValue += $BS[1];
                } 
                if($b4bValue)
                {
                    $TatResult['Button4Value'] .= ' ['.$b4bValue.'] ';
                }
                
            }
            else
            {
               $TatResult = array(
                   
                   'TatID'=>'',
                   'TatType'=>'OpenJobs',
                   'Button1'=>'',
                   'Button3'=>'',
                   'Button4'=>'',
                   'Button1Colour'=>'',
                   'Button2Colour'=>'',
                   'Button3Colour'=>'',
                   'Button4Colour'=>'',
                   'Button1Value'=>'',
                   'Button2Value'=>'',
                   'Button3Value'=>'',
                   'Button4Value'=>''
                   
               );
            }
           
            
            for($r=0;$r<count($statsResult);$r++)
            {
                $statsResult[$r][4] = $statsResult[$r][0];
                $statsResult[$r][0] = $statsResult[$r][0]." (".$statsResult[$r][1].")";
                
            }
            
            $totalRows = 0;
            $selectedStatusClient = '';
            for($r=0;$r<count($statsResult);$r++)
            {
                
                if($args['sType'] && $args['sType']==$statsResult[$r][2])
                {
                    $totalRows = $statsResult[$r][1];
                    $selectedStatusClient = " ".$statsResult[$r][4];
                    break;
                }
                else
                {
                    $totalRows += $statsResult[$r][1];
                }
            }
           
             
             
            
            // this section of code re-instated following loss in bitbucket repository 31/07/2012
            if( isset($args['manufacturer']) ) {                                /* Check if a manufacturer ID is passed as a parameter */
                $this->smarty->assign('manufacturer',$args['manufacturer']);
            } else {
                $this->smarty->assign('manufacturer',0);
            }
            
            if( isset($args['client']) ) {                                      /* Check if a client ID is passed as a parameter */
                $this->smarty->assign('client',$args['client']);
            } else {
                $this->smarty->assign('client',0);
            }
            
            if( isset($args['network']) ) {                                /* Check if a Network ID is passed as a parameter */
                $this->smarty->assign('network',$args['network']);
            } else {
                $this->smarty->assign('network',0);
            }
            
            if( isset($args['branch']) ) {                                /* Check if a Branch ID is passed as a parameter */
                $this->smarty->assign('branch',$args['branch']);
            } else {
                $this->smarty->assign('branch',0);
            }
            
            if( isset($args['unitType']) ) {                                /* Check if a Unit Type ID is passed as a parameter */
                $this->smarty->assign('unitType',$args['unitType']);
            } else {
                $this->smarty->assign('unitType',0);
            }
            
            if( isset($args['skillSet']) ) {                                /* Check if a Unit Type ID is passed as a parameter */
                $this->smarty->assign('skillSet',$args['skillSet']);
            } else {
                $this->smarty->assign('skillSet',0);
            }
                    
           /*
            $afn_job = $this->messages->getAFN('AFN-JOB');       
            $page['Buttons']['print_job_record'] = str_replace("%%afn-job%%", $afn_job, $page['Buttons']['print_job_record']);
            $page['Buttons']['open_job_record'] = str_replace("%%afn-job%%", $afn_job, $page['Buttons']['open_job_record']);
            */
            
            
            
            $this->smarty->assign('page', $page);
            $this->smarty->assign('brand', $args['brand']);
            $this->smarty->assign('serviceProvider', $args['serviceProvider']);
            $this->smarty->assign('sType', $args['sType']);
            $this->smarty->assign('scType', $args['scType']);
            
            $this->smarty->assign('TatResult', $TatResult);
            $this->smarty->assign('ojBy', $args['ojBy']);
            $this->smarty->assign('ojvBy', $args['ojvBy']);
            $this->smarty->assign('tView', $args['tView']);
            $this->smarty->assign('btnName', $args['btnName']);
            $this->smarty->assign('btnValue', $args['btnValue']);
            $this->smarty->assign('btnValue2', $args['btnValue2']);
            
            $this->smarty->assign('statsResult', $statsResult);
            
            $SkylineModel = $this->loadModel('Skyline');
            
            $jobStatusList = $SkylineModel->getSystemStatus();
            
            if($this->user->NetworkID)
            {
               $manufacturerList = $SkylineModel->getManufacturer('', $this->user->NetworkID);
            }
            else
            {
               $manufacturerList = $SkylineModel->getManufacturer('', null);
            }
            
            $UserModel = $this->loadModel('Users');
            $PreferredManufacturers = $UserModel->getPreferredClientGroup('Manufacturer','openJobs');
            $PreferredBrands = $UserModel->getPreferredClientGroup('Brand','openJobs');
            $PreferredServiceProviders = $UserModel->getPreferredClientGroup('ServiceProvider','openJobs');
            
            $SelectedManufacturers      = array();
            $SelectedBrands             = array();
            $SelectedServiceProviders   = array();
            
            $PreferredNetworks   = $UserModel->getPreferredClientGroup('Network','openJobs');
            $PreferredBranches   = $UserModel->getPreferredClientGroup('Branch','openJobs');
            $PreferredClients    = $UserModel->getPreferredClientGroup('Client','openJobs');            
            $PreferredUnitTypes  = $UserModel->getPreferredClientGroup('UnitType','openJobs');
            $PreferredSkillset   = $UserModel->getPreferredClientGroup('SkillSet','openJobs');

            $SelectedNetworks           = array();
            $SelectedBranches           = array();
            $SelectedClient             = array();
            $SelectedUnitTypes          = array();
            $SelectedSkillSet           = array();
            
            $manuPriority  = array();
            $brandPriority = array();
            $serviceProviderPriority = array();
            
            
            if(count($PreferredManufacturers))
            {
                $ManufacturerGroupEnable = $PreferredManufacturers[0]['GroupEnable'];
                for($i=0;$i<count($manufacturerList);$i++)
                {
                    $manufacturerList[$i]['Exists']   = false;
                    $manufacturerList[$i]['Priority'] = '';
                    
                    foreach($PreferredManufacturers as $pm)
                    {
                        if($manufacturerList[$i]['ManufacturerID']==$pm['PreferentialID'])
                        {
                            $manufacturerList[$i]['Exists']   = true;
                            $manuPriority[] =  $manufacturerList[$i]['Priority'] = $pm['Priority'];
                            
                            if(!$manufacturerList[$i]['ManufacturerLogo'])
                            {
                                //$manufacturerList[$i]['ManufacturerLogo'] = "no_logo.png";
                                $manufacturerList[$i]['ManufacturerLogo'] = false;
                            }  
                            
                            $SelectedManufacturers[] = $manufacturerList[$i];
                            
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedManufacturers)>0)
                {
                    array_multisort($manuPriority, SORT_ASC, $SelectedManufacturers);
                }
                
                
                
            }  
            else {
                $ManufacturerGroupEnable = 0;
                for($i=0;$i<count($manufacturerList);$i++)
                {
                    $manufacturerList[$i]['Exists'] = false;
                    $manufacturerList[$i]['Priority'] = '';
                }
            }
            
            $brandsList = $SkylineModel->getBrands($this->user->NetworkID, $this->user->ClientID, $this->user->BranchID, $this->user->ManufacturerID, $this->user->ServiceProviderID);
            
            
            
            if(count($PreferredBrands))
            {
                $BrandGroupEnable = $PreferredBrands[0]['GroupEnable'];
                for($i=0;$i<count($brandsList);$i++)
                {
                    $brandsList[$i]['Exists'] = false;
                    $brandsList[$i]['Priority'] = '';
                    
                    foreach($PreferredBrands as $pb)
                    {
                        if($brandsList[$i]['BrandID']==$pb['PreferentialID'])
                        {
                            $brandsList[$i]['Exists']   = true;
                            $brandPriority[] = $brandsList[$i]['Priority'] = $pb['Priority'];
                            
                            if(!$brandsList[$i]['BrandLogo'])
                            {
                                //$brandsList[$i]['BrandLogo'] = "no_logo.png";
                                $brandsList[$i]['BrandLogo'] = false;
                            }    
                            
                            $SelectedBrands[] = $brandsList[$i];
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedBrands)>0)
                {
                    array_multisort($brandPriority, SORT_ASC, $SelectedBrands);
                }
            }  
            else {
                $BrandGroupEnable = 0;
                for($i=0;$i<count($brandsList);$i++)
                {
                    $brandsList[$i]['Exists']   = false;
                    $brandsList[$i]['Priority'] = '';
                }
            }
            

            $ServiceProvidersModel = $this->loadModel('ServiceProviders');
            
            $serviceProvidersList  = $ServiceProvidersModel->getServiceProvidersHaveJobs($this->user);
            
                    
            if(count($PreferredServiceProviders))
            {
                $ServiceProviderGroupEnable = $PreferredServiceProviders[0]['GroupEnable'];
                for($i=0;$i<count($serviceProvidersList);$i++)
                {
                    $serviceProvidersList[$i]['Exists'] = false;
                    $serviceProvidersList[$i]['Priority'] = '';
                    
                    foreach($PreferredServiceProviders as $psp)
                    {
                        if($serviceProvidersList[$i]['ServiceProviderID']==$psp['PreferentialID'])
                        {
                            $serviceProvidersList[$i]['Exists']   = true;
                            $serviceProviderPriority[] = $serviceProvidersList[$i]['Priority'] = $psp['Priority'];
                            
                            if(!isset($serviceProvidersList[$i]['ServiceProviderLogo']) || !$serviceProvidersList[$i]['ServiceProviderLogo'])
                            {
                                $serviceProvidersList[$i]['ServiceProviderLogo'] = "no_logo.png";
                            }    
                            
                            $SelectedServiceProviders[] = $serviceProvidersList[$i];
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedServiceProviders)>0)
                {
                    array_multisort($serviceProviderPriority, SORT_ASC, $SelectedServiceProviders);
                }
            }  
            else {
                $ServiceProviderGroupEnable = 0;
                for($i=0;$i<count($serviceProvidersList);$i++)
                {
                    $serviceProvidersList[$i]['Exists']   = false;
                    $serviceProvidersList[$i]['Priority'] = '';
                }
            }        
            //Network    
            $networksList = $SkylineModel->getNetworks(null,$this->user);
            if(count($PreferredNetworks))
            {
                $NetworkGroupEnable = $PreferredNetworks[0]['GroupEnable'];
                for($i=0;$i<count($networksList);$i++)
                {
                    $networksList[$i]['Exists'] = false;
                    $networksList[$i]['Priority'] = '';
                    foreach($PreferredNetworks as $pb)
                    {
                        if($networksList[$i]['NetworkID']==$pb['PreferentialID'])
                        {
                            $networksList[$i]['Exists']   = true;
                            $networkPriority[] = $networksList[$i]['Priority'] = $pb['Priority'];
                            $SelectedNetworks[] = $networksList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedNetworks)>0)
                {
                    array_multisort($networkPriority, SORT_ASC, $SelectedNetworks);
                }
            }  
            else {
                $NetworkGroupEnable = 0;
                for($i=0;$i<count($networksList);$i++)
                {
                    $networksList[$i]['Exists']   = false;
                    $networksList[$i]['Priority'] = '';
                }
            }
            
            //SkillSets
            $skillSetList = $SkylineModel->getRepairSkills();
            if(count($PreferredSkillset))
            {
                $SkillSetGroupEnable = $PreferredSkillset[0]['GroupEnable'];
                for($i=0;$i<count($skillSetList);$i++)
                {
                    $skillSetList[$i]['Exists'] = false;
                    $skillSetList[$i]['Priority'] = '';
                    foreach($PreferredSkillset as $pss)
                    {
                        if($skillSetList[$i]['RepairSkillID']==$pss['PreferentialID'])
                        {
                            $skillSetList[$i]['Exists']   = true;
                            $skillSetPriority[] = $skillSetList[$i]['Priority'] = $pss['Priority'];
                            $SelectedSkillSet[] = $skillSetList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedSkillSet)>0)
                {
                    array_multisort($skillSetPriority, SORT_ASC, $SelectedSkillSet);
                }
            }  
            else {
                $SkillSetGroupEnable = 0;
                for($i=0;$i<count($skillSetList);$i++)
                {
                    $skillSetList[$i]['Exists']   = false;
                    $skillSetList[$i]['Priority'] = '';
                }
            }
            
            //Unit Types
            $unitTypesList = $SkylineModel->getUnitTypes();
            if(count($PreferredUnitTypes))
            {
                $UnitTypeGroupEnable = $PreferredUnitTypes[0]['GroupEnable'];
                for($i=0;$i<count($unitTypesList);$i++)
                {
                    $unitTypesList[$i]['Exists'] = false;
                    $unitTypesList[$i]['Priority'] = '';
                    foreach($PreferredUnitTypes as $punt)
                    {
                        if($unitTypesList[$i]['UnitTypeID']==$punt['PreferentialID'])
                        {
                            $unitTypesList[$i]['Exists']   = true;
                            $unitTypePriority[] = $unitTypesList[$i]['Priority'] = $punt['Priority'];
                            $SelectedUnitTypes[] = $unitTypesList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedUnitTypes)>0)
                {
                    array_multisort($unitTypePriority, SORT_ASC, $SelectedUnitTypes);
                }
            }  
            else {
                $UnitTypeGroupEnable = 0;
                for($i=0;$i<count($unitTypesList);$i++)
                {
                    $unitTypesList[$i]['Exists']   = false;
                    $unitTypesList[$i]['Priority'] = '';
                }
            }
            
            //Clients
            $clientList = $SkylineModel->getClients($this->user);
            if(count($PreferredClients))
            {
                $ClientGroupEnable = $PreferredClients[0]['GroupEnable'];
                for($i=0;$i<count($clientList);$i++)
                {
                    $clientList[$i]['Exists'] = false;
                    $clientList[$i]['Priority'] = '';
                    foreach($PreferredClients as $clnt)
                    {
                        if($clientList[$i]['ID']==$clnt['PreferentialID'])
                        {
                            $clientList[$i]['Exists']   = true;
                            $clientPriority[] = $clientList[$i]['Priority'] = $clnt['Priority'];
                            $SelectedClient[] = $clientList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedClient)>0)
                {
                    array_multisort($clientPriority, SORT_ASC, $SelectedClient);
                }
            }  
            else {
                $ClientGroupEnable = 0;
                for($i=0;$i<count($clientList);$i++)
                {
                    $clientList[$i]['Exists']   = false;
                    $clientList[$i]['Priority'] = '';
                }
            }
            
            //Branches
            $branchesList = $SkylineModel->getBranches($this->user->BranchID, '', $this->user->ClientID, $this->user->NetworkID);
            if(count($PreferredBranches))
            {
                $BranchGroupEnable = $PreferredBranches[0]['GroupEnable'];
                for($i=0;$i<count($branchesList);$i++)
                {
                    $branchesList[$i]['Exists'] = false;
                    $branchesList[$i]['Priority'] = '';
                    foreach($PreferredBranches as $pbrnch)
                    {
                        if($branchesList[$i]['BranchID']==$pbrnch['PreferentialID'])
                        {
                            $branchesList[$i]['Exists']   = true;
                            $branchPriority[] = $branchesList[$i]['Priority'] = $pbrnch['Priority'];
                            $SelectedBranches[] = $branchesList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedBranches)>0)
                {
                    array_multisort($branchPriority, SORT_ASC, $SelectedBranches);
                }
            }  
            else {
                $BranchGroupEnable = 0;
                for($i=0;$i<count($branchesList);$i++)
                {
                    $branchesList[$i]['Exists']   = false;
                    $branchesList[$i]['Priority'] = '';
                }
            }       
            

            
            $this->smarty->assign('manufacturerList', $manufacturerList);
            $this->smarty->assign('brandsList', $brandsList);
            $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
            $this->smarty->assign('networkList', $networksList);
            $this->smarty->assign('branchList', $branchesList);
            $this->smarty->assign('clientList', $clientList);
            $this->smarty->assign('unitTypesList', $unitTypesList);
            $this->smarty->assign('skillSetList', $skillSetList);
            
            
            $this->smarty->assign('SelectedBrands', $SelectedBrands);
            $this->smarty->assign('SelectedManufacturers', $SelectedManufacturers);
            $this->smarty->assign('SelectedServiceProviders', $SelectedServiceProviders);
            $this->smarty->assign('SelectedNetworks', $SelectedNetworks);
            $this->smarty->assign('SelectedBranch', $SelectedBranches);
            $this->smarty->assign('SelectedClient', $SelectedClient);
            $this->smarty->assign('SelectedUnitType', $SelectedUnitTypes);
            $this->smarty->assign('SelectedSkillSet', $SelectedSkillSet);
            
            
            if($args['unitType'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedUnitTypes as $sb)
                {    
                    if($sb['UnitTypeID']==$args['unitType'])
                    {
                        $selectedStatusClient .= " ".$sb['UnitTypeName'];
                    }
                }    
            }
            
            if($args['skillSet'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedSkillSet as $sb)
                {    
                    if($sb['RepairSkillID']==$args['skillSet'])
                    {
                        $selectedStatusClient .= " ".$sb['RepairSkillName'];
                    }
                }    
            }
            
            if($args['client'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedClient as $sb)
                {    
                    if($sb['ID']==$args['client'])
                    {
                        $selectedStatusClient .= " ".$sb['CompanyName'];
                    }
                }    
            }
            
            if($args['branch'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedBranches as $sb)
                {    
                    if($sb['BranchID']==$args['branch'])
                    {
                        $selectedStatusClient .= " ".$sb['BranchName'];
                    }
                }    
            }
            
            
            if($args['network'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedNetworks as $sb)
                {    
                    if($sb['NetworkID']==$args['network'])
                    {
                        $selectedStatusClient .= " ".$sb['CompanyName'];
                    }
                }    
            }
            
            
            if($args['brand'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedBrands as $sb)
                {    
                    if($sb['BrandID']==$args['brand'])
                    {
                        $selectedStatusClient .= " ".$sb['BrandName'];
                    }
                }    
            }
            
            if($args['manufacturer'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedManufacturers as $sm)
                {    
                    if($sm['ManufacturerID']==$args['manufacturer'])
                    {
                        $selectedStatusClient .= " ".$sm['ManufacturerName'];
                    }
                }    
            }
            
            
            if($args['serviceProvider'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedServiceProviders as $ss)
                {    
                    if($ss['ServiceProviderID']==$args['serviceProvider'])
                    {
                        $selectedStatusClient .= " ".$ss['CompanyName'];
                    }
                }    
            }
            
            $SelectedBrandsCnt           = count($SelectedBrands);
            $SelectedManufacturersCnt    = count($SelectedManufacturers);
            $SelectedServiceProvidersCnt = count($SelectedServiceProviders);
            $SelectedNetworksCnt         = count($SelectedNetworks);
            $SelectedBranchesCnt         = count($SelectedBranches);
            $SelectedClientsCnt          = count($SelectedClient);
            $SelectedUnitTypesCnt        = count($SelectedUnitTypes);
            $SelectedSkillSetCnt        = count($SelectedSkillSet);
            
            $contentWidth              = $SelectedBrandsCnt;
            
            if($contentWidth<$SelectedManufacturersCnt)
            {
                $contentWidth = $SelectedManufacturersCnt;
            }    
            if($contentWidth<$SelectedServiceProvidersCnt)
            {
                $contentWidth = $SelectedServiceProvidersCnt;
            }
            if($contentWidth<$SelectedNetworksCnt)
            {
                $contentWidth = $SelectedNetworksCnt;
            }
            if($contentWidth<$SelectedBranchesCnt)
            {
                $contentWidth = $SelectedBranchesCnt;
            }
            if($contentWidth<$SelectedClientsCnt)
            {
                $contentWidth = $SelectedClientsCnt;
            }
            if($contentWidth<$SelectedUnitTypesCnt)
            {
                $contentWidth = $SelectedUnitTypesCnt;
            }
            if($contentWidth<$SelectedSkillSetCnt)
            {
                $contentWidth = $SelectedSkillSetCnt;
            }
            
            $contentWidth = $contentWidth*110;
            
            
            if($contentWidth<850)
            {
                $contentWidth = 850;
            }
            
            $this->smarty->assign('contentWidth', $contentWidth);
            
           
            
           // $this->log($UserStatusPreferences);
            
            if(count($UserStatusPreferences))
            {
                for($i=0;$i<count($jobStatusList);$i++)
                {
                    $jobStatusList[$i]['Exists'] = false;
                    
                    foreach($UserStatusPreferences as $usf)
                    {
                        if(in_array($jobStatusList[$i]['StatusID'], $usf))
                        {
                            $jobStatusList[$i]['Exists'] = true;
                            break;
                        }
                    }
                    
                }
            }  
            else {
                
                for($i=0;$i<count($jobStatusList);$i++)
                {
                    $jobStatusList[$i]['Exists'] = true;
                }
            }
            
             $this->smarty->assign('jobStatusList', $jobStatusList);

             
	     if($this->user->ClientID) {
		$clientName = $SkylineModel->getClientName($this->user->ClientID);
		if(stripos($clientName,"loewe") !== false) {
		    $this->smarty->assign("hideLogos",true);
		}
	     }

            $graph_title = $totalRows." ".$page['Text']['total_open_jobs'].$selectedStatusClient; 
            
            $this->smarty->assign('graph_title', $graph_title); 
            
            
            $this->smarty->assign('ServiceProviderGroupEnable', $ServiceProviderGroupEnable); 
            $this->smarty->assign('BrandGroupEnable', $BrandGroupEnable); 
            $this->smarty->assign('ManufacturerGroupEnable', $ManufacturerGroupEnable); 
            $this->smarty->assign('NetworkGroupEnable', $NetworkGroupEnable); 
            $this->smarty->assign('BranchGroupEnable', $BranchGroupEnable); 
            $this->smarty->assign('ClientGroupEnable', $ClientGroupEnable); 
            $this->smarty->assign('UnitTypeGroupEnable', $UnitTypeGroupEnable); 
            $this->smarty->assign('SkillSetGroupEnable', $SkillSetGroupEnable); 
             
            
             $this->smarty->assign('hideNonSbJobs', isset($this->session->hideNonSbJobs)?$this->session->hideNonSbJobs:""); 
	     
             if($args['ojvBy'] == "d"){
            $this->smarty->display('openJobs.tpl'); 
             }
             else{
                 $this->smarty->display('openJobs_summary.tpl'); 
             }
            return;
     } 
     
     
     
    /**
    * Description
    * 
    * This method is for to display ra jobs.
    * 
    * @param array $args Its an associative array contains ra type, ra status etc.
    * @return void   
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     
    public function raJobsAction($args) { 
         
	$Job = $this->loadModel("Job");

	$page = $this->messages->getPage("raJobs", $this->lang);

	$SystemStatusesModel = $this->loadModel("SystemStatuses");
	$UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences("raJobs", "ra");

	$Skyline = $this->loadModel("Skyline");

	$AuthorisationTypes = $Skyline->getAuthorisationTypes();

	$RAStatusTypes = $this->loadModel("RAStatusTypes");
	$RAStatusTypesList = $RAStatusTypes->getRAStatusTypes(false, false, false, false, true);

	if (!isset($args["atType"]) && isset($_SESSION["SessionATType"]) && $_SESSION["SessionATType"]) {
	    $args["atType"] = $_SESSION["SessionATType"];
	}

	$args["atType"] = isset($args["atType"]) ? $args["atType"] : 0;
	$args["sType"] = isset($args["sType"]) ? $args["sType"] : 0;
	$args["NewRAStatus"] = isset($args["NewRAStatus"]) ? $args["NewRAStatus"] : 0;

	$args["RATypes"] = $AuthorisationTypes;

	$_SESSION["SessionATType"] = $args["atType"];

	$statsResult = $Job->fetchAllRAStats($args);
	
	for ($r = 0; $r < count($statsResult); $r++) {
	    $statsResult[$r][4] = $statsResult[$r][0];
	    $statsResult[$r][0] = $statsResult[$r][0] . " (" . $statsResult[$r][1] . ")";
	}

	$totalRows = 0;
	$selectedStatusRAType = "";
	for ($r = 0; $r < count($statsResult); $r++) {
	    if ($args["sType"] && $args["sType"] == $statsResult[$r][2]) {
		$totalRows = $statsResult[$r][1];
		$selectedStatusRAType = " - " . $statsResult[$r][4];
		break;
	    } else {
		$totalRows += $statsResult[$r][1];
	    }
	}

	if ($args["atType"]) {
	    foreach ($AuthorisationTypes AS $atKey=>$atValue) {
		if ($args["atType"] == $atValue["Name"]) {
		    $selectedStatusRAType = " - " . $atValue["Value"] . " " . $selectedStatusRAType;
		    break;
		}    
	    }  
	} 
        
        
        ///table display preferences ses vars
              
        $this->session->mainTable="job";
        $this->session->mainPage="raJobs";
        $this->page =  $page;
		
		$datatable= $this->loadModel('DataTable');
            $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID']);
        
        
       
        $keys=$keys_data[1];
        
        $this->smarty->assign('data_keys', $keys);
        
        
	$this->smarty->assign("page", $page);

	$this->smarty->assign("atType", $args["atType"]);
	$this->smarty->assign("sType", $args["sType"]);

	$graph_title = $totalRows . " " . $page["Text"]["total_ra_jobs"] . $selectedStatusRAType; 

	$this->smarty->assign("graph_title", $graph_title);

	$this->smarty->assign("statsResult", $statsResult);
	$this->smarty->assign("AuthorisationTypes", $AuthorisationTypes);
	$this->smarty->assign("totalRows", $totalRows);

	$this->smarty->assign("NewRAStatus", $args["NewRAStatus"]);

	if (count($UserStatusPreferences)) {
	    for ($i = 0; $i < count($RAStatusTypesList); $i++) {
		$RAStatusTypesList[$i]["Exists"] = false;

		foreach ($UserStatusPreferences as $usf) {
		    if (in_array($RAStatusTypesList[$i]["RAStatusID"], $usf)) {
			$RAStatusTypesList[$i]["Exists"] = true;
			break;
		    }
		}

	    }
	} else {
	    for ($i = 0; $i < count($RAStatusTypesList); $i++) {
		$RAStatusTypesList[$i]["Exists"] = false;
	    }
	}

	$this->smarty->assign("RAStatusTypesList", $RAStatusTypesList);

	$RAStatusTypesSubList = $RAStatusTypes->getRAStatusTypes($args["sType"]);

	$this->smarty->assign("RAStatusTypesSubList", $RAStatusTypesSubList);

	$this->smarty->display("raJobs.tpl"); 

	return;
	
     }
     
    /**
    * Description
    * 
    * This method is for to display a wall board details all jobs booked per network.
    * 
    * @param array $args Its an associative array contains ra type, ra status etc.

    * @return void   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     
    public function bookingPerformanceAction($args) { 
         
        if($this->user->SuperAdmin || $this->user->NetworkID || $this->user->ClientID || $this->user->BranchID)
        {
                //$JobModel     = $this->loadModel('Job');
                $SkylineModel = $this->loadModel('Skyline');
                
                $BranchID  = (isset($args['BranchID']))?$args['BranchID']:false;
                $ClientID  = (isset($args['ClientID']))?$args['ClientID']:false;
                $NetworkID = (isset($args['NetworkID']))?$args['NetworkID']:false;
                $BrandID  = (isset($args['BrandID']))?$args['BrandID']:false;
                
                $cvt  = (isset($args['cvt']))?$args['cvt']:'mtd';
                $cdt  = (isset($args['cdt']))?$args['cdt']:'d';
                $dispAll  = (isset($args['dispAll']))?$args['dispAll']:'';
                $dispCancel  = (isset($args['displayCancelled']))?$args['displayCancelled']:'';     //Added for Include Cancelled Jobs functionality @author Soubhik
                
                if($cvt=='dr')
                {    
                    $date_from  = (isset($args['date_from']))?$args['date_from']:'';
                    $date_to    = (isset($args['date_to']))?$args['date_to']:'';
                    
                    if($date_from>$date_to)
                    {
                        $temp_var  = $date_from;
                        $date_from = $date_to;
                        $date_to   = $temp_var;
                        
                    }    
                    
                    if($date_from)
                    {
                        $date_from = date("d/m/Y", strtotime($date_from));
                    } 
                    
                    if($date_to)
                    {
                        $date_to = date("d/m/Y", strtotime($date_to));
                    }
                    
                    if(!$date_from || !$date_to)
                    {
                        $date_from = date("01/m/Y", strtotime("-3 month") ) ;
                        $date_to  = date('d/m/Y');
                    }
                }
                else
                {
                   $date_from = date("01/m/Y", strtotime("-3 month") ) ;
                   $date_to  = date('d/m/Y');
                }
                
                $SuperAdmin  = false;
                $NetworkUser = false;
                $ClientUser  = false;
                $BranchUser  = false;
                
                $NetworkList = $ClientList = $BranchList = $BrandList = array();
                
                
                if($this->user->BranchID)
                {
                    $BranchID = $this->user->BranchID;
                    $ClientID = $this->user->ClientID;
                    $NetworkID = $this->user->NetworkID;
                     
                    $BranchUser = true;
                }    
                else if($this->user->ClientID)
                {
                    $ClientID = $this->user->ClientID;
                    $NetworkID = $this->user->NetworkID;
                    
                    $ClientUser = true;
                } 
                else if($this->user->NetworkID)
                {
                    $NetworkID = $this->user->NetworkID;
                    $NetworkUser = true;
                }
                else if($this->user->SuperAdmin)
                {
                    $SuperAdmin  = true;
                    $NetworkList = $SkylineModel->getNetworks();
                   
                }
                
                if($NetworkID)
                {
                    $ClientList  = $SkylineModel->getNetworkClients($NetworkID);
                } 
                
                
                if($ClientID && $NetworkID)
                {
                     $BranchList  = $SkylineModel->getBranches(null, null, $ClientID, $NetworkID);
                    
                }
                
                
                $BrandList  = $SkylineModel->getBrands($NetworkID, $ClientID, $BranchID);
                
                
                $this->smarty->assign('NetworkList', $NetworkList);
                $this->smarty->assign('ClientList', $ClientList);
                $this->smarty->assign('BranchList', $BranchList);
                $this->smarty->assign('BrandList', $BrandList);
            
                $page = $this->messages->getPage('bookingPerformance', $this->lang);


                $this->smarty->assign('NetworkID', $NetworkID);
                $this->smarty->assign('BranchID', $BranchID);
                $this->smarty->assign('ClientID', $ClientID);
                $this->smarty->assign('BrandID', $BrandID);

                $this->smarty->assign('SuperAdmin', $SuperAdmin);
                $this->smarty->assign('NetworkUser', $NetworkUser);
                $this->smarty->assign('ClientUser', $ClientUser);
                $this->smarty->assign('BranchUser', $BranchUser);


                
                
                
                $this->smarty->assign('cvt', $cvt);
                $this->smarty->assign('cdt', $cdt);
                $this->smarty->assign('date_from', $date_from);
                $this->smarty->assign('date_to', $date_to);
                $this->smarty->assign('dispAll', $dispAll);
                $this->smarty->assign('displayCancelled', $dispCancel);

                
                $this->smarty->assign('page', $page);

                
                $JobModel    = $this->loadModel('Job');
                
                $dataArgs = array(
                    
                  'NetworkID'=>$NetworkID,  
                  'BranchID'=>$BranchID,
                  'ClientID'=>$ClientID,
                  'BrandID'=>$BrandID,
                  'cvt'=>$cvt,  
                  'cdt'=>$cdt,  
                  'date_from'=>$date_from,
                  'date_to'=>$date_to,
                  'dispAll'=>$dispAll,
                  'displayCancelled'=>$dispCancel
                );
                /*
                 * Code for System Status Preferences
                 * @Author Soubhik
                 */
                $SystemStatusesModel   = $this->loadModel('SystemStatuses');
                $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('bookingPerformance', 'js');
                $jobStatusList = $SkylineModel->getSystemStatus();
                if(count($UserStatusPreferences))
                {
                    for($i=0;$i<count($jobStatusList);$i++)
                    {
                        $jobStatusList[$i]['Exists'] = false;
                        foreach($UserStatusPreferences as $usf)
                        {
                            if(in_array($jobStatusList[$i]['StatusID'], $usf))
                            {
                                $jobStatusList[$i]['Exists'] = true;
                                break;
                            }
                        }

                    }
                }  
                else {

                    for($i=0;$i<count($jobStatusList);$i++)
                    {
                        $jobStatusList[$i]['Exists'] = true;
                    }
                }
                $UserStatusPreferencesList = "";
                foreach($UserStatusPreferences as $usf)
                {
                    $UserStatusPreferencesList .=  $usf['StatusID'].",";
                }
                $this->smarty->assign('jobStatusList', $jobStatusList);
                
                /*
                 * End Code for System Status Preferences
                 * @Author Soubhik
                 */
                $statsResult = $JobModel->bookingPerformanceStats($dataArgs, $UserStatusPreferencesList);   //Added parameter $UserStatusPreferencesList to pass Status Preference List
                
                $chartJobsTotal = 0;
                foreach($statsResult as $sr)
                {
                    $chartJobsTotal += $sr[1];
                } 
                
                if($chartJobsTotal==1)
                {
                    $bottomText = $page['Text']['job'];
                }    
                else
                {
                    $bottomText = $page['Text']['jobs'];
                }
                
                $graph_title = '';
                
                if($cvt=="dr")
                {
                    $graph_title = $chartJobsTotal.' '.$bottomText;
                }
                else if($cvt=="mtd")
                {
                    $graph_title = $page['Text']['mtd']." - ".$chartJobsTotal.' '.$bottomText;
                }
                else if($cvt=="lm")
                {
                    $graph_title = $page['Text']['last_month']." - ".$chartJobsTotal.' '.$bottomText;
                }
                else if($cvt=="ytd")
                {
                    $graph_title = $page['Text']['ytd']." - ".$chartJobsTotal.' '.$bottomText;
                }
                
                $this->smarty->assign('statsResult', $statsResult);
                $this->smarty->assign('graph_title', $graph_title);
                $this->smarty->display('bookingPerformance.tpl'); 

                return;
        }
        else
        {
             $this->redirect('index'); 
        }
     }
     
     
     
     
     
     /**
     * Description
     * 
     * This method is for to change RA status of tagged jobs list..
     * 
     * @param array $args Its an associative array contains current ra status, new ra status, jobs list etc.
     * @return Json   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function updateRAJobsAction($args) {
        
	//$this->log("POST: " . var_export($_POST, true));
	 
	$result = "OK";

	$taggedJobs = isset($_POST['RAJobsTagged']) ? $_POST['RAJobsTagged'] : [];
	$RAType = (isset($_POST['atType_hidden']) && $_POST['atType_hidden'] && $_POST['atType_hidden'] != '') ? $_POST['atType_hidden'] : NULL;
	$sltRAJobsListStr = isset($_POST['sltRAJobsList']) ? $_POST['sltRAJobsList'] : '';
	$sltRAJobsList = explode(",", $sltRAJobsListStr);  
	$authNo = isset($_POST["authNo"]) ? $_POST["authNo"] : null;
	
	$CurrentRAStatus = isset($_POST['CurrentRAStatus']) ? $_POST['CurrentRAStatus'] : false;
	$NewRAStatus = isset($_POST['NewRAStatus']) ? $_POST['NewRAStatus'] : false;

	if(!is_array($taggedJobs)) {
	    $taggedJobs = [];
	}    
        
        $RAStatusTypes = $this->loadModel('RAStatusTypes');
	$jobModel = $this->loadModel('Job');

	$sysRAStatus = $RAStatusTypes->getPrimaryRAStatus($_POST["NewRAStatus"]);

	
	if($NewRAStatus) {
	    
	    $RAStatusTypes = $this->loadModel('RAStatusTypes');

	    foreach($sltRAJobsList as $JobID) {    
            
		if(in_array($JobID, $taggedJobs)) {
                
		    $updateResult = $jobModel->update([
			"RAStatusID" => $NewRAStatus, 
			"JobID" => $JobID, 
			"AuthorisationNo" => $authNo
		    ]);

		    if($updateResult['status'] == 'FAIL') {
			$result = 'FAIL';
		    } else {
                            
			if($RAType && $RAType != '') {   
                                
			    $CSName = $RAStatusTypes->fetchRow(["RAStatusID" => $CurrentRAStatus]); 
                                
			    $NSName = $RAStatusTypes->fetchRow(["RAStatusID" => $NewRAStatus]);
                                
                            $RAStatusTypes->recordRAHisotry([
				"JobID"=>$JobID,
				"RAType"=>$RAType,
				"RAStatusID"=>$NewRAStatus,
				"Notes"=>$_POST['Notes']
			    ]);
			}
			
		    }
                        
		}   
                      
	    }
	    
	} else {
	    
	    $result = 'FAIL';
	    
        }
	
	if($sysRAStatus && $sysRAStatus["RAStatusName"] == "Authorisation Accepted") {
	    foreach($taggedJobs as $jobID) {
		$jobModel->processRAAccepted($jobID);
	    }
	}
	if($sysRAStatus && $sysRAStatus["RAStatusName"] == "Authorisation Declined") {
	    foreach($taggedJobs as $jobID) {
		$jobModel->authRejected($jobID, $_POST['Notes']);
	    }
	}
          
	echo json_encode($result);
          
    }
      
      
      
    /**
    * Description
    * This method is for to change RA type and status of given job.
    * @param array $args Its an associative array contains job id.
    * @return JSON   
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     
    public function updateRAJobTypeAction($args) {
        
	$result = "OK";
        
        $JobID = isset($args[0]) ? $args[0] : false;
	
	$RAStatusID = isset($_POST["RAStatusID"]) ? $_POST["RAStatusID"] : null;
	$RAType = isset($_POST["ATType"]) ? $_POST["ATType"] : null;
	$OldStatusID = isset($_POST["OldStatusID"]) ? $_POST["OldStatusID"] : null;
        $authNo = isset($_POST["authNo"]) ? $_POST["authNo"] : null;
	
	
	if ($JobID) {
	    
	    $jobModel = $this->loadModel("Job");
	    $RAStatusTypes = $this->loadModel("RAStatusTypes");
	    
	    $job = $jobModel->fetch($JobID);
	    
	    if ($RAType == null) {   //If the RA Type is null then we are also updating status id to null
		$RAStatusID = null;
	    }    
	    
            $updateResult = $jobModel->update([  
		"RAStatusID" => $RAStatusID, 
		"RAType" => $RAType, 
		"JobID" => $JobID, 
		"AuthorisationNo" => $authNo
	    ]);
	    
	    if ($updateResult["status"] == "FAIL") {
		
		$result = "FAIL";
		if ($this->debug) $this->log("FAIL");
		
	    } else {
		
		if ($RAStatusID != null && $RAType != null) { 
		    
		    $fileName = null;
		    
		    if (isset($_FILES["raImage"])) {
			if ($_FILES["raImage"]["type"] == "image/jpeg" || $_FILES["raImage"]["type"] == "image/png" || $_FILES["raImage"]["type"] == "image/gif") {
			    $temp = explode(".", $_FILES["raImage"]["name"]);
			    end($temp);
			    $ext = $temp[key($temp)];
			    $fileHash = md5(microtime() . rand(1, 10000));
			    $fileName = $fileHash . "." . $ext;
			}
		    }		    
		    
		    $CSName = $RAStatusTypes->fetchRow(["RAStatusID" => $OldStatusID]); 
		    $NSName = $RAStatusTypes->fetchRow(["RAStatusID" => $RAStatusID]);
                    $RAStatusTypes->recordRAHisotry([
			"JobID" => $JobID,
			"RAType" => $RAType,
			"RAStatusID" => $RAStatusID,
			"Notes" => $_POST["RANotes"],
			"Image" => $fileName
		    ]);
		    $jobUpdateModel = $this->loadModel("JobUpdate");
		    
		    $id = $jobUpdateModel->getLastInsertID();
		    if ($id != 0) {
			if (isset($_FILES["raImage"])) {
			    if ($_FILES["raImage"]["type"] == "image/jpeg" || $_FILES["raImage"]["type"] == "image/png" || $_FILES["raImage"]["type"] == "image/gif") {
				move_uploaded_file($_FILES["raImage"]["tmp_name"], "images/ra/" . $fileName);
			    }
			}
		    }
		    
		}
		
            }
	    
	    $raStatusModel = $this->loadModel("RAStatusTypes");
	    $raStatus = $raStatusModel->getPrimaryRAStatus($RAStatusID)["RAStatusName"];
	    
	    //RA Status changed to Authorisation Accepted	    
	    if ($raStatus == "Authorisation Accepted") {
		$jobModel->processRAAccepted($JobID);
	    }
	    
	    //RA Status changed to Authorisation Declined
	    if ($raStatus == "Authorisation Declined") {
                $jobModel->authRejected($JobID, $_POST['RANotes']);
	    }
	    
	    //RA Status changed to Query Answered
	    if ($raStatus == "Query Answered") {
		$emailModel = $this->loadModel("Email");
		$manufacturerModel = $this->loadModel("Manufacturers");
		$manufacturerEmail = $manufacturerModel->getManufacturer($job["ManufacturerID"])["AuthorisationManagerEmail"];
		
                $job = $jobModel->fetch($JobID);
		$email = $emailModel->fetchRow(["EmailCode" => "ra_query_answered_manufacturer"]);
		$email["toEmail"] = $manufacturerEmail;
		$email["fromName"] = "Skyline";
		$email["fromEmail"] = "no-reply@skylinecms.co.uk";
		$email["replyEmail"] = false;
		$email["replyName"] = false;
		$email["ccEmails"] = false;
		$emailModel->processEmail($email, $job);
                $emailModel->sendAuthorisationEmail($JobID, "query_raised_against_authorisation");
	    }
	    
	    //Post the update to Service Base
	    $simpleAPIModel = $this->loadModel("SimpleAPI");
	    $simpleAPIModel->updateSBRA($job);
	
	} else {

	    $result = "FAIL";
	    
        }

        echo json_encode($result);
          
    }
    
    
    
    /**
    * Description
    * 
    * This method is for to update collection address 
    * 
    * @param array $args Its an associative array contains job id.

    * @return Json   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     
    public function updateColAddressAction(  /*$args*/  ) {
          
            $result = "OK";
          
            $JobID      = isset($_POST['JobID'])?$_POST['JobID']:false;
            $CustomerID = isset($_POST['CustomerID'])?$_POST['CustomerID']:false;
            
            
            if($JobID && $CustomerID)
            {
                
                
                $skyline = $this->loadModel('Skyline');
                $audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::CUSTOMER_ADDRESS);


                // Create Audit Record
                $audit = $this->loadModel('Audit');
                $audit->create (array( 'JobID' => $JobID,
                                       'AuditTrailActionID' => $audit_actionID,
                                       'Description' => 'Collection address has been changed.') );
                
                
                
                $customer = $this->loadModel('Customer');

                $cust_data = array(
                               
                               'ContactEmail' => $_POST['ContactEmail'],
                               'ContactHomePhone' => $_POST['ContactHomePhone'], 
                               'ContactWorkPhoneExt' => $_POST['ContactHomePhoneExt'], // Note: Home phone BUT work phone Extension !!!!!!
                               'ContactMobile' => $_POST['ContactMobile']
                               
                                );


                  $customer->updateByID($cust_data, $CustomerID);
                  
                  
                  
                  
                 $Job = $this->loadModel('Job');

                 $job_data = array(
                               
                                'ColAddCompanyName' => $_POST['ColAddCompanyName'],
                                'ColAddBuildingNameNumber' => $_POST['ColAddBuildingNameNumber'],
                                'ColAddStreet' => (isset($_POST['ColAddStreet']) && $_POST['ColAddStreet']!='') ? $_POST['ColAddStreet'] : NULL,
                                'ColAddLocalArea' => isset($_POST['ColAddLocalArea']) ? $_POST['ColAddLocalArea'] : NULL,
                                'ColAddTownCity' => isset($_POST['ColAddCity']) ? $_POST['ColAddCity'] : NULL,
                                'ColAddCountyID' => (isset($_POST['ColAddCountyID']) && $_POST['ColAddCountyID']!='') ? $_POST['ColAddCountyID'] : NULL,
                                'ColAddCountryID' => (isset($_POST['ColAddCountryID']) && $_POST['ColAddCountryID']!='') ? $_POST['ColAddCountryID'] : NULL,
                                'ColAddPostcode' => isset($_POST['ColAddPostcode']) ? strtoupper($_POST['ColAddPostcode']) : NULL,
                                "JobID"=>$JobID
                                
                            );


                $Job->update($job_data);
                
                
                
            }
            else 
            {
                $result  = 'FAIL';
            }

          echo json_encode( $result );
          
      }
      
      
      
      
        /**
     * Description
     * 
     * This method is for to update collection date and courier details.
     * 
     * @param array $args Its an associative array contains job id.
    
     * @return Json   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
      public function updateJobCourierPreferenceAction(  /*$args*/  ) {
          
            $result = "OK";
          
            $JobID           = ($_POST['JobID']!='')?$_POST['JobID']:false;
            $CollectionDate  = ($_POST['CollectionDate']!='')?$_POST['CollectionDate']:false;
            $CourierID       = ($_POST['CourierID']!='')?$_POST['CourierID']:false;
            
            if($CollectionDate)
            {
               $CollectionDateArray =  explode("/", $CollectionDate);
                if(count($CollectionDateArray)==3)
                {
                    $CollectionDate = $CollectionDateArray[2]."-".$CollectionDateArray[1]."-".$CollectionDateArray[0];
                }   
                else
                {
                    $CollectionDate = false;
                }
            }
            
            if($JobID && ($CollectionDate || $CourierID))
            {
                
                
                $skyline = $this->loadModel('Skyline');
                $audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::ADDITIONAL_INFO);


                // Create Audit Record
                $audit = $this->loadModel('Audit');
                $audit->create (array( 'JobID' => $JobID,
                                       'AuditTrailActionID' => $audit_actionID,
                                       'Description' => 'Collection address has been changed.') );
                
                
                  
                 $Job = $this->loadModel('Job');

                 $job_data = array(
                               
                                'CollectionDate' => ($CollectionDate) ? $CollectionDate : NULL,
                                'CourierID' => ($CourierID) ? $CourierID : NULL,
                                "JobID"=>$JobID
                                
                            );


                $Job->update($job_data);
                
                
                
            }
            else 
            {
                $result  = 'FAIL';
            }

          echo json_encode( $result );
          
      }
      
      
      
      
      
      
     
      public function overdueJobsAction($args) { 
        
         //---unset session values when click on show all----------------------------
         if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='s'){
             unset($_SESSION['overduejob']['summary']);
         }
         else if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='d'){
             unset($_SESSION['overduejob']['detail']);
         }
         else if(isset($args[0]) && strtolower($args[0]) == "showall" ){
             unset($_SESSION['overduejob']);
         }
          
             /// // $Skyline = $this->loadModel('Skyline');
        $Job = $this->loadModel('Job');
        $users_model = $this->loadModel('Users');
            
        $page    = $this->messages->getPage('overdueJobs', $this->lang);
            
        //$args['ojvBy'] = isset($args['ojvBy']) ? $args['ojvBy'] : 's';
        //store TAT View in Session
        $args['ojvBy'] = isset($args['ojvBy']) ? $args['ojvBy'] : (isset($_SESSION['overduejob']['ojvBy'])?$_SESSION['overduejob']['ojvBy']: 's');
        $_SESSION['overduejob']['ojvBy'] = $args['ojvBy'];
            
            
        $this->smarty->assign('AP9000', Functions::hasPermission($this->user, 'AP9000'));     /* Servicebase Refresh */
        $this->smarty->assign('AP9001', Functions::hasPermission($this->user, 'AP9001'));     /* Servicebase Bulk Refresh */
            
            ///table display preferences ses vars
              
        $this->session->mainTable="job";
        $this->session->mainPage="overdueJobs";
        $this->page =  $page;

        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'OverdueJobs'));
      
        if($args['ojvBy'] == "s"){
            $keys_data = $Job->getSummaryFields($page,$TatResult);
        }
        else{
        $datatable= $this->loadModel('DataTable');
        $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID']);
        }

        if(isset($keys_data[1])){
        $keys=  array_values($keys_data[1]);
        
        }else{
        $keys=array();
        }
//      echo"<pre>";
//      print_r($keys);
//      echo"</pre>";
        $this->smarty->assign('data_keys', $keys);
               ////table display preferences ses vars end
                   
            //$Skyline = $this->loadModel('Skyline');
          
           //$EmsoapModel = $this->loadModel('Emsoap');
           // $EmsoapModel->sendSMS(array());
        
            $Job = $this->loadModel('Job');
            
	    $this->smarty->assign("ref", "overdueJobs");
	    
            $SystemStatusesModel   = $this->loadModel('SystemStatuses');
            $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('overdueJobs', 'js');
            
            $args['tatType']         = isset($args['tatType']) ? $args['tatType'] : 'j';
          
//            $args['network']         = isset($args['network']) ? $args['network'] : 0;
//            $args['branch']          = isset($args['branch']) ? $args['branch'] : 0; 
//            $args['client']          = isset($args['client']) ? $args['client'] : 0;
//            $args['unitType']        = isset($args['unitType']) ? $args['unitType'] : 0;
//            $args['skillSet']        = isset($args['skillSet']) ? $args['skillSet'] : 0;
//                        
//            $args['brand']           = isset($args['brand']) ? $args['brand'] : 0;
//            $args['manufacturer']    = isset($args['manufacturer']) ? $args['manufacturer'] : 0;
//            $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : 0;
//            $args['sType']           = isset($args['sType']) ? $args['sType'] : 0;
//            $args['scType']          = isset($args['scType']) ? $args['scType'] : 0;
//            $args['ojBy']            = isset($args['ojBy']) ? $args['ojBy'] : 'sp';
//            $args['btnName']         = isset($args['btnName']) ? $args['btnName'] : false;
//            $args['btnValue']        = isset($args['btnValue']) ? $args['btnValue'] : false;
//            $args['btnValue2']       = isset($args['btnValue2']) ? $args['btnValue2'] : false;
            
             //----------store filter value in session----------------------------------
            
             if($args['ojvBy'] == "s"){
                 $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['overduejob']['summary']['network'])?$_SESSION['overduejob']['summary']['network']: 0);
                $_SESSION['overduejob']['summary']['network'] = $args['network'];

                $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['overduejob']['summary']['branch'])?$_SESSION['overduejob']['summary']['branch']: 0);
                $_SESSION['overduejob']['summary']['branch'] = $args['branch'];

                $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['overduejob']['summary']['client'])?$_SESSION['overduejob']['summary']['client']: 0);
                $_SESSION['overduejob']['summary']['client'] = $args['client'];

                $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['overduejob']['summary']['unitType'])?$_SESSION['overduejob']['summary']['unitType']: 0);
                $_SESSION['overduejob']['summary']['unitType'] = $args['unitType'];

                $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['overduejob']['summary']['skillSet'])?$_SESSION['overduejob']['summary']['skillSet']: 0);
                $_SESSION['overduejob']['summary']['skillSet'] = $args['skillSet'];

                $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['overduejob']['summary']['brand'])?$_SESSION['overduejob']['summary']['brand']: 0);
                $_SESSION['overduejob']['summary']['brand'] = $args['brand'];

                $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['overduejob']['summary']['manufacturer'])?$_SESSION['overduejob']['summary']['manufacturer']: 0);
                $_SESSION['overduejob']['summary']['manufacturer'] = $args['manufacturer'];

                $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['overduejob']['summary']['serviceProvider'])?$_SESSION['overduejob']['summary']['serviceProvider']: 0);
                $_SESSION['overduejob']['summary']['serviceProvider'] = $args['serviceProvider'];

                $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['overduejob']['summary']['sType'])?$_SESSION['overduejob']['summary']['sType']: 0);
                $_SESSION['overduejob']['summary']['sType'] = $args['sType'];

                $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['overduejob']['summary']['scType'])?$_SESSION['overduejob']['summary']['scType']: 0);
                $_SESSION['overduejob']['summary']['scType'] = $args['scType'];

                $args['ojBy'] = isset($args['ojBy']) ? $args['ojBy'] : (isset($_SESSION['overduejob']['summary']['ojBy'])?$_SESSION['overduejob']['summary']['ojBy']: 'sp');
                $_SESSION['overduejob']['summary']['ojBy'] = $args['ojBy'];

                $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['overduejob']['summary']['btnName'])?$_SESSION['overduejob']['summary']['btnName']: false);
                $_SESSION['overduejob']['summary']['btnName'] = $args['btnName'];

                $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['overduejob']['summary']['btnValue'])?$_SESSION['overduejob']['summary']['btnValue']: false);
                $_SESSION['overduejob']['summary']['btnValue'] = $args['btnValue'];

                $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['overduejob']['summary']['btnValue2'])?$_SESSION['overduejob']['summary']['btnValue2']: false);
                $_SESSION['overduejob']['summary']['btnValue2'] = $args['btnValue2'];
             }
             else{
                 $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['overduejob']['detail']['network'])?$_SESSION['overduejob']['detail']['network']: 0);
                $_SESSION['overduejob']['detail']['network'] = $args['network'];

                $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['overduejob']['detail']['branch'])?$_SESSION['overduejob']['detail']['branch']: 0);
                $_SESSION['overduejob']['detail']['branch'] = $args['branch'];

                $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['overduejob']['detail']['client'])?$_SESSION['overduejob']['detail']['client']: 0);
                $_SESSION['overduejob']['detail']['client'] = $args['client'];

                $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['overduejob']['detail']['unitType'])?$_SESSION['overduejob']['detail']['unitType']: 0);
                $_SESSION['overduejob']['detail']['unitType'] = $args['unitType'];

                $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['overduejob']['detail']['skillSet'])?$_SESSION['overduejob']['detail']['skillSet']: 0);
                $_SESSION['overduejob']['detail']['skillSet'] = $args['skillSet'];

                $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['overduejob']['detail']['brand'])?$_SESSION['overduejob']['detail']['brand']: 0);
                $_SESSION['overduejob']['detail']['brand'] = $args['brand'];

                $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['overduejob']['detail']['manufacturer'])?$_SESSION['overduejob']['detail']['manufacturer']: 0);
                $_SESSION['overduejob']['detail']['manufacturer'] = $args['manufacturer'];

                $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['overduejob']['detail']['serviceProvider'])?$_SESSION['overduejob']['detail']['serviceProvider']: 0);
                $_SESSION['overduejob']['detail']['serviceProvider'] = $args['serviceProvider'];

                $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['overduejob']['detail']['sType'])?$_SESSION['overduejob']['detail']['sType']: 0);
                $_SESSION['overduejob']['detail']['sType'] = $args['sType'];

                $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['overduejob']['detail']['scType'])?$_SESSION['overduejob']['detail']['scType']: 0);
                $_SESSION['overduejob']['detail']['scType'] = $args['scType'];

                $args['ojBy'] = isset($args['ojBy']) ? $args['ojBy'] : (isset($_SESSION['overduejob']['detail']['ojBy'])?$_SESSION['overduejob']['detail']['ojBy']: 'sp');
                $_SESSION['overduejob']['detail']['ojBy'] = $args['ojBy'];

                $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['overduejob']['detail']['btnName'])?$_SESSION['overduejob']['detail']['btnName']: false);
                $_SESSION['overduejob']['detail']['btnName'] = $args['btnName'];

                $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['overduejob']['detail']['btnValue'])?$_SESSION['overduejob']['detail']['btnValue']: false);
                $_SESSION['overduejob']['detail']['btnValue'] = $args['btnValue'];

                $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['overduejob']['detail']['btnValue2'])?$_SESSION['overduejob']['detail']['btnValue2']: false);
                $_SESSION['overduejob']['detail']['btnValue2'] = $args['btnValue2'];
                
                if(isset($_SESSION['overduejob']['detail']['JobID']) && $_SESSION['overduejob']['detail']['JobID']){
                    $this->smarty->assign('selectedJobId',$_SESSION['overduejob']['detail']['JobID']);
                }
                else{
                    $this->smarty->assign('selectedJobId',0);
                }
                
             }
            
            
          
            $statsResult = $Job->fetchAllOverdueStats($args);
                       
            if(isset($TatResult['TatID']))
            {
                $tempArgs = $args;
                
                $tempArgs['btnName']    = 'b1';
                $tempArgs['btnValue']   = $TatResult['Button1'];
                $tempArgs['btnValue2']  = false;
                
                $button1StatsResult = $Job->fetchAllOverdueStats($tempArgs);
                $TatResult['Button1Value'] = 0;
                foreach($button1StatsResult AS $BS)
                {
                    $TatResult['Button1Value'] += $BS[1];
                }  
                               
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button1StatsResult_1 = $Job->fetchAllOverdueStats($tempArgs1);
                $b1bValue = 0;
                foreach($button1StatsResult_1 AS $BS)
                {
                    $b1bValue += $BS[1];
                } 
                if($b1bValue)
                {
                    $TatResult['Button1Value'] .= ' ['.$b1bValue.'] ';
                } 
                
                                
                $tempArgs['btnName']    = 'b2';
                $tempArgs['btnValue']   = $TatResult['Button1'];
                $tempArgs['btnValue2']  = $TatResult['Button3'];
                
                $button2StatsResult = $Job->fetchAllOverdueStats($tempArgs);
                $TatResult['Button2Value'] = 0;
                foreach($button2StatsResult AS $BS)
                {
                    $TatResult['Button2Value'] += $BS[1];
                }
                               
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button2StatsResult_1 = $Job->fetchAllOverdueStats($tempArgs1);
                $b2bValue = 0;
                foreach($button2StatsResult_1 AS $BS)
                {
                    $b2bValue += $BS[1];
                } 
                if($b2bValue)
                {
                    $TatResult['Button2Value'] .= ' ['.$b2bValue.'] ';
                }
                
                
                                
                $tempArgs['btnName']    = 'b3';
                $tempArgs['btnValue']   = $TatResult['Button3'];
                $tempArgs['btnValue2']  = false;
                
                $button3StatsResult = $Job->fetchAllOverdueStats($tempArgs);
                $TatResult['Button3Value'] = 0;
                foreach($button3StatsResult AS $BS)
                {
                    $TatResult['Button3Value'] += $BS[1];
                } 
                
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button3StatsResult_1 = $Job->fetchAllOverdueStats($tempArgs1);
                $b3bValue = 0;
                foreach($button3StatsResult_1 AS $BS)
                {
                    $b3bValue += $BS[1];
                } 
                if($b3bValue)
                {
                    $TatResult['Button3Value'] .= ' ['.$b3bValue.'] ';
                }
                
                                
                $tempArgs['btnName']    = 'b4';
                $tempArgs['btnValue']   = $TatResult['Button4'];
                $tempArgs['btnValue2']  = false;
                
                $button4StatsResult = $Job->fetchAllOverdueStats($tempArgs);
                $TatResult['Button4Value'] = 0;
                foreach($button4StatsResult AS $BS)
                {
                    $TatResult['Button4Value'] += $BS[1];
                }
                
                
                //Getting value in brackets..
                $tempArgs1 = $tempArgs;
                $tempArgs1['rcDate'] = 1;
                $button4StatsResult_1 = $Job->fetchAllOverdueStats($tempArgs1);
                $b4bValue = 0;
                foreach($button4StatsResult_1 AS $BS)
                {
                    $b4bValue += $BS[1];
                } 
                if($b4bValue)
                {
                    $TatResult['Button4Value'] .= ' ['.$b4bValue.'] ';
                }
                               
            }
            else
            {
               $TatResult = array(
                   
                   'TatID'=>'',
                   'TatType'=>'OverdueJobs',
                   'Button1'=>'',
                   'Button3'=>'',
                   'Button4'=>'',
                   'Button1Colour'=>'',
                   'Button2Colour'=>'',
                   'Button3Colour'=>'',
                   'Button4Colour'=>'',
                   'Button1Value'=>'',
                   'Button2Value'=>'',
                   'Button3Value'=>'',
                   'Button4Value'=>''
                   
               );
            }
            
            for($r=0;$r<count($statsResult);$r++)
            {
                $statsResult[$r][4] = $statsResult[$r][0];
                $statsResult[$r][0] = $statsResult[$r][0]." (".$statsResult[$r][1].")";
                
            }
            
            $totalRows = 0;
            $max_value = 0;
            $selectedStatusClient = '';
            for($r=0;$r<count($statsResult);$r++)
            {
                if($args['sType'] && $args['sType']==$statsResult[$r][2])
                {
                    $totalRows = $statsResult[$r][1];
                    $selectedStatusClient = " ".$statsResult[$r][4];
                    break;
                }
                else
                {
                    $totalRows += $statsResult[$r][1];
                }
                
                if ($statsResult[$r][1] > $max_value) {
                    $max_value = $statsResult[$r][1];
                }
            }
            
            if( isset($args['manufacturer']) ) {                                /* Check if a manufacturer ID is passed as a parameter */
                $this->smarty->assign('manufacturer',$args['manufacturer']);
            } else {
                $this->smarty->assign('manufacturer',0);
            }
            
            if( isset($args['client']) ) {                                      /* Check if a client ID is passed as a parameter */
                $this->smarty->assign('client',$args['client']);
            } else {
                $this->smarty->assign('client',0);
            }
            
            if( isset($args['network']) ) {                                /* Check if a Network ID is passed as a parameter */
                $this->smarty->assign('network',$args['network']);
            } else {
                $this->smarty->assign('network',0);
            }
            
            if( isset($args['branch']) ) {                                /* Check if a Branch ID is passed as a parameter */
                $this->smarty->assign('branch',$args['branch']);
            } else {
                $this->smarty->assign('branch',0);
            }
            
            if( isset($args['unitType']) ) {                                /* Check if a Unit Type ID is passed as a parameter */
                $this->smarty->assign('unitType',$args['unitType']);
            } else {
                $this->smarty->assign('unitType',0);
            }
            
            if( isset($args['skillSet']) ) {                                /* Check if a Unit Type ID is passed as a parameter */
                $this->smarty->assign('skillSet',$args['skillSet']);
            } else {
                $this->smarty->assign('skillSet',0);
            }
            
            $page    = $this->messages->getPage('overdueJobs', $this->lang);
            
            $this->smarty->assign('page', $page);
            $this->smarty->assign('brand', $args['brand']);
            $this->smarty->assign('sType', $args['sType']);
            $this->smarty->assign('scType', $args['scType']);
            $this->smarty->assign('serviceProvider', $args['serviceProvider']);
            
            $this->smarty->assign('TatResult', $TatResult);
            $this->smarty->assign('ojBy', $args['ojBy']);
            $this->smarty->assign('ojvBy', $args['ojvBy']);
            $this->smarty->assign('tatType', $args['tatType']);
            $this->smarty->assign('btnName', $args['btnName']);
            $this->smarty->assign('btnValue', $args['btnValue']);
            $this->smarty->assign('btnValue2', $args['btnValue2']);
       
            $SkylineModel = $this->loadModel('Skyline');
            
            $jobStatusList = $SkylineModel->getSystemStatus();
                       
            if($this->user->NetworkID)
            {
               $manufacturerList = $SkylineModel->getManufacturer('', $this->user->NetworkID);
            }
            else
            {
               $manufacturerList = $SkylineModel->getManufacturer('', null);
            }
                       
            $UserModel = $this->loadModel('Users');
            $PreferredManufacturers = $UserModel->getPreferredClientGroup('Manufacturer','overdueJobs');
            $PreferredBrands = $UserModel->getPreferredClientGroup('Brand','overdueJobs');
            $PreferredServiceProviders = $UserModel->getPreferredClientGroup('ServiceProvider','overdueJobs');
                       
            $SelectedManufacturers = array();
            $SelectedBrands        = array();
            $SelectedServiceProviders   = array();
            
            $PreferredNetworks   = $UserModel->getPreferredClientGroup('Network','overdueJobs');
            $PreferredBranches   = $UserModel->getPreferredClientGroup('Branch','overdueJobs');
            $PreferredClients    = $UserModel->getPreferredClientGroup('Client','overdueJobs');            
            $PreferredUnitTypes  = $UserModel->getPreferredClientGroup('UnitType','overdueJobs');
            $PreferredSkillset   = $UserModel->getPreferredClientGroup('SkillSet','overdueJobs');

            $SelectedNetworks           = array();
            $SelectedBranches           = array();
            $SelectedClient             = array();
            $SelectedUnitTypes          = array();
            $SelectedSkillSet           = array();
            
             
            $manuPriority  = array();
            $brandPriority = array();
            $serviceProviderPriority = array();
            $networkPriority = array();
            
            if(count($PreferredManufacturers))
            {
                $ManufacturerGroupEnable = $PreferredManufacturers[0]['GroupEnable'];
                for($i=0;$i<count($manufacturerList);$i++)
                {
                    $manufacturerList[$i]['Exists']   = false;
                    $manufacturerList[$i]['Priority'] = '';
                    
                    foreach($PreferredManufacturers as $pm)
                    {
                        if($manufacturerList[$i]['ManufacturerID']==$pm['PreferentialID'])
                        {
                            $manufacturerList[$i]['Exists']   = true;
                            $manuPriority[] =  $manufacturerList[$i]['Priority'] = $pm['Priority'];
                            
                            if(!$manufacturerList[$i]['ManufacturerLogo'])
                            {
                                //$manufacturerList[$i]['ManufacturerLogo'] = "no_logo.png";
                                $manufacturerList[$i]['ManufacturerLogo'] = false;
                            }  
                            
                            $SelectedManufacturers[] = $manufacturerList[$i];
                            
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedManufacturers)>0)
                {
                    array_multisort($manuPriority, SORT_ASC, $SelectedManufacturers);
                }
                
            }  
            else {
                $ManufacturerGroupEnable = 0;
                for($i=0;$i<count($manufacturerList);$i++)
                {
                    $manufacturerList[$i]['Exists'] = false;
                    $manufacturerList[$i]['Priority'] = '';
                }
            }
            
            $brandsList = $SkylineModel->getBrands($this->user->NetworkID, $this->user->ClientID, $this->user->BranchID, $this->user->ManufacturerID, $this->user->ServiceProviderID);
            
            if(count($PreferredBrands))
            {
                $BrandGroupEnable = $PreferredBrands[0]['GroupEnable'];
                for($i=0;$i<count($brandsList);$i++)
                {
                    $brandsList[$i]['Exists'] = false;
                    $brandsList[$i]['Priority'] = '';
                    
                    foreach($PreferredBrands as $pb)
                    {
                        if($brandsList[$i]['BrandID']==$pb['PreferentialID'])
                        {
                            $brandsList[$i]['Exists']   = true;
                            $brandPriority[] = $brandsList[$i]['Priority'] = $pb['Priority'];
                            
                            if(!$brandsList[$i]['BrandLogo'])
                            {
                                //$brandsList[$i]['BrandLogo'] = "no_logo.png";
                                $brandsList[$i]['BrandLogo'] = false;
                            }    
                            
                            $SelectedBrands[] = $brandsList[$i];
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedBrands)>0)
                {
                    array_multisort($brandPriority, SORT_ASC, $SelectedBrands);
                }
            }  
            else {
                $BrandGroupEnable = 0;
                for($i=0;$i<count($brandsList);$i++)
                {
                    $brandsList[$i]['Exists']   = false;
                    $brandsList[$i]['Priority'] = '';
                }
            }
            
            
            
            $ServiceProvidersModel = $this->loadModel('ServiceProviders');
            
            $serviceProvidersList  = $ServiceProvidersModel->getServiceProvidersHaveJobs($this->user);
            
                               
            if(count($PreferredServiceProviders))
            {
                $ServiceProviderGroupEnable = $PreferredServiceProviders[0]['GroupEnable'];
                for($i=0;$i<count($serviceProvidersList);$i++)
                {
                    $serviceProvidersList[$i]['Exists'] = false;
                    $serviceProvidersList[$i]['Priority'] = '';
                    
                    foreach($PreferredServiceProviders as $psp)
                    {
                        if($serviceProvidersList[$i]['ServiceProviderID']==$psp['PreferentialID'])
                        {
                            $serviceProvidersList[$i]['Exists']   = true;
                            $serviceProviderPriority[] = $serviceProvidersList[$i]['Priority'] = $psp['Priority'];
                            
                            if(!isset($serviceProvidersList[$i]['ServiceProviderLogo']) || !$serviceProvidersList[$i]['ServiceProviderLogo'])
                            {
                                $serviceProvidersList[$i]['ServiceProviderLogo'] = "no_logo.png";
                            }    
                            
                            $SelectedServiceProviders[] = $serviceProvidersList[$i];
                            break;
                        }
                    }
                    
                }
                
                if(count($SelectedServiceProviders)>0)
                {
                    array_multisort($serviceProviderPriority, SORT_ASC, $SelectedServiceProviders);
                }
            }  
            else {
                $ServiceProviderGroupEnable = 0;
                for($i=0;$i<count($serviceProvidersList);$i++)
                {
                    $serviceProvidersList[$i]['Exists']   = false;
                    $serviceProvidersList[$i]['Priority'] = '';
                }
            }

            //Network    
            $networksList = $SkylineModel->getNetworks(null,$this->user);
            if(count($PreferredNetworks))
            {
                $NetworkGroupEnable = $PreferredNetworks[0]['GroupEnable'];
                for($i=0;$i<count($networksList);$i++)
                {
                    $networksList[$i]['Exists'] = false;
                    $networksList[$i]['Priority'] = '';
                    foreach($PreferredNetworks as $pb)
                    {
                        if($networksList[$i]['NetworkID']==$pb['PreferentialID'])
                        {
                            $networksList[$i]['Exists']   = true;
                            $networkPriority[] = $networksList[$i]['Priority'] = $pb['Priority'];
                            $SelectedNetworks[] = $networksList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedNetworks)>0)
                {
                    array_multisort($networkPriority, SORT_ASC, $SelectedNetworks);
                }
            }  
            else {
                $NetworkGroupEnable = 0;
                for($i=0;$i<count($networksList);$i++)
                {
                    $networksList[$i]['Exists']   = false;
                    $networksList[$i]['Priority'] = '';
                }
            }
            
            //SkillSets
            $skillSetList = $SkylineModel->getRepairSkills();
            if(count($PreferredSkillset))
            {
                $SkillSetGroupEnable = $PreferredSkillset[0]['GroupEnable'];
                for($i=0;$i<count($skillSetList);$i++)
                {
                    $skillSetList[$i]['Exists'] = false;
                    $skillSetList[$i]['Priority'] = '';
                    foreach($PreferredSkillset as $pss)
                    {
                        if($skillSetList[$i]['RepairSkillID']==$pss['PreferentialID'])
                        {
                            $skillSetList[$i]['Exists']   = true;
                            $skillSetPriority[] = $skillSetList[$i]['Priority'] = $pss['Priority'];
                            $SelectedSkillSet[] = $skillSetList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedSkillSet)>0)
                {
                    array_multisort($skillSetPriority, SORT_ASC, $SelectedSkillSet);
                }
            }  
            else {
                $SkillSetGroupEnable = 0;
                for($i=0;$i<count($skillSetList);$i++)
                {
                    $skillSetList[$i]['Exists']   = false;
                    $skillSetList[$i]['Priority'] = '';
                }
            }
            
            //Unit Types
            $unitTypesList = $SkylineModel->getUnitTypes();
            if(count($PreferredUnitTypes))
            {
                $UnitTypeGroupEnable = $PreferredUnitTypes[0]['GroupEnable'];
                for($i=0;$i<count($unitTypesList);$i++)
                {
                    $unitTypesList[$i]['Exists'] = false;
                    $unitTypesList[$i]['Priority'] = '';
                    foreach($PreferredUnitTypes as $punt)
                    {
                        if($unitTypesList[$i]['UnitTypeID']==$punt['PreferentialID'])
                        {
                            $unitTypesList[$i]['Exists']   = true;
                            $unitTypePriority[] = $unitTypesList[$i]['Priority'] = $punt['Priority'];
                            $SelectedUnitTypes[] = $unitTypesList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedUnitTypes)>0)
                {
                    array_multisort($unitTypePriority, SORT_ASC, $SelectedUnitTypes);
                }
            }  
            else {
                $UnitTypeGroupEnable = 0;
                for($i=0;$i<count($unitTypesList);$i++)
                {
                    $unitTypesList[$i]['Exists']   = false;
                    $unitTypesList[$i]['Priority'] = '';
                }
            }
            
            //Clients
            $clientList = $SkylineModel->getClients($this->user);
            if(count($PreferredClients))
            {
                $ClientGroupEnable = $PreferredClients[0]['GroupEnable'];
                for($i=0;$i<count($clientList);$i++)
                {
                    $clientList[$i]['Exists'] = false;
                    $clientList[$i]['Priority'] = '';
                    foreach($PreferredClients as $clnt)
                    {
                        if($clientList[$i]['ID']==$clnt['PreferentialID'])
                        {
                            $clientList[$i]['Exists']   = true;
                            $clientPriority[] = $clientList[$i]['Priority'] = $clnt['Priority'];
                            $SelectedClient[] = $clientList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedClient)>0)
                {
                    array_multisort($clientPriority, SORT_ASC, $SelectedClient);
                }
            }  
            else {
                $ClientGroupEnable = 0;
                for($i=0;$i<count($clientList);$i++)
                {
                    $clientList[$i]['Exists']   = false;
                    $clientList[$i]['Priority'] = '';
                }
            }
            
            //Branches
            $branchesList = $SkylineModel->getBranches($this->user->BranchID, '', $this->user->ClientID, $this->user->NetworkID);
            if(count($PreferredBranches))
            {
                $BranchGroupEnable = $PreferredBranches[0]['GroupEnable'];
                for($i=0;$i<count($branchesList);$i++)
                {
                    $branchesList[$i]['Exists'] = false;
                    $branchesList[$i]['Priority'] = '';
                    foreach($PreferredBranches as $pbrnch)
                    {
                        if($branchesList[$i]['BranchID']==$pbrnch['PreferentialID'])
                        {
                            $branchesList[$i]['Exists']   = true;
                            $branchPriority[] = $branchesList[$i]['Priority'] = $pbrnch['Priority'];
                            $SelectedBranches[] = $branchesList[$i];
                            break;
                        }
                    }
                }
                if(count($SelectedBranches)>0)
                {
                    array_multisort($branchPriority, SORT_ASC, $SelectedBranches);
                }
            }  
            else {
                $BranchGroupEnable = 0;
                for($i=0;$i<count($branchesList);$i++)
                {
                    $branchesList[$i]['Exists']   = false;
                    $branchesList[$i]['Priority'] = '';
                }
            }
            
            $this->smarty->assign('manufacturerList', $manufacturerList);
            $this->smarty->assign('brandsList', $brandsList);
            $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
            
            $this->smarty->assign('networkList', $networksList);
            $this->smarty->assign('branchList', $branchesList);
            $this->smarty->assign('clientList', $clientList);
            $this->smarty->assign('unitTypesList', $unitTypesList);
            $this->smarty->assign('skillSetList', $skillSetList);
       
            
            $this->smarty->assign('SelectedBrands', $SelectedBrands);
            $this->smarty->assign('SelectedManufacturers', $SelectedManufacturers);
            $this->smarty->assign('SelectedServiceProviders', $SelectedServiceProviders);
            
            $this->smarty->assign('SelectedNetworks', $SelectedNetworks);
            $this->smarty->assign('SelectedBranch', $SelectedBranches);
            $this->smarty->assign('SelectedClient', $SelectedClient);
            $this->smarty->assign('SelectedUnitType', $SelectedUnitTypes);
            $this->smarty->assign('SelectedSkillSet', $SelectedSkillSet);
       
            
            if($args['unitType'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedUnitTypes as $sb)
                {    
                    if($sb['UnitTypeID']==$args['unitType'])
                    {
                        $selectedStatusClient .= " ".$sb['UnitTypeName'];
                    }
                }    
            }
            
            if($args['skillSet'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedSkillSet as $sb)
                {    
                    if($sb['RepairSkillID']==$args['skillSet'])
                    {
                        $selectedStatusClient .= " ".$sb['RepairSkillName'];
                    }
                }    
            }
            
            if($args['client'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedClient as $sb)
                {    
                    if($sb['ID']==$args['client'])
                    {
                        $selectedStatusClient .= " ".$sb['CompanyName'];
                    }
                }    
            }
            
            if($args['branch'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedBranches as $sb)
                {    
                    if($sb['BranchID']==$args['branch'])
                    {
                        $selectedStatusClient .= " ".$sb['BranchName'];
                    }
                }    
            }
            
            
            if($args['network'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedNetworks as $sb)
                {    
                    if($sb['NetworkID']==$args['network'])
                    {
                        $selectedStatusClient .= " ".$sb['CompanyName'];
                    }
                }    
            }
            
            if($args['brand'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedBrands as $sb)
                {    
                    if($sb['BrandID']==$args['brand'])
                    {
                        $selectedStatusClient .= " ".$sb['BrandName'];
                    }
                }    
            }
            
            if($args['manufacturer'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedManufacturers as $sm)
                {    
                    if($sm['ManufacturerID']==$args['manufacturer'])
                    {
                        $selectedStatusClient .= " ".$sm['ManufacturerName'];
                    }
                }    
            }
            
            
            if($args['serviceProvider'])
            {
                if($selectedStatusClient)
                {
                    $selectedStatusClient .= " -";
                } 
                
                foreach($SelectedServiceProviders as $ss)
                {    
                    if($ss['ServiceProviderID']==$args['serviceProvider'])
                    {
                        $selectedStatusClient .= " ".$ss['CompanyName'];
                    }
                }    
            }
            
            
            
            $SelectedBrandsCnt           = count($SelectedBrands);
            $SelectedManufacturersCnt    = count($SelectedManufacturers);
            $SelectedServiceProvidersCnt = count($SelectedServiceProviders);
            
            $SelectedNetworksCnt         = count($SelectedNetworks);
            $SelectedBranchesCnt         = count($SelectedBranches);
            $SelectedClientsCnt          = count($SelectedClient);
            $SelectedUnitTypesCnt        = count($SelectedUnitTypes);
            $SelectedSkillSetCnt         = count($SelectedSkillSet);
            
            $contentWidth              = $SelectedBrandsCnt;
            
            if($contentWidth<$SelectedManufacturersCnt)
            {
                $contentWidth = $SelectedManufacturersCnt;
            }    
            if($contentWidth<$SelectedServiceProvidersCnt)
            {
                $contentWidth = $SelectedServiceProvidersCnt;
            }
            
            if($contentWidth<$SelectedNetworksCnt)
            {
                $contentWidth = $SelectedNetworksCnt;
            }
            if($contentWidth<$SelectedBranchesCnt)
            {
                $contentWidth = $SelectedBranchesCnt;
            }
            if($contentWidth<$SelectedClientsCnt)
            {
                $contentWidth = $SelectedClientsCnt;
            }
            if($contentWidth<$SelectedUnitTypesCnt)
            {
                $contentWidth = $SelectedUnitTypesCnt;
            }
            if($contentWidth<$SelectedSkillSetCnt)
            {
                $contentWidth = $SelectedSkillSetCnt;
            }
            
            $contentWidth = $contentWidth*110;
            
            
            if($contentWidth<850)
            {
                $contentWidth = 850;
            }
            
            $this->smarty->assign('contentWidth', $contentWidth);
            
            
           
            
            //$this->log($UserStatusPreferences);
            
            if(count($UserStatusPreferences))
            {
                for($i=0;$i<count($jobStatusList);$i++)
                {
                    $jobStatusList[$i]['Exists'] = false;
                    
                    foreach($UserStatusPreferences as $usf)
                    {
                        if(in_array($jobStatusList[$i]['StatusID'], $usf))
                        {
                            $jobStatusList[$i]['Exists'] = true;
                            break;
                        }
                    }
                    
                }
            }  
            else {
                
                for($i=0;$i<count($jobStatusList);$i++)
                {
                    $jobStatusList[$i]['Exists'] = true;
                }
            }
            
             $this->smarty->assign('jobStatusList', $jobStatusList);
            
            
	     if($this->user->ClientID) {
		$clientName = $SkylineModel->getClientName($this->user->ClientID);
		if(stripos($clientName,"loewe") !== false) {
		    $this->smarty->assign("hideLogos",true);
		}
	     }

	     
            $this->smarty->assign('statsResult', $statsResult);
            
            if ($max_value > 9) $max_value = 9;                                 /* We only want 10 (9+1) max gridlines on the graph */
            $this->smarty->assign('vAxisGridline', $max_value+1);
            
            
            $graph_title = $totalRows." ".$page['Text']['total_overdue_jobs'].$selectedStatusClient; 
            
            $this->smarty->assign('graph_title', $graph_title);
            
            $this->smarty->assign('ServiceProviderGroupEnable', $ServiceProviderGroupEnable); 
            $this->smarty->assign('BrandGroupEnable', $BrandGroupEnable); 
            $this->smarty->assign('ManufacturerGroupEnable', $ManufacturerGroupEnable); 
            $this->smarty->assign('NetworkGroupEnable', $NetworkGroupEnable); 
            $this->smarty->assign('BranchGroupEnable', $BranchGroupEnable); 
            $this->smarty->assign('ClientGroupEnable', $ClientGroupEnable); 
            $this->smarty->assign('UnitTypeGroupEnable', $UnitTypeGroupEnable); 
            $this->smarty->assign('SkillSetGroupEnable', $SkillSetGroupEnable); 
            
            if($args['ojvBy'] == "d"){
            $this->smarty->display('overdueJobs.tpl'); 
            } else{
                $this->smarty->display('overdueJobs_summary.tpl'); 
            }
            return;
     }
     
      public function notConfirmedAction( /* $args */  ) { 
          
            
      
      }
      
        /**
        * Description
        * 
        * This method called when network or client or super admin try to book a job.
        * 
        * @param array $args 
        * @return void It prints popup form with Network and client dropdown or client dropdown only which depends on user access permission.
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        * 
        **************************************************************************/
       public function networkClientPopUpAction( $args ) {
           
            $JobTypeID = isset($args['JobTypeID'])?$args['JobTypeID']:false;
           
            $Skyline          = $this->loadModel('Skyline');
            $ClientID         = $this->user->ClientID;
            $BranchID         = $this->user->BranchID;
            $DefaultBranchID  = false;
            $branches         = array();
            
            if(!$this->user->NetworkID)
            {    
                $showNetworkDropDown = true; 
                $networks  = $Skyline->getNetworks();
                $this->smarty->assign('networks', $networks); 
                $clients   = array();
            }
            else
            {
                $showNetworkDropDown = false; 
                
                if($this->user->ClientID)
                {
                    $clients   = array();
                    
                    $DefaultBranchID  = $Skyline->getClientDefaultBranch($this->user->ClientID);
                } 
                else
                {
                     $clients  = $Skyline->getNetworkClients($this->user->NetworkID);
                }

                $this->smarty->assign('NetworkID', $this->user->NetworkID); 
            }
                                 
            if(!$JobTypeID)
            {
                $jobTypes = $Skyline->getJobTypes();
                $this->smarty->assign('jobTypes', $jobTypes); 
            }
            
            if(!$BranchID && !$DefaultBranchID && $ClientID)
            {
                $branches  = $Skyline->getBranches(null, null, $ClientID, $this->user->NetworkID);
            }             
           
            $this->smarty->assign('JobTypeID', $JobTypeID); 
            $this->smarty->assign('ClientID', $ClientID); 
            $this->smarty->assign('BranchID', $BranchID);
            $this->smarty->assign('DefaultBranchID', $DefaultBranchID);
            
            
            $this->smarty->assign('clients', $clients); 
            $this->smarty->assign('branches', $branches); 
            
            $this->smarty->assign('showNetworkDropDown', $showNetworkDropDown); 
            
            /*if (!$this->user->BranchID) {
                $this->smarty->assign('branches', $branches); 
                $this->smarty->display('./popup/JobNetworkClientBranch.tpl'); 
            } else {*/
            
                $this->smarty->display('./popup/JobNetworkClient.tpl'); 
                              
           // }
          
            return;
           
       }  
     
    
   
        public function indexAction($args) { 
	
            $Skyline     = $this->loadModel('Skyline');
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && (!$this->user->BranchID || !$this->user->ClientID) ) {
            
                $NetworkID         = isset($_POST['NetworkID'])?$_POST['NetworkID']:false;
                $ClientID          = isset($_POST['ClientID'])?$_POST['ClientID']:false;
                $JobTypeID         = isset($_POST['JobTypeID'])?$_POST['JobTypeID']:false;
                $DefaultBranchID   = isset($_POST['DefaultBranchID'])?$_POST['DefaultBranchID']:false;
                $BranchID          = false;
                
                if(!$DefaultBranchID)
                {    
                    $DefaultBranchID   = $Skyline->getClientDefaultBranch($ClientID);
                }
                
                $AutoPopulateBranchID  = $DefaultBranchID;
                
                /**
                if( isset($_POST['BranchID']) ) {
                    $BranchID = $_POST['BranchID'];
                    
                    if(!$DefaultBranchID)
                    {
                        $DefaultBranchID = $BranchID; 
                    }   
                    
                } else {
                    $BranchID          = false;
                }
                **/
                
                
                
                
                
               /* if(!$DefaultBranchID && count($branches)==1)
                {
                    $DefaultBranchID = $branches[0]["BranchID"];
                }   */ 
            
            }
            /*Tracker Base Log 541 - Replicate Job Code changes starts here added by Raju on 4th July 2013 */
            else if(isset($_GET['rJobId']) && isset($_GET['rType']))
            {
                $job_model = $this->loadModel( 'Job' );
                $jobData = $job_model->fetch($_GET['rJobId']);
                //$this->log("Arr:-".$jobData['NetworkID'],"xxxx");
                $NetworkID = $jobData['NetworkID'];
                $ClientID = $jobData['ClientID'];
                $JobTypeID = $jobData['JobTypeID'];
                $BranchID = $jobData['BranchID'];
                if($BranchID)
                {
                    $DefaultBranchID = $BranchID;
                    $AutoPopulateBranchID = $BranchID;
                }
                else
                {
                    $DefaultBranchID = $Skyline->getClientDefaultBranch($ClientID);
                    $AutoPopulateBranchID = false;
                }
                if($_GET['rType'] == "Both")
                {
                    $rep_jb_datarow = array(
                        'CustomerType'=> (isset($jobData['CustomerType']) && $jobData['CustomerType']=='Business')?'Business':'Consumer',
                        'CompanyName'=>(isset($jobData['CompanyName']) && $jobData['CompanyName'])?$jobData['CompanyName']:'',
                        'ContactTitle'=>(isset($jobData['CustomerTitleID']) && $jobData['CustomerTitleID'])?$jobData['CustomerTitleID']:'', 
                        'ContactFirstName'=>(isset($jobData['ContactFirstName']) && $jobData['ContactFirstName'])?$jobData['ContactFirstName']:'',
                        'ContactLastName'=>(isset($jobData['ContactLastName']) && $jobData['ContactLastName'])?$jobData['ContactLastName']:'',
                        'PostalCode'=>(isset($jobData['PostalCode']) && $jobData['PostalCode'])?$jobData['PostalCode']:'', 
                        'BuildingName'=>(isset($jobData['BuildingNameNumber']) && $jobData['BuildingNameNumber'])?$jobData['BuildingNameNumber']:'',
                        'Street'=>(isset($jobData['Street']) && $jobData['Street'])?$jobData['Street']:'',
                        'Area'=>(isset($jobData['LocalArea']) && $jobData['LocalArea'])?$jobData['LocalArea']:'',
                        'City'=>(isset($jobData['TownCity']) && $jobData['TownCity'])?$jobData['TownCity']:'',
                        'County'=>(isset($jobData['CountyID']) && $jobData['CountyID'])?$jobData['CountyID']:'',
                        'Country'=>(isset($jobData['CountryID']) && $jobData['CountryID'])?$jobData['CountryID']:'',
                        'ContactEmail'=>(isset($jobData['ContactEmail']) && $jobData['ContactEmail'])?$jobData['ContactEmail']:'',
                        'ContactHomePhone'=>(isset($jobData['ContactHomePhone']) && $jobData['ContactHomePhone'])?$jobData['ContactHomePhone']:'',
                        'ContactHomePhoneExt'=>(isset($jobData['ContactWorkPhoneExt']) && $jobData['ContactWorkPhoneExt'])?$jobData['ContactWorkPhoneExt']:'',
                        'ContactMobile'=>(isset($jobData['ContactMobile']) && $jobData['ContactMobile'])?$jobData['ContactMobile']:'',
                        'ManufacturerID'=>(isset($jobData['ManufacturerID']) && $jobData['ManufacturerID'])?$jobData['ManufacturerID']:'',
                        'UnitTypeID'=>(isset($jobData['UnitTypeID']) && $jobData['UnitTypeID'])?$jobData['UnitTypeID']:'',
                        'SerialNo'=>(isset($jobData['SerialNo']) && $jobData['SerialNo'])?$jobData['SerialNo']:'',
                        'DateOfPurchase'=>(isset($jobData['DateOfPurchase']) && $jobData['DateOfPurchase'])?$jobData['DateOfPurchase']:'',
                        'ServiceTypeID'=>(isset($jobData['ServiceTypeID']) && $jobData['ServiceTypeID'])?$jobData['ServiceTypeID']:'',
                        'ImeiNo'=>(isset($jobData['ImeiNo']) && $jobData['ImeiNo'])?$jobData['ImeiNo']:'');
                    if(isset($jobData['ModelID']) && !empty($jobData['ModelID']))
                    {
                        $rep_jb_datarow['LocateBy'] = 1;
                        $rep_jb_datarow['ModelName'] = ((isset($jobData['ModelNumber']) && $jobData['ModelNumber'])?$jobData['ModelNumber']:'');
                    }
                    else
                    {
                        $rep_jb_datarow['LocateBy'] = 2;
                        $rep_jb_datarow['ProductDescription'] = ((isset($jobData['ServiceBaseModel']) && $jobData['ServiceBaseModel'])?$jobData['ServiceBaseModel']:'');
                    }
                }
                else
                {
                    $rep_jb_datarow = array(
                        'CustomerType'=> (isset($jobData['CustomerType']) && $jobData['CustomerType']=='Business')?'Business':'Consumer',
                        'CompanyName'=>(isset($jobData['CompanyName']) && $jobData['CompanyName'])?$jobData['CompanyName']:'',
                        'ContactTitle'=>(isset($jobData['CustomerTitleID']) && $jobData['CustomerTitleID'])?$jobData['CustomerTitleID']:'', 
                        'ContactFirstName'=>(isset($jobData['ContactFirstName']) && $jobData['ContactFirstName'])?$jobData['ContactFirstName']:'',
                        'ContactLastName'=>(isset($jobData['ContactLastName']) && $jobData['ContactLastName'])?$jobData['ContactLastName']:'',
                        'PostalCode'=>(isset($jobData['PostalCode']) && $jobData['PostalCode'])?$jobData['PostalCode']:'', 
                        'BuildingName'=>(isset($jobData['BuildingNameNumber']) && $jobData['BuildingNameNumber'])?$jobData['BuildingNameNumber']:'',
                        'Street'=>(isset($jobData['Street']) && $jobData['Street'])?$jobData['Street']:'',
                        'Area'=>(isset($jobData['LocalArea']) && $jobData['LocalArea'])?$jobData['LocalArea']:'',
                        'City'=>(isset($jobData['TownCity']) && $jobData['TownCity'])?$jobData['TownCity']:'',
                        'County'=>(isset($jobData['CountyID']) && $jobData['CountyID'])?$jobData['CountyID']:'',
                        'Country'=>(isset($jobData['CountryID']) && $jobData['CountryID'])?$jobData['CountryID']:'',
                        'ContactEmail'=>(isset($jobData['ContactEmail']) && $jobData['ContactEmail'])?$jobData['ContactEmail']:'',
                        'ContactHomePhone'=>(isset($jobData['ContactHomePhone']) && $jobData['ContactHomePhone'])?$jobData['ContactHomePhone']:'',
                        'ContactHomePhoneExt'=>(isset($jobData['ContactWorkPhoneExt']) && $jobData['ContactWorkPhoneExt'])?$jobData['ContactWorkPhoneExt']:'',
                        'ContactMobile'=>(isset($jobData['ContactMobile']) && $jobData['ContactMobile'])?$jobData['ContactMobile']:'');
                }
            }
            /*Tracker Base Log 541 - Replicate Job Code changes ends here added by Raju on 4th July 2013 */
            else
            {
                $NetworkID         = $this->user->NetworkID;
                $ClientID          = $this->user->ClientID;
                $BranchID          = $this->user->BranchID;
                
                $AutoPopulateBranchID   = $BranchID;
                
                if($BranchID)
                {
                    $DefaultBranchID = $BranchID;
                }    
                else
                {
                    $DefaultBranchID        = $Skyline->getClientDefaultBranch($ClientID);
                }
                
                $JobTypeID              = isset($args['JobTypeID']) ? $args['JobTypeID'] : (isset($_POST['JobTypeID'])? $_POST['JobTypeID'] : '');
                
            }
            
 
	    if(isset($_GET['rJobId']) && isset($_GET['rType']))
            {
                $this->smarty->assign("replicateJobId",$_GET['rJobId']);
                $this->smarty->assign("replicateType",$_GET['rType']);
            }
            else
            {
                $this->smarty->assign("replicateJobId","");
                $this->smarty->assign("replicateType","");
            }
	    if($_SERVER['REQUEST_METHOD'] == "POST") {
		if(isset($_POST["jbStockCodePrefill"])) {
		    
		    $NetworkID = $this->user->NetworkID;
		    $ClientID  = $this->user->ClientID;
		    $BranchID  = $this->user->BranchID;

		    $AutoPopulateBranchID = false;

		    if($BranchID) {
			$DefaultBranchID = $BranchID;
		    } else {
			$DefaultBranchID = $Skyline->getClientDefaultBranch($ClientID);
		    }

		    $JobTypeID = isset($args['JobTypeID']) ? $args['JobTypeID'] : (isset($_POST['JobTypeID']) ? $_POST['JobTypeID'] : '');
		    
		    $this->smarty->assign("prefillValues",$_POST);
		    
		}
	    }
	    
            
            
            //Getting Job type code
            $JobTypesModel     = $this->loadModel("JobTypes");
            $JobTyeRowDetails  = $JobTypesModel->fetchRow(array("JobTypeID"=>$JobTypeID));
            $JobTypeCode = isset($JobTyeRowDetails["TypeCode"])?$JobTyeRowDetails["TypeCode"]:null;
            
            
            //if(!$NetworkID || !$ClientID || (!$DefaultBranchID && !$BranchID) || !$JobTypeID)
            if(!$NetworkID || !$ClientID || !$JobTypeID)
            {
                $this->log("Job Booking Error: NetworkID or ClientID or JobTypeID is missing.");
                
                $this->log(var_export("NetworkID:".$NetworkID, true));
                $this->log(var_export("ClientID:".$ClientID, true));
                $this->log(var_export("DefaultBranchID:".$DefaultBranchID, true));
                $this->log(var_export("BranchID:".$BranchID, true));
                $this->log(var_export("JobTypeID:".$JobTypeID, true));
                
                $this->redirect('index',null, null);
            }
            
             
            $branches = $Skyline->getBranches(null, null, $ClientID, $NetworkID);
            
            
           
            $afn_job     = $this->messages->getAFN('AFN-JOB');
            $afn_product = $this->messages->getAFN('AFN-PRODUCT');
            $afn_stock_code = $this->messages->getAFN('AFN-STOCK CODE');
           
            $page = $this->messages->getPage('Job', $this->lang);
            
            $page['Labels']['job_type'] = str_replace("%%afn-job%%", $afn_job, $page['Labels']['job_type']);
            $page['Errors']['job_type'] = str_replace("%%afn-job%%", $afn_job, $page['Errors']['job_type']);
            
            $page['Labels']['product_search'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_search']); 
            $page['Labels']['product_description'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_description']); 
            $page['Labels']['product_type'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_type']); 
            $page['Labels']['product_location'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_location']); 
            $page['Text']['free_product_description'] = str_replace("%%afn-product%%", strtolower($afn_product), $page['Text']['free_product_description']); 

            $page['Labels']['stock_code']     = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Labels']['stock_code']);
            $page['Text']['stock_code_title'] = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Text']['stock_code_title']);

            
            //Changing following lables for DSGi for demo purpose, we must remove when Vic implemented alternative fields names work.
            if($ClientID==133)
            {
               $page['Labels']['policy_no'] = 'Agreement Ref';
               $page['Labels']['authorisation_no'] = 'Client ID';
               $page['Labels']['referral_no'] = 'Referral No.';
            }
            /**********************/
            
            
            
            $this->smarty->assign('page', $page);

            $countries  = $Skyline->getCountriesCounties();
            $selectTitle  = $Skyline->getTitles();
            $job_type_rows  = $Skyline->getJobTypes();

            

           // $RepairType  = $Skyline->getRepairTypes();

            $mfg_rows   = $Skyline->getManufacturer('', null, $ClientID);
            
            if(!count($mfg_rows))
            {
                $mfg_rows   = $Skyline->getManufacturer('', $NetworkID);
            }
            if(!count($mfg_rows))
            {
                 $mfg_rows   = $Skyline->getManufacturer();
            }
            
            
            
            $product_type_rows   = $Skyline->getUnitTypes('', $ClientID);
            if(!count($product_type_rows))
            {
                 $product_type_rows   = $Skyline->getUnitTypes();
            }    

            $product_location_rows   = $Skyline->getProductLocations(true);


           // $firstArg = isset($args[0]) ? $args[0] : '';
         
//            if($firstArg)
//            {
//                $Job = $this->loadModel('Job');
//                $jb_datarow = $Job->fetch($firstArg);
//                
//                $this->smarty->assign('page_legend', $page['Text']['update_legend']);
//                $this->smarty->assign('page_title', $page['Text']['update_page_title']);
//            }
//            else
//            {
                     //Checking product database enable or not for client. - starts here..
                    $ProductDatabaseEnabled  = false;
                    $ReferralNumberRequired  = false;
                  
                    
                    if(!$ClientID)
                    {    
                        $brands_model  = $this->loadModel('Brands');
                        $BrandsDetails = $brands_model->fetchRow(array("BrandID"=>$_COOKIE['brand']));

                        $ClientID = isset($BrandsDetails['ClientID'])?$BrandsDetails['ClientID']:0;
                    }

                    if($ClientID)
                    {    
                        $client_model = $this->loadModel('Clients');
                        $ProductDatabaseEnabled = $client_model->isProductDatabaseEnabled($ClientID);
                        $ReferralNumberRequired = $client_model->isReferralNumberRequired($ClientID);
                        
                    }
                    //Checking product database enable or not for client. - ends here..
                
                
                    $JobSourceID = $Skyline->getJobSourceID('Skyline');
                    $BranchModel       = $this->loadModel("Branches");
                    
                    
                    $OriginalRetailerFormElement = 'FreeText';
                    if($BranchID)
                    {
                        $branchFullDetails = $BranchModel->fetchRow(array('BranchID'=>$BranchID));
                        $OriginalRetailerFormElement = isset($branchFullDetails['OriginalRetailerFormElement'])?$branchFullDetails['OriginalRetailerFormElement']:$OriginalRetailerFormElement;
                    }
                    else if($DefaultBranchID)
                    {
                        $branchFullDetails = $BranchModel->fetchRow(array('BranchID'=>$DefaultBranchID));
                        $OriginalRetailerFormElement = isset($branchFullDetails['OriginalRetailerFormElement'])?$branchFullDetails['OriginalRetailerFormElement']:$OriginalRetailerFormElement;
                    }
                    
                    $ProductLocation = '';
                    if(isset($branchFullDetails['CurrentLocationOfProduct']))
                    {
                        $ProductLocation = $branchFullDetails['CurrentLocationOfProduct'];
                    } 
                    
                    
                    $SystemStatusesModel   = $this->loadModel('SystemStatuses');
                    $DisplayCompulsory     = $SystemStatusesModel->getUserStatusPreferences('jobBooking', 'cf');
                    
                    
                    //Getting Job Booking Settings
                    $JobSettingsModel   = $this->loadModel('JobSettings');
                    $jb_settings = $JobSettingsModel->fetchRow(array('NetworkID'=>$NetworkID, 'ClientID'=>$ClientID, 'JobTypeID'=>$JobTypeID));
                    
                    if(!count($jb_settings))
                    {
                        $jb_settings = $JobSettingsModel->fetchRow(array('NetworkID'=>$NetworkID, 'JobTypeID'=>$JobTypeID));
                    }
                    
                    if(!count($jb_settings))
                    {
                        $jb_settings = $JobSettingsModel->fetchRow(array('JobTypeID'=>$JobTypeID));
                    }
                    
                    if(!count($jb_settings))
                    {
                        $fieldNames = $JobSettingsModel->fetchColumnNames();
                        
                        foreach($fieldNames as $fnIndex=>$fnValue)
                        {
                            if(strpos($fnValue, 'Chk1'))
                            {
                                 $jb_settings[$fnValue] = 1;
                            }
                            else
                            {
                                $jb_settings[$fnValue] = '';
                            }
                        }    
                    }
                    
                     
                  
                    if(isset($jb_settings['StockCode']) && $jb_settings['StockCode'])
                    {
                       $stockCodeArray =  explode(";;", $jb_settings['StockCode']);
                       
                       $jb_settings['StockCode']  = (isset($stockCodeArray[0]))?$stockCodeArray[0]:'';
                    }    
                    
                               
                    $jb_datarow = array(
                        'NetworkID'=>$NetworkID, 
                        'ClientID'=>$ClientID, 
                        'BranchID'=>$BranchID, 
                        'DefaultBranchID'=>$DefaultBranchID, 
                        'BrandID'=>$this->user->DefaultBrandID, 
                        'CustomerType'=> (isset($jb_settings['CustomerType']) && $jb_settings['CustomerType']=='Business')?'Business':'Consumer',
                        'CompanyName'=>(isset($jb_settings['CompanyName']) && $jb_settings['CompanyName'])?$jb_settings['CompanyName']:'',
                        'ContactTitle'=>(isset($jb_settings['CustomerTitleID']) && $jb_settings['CustomerTitleID'])?$jb_settings['CustomerTitleID']:'', 
                        'ContactFirstName'=>(isset($jb_settings['ContactFirstName']) && $jb_settings['ContactFirstName'])?$jb_settings['ContactFirstName']:'',
                        'ContactLastName'=>(isset($jb_settings['ContactLastName']) && $jb_settings['ContactLastName'])?$jb_settings['ContactLastName']:'',
                        'PostalCode'=>(isset($jb_settings['PostalCode']) && $jb_settings['PostalCode'])?$jb_settings['PostalCode']:'', 
                        'BuildingName'=>(isset($jb_settings['BuildingNameNumber']) && $jb_settings['BuildingNameNumber'])?$jb_settings['BuildingNameNumber']:'',
                        'Street'=>(isset($jb_settings['Street']) && $jb_settings['Street'])?$jb_settings['Street']:'',
                        'Area'=>(isset($jb_settings['LocalArea']) && $jb_settings['LocalArea'])?$jb_settings['LocalArea']:'',
                        'City'=>(isset($jb_settings['TownCity']) && $jb_settings['TownCity'])?$jb_settings['TownCity']:'',
                        'County'=>(isset($jb_settings['CountyID']) && $jb_settings['CountyID'])?$jb_settings['CountyID']:'',
                        'Country'=>(isset($jb_settings['CountryID']) && $jb_settings['CountryID'])?$jb_settings['CountryID']:'',
                        'ContactEmail'=>(isset($jb_settings['ContactEmail']) && $jb_settings['ContactEmail'])?$jb_settings['ContactEmail']:'',
                        'ContactHomePhone'=>(isset($jb_settings['ContactHomePhone']) && $jb_settings['ContactHomePhone'])?$jb_settings['ContactHomePhone']:'',
                        'ContactHomePhoneExt'=>(isset($jb_settings['ContactWorkPhoneExt']) && $jb_settings['ContactWorkPhoneExt'])?$jb_settings['ContactWorkPhoneExt']:'',
                        'ContactMobile'=>(isset($jb_settings['ContactMobile']) && $jb_settings['ContactMobile'])?$jb_settings['ContactMobile']:'',
                        'JobTypeID'=>$JobTypeID,
                        'ServiceTypeID'=>(isset($jb_settings['ServiceTypeID']) && $jb_settings['ServiceTypeID'])?$jb_settings['ServiceTypeID']:'',
                        //'RepairTypeID'=>'1',
                        'LocateBy'=>(isset($jb_settings['LocateBy']) && $jb_settings['LocateBy'])?$jb_settings['LocateBy']:'',
                        'StockCode'=>(isset($jb_settings['StockCode']) && $jb_settings['StockCode'])?$jb_settings['StockCode']:'',
                        'ModelName'=>(isset($jb_settings['ModelName']) && $jb_settings['ModelName'])?$jb_settings['ModelName']:'',
                        'ProductDescription'=>(isset($jb_settings['ProductDescription']) && $jb_settings['ProductDescription'])?$jb_settings['ProductDescription']:'',
                        'UnitTypeID'=>(isset($jb_settings['UnitTypeID']) && $jb_settings['UnitTypeID'])?$jb_settings['UnitTypeID']:'',
                        'ManufacturerID'=>(count($mfg_rows)==1 && $mfg_rows[0]['ManufacturerID'])?$mfg_rows[0]['ManufacturerID']:'',
                        'SerialNo'=>(isset($jb_settings['SerialNo']) && $jb_settings['SerialNo'])?$jb_settings['SerialNo']:'',
                        'ReportedFault'=>(isset($jb_settings['ReportedFault']) && $jb_settings['ReportedFault'])?$jb_settings['ReportedFault']:'',
                        'ProductLocation'=>(isset($jb_settings['ProductLocation']) && $jb_settings['ProductLocation'])?$jb_settings['ProductLocation']:$ProductLocation,
                        'OriginalRetailerPart1'=>(isset($jb_settings['OriginalRetailerPart1']) && $jb_settings['OriginalRetailerPart1'])?$jb_settings['OriginalRetailerPart1']:'',
                        'OriginalRetailerPart2'=>(isset($jb_settings['OriginalRetailerPart2']) && $jb_settings['OriginalRetailerPart2'])?$jb_settings['OriginalRetailerPart2']:'',
                        'OriginalRetailerPart3'=>(isset($jb_settings['OriginalRetailerPart3']) && $jb_settings['OriginalRetailerPart3'])?$jb_settings['OriginalRetailerPart3']:'',
                        'DateOfPurchase'=>(isset($jb_settings['DateOfPurchase']) && $jb_settings['DateOfPurchase'])?$jb_settings['DateOfPurchase']:'',
                        'ReceiptNo'=>(isset($jb_settings['ReceiptNo']) && $jb_settings['ReceiptNo'])?$jb_settings['ReceiptNo']:'',
                        'Insurer'=>(isset($jb_settings['Insurer']) && $jb_settings['Insurer'])?$jb_settings['Insurer']:'',
                        'PolicyNo'=>(isset($jb_settings['PolicyNo']) && $jb_settings['PolicyNo'])?$jb_settings['PolicyNo']:'',
                        'AuthorisationNo'=>(isset($jb_settings['AuthorisationNo']) && $jb_settings['AuthorisationNo'])?$jb_settings['AuthorisationNo']:'',
                        'Notes'=>(isset($jb_settings['Notes']) && $jb_settings['Notes'])?$jb_settings['Notes']:'',
                        'ReferralNo'=>(isset($jb_settings['ReferralNo']) && $jb_settings['ReferralNo'])?$jb_settings['ReferralNo']:'',
                        'PostcodeLookup'=>1,
                        'BrandCountry'=>1,
                        'CountryMandatory'=>1,
                        'ShowStockCode'=>($ProductDatabaseEnabled)?1:0,
                        'ReferralNumberRequired'=>$ReferralNumberRequired,
                        'JobTypeCode'=>$JobTypeCode,
                        'data_protection_act'=>'',
                        'ColAddPostalCode'=>(isset($jb_settings['ColAddPostcode']) && $jb_settings['ColAddPostcode'])?$jb_settings['ColAddPostcode']:'',
                        'ColAddCompanyName'=>(isset($jb_settings['ColAddCompanyName']) && $jb_settings['ColAddCompanyName'])?$jb_settings['ColAddCompanyName']:'',
                        'ColAddBuildingName'=>(isset($jb_settings['ColAddBuildingNameNumber']) && $jb_settings['ColAddBuildingNameNumber'])?$jb_settings['ColAddBuildingNameNumber']:'',
                        'ColAddStreet'=>(isset($jb_settings['ColAddStreet']) && $jb_settings['ColAddStreet'])?$jb_settings['ColAddStreet']:'',
                        'ColAddArea'=>(isset($jb_settings['ColAddLocalArea']) && $jb_settings['ColAddLocalArea'])?$jb_settings['ColAddLocalArea']:'',
                        'ColAddCity'=>(isset($jb_settings['ColAddTownCity']) && $jb_settings['ColAddTownCity'])?$jb_settings['ColAddTownCity']:'',
                        'ColAddCounty'=>(isset($jb_settings['ColAddCountyID']) && $jb_settings['ColAddCountyID'])?$jb_settings['ColAddCountyID']:'',
                        'ColAddCountry'=>(isset($jb_settings['ColAddCountryID']) && $jb_settings['ColAddCountryID'])?$jb_settings['ColAddCountryID']:'',
                        'ColAddEmail'=>(isset($jb_settings['ColAddEmail']) && $jb_settings['ColAddEmail'])?$jb_settings['ColAddEmail']:'',
                        'ColAddPhone'=>(isset($jb_settings['ColAddPhone']) && $jb_settings['ColAddPhone'])?$jb_settings['ColAddPhone']:'',
                        'ColAddPhoneExt'=>'',
                        'AutoPopulateBranchID'=>$AutoPopulateBranchID,
                        'JobSourceID'=>$JobSourceID,
                        'Accessories'=>(isset($jb_settings['Accessories']) && $jb_settings['Accessories'])?$jb_settings['Accessories']:'',
                        'UnitCondition'=>(isset($jb_settings['UnitCondition']) && $jb_settings['UnitCondition'])?$jb_settings['UnitCondition']:'',
                        'OriginalRetailerFormElement'=>$OriginalRetailerFormElement,
                        'DisplayCompulsory'=>(isset($DisplayCompulsory[0][0]) && $DisplayCompulsory[0][0])?true:false,
                        'MobilePhoneNetworkID'=>(isset($jb_settings['MobilePhoneNetworkID']) && $jb_settings['MobilePhoneNetworkID'])?$jb_settings['MobilePhoneNetworkID']:'',
                        'SendRepairCompleteTextMessage'=>(isset($branchFullDetails['SendRepairCompleteTextMessage']) && $branchFullDetails['SendRepairCompleteTextMessage']=='Yes')?'1':'0',
                        'ImeiNo'=>(isset($jb_settings['ImeiNo']) && $jb_settings['ImeiNo'])?$jb_settings['ImeiNo']:''

                        );		   
                    
                        if(isset($jb_settings['ManufacturerID']) && $jb_settings['ManufacturerID'] && $jb_datarow['ManufacturerID']=='')
                        {
                            $jb_datarow['ManufacturerID']  = $jb_settings['ManufacturerID'];
                        }
                    
                    
                        
                        if($jb_datarow["BranchID"])
                        {    

                            $BranchIDAddress   = $jb_datarow["BranchID"];
                        }
                        else
                        {
                            $BranchIDAddress   = $jb_datarow["DefaultBranchID"];
                        }
                        
                        $BranchDetails   =  $BranchModel->getBranchesWithAddress($ClientID, $NetworkID);
                        $this->smarty->assign('BranchDetails', $BranchDetails);
                        
                      //  if($JobTypeCode==2)
                      //  {

                           
                           $this->smarty->assign('BranchIDAddress', $BranchIDAddress);
                           
                      //  }
                     
                     
                     
                    if($AutoPopulateBranchID)
                    {
                        $autoPopulateDetails = $BranchModel->getBranchWithBrand($AutoPopulateBranchID);
                        
                        if(isset($autoPopulateDetails['BranchType']) && $autoPopulateDetails['BranchType']=="Store")
                        {    
                            $jb_datarow["AutoPopulateBranchID"] =    $AutoPopulateBranchID;
                        }
                        else
                        {
                            $jb_datarow["AutoPopulateBranchID"] = '-2';
                        }    
                        
                    }    
                        
                        
                        
                     if($JobTypeCode!=3 && $this->user->BranchID)
                     {   
                        $OriginalRetailDetails = $BranchModel->getBranchWithBrand($BranchIDAddress);

                        if(is_array($OriginalRetailDetails) && $OriginalRetailDetails['BranchType']=="Store")
                        {
                           $jb_datarow["OriginalRetailerPart1"] = isset($OriginalRetailDetails['BrandName'])?$OriginalRetailDetails['BrandName']:$jb_datarow["OriginalRetailerPart1"];
                           
                           if(strlen($jb_datarow["OriginalRetailerPart1"])>25)
                           {
                               $jb_datarow["OriginalRetailerPart1"] = substr($jb_datarow["OriginalRetailerPart1"], 0, 25);
                           }
                          
                           
                           $jb_datarow["OriginalRetailerPart2"] = isset($OriginalRetailDetails['BranchName'])?$OriginalRetailDetails['BranchName']:$jb_datarow["OriginalRetailerPart2"];
                           
                           if(strlen($jb_datarow["OriginalRetailerPart2"])>25)
                           {
                               $jb_datarow["OriginalRetailerPart2"] = substr($jb_datarow["OriginalRetailerPart2"], 0, 25);
                           }
                           
                           $jb_datarow["OriginalRetailerPart3"] = isset($OriginalRetailDetails['TownCity'])?$OriginalRetailDetails['TownCity']:$jb_datarow["OriginalRetailerPart3"];
                           
                           
                           if(strlen($jb_datarow["OriginalRetailerPart3"])>25)
                           {
                               $jb_datarow["OriginalRetailerPart3"] = substr($jb_datarow["OriginalRetailerPart3"], 0, 25);
                           }
                           
                        }
                     }
                      
                     $this->smarty->assign('branches', $branches);
                     $this->smarty->assign('page_legend', $page['Text']['booking_legend']);
                     $this->smarty->assign('page_title', $page['Text']['booking_page_title']);
                     
                     
                     //Getting Catalogue numbers.
                     $ProductNumbersModel = $this->loadModel('ProductNumbers');
                     $stockcode_rows      = $ProductNumbersModel->fetchProductNumbers($NetworkID, $ClientID);
                     
                     $this->smarty->assign('stockcode_rows', $stockcode_rows);
                     
                      
          //  }

            $service_type_rows = array();
            if($jb_datarow['NetworkID'])
            {
                $service_type_rows  = $Skyline->getServiceTypes($jb_datarow['BrandID'], $jb_datarow['JobTypeID'], $jb_datarow['NetworkID'], $jb_datarow['ClientID']);
            }
            
            $MobilePhoneNetworkModel = $this->loadModel('MobilePhoneNetwork');
            $mobile_phone_networks = $MobilePhoneNetworkModel->fetchAll();
            
          
            
            $jb_datarow['ServiceTypeID'] = (count($service_type_rows)==1 && $service_type_rows[0]['ServiceTypeID'])?$service_type_rows[0]['ServiceTypeID']:'';
            
            
            //Comment by Nag.  Joe asked me to do this, users dont see the job booking if service types are not associated with Job type, client.
            /*
            if(count($service_type_rows)==0)
            {
                $service_type_rows  = $Skyline->getServiceTypes($jb_datarow['BrandID'], $jb_datarow['JobTypeID']);
            }*/
            
            
            if(isset($_GET['rJobId']) && isset($_GET['rType']))
                $jb_datarow = array_merge($jb_datarow,$rep_jb_datarow);
            $this->smarty->assign('jb_datarow', $jb_datarow);
            $this->smarty->assign('jb_settings', $jb_settings);
            
            
            $this->smarty->assign('selectTitle',$selectTitle);
            $this->smarty->assign('service_type_rows',$service_type_rows);
            $this->smarty->assign('job_type_rows',$job_type_rows);
            $this->smarty->assign('mobile_phone_networks',$mobile_phone_networks);
            //$this->smarty->assign('RepairType',$RepairType);
            $this->smarty->assign('mfg_rows',$mfg_rows);
            $this->smarty->assign('product_type_rows',$product_type_rows);
            $this->smarty->assign('product_location_rows',$product_location_rows);
            $this->smarty->assign('countries', $countries);
            
            //Insert Branch when booking job permission.
            /*$displayAddBranchLink = false;
            if (isset($this->user->Permissions['AP7007']) || $this->user->SuperAdmin)
            {
                $displayAddBranchLink = true;
            }
            $this->smarty->assign('displayAddBranchLink', $displayAddBranchLink);*/
            
            $this->smarty->assign('currentDate', date("Ymd"));
                        
            $this->smarty->assign('AP10000', Functions::hasPermission($this->user, 'AP10000'));
            $this->smarty->assign('displayAddBranchLink', Functions::hasPermission($this->user, 'AP7007'));
            
            // clear Book Courier Collection Session Data if there is any
            unset($this->session->BookCollectionFormData);            
	 
		
	    $this->smarty->display('Job.tpl'); 
		
			
		

            return;
       
      }    
    
      
      
    /**
    Returns job import batch list
    2012-10-01 © Vic <v.rutkunas@pccsuk.com>
    */  
      
    public function getBatchListAction() {
    
	$job_model = $this->loadModel("Job");
	echo(json_encode($job_model->getBatchList()));
	
    }
	

    
    /**
    Returns jobs for homepage Open Jobs facility
    2012-10-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function getJobsAction() {
	
	$jobModel = $this->loadModel("Job");
        
        $status = $_GET["status"];
        
        if($status == "awaiting_parts" || $status == "in_store" || $status == "with_supplier" || $status == "awaiting_collection" || $status == "customer_notified") {
            $result = $jobModel->getJobsByStatus($this->user, $status);
        } else if ($status == "auth_required") {
            $result = $jobModel->getJobsAuthRequired($this->user);
	} else if($status == "query") {
            $result = $jobModel->getJobsQuery($this->user);
	} else if($status == "appraisal_required") {
	    $result = $jobModel->getJobsAppraisalRequired($this->user);
	} else if($status == "branch_repair") {
	    $result = $jobModel->getJobsBranchRepair($this->user);
	} else {
            $result = $jobModel->getOverdueJobs($this->user, $status);
        }
	
	$return = json_encode($result);
	
	echo $return;
	
    }
    

    
    /**
    @action bulk job.OpenJobStatus update
    @input  list of job IDs
    @return void
    @output success true/false
    2012-11-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function updateOpenJobsStatusAction() {
	
	$jobModel = $this->loadModel("Job");
	$result = $jobModel->updateOpenJobsStatus($_POST);
	echo ($result) ? 1 : 0;	
	
    }

    
    
    /**
    @action get open job count by job.OpenJobStatus type
    @input  void
    @return void
    @output job count for each status
    2012-10-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function refreshStatusNumbersAction() {
	
	$job_model = $this->loadModel("Job");
	
	$appraisalRequired = $job_model->getJobsAppraisalRequiredNumber($this->user);
	$authRequired = $job_model->getJobsAuthRequiredNumber($this->user);
	$inStore = $job_model->getJobsByStatusNumber($this->user, "in_store");
        $awaitingParts = $job_model->getJobsByStatusNumber($this->user, "awaiting_parts");
	$withSupplier = $job_model->getJobsByStatusNumber($this->user, "with_supplier");
	$awaitingCollection = $job_model->getJobsByStatusNumber($this->user, "awaiting_collection");
	$customerNotified = $job_model->getJobsByStatusNumber($this->user, "customer_notified");
        $branchRepair = $job_model->getJobsBranchRepairNumber($this->user);
	
	$result = [ 
	    "appraisalRequired" => $appraisalRequired,
	    "branchRepair" => $branchRepair,
	    "authRequired" => $authRequired,
            "awaitingParts" => $awaitingParts, 
	    "inStore" => $inStore, 
	    "withSupplier" => $withSupplier, 
	    "awaitingCollection" => $awaitingCollection,
	    "customerNotified" => $customerNotified,
	    "total" => $appraisalRequired + $authRequired + $inStore + $awaitingParts + $withSupplier + $awaitingCollection + $customerNotified
	];
	
	echo json_encode($result);
	
    }
    

    
    /**
    Sets Authorisation number
    2012-12-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function setAuthNoAction() {
        
        $authNo = $_POST["authNo"];
        $jobID = $_POST["jobID"];
	
	$jobModel = $this->loadModel("Job");
	
	$jobModel->setRAStatus($jobID, "confirmed");
	
	$jobModel->processRAAccepted($jobID);
	
        $result = $jobModel->setAuthNo($jobID, $authNo);
	
        echo($result);
        
    }
    

    
    /**
    Process Repair Authorisaton Accepted
    2012-12-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function processRAAcceptedAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobID = $_POST["jobID"];
	$jobModel->processRAAccepted($jobID);
	
    }
    

    
    /**
    Process Repair Authorisaton Declined
    2012-12-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function authRejectedAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobID = $_POST["jobID"];
	$text = $_POST["reasonText"];
	$jobModel->authRejected($jobID, $text);
	
    }
    

    
    /**
    @input  job search parameters
    @return void
    @output list of service providers from current job search
    2012-10-01 © Vic <v.rutkunas@pccsuk.com>
    */  

    public function getServiceCentersFromSearchAction($params) {
	
	$args['jobSearch'] = isset($params[0]) ? $params[0] : '';
	$args['activeFilter'] = isset($params[1]) ? $params[1] : '';
	$args['filterID'] = isset($params[2]) ? $params[2] : '';
	$args['batchID'] = isset($params[3]) ? $params[3] : '';
	
	$jobModel = $this->loadModel("Job");
	$result = $jobModel->getServiceCentersFromSearch($args);
	
	echo(json_encode($result));
	
    }
    

    
    /**
    @input  void
    @return void
    @output job requires Repair Authorisation true/false
    2012-12-01 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function getRARequiredAction() {
	
	$jobModel = $this->loadModel("Job");
	$result = $jobModel->getRARequired($_POST["id"]);
	echo($result) ? 1 : 0;
	
    }
    

    
    /**
    @action Generates Despatch Manifest PDF document and forces browser Open/Save file dialog
    @input  void
    @return void
    @output despatch manifest PDF doc
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    */  
    
    public function printDespatchManifestAction() {

	$brandModel = $this->loadModel("Brands");
	$brand = $brandModel->getBrandByID($this->user->DefaultBrandID);

	if(defined('SUB_DOMAIN')) {
	    $subdomain = SUB_DOMAIN;
	} else {
	    $subdomain = '';
	}

	if(isset($_SERVER['HTTPS'])) { 
	    $domainName = 'https://' . $_SERVER["HTTP_HOST"] . $subdomain . "/";
	} else { 
	    $domainName = 'http://' . $_SERVER["HTTP_HOST"] . $subdomain . "/";
	} 
	
	$brandLogo = $domainName . $this->config['Path']['brandLogosPath'];
	if($brand['BrandLogo']) {    
	    if(!file_exists(APPLICATION_PATH . "/../" . $this->config['Path']['brandLogosPath'] . $brand['BrandLogo'])) {
		$brand['BrandLogo'] = 'default_logo.png';
	    }        
	    $brandLogo = $brandLogo . $brand['BrandLogo'];
	} else {
	    $brandLogo = $brandLogo . 'default_logo.png';
	}
	$this->smarty->assign('brandLogo', $brandLogo);
	
	$branchModel = $this->loadModel("Branches");
	$branch = $branchModel->getBranchByID($this->user->BranchID);
	$this->smarty->assign("branch", $branch);
	
	$jobModel = $this->loadModel("Job");//
	$jobs = $jobModel->getJobs(json_decode($_GET["data"]));
	$this->smarty->assign("jobs", $jobs);

	$serviceModel = $this->loadModel("ServiceProviders");
	$serviceProvider = $serviceModel->getServiceProvider($jobs[0]["ServiceProviderID"]);
	$this->smarty->assign("serviceProvider", $serviceProvider);
	
	$this->smarty->assign("userName", $this->user->Username);
	
	$html = $this->smarty->fetch('despatchManifest.tpl');
	
	include('libraries/MPDF56/mpdf.php');
	$pdf = new mPDF();
	$pdf->WriteHTML($html);
	$pdf->Output();
	exit;
	
    }
    

    
    /**
    @action get audit trail data by job.JobID for job table
    @input  job ID
    @output void
    @return audit trail data
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getAuditTrailByJobIDAction() {
	
	$auditModel = $this->loadModel("Audit");
	$result = $auditModel->getAuditTrailByJobID($_GET["jobID"]);
	echo(json_encode(["aaData" => $result]));
	
    }
    
   
    
    /**
    @action get update a part modal window template (Job Update page)
    @input  part ID
    @output html template
    @return void
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    2013-04-25 © Andris <a.polnikovs@pccsuk.com>
    */

    public function getUpdatePartModalAction($args) {
	if(!isset($this->user->ServiceProviderID)){$this->user->ServiceProviderID=0;}
	$this->page = $this->messages->getPage("jobDetails", $this->lang);
	$this->smarty->assign("page", $this->page);
        $jobModel = $this->loadModel("Job");
        $partModel = $this->loadModel("Part");
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$orderStatusModel = $this->loadModel("PartOrderStatus");
        $supplier_model     = $this->loadModel('Suppliers');
        $serviceProviders_model     = $this->loadModel('ServiceProviders');
        $stock_model     = $this->loadModel('Stock');
	$modelsModel =$this->loadModel('Models');
        $skyline_model=$this->loadModel('Skyline');
       if(isset($_POST["jobID"])){
           $jobid=$_POST["jobID"];
           $partid=$_POST["partID"];
       }else{
            $jobid=$args["jobID"];
           $partid=$args["partID"];
       }
       
        $SPModelList=$modelsModel->getAllSPModels($this->user->ServiceProviderID);
        $job = $jobModel->fetch($jobid);
	$part = $partModel->getPartByID(trim($partid));
      
            $PartSectionCodes = $jobUpdateModel->getPartSectionCodes($job);
            $this->smarty->assign("SectionCodes", $PartSectionCodes);
            $PartDefectCodes = $jobUpdateModel->getPartDefectCodes($job);
            $this->smarty->assign("DefectCodes", $PartDefectCodes);
            $PartRepairCodes = $jobUpdateModel->getPartRepairCodes($job);
            $this->smarty->assign("RepairCodes", $PartRepairCodes);
            $OrderStatus=$orderStatusModel->getPartOrderStatusList('addPart');
            $this->smarty->assign("OrderStatus", $OrderStatus);
            $AutoReorderStatus=$serviceProviders_model->getServiceProviderAutoReorderStatus($this->user->ServiceProviderID);
            $this->smarty->assign("AutoReorderStatus", $AutoReorderStatus);
            $suppliers  = $supplier_model->getSPSuppliers($this->user->ServiceProviderID);
            
            $this->smarty->assign('suppliers', $suppliers);
            $this->smarty->assign('SPModelList', $SPModelList);
            $this->smarty->assign('job', $job);
            $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
            
            $jobChargeType=$skyline_model->getJobChargeType($jobid);
            $this->smarty->assign('jobChargeType', $jobChargeType);
                        
            $partsUsed=$jobModel->getPartsUsed($jobid);
            
            $stockAvailable=$stock_model->checkStockAvailable($this->user->ServiceProviderID);
            
            $spInfo=$serviceProviders_model->getServiceProvider($this->user->ServiceProviderID);
            
            $this->smarty->assign("spInfo", $spInfo);
            $this->smarty->assign("stockAvailable", $stockAvailable);
            $this->smarty->assign("data", $part);
	    $this->smarty->assign("partsUsed", sizeof($partsUsed));
            $this->smarty->assign("serviceType", $job['ServiceTypeID']);
	
	if($part==""){
            $this->smarty->assign("mode", "new");
            
        }else{
            
            
             $this->smarty->assign("mode", "update");
        }
        if(isset($args['mode'])&&$args['mode']=="codeOnly"){
            $this->smarty->assign("mode", "codeOnly");
        }
	$this->smarty->assign("JobID", (isset($jobid) ? $jobid : ""));
	
	$this->smarty->display('updatePartForm.tpl');
	
	
    }

    
    
    /**
    @action create/update part (Job Update page)
    @input  part data
    @output void
    @return void
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    2013-05-17 © Andris <a.polnikovs@pccsuk.com>
    */

    public function updatePartAction() {
	 $stockModel = $this->loadModel("Stock");
	$partModel = $this->loadModel("Part");
	$this->log($_POST,"post");
        if(isset($_POST['SpPartStockTemplateID'])&&$_POST['SpPartStockTemplateID']!=""&&!isset($_POST['IsOtherCost'])){
           
            $ret=$stockModel->makePartsUsed($_POST);
           
            if($ret){
                if($_POST['PartID']==""){//check if assigning new part, not updating
               
                    if($_POST['instockval']<$_POST['Quantity']){ //check if more parts need then in stock
                        $partsOrderNeedet=$_POST['Quantity']-$_POST['instockval'];
                        $_POST['Quantity']=$_POST['instockval'];
                        if($_POST['instockval']!=0){
                         $insertPartID=$partModel->updatePart($_POST);
                         $stockModel->setItemPartID($insertPartID,11);
                         
                        }
                          $_POST['Quantity']=$partsOrderNeedet;
                          
                          
                         
                          
                           $templateData=$stockModel->getSpPartTemplateDataFromId($this->user->ServiceProviderID,$_POST['SpPartStockTemplateID']);
                               // if(isset($templateData['DefaultServiceProviderSupplierID'])){//need make check if supplier is samsung at moment only samsung can be
                            
                             //if autoOrder stock if supplier is samsung;
                            if($this->user->ServiceProviderID==134){//only samsung exp store auto gspn order
                             $orderID=$stockModel->createSingleGSPNPartOrder($templateData['DefaultServiceProviderSupplierID']);
                            }else{
                                $orderID=NULL;
                            }
                               $_POST['OrderNo']=$orderID;
                              $_POST['PartStatusID']=2;
                              $insertPartID=$partModel->updatePart($_POST);
                              $p=[
                                  "SpPartStockTemplateID"=>$_POST['SpPartStockTemplateID'],
                                  "qty"=>$_POST['Quantity'],
                                  "SPPartOrderID"=>$orderID,
                                  "JobID"=>$_POST['JobID'],
                              ];
                              $stockModel->insertStockOrderReqItem($this->user->ServiceProviderID,$p);
                              $stockModel->setItemPartID($insertPartID,2);
                              echo"$insertPartID";
                         }else{
                        
                        
                        
                        $insertPartID=$partModel->updatePart($_POST); //quantity enought
                         $stockModel->setItemPartID($insertPartID,11);
                         
                         
                         
                         if(isset($_POST['AutoReorder'])&&$this->user->ServiceProviderID==134){//only samsung exp store
                            
                             //if autoreorder is set to true then create part with status order required and create order;
                             $templateData=$stockModel->getSpPartTemplateDataFromId($this->user->ServiceProviderID,$_POST['SpPartStockTemplateID']);
                             $orderID=$stockModel->createSingleGSPNPartOrder($templateData['DefaultServiceProviderSupplierID']);
                              $_POST['OrderNo']=$orderID;
                              $_POST['PartStatusID']=2;
                              $_POST['JobID']=null;
                              $insertPartID=$partModel->updatePart($_POST);
                              $p=[
                                  "SpPartStockTemplateID"=>$_POST['SpPartStockTemplateID'],
                                  "qty"=>$_POST['Quantity'],
                                  "SPPartOrderID"=>$orderID,
                                  "JobID"=>$_POST['JobID'],
                              ];
                              $stockModel->insertStockOrderReqItem($this->user->ServiceProviderID,$p,$insertPartID);
                               $stockModel->setItemPartID($insertPartID,2);
                              echo"$insertPartID";
                         }
                         
                         
                         
                    }
                    
                }else{
                     
                $partModel->updatePart($_POST); //update  
                }
            }
        }else{
            $partModel->updatePart($_POST); //insert update for non stock items
        }
	
    }
    
    
    
    /**
    @action delete part (Job Update page)
    @input  part ID
    @output void
    @return void
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    */

    public function deletePartAction() {
	
	$partModel = $this->loadModel("Part");
        $stockModel = $this->loadModel("Stock");
        $stockModel->puttPartBackToStock($_POST["partID"],$_POST["JobID"]);
        if ($this->debug) $this->log($_POST,"delpart");
        $partModel->deletePart($_POST["partID"]);
	
    }
    
    /**
    @action fit part (Job Update page/Servicebase Page)
    @input  part ID
    @output void
    @return void
    2013-05-22 Andris <a.polnikovs@pccsuk.com>
    */
    public function fitPartAction(){
         $stockModel = $this->loadModel("Stock");
        $stockModel->changePartsStatus(array($_POST['partID']),11);
    }


    
    /**
    @action get entity status list from status_permission table (Job Update page)
    @input  void
    @output entity status list
    @return void
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getEntityStatusListAction() {
	
	$statusModel = $this->loadModel("SystemStatuses");
	$statusList = $statusModel->getEntityStatusList($this->user);
	$this->smarty->assign("statusList", $statusList);

	$jobModel = $this->loadModel("Job");
	$job = $jobModel->fetch($_POST["jobID"]);
	$this->smarty->assign("job", $job);
	
	$this->page = $this->messages->getPage("jobDetails", $this->lang);
	$this->smarty->assign("page", $this->page);
	
	$this->smarty->assign("JobID", (isset($_POST["jobID"]) ? $_POST["jobID"] : ""));
	
	$html = $this->smarty->fetch('editJobStatusForm.tpl');
	echo($html);
	
    }
    
    
    
    /**
    @action updates job status (Job Update page)
    @input  job ID, status ID
    @output void
    @return void
    2013-01-31 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function updateJobStatusAction() {
	
	$closed = (isset($_GET["closed"])) ? true : false;
	
	$jobModel = $this->loadModel("Job");
	$jobModel->updateJobStatus($_POST, $closed);
	
    }    
    
  
    
    /**
    @action ends Service Appraisal
    @input  job data
    @output void
    @return void
    2013-02-14 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function endAppraisalAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobModel->endAppraisal($_POST);
	
    }    

    
    
    /**
    @action updates job.Accessories
    @input  accessories text, job ID
    @output void
    @return void
    2013-02-19 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function editAccessoriesAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobModel->editAccessories($_POST["jobID"], $_POST["text"]);
	
    }
    

    
    /**
    @action updates job.UnitCondition
    @input  condition text, job ID
    @output void
    @return void
    2013-02-19 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function editConditionAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobModel->editCondition($_POST["jobID"], $_POST["text"]);
	
    }
    
    
   /**
    * closedJobsAction
    * 
    * displays Closed Jobs page
    *
    * @param array $args
    * @return void
    * 
    * @author Vykintas Rutkunas <v.rutkunas@pccsuk.com>
    ***************************************************************************/
  
//-----alter closedJobsAction fumction fro new functionality of gauge preferences.
    public function closedJobsAction($args) {
	
        //---unset session values when click on show all----------------------------
        if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='s'){
            unset($_SESSION['closedjobs']['summary']);
        }
        else if(isset($args[0]) && strtolower($args[0]) == "showall" && isset($args['ojvBy']) && $args['ojvBy'] =='d'){
            unset($_SESSION['closedjobs']['detail']);
        }
        else if(isset($args[0]) && strtolower($args[0]) == "showall" ){
            unset($_SESSION['closedjobs']);
        }
        
        ///table display preferences ses vars
        
        $datatable= $this->loadModel('DataTable');
            
        $this->smarty->assign('AP9000', Functions::hasPermission($this->user, 'AP9000'));     /* Servicebase Refresh */
        $this->smarty->assign('AP9001', Functions::hasPermission($this->user, 'AP9001'));     /* Servicebase Bulk Refresh */
        
        /*
         * Find Tat setting for closed jobs
         */
        
        $job_model = $this->loadModel('Job');
        $Job = $this->loadModel('Job');
        $users_model = $this->loadModel('Users');
        
        $this->smarty->assign('loggedin_user', $this->user);
        $gaugePrefs = $users_model->getGaugePreferences( $this->user->UserID );
        
//        echo '<pre>';
//        print_r($gaugePrefs);
//        echo '</pre>';
        
        //----completion status for gauge preferences
        $SkylineModel = $this->loadModel('Skyline');
        $completionStatusResult = $SkylineModel->getCompletionStatuses();
        $completionStatus = array(''=>'Choose Closed Status');
        foreach($completionStatusResult as $ini=>$ival){
            $gp['id'][] = $ival['CompletionStatusID'];
            $gp['name'][] = $ival['CompletionStatusName'];
            $completionStatus[$ival['CompletionStatusID']] = $ival['CompletionStatusName'];
        }
        $this->smarty->assign('completionStatus',$completionStatus);
        
        if(count($gaugePrefs) == 0){ 

            for($i=0;$i<count($gp['id']);$i++){ 
                if($i < 5){
                    $gaugePrefs[] = array(
                        'GaDialStatusID' => $gp['id'][$i],
                        'GaDialStatus' => $gp['name'][$i],
                        'GaDialYellow' => 0,
                        'GaDialRed' => 0,
                        'GaDialSwap' => 0,
                        'GaDialOrder' => 0
                    );
                }
            }
            

            $gaugePrefs[] = array(

                'GaDialStatusID' => 0,
                'GaDialStatus' => "All Others",
                'GaDialYellow' => 0,
                'GaDialRed' => 0,
                'GaDialSwap' => 0,
                'GaDialOrder' => 0
            );
        }
        
        $this->smarty->assign('gaugePrefs', $gaugePrefs);
        $page    = $this->messages->getPage('closedJobs', $this->lang);
            
        $this->session->mainTable="closedJob";
        $this->session->mainPage="closedJobs";
        $this->page =  $page;
            
        //store TAT View in Session
        $args['ojvBy'] = isset($args['ojvBy']) ? $args['ojvBy'] : (isset($_SESSION['closedjobs']['ojvBy'])?$_SESSION['closedjobs']['ojvBy']: 's');
        $_SESSION['closedjobs']['ojvBy'] = $args['ojvBy'];
            
        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'ClosedJobs'));
              
        if($args['ojvBy'] == "s"){
            $keys_data = $Job->getSummaryFields($page,$TatResult,"closedJobs",$args);
        }
        else{
            $datatable= $this->loadModel('DataTable');
            $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID']);
        }
        
//        echo "<pre>";
//        print_r($keys_data);
//        echo "</pre>";
        
        $SystemStatusesModel   = $this->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('closedJobs', 'js');
        
        if(isset($keys_data[1])){
//        $keys=$keys_data[1];
            $keys=  array_values($keys_data[1]);
        }else{
        $keys=array();
        }
        
        $this->smarty->assign('data_keys', $keys);
               ////table display preferences ses vars end
        
        //----------store filter value in session----------------------------------            
        if($args['ojvBy'] == "s"){
            $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['closedjobs']['summary']['network'])?$_SESSION['closedjobs']['summary']['network']: 0);
            $_SESSION['closedjobs']['summary']['network'] = $args['network'];

            $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['closedjobs']['summary']['branch'])?$_SESSION['closedjobs']['summary']['branch']: 0);
            $_SESSION['closedjobs']['summary']['branch'] = $args['branch'];

            $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['closedjobs']['summary']['client'])?$_SESSION['closedjobs']['summary']['client']: 0);
            $_SESSION['closedjobs']['summary']['client'] = $args['client'];

            $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['closedjobs']['summary']['unitType'])?$_SESSION['closedjobs']['summary']['unitType']: 0);
            $_SESSION['closedjobs']['summary']['unitType'] = $args['unitType'];

            $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['closedjobs']['summary']['skillSet'])?$_SESSION['closedjobs']['summary']['skillSet']: 0);
            $_SESSION['closedjobs']['summary']['skillSet'] = $args['skillSet'];

            $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['closedjobs']['summary']['brand'])?$_SESSION['closedjobs']['summary']['brand']: 0);
            $_SESSION['closedjobs']['summary']['brand'] = $args['brand'];

            $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['closedjobs']['summary']['serviceProvider'])?$_SESSION['closedjobs']['summary']['serviceProvider']: 0);
            $_SESSION['closedjobs']['summary']['serviceProvider'] = $args['serviceProvider'];

            $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['closedjobs']['summary']['manufacturer'])?$_SESSION['closedjobs']['summary']['manufacturer']: 0);
            $_SESSION['closedjobs']['summary']['manufacturer'] = $args['manufacturer'];

            $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['closedjobs']['summary']['sType'])?$_SESSION['closedjobs']['summary']['sType']: 0);
            $_SESSION['closedjobs']['summary']['sType'] = $args['sType'];

            $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['closedjobs']['summary']['scType'])?$_SESSION['closedjobs']['summary']['scType']: 0);
            $_SESSION['closedjobs']['summary']['scType'] = $args['scType'];
            
            $args['cjBy'] = isset($args['cjBy']) ? $args['cjBy'] : (isset($_SESSION['closedjobs']['summary']['cjBy'])?$_SESSION['closedjobs']['summary']['cjBy']: 'g');
            $_SESSION['closedjobs']['summary']['cjBy'] = $args['cjBy'];

            $args['tView'] = isset($args['tView']) ? $args['tView'] : (isset($_SESSION['closedjobs']['summary']['tView'])?$_SESSION['closedjobs']['summary']['tView']: 's');
            $_SESSION['closedjobs']['summary']['tView'] = $args['tView'];

            $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['closedjobs']['summary']['btnName'])?$_SESSION['closedjobs']['summary']['btnName']: false);
            $_SESSION['closedjobs']['summary']['btnName'] = $args['btnName'];

            $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['closedjobs']['summary']['btnValue'])?$_SESSION['closedjobs']['summary']['btnValue']: false);
            $_SESSION['closedjobs']['summary']['btnValue'] = $args['btnValue'];

            $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['closedjobs']['summary']['btnValue2'])?$_SESSION['closedjobs']['summary']['btnValue2']: false);
            $_SESSION['closedjobs']['summary']['btnValue2'] = $args['btnValue2'];
            
            $args['openby'] = isset($args['openby']) ? $args['openby'] : (isset($_SESSION['closedjobs']['summary']['openby'])?$_SESSION['closedjobs']['summary']['openby']: 'sp');
            $_SESSION['closedjobs']['summary']['openby'] = $args['openby'];

            $args['display'] = isset($args['display']) ? $args['display'] : (isset($_SESSION['closedjobs']['summary']['display'])?$_SESSION['closedjobs']['summary']['display']: 'mtd');
            $_SESSION['closedjobs']['summary']['display'] = $args['display'];

            $args['datefrom'] = isset($args['datefrom']) ? $args['datefrom'] : (isset($_SESSION['closedjobs']['summary']['datefrom'])?$_SESSION['closedjobs']['summary']['datefrom']: (date('m') == '01'?(date('Y')-1)."-01-01":(date('Y')."-01-01" )));
            $_SESSION['closedjobs']['summary']['datefrom'] = $args['datefrom'];

            $args['displaylength'] = isset($args['displaylength']) ? intval($args['displaylength']) : (isset($_SESSION['closedjobs']['summary']['displaylength'])?$_SESSION['closedjobs']['summary']['displaylength']: 10);
            $_SESSION['closedjobs']['summary']['displaylength'] = $args['displaylength'];

            $args['dateto'] = isset($args['dateto']) ? $args['dateto'] : (isset($_SESSION['closedjobs']['summary']['dateto'])?$_SESSION['closedjobs']['summary']['dateto']: (date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y")))));
            $_SESSION['closedjobs']['summary']['dateto'] = $args['dateto'];

            $args['guage'] = isset($args['guage']) ? $args['guage'] : (isset($_SESSION['closedjobs']['summary']['guage'])?$_SESSION['closedjobs']['summary']['guage']: 'all');
            $_SESSION['closedjobs']['summary']['guage'] = $args['guage'];
            
            $args['showguage'] = isset($args['showguage']) ? $args['showguage'] : (isset($_SESSION['closedjobs']['summary']['showguage'])?$_SESSION['closedjobs']['summary']['showguage']:0);
            $_SESSION['closedjobs']['summary']['showguage'] = $args['showguage'];
            
        }
        else{
            $args['network'] = isset($args['network']) ? $args['network'] : (isset($_SESSION['closedjobs']['detail']['network'])?$_SESSION['closedjobs']['detail']['network']: 0);
            $_SESSION['closedjobs']['detail']['network'] = $args['network'];

            $args['branch'] = isset($args['branch']) ? $args['branch'] : (isset($_SESSION['closedjobs']['detail']['branch'])?$_SESSION['closedjobs']['detail']['branch']: 0);
            $_SESSION['closedjobs']['detail']['branch'] = $args['branch'];

            $args['client'] = isset($args['client']) ? $args['client'] : (isset($_SESSION['closedjobs']['detail']['client'])?$_SESSION['closedjobs']['detail']['client']: 0);
            $_SESSION['closedjobs']['detail']['client'] = $args['client'];

            $args['unitType'] = isset($args['unitType']) ? $args['unitType'] : (isset($_SESSION['closedjobs']['detail']['unitType'])?$_SESSION['closedjobs']['detail']['unitType']: 0);
            $_SESSION['closedjobs']['detail']['unitType'] = $args['unitType'];

            $args['skillSet'] = isset($args['skillSet']) ? $args['skillSet'] : (isset($_SESSION['closedjobs']['detail']['skillSet'])?$_SESSION['closedjobs']['detail']['skillSet']: 0);
            $_SESSION['closedjobs']['detail']['skillSet'] = $args['skillSet'];

            $args['brand'] = isset($args['brand']) ? $args['brand'] : (isset($_SESSION['closedjobs']['detail']['brand'])?$_SESSION['closedjobs']['detail']['brand']: 0);
            $_SESSION['closedjobs']['detail']['brand'] = $args['brand'];

            $args['serviceProvider'] = isset($args['serviceProvider']) ? $args['serviceProvider'] : (isset($_SESSION['closedjobs']['detail']['serviceProvider'])?$_SESSION['closedjobs']['detail']['serviceProvider']: 0);
            $_SESSION['closedjobs']['detail']['serviceProvider'] = $args['serviceProvider'];

            $args['manufacturer'] = isset($args['manufacturer']) ? $args['manufacturer'] : (isset($_SESSION['closedjobs']['detail']['manufacturer'])?$_SESSION['closedjobs']['detail']['manufacturer']: 0);
            $_SESSION['closedjobs']['detail']['manufacturer'] = $args['manufacturer'];

            $args['sType'] = isset($args['sType']) ? $args['sType'] : (isset($_SESSION['closedjobs']['detail']['sType'])?$_SESSION['closedjobs']['detail']['sType']: 0);
            $_SESSION['closedjobs']['detail']['sType'] = $args['sType'];

            $args['scType'] = isset($args['scType']) ? $args['scType'] : (isset($_SESSION['closedjobs']['detail']['scType'])?$_SESSION['closedjobs']['detail']['scType']: 0);
            $_SESSION['closedjobs']['detail']['scType'] = $args['scType'];

            $args['cjBy'] = isset($args['cjBy']) ? $args['cjBy'] : (isset($_SESSION['closedjobs']['detail']['cjBy'])?$_SESSION['closedjobs']['detail']['cjBy']: 'g');
            $_SESSION['closedjobs']['detail']['cjBy'] = $args['cjBy'];

            $args['tView'] = isset($args['tView']) ? $args['tView'] : (isset($_SESSION['closedjobs']['detail']['tView'])?$_SESSION['closedjobs']['detail']['tView']: 's');
            $_SESSION['closedjobs']['detail']['tView'] = $args['tView'];

            $args['btnName'] = isset($args['btnName']) ? $args['btnName'] : (isset($_SESSION['closedjobs']['detail']['btnName'])?$_SESSION['closedjobs']['detail']['btnName']: false);
            $_SESSION['closedjobs']['detail']['btnName'] = $args['btnName'];

            $args['btnValue'] = isset($args['btnValue']) ? $args['btnValue'] : (isset($_SESSION['closedjobs']['detail']['btnValue'])?$_SESSION['closedjobs']['detail']['btnValue']: false);
            $_SESSION['closedjobs']['detail']['btnValue'] = $args['btnValue'];

            $args['btnValue2'] = isset($args['btnValue2']) ? $args['btnValue2'] : (isset($_SESSION['closedjobs']['detail']['btnValue2'])?$_SESSION['closedjobs']['detail']['btnValue2']: false);
            $_SESSION['closedjobs']['detail']['btnValue2'] = $args['btnValue2'];
            
            $args['openby'] = isset($args['openby']) ? $args['openby'] : (isset($_SESSION['closedjobs']['detail']['openby'])?$_SESSION['closedjobs']['detail']['openby']: 'sp');
            $_SESSION['closedjobs']['detail']['openby'] = $args['openby'];

            $args['display'] = isset($args['display']) ? $args['display'] : (isset($_SESSION['closedjobs']['detail']['display'])?$_SESSION['closedjobs']['detail']['display']: 'mtd');
            $_SESSION['closedjobs']['detail']['display'] = $args['display'];

            $args['datefrom'] = isset($args['datefrom']) ? $args['datefrom'] : (isset($_SESSION['closedjobs']['detail']['datefrom'])?$_SESSION['closedjobs']['detail']['datefrom']: (date('m') == '01'?(date('Y')-1)."-01-01":(date('Y')."-01-01" )));
            $_SESSION['closedjobs']['detail']['datefrom'] = $args['datefrom'];

            $args['displaylength'] = isset($args['displaylength']) ? intval($args['displaylength']) : (isset($_SESSION['closedjobs']['detail']['displaylength'])?$_SESSION['closedjobs']['detail']['displaylength']: 10);
            $_SESSION['closedjobs']['detail']['displaylength'] = $args['displaylength'];

            $args['dateto'] = isset($args['dateto']) ? $args['dateto'] : (isset($_SESSION['closedjobs']['detail']['dateto'])?$_SESSION['closedjobs']['detail']['dateto']: (date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y")))));
            $_SESSION['closedjobs']['detail']['dateto'] = $args['dateto'];

            $args['guage'] = isset($args['guage']) ? $args['guage'] : (isset($_SESSION['closedjobs']['detail']['guage'])?$_SESSION['closedjobs']['detail']['guage']: 'all');
            $_SESSION['closedjobs']['detail']['guage'] = $args['guage'];
            
            $args['showguage'] = isset($args['showguage']) ? $args['showguage'] : (isset($_SESSION['closedjobs']['detail']['showguage'])?$_SESSION['closedjobs']['detail']['showguage']:0);
            $_SESSION['closedjobs']['detail']['showguage'] = $args['showguage']; 
            
            if(isset($_SESSION['closedjobs']['detail']['JobID']) && $_SESSION['closedjobs']['detail']['JobID']){
                $this->smarty->assign('selectedJobId',$_SESSION['closedjobs']['detail']['JobID']);
            }
            else{
                $this->smarty->assign('selectedJobId',0);
            }
            
        }
        
        //----------End of store filter value in session----------------------------------
        
        $statsResult = $Job->fetchAllClosedStats( $args,$gaugePrefs,$completionStatus );
        

        if(isset($TatResult['TatID'])){
            $tempArgs = $args;
            $tempArgs['btnName']    = 'b1';
            $tempArgs['btnValue']   = $TatResult['Button1'];
            $tempArgs['btnValue2']  = false;

            $button1StatsResult = $Job->fetchAllClosedStats($tempArgs,$gaugePrefs,$completionStatus);
            $TatResult['Button1Value'] = 0;
            foreach($button1StatsResult AS $BS)
            {
                $TatResult['Button1Value'] += $BS[1];
            }    

            //Getting value in brackets..
            $tempArgs1 = $tempArgs;
            $tempArgs1['rcDate'] = 1;
            $button1StatsResult_1 = $Job->fetchAllClosedStats($tempArgs1,$gaugePrefs,$completionStatus);
            $b1bValue = 0;
            foreach($button1StatsResult_1 AS $BS)
            {
                $b1bValue += $BS[1];
            } 
            if($b1bValue)
            {
                $TatResult['Button1Value'] .= ' ['.$b1bValue.'] ';
            }    


            $tempArgs['btnName']    = 'b2';
            $tempArgs['btnValue']   = $TatResult['Button1'];
            $tempArgs['btnValue2']  = $TatResult['Button3'];

            $button2StatsResult = $Job->fetchAllClosedStats($tempArgs,$gaugePrefs,$completionStatus);
            $TatResult['Button2Value'] = 0;
            foreach($button2StatsResult AS $BS)
            {
                $TatResult['Button2Value'] += $BS[1];
            }

            //Getting value in brackets..
            $tempArgs1 = $tempArgs;
            $tempArgs1['rcDate'] = 1;
            $button2StatsResult_1 = $Job->fetchAllClosedStats($tempArgs1,$gaugePrefs,$completionStatus);
            $b2bValue = 0;
            foreach($button2StatsResult_1 AS $BS)
            {
                $b2bValue += $BS[1];
            } 
            if($b2bValue)
            {
                $TatResult['Button2Value'] .= ' ['.$b2bValue.'] ';
            }

            $tempArgs['btnName']    = 'b3';
            $tempArgs['btnValue']   = $TatResult['Button3'];
            $tempArgs['btnValue2']  = false;

            $button3StatsResult = $Job->fetchAllClosedStats($tempArgs,$gaugePrefs,$completionStatus);
            $TatResult['Button3Value'] = 0;
            foreach($button3StatsResult AS $BS)
            {
                $TatResult['Button3Value'] += $BS[1];
            } 

            //Getting value in brackets..
            $tempArgs1 = $tempArgs;
            $tempArgs1['rcDate'] = 1;
            $button3StatsResult_1 = $Job->fetchAllClosedStats($tempArgs1,$gaugePrefs,$completionStatus);
            $b3bValue = 0;
            foreach($button3StatsResult_1 AS $BS)
            {
                $b3bValue += $BS[1];
            } 
            if($b3bValue)
            {
                $TatResult['Button3Value'] .= ' ['.$b3bValue.'] ';
            }

            $tempArgs['btnName']    = 'b4';
            $tempArgs['btnValue']   = $TatResult['Button4'];
            $tempArgs['btnValue2']  = false;

            $button4StatsResult = $Job->fetchAllClosedStats($tempArgs,$gaugePrefs,$completionStatus);
            $TatResult['Button4Value'] = 0;
            foreach($button4StatsResult AS $BS)
            {
                $TatResult['Button4Value'] += $BS[1];
            }

            //Getting value in brackets..
            $tempArgs1 = $tempArgs;
            $tempArgs1['rcDate'] = 1;
            $button4StatsResult_1 = $Job->fetchAllClosedStats($tempArgs1,$gaugePrefs,$completionStatus);
            $b4bValue = 0;
            foreach($button4StatsResult_1 AS $BS)
            {
                $b4bValue += $BS[1];
            } 
            if($b4bValue)
            {
                $TatResult['Button4Value'] .= ' ['.$b4bValue.'] ';
            }
        }
        else{
            $TatResult = array(
                'TatID'=>'',
                'TatType'=>'ClosedJobs',
                'Button1'=>'',
                'Button3'=>'',
                'Button4'=>'',
                'Button1Colour'=>'',
                'Button2Colour'=>'',
                'Button3Colour'=>'',
                'Button4Colour'=>'',
                'Button1Value'=>'',
                'Button2Value'=>'',
                'Button3Value'=>'',
                'Button4Value'=>''
            );
        }
        
       
        
        for($r=0;$r<count($statsResult);$r++)
        {
            $statsResult[$r][4] = $statsResult[$r][0];
            $statsResult[$r][0] = $statsResult[$r][0]." (".$statsResult[$r][1].")";

        }
        
        $totalRows = 0;
        $selectedStatusClient = '';
        for($r=0;$r<count($statsResult);$r++)
        {

            if($args['sType'] && $args['sType']==$statsResult[$r][2])
            {
                $totalRows = $statsResult[$r][1];
                $selectedStatusClient = " ".$statsResult[$r][4];
                break;
            }
            else
            {
                $totalRows += $statsResult[$r][1];
            }
        }
        
        $this->smarty->assign('TatResult', $TatResult);
        $this->smarty->assign('hideNonSbJobs', isset($this->session->hideNonSbJobs)?$this->session->hideNonSbJobs:""); 
        
        $this->smarty->assign('manufacturer',$args['manufacturer']);
        $this->smarty->assign('client',$args['client']);
        $this->smarty->assign('network',$args['network']);
        $this->smarty->assign('branch',$args['branch']);
        $this->smarty->assign('unitType',$args['unitType']);
        $this->smarty->assign('skillSet',$args['skillSet']);
        $this->smarty->assign('brand', $args['brand']);
        $this->smarty->assign('serviceProvider', $args['serviceProvider']);
        
        $this->smarty->assign('sType', $args['sType']);
        $this->smarty->assign('scType', $args['scType']);
        $this->smarty->assign('cjBy', $args['cjBy']);
        $this->smarty->assign('ojvBy', $args['ojvBy']);
        $this->smarty->assign('tView', $args['tView']);
        
        $this->smarty->assign('btnName', $args['btnName']);
        $this->smarty->assign('btnValue', $args['btnValue']);
        $this->smarty->assign('btnValue2', $args['btnValue2']);
        
        $this->smarty->assign('statsResult', $statsResult);
        
        $this->smarty->assign('page', $page);
        
        $PreferredManufacturers    = $users_model->getPreferredClientGroup('Manufacturer','closedJobs');
        $PreferredBrands           = $users_model->getPreferredClientGroup('Brand','closedJobs');
        $PreferredServiceProviders = $users_model->getPreferredClientGroup('ServiceProvider','closedJobs');
        $PreferredNetworks         = $users_model->getPreferredClientGroup('Network','closedJobs');
        $PreferredBranches         = $users_model->getPreferredClientGroup('Branch','closedJobs');
        $PreferredClients          = $users_model->getPreferredClientGroup('Client','closedJobs');            
        $PreferredUnitTypes        = $users_model->getPreferredClientGroup('UnitType','closedJobs');
        $PreferredSkillset         = $users_model->getPreferredClientGroup('SkillSet','closedJobs');
        
        $SelectedManufacturers      = array();
        $SelectedBrands             = array();
        $SelectedServiceProviders   = array();
        $SelectedNetworks           = array();
        $SelectedBranches           = array();
        $SelectedClient             = array();
        $SelectedUnitTypes          = array();
        $SelectedSkillSet           = array();
        
        $manuPriority            = array();
        $brandPriority           = array();
        $serviceProviderPriority = array();
        $networkPriority         = array();
        $skillSetPriority        = array();
        $unitTypePriority        = array();
        $clientPriority          = array();
        $branchPriority          = array();
        
        
        
//        $jobStatusList = $SkylineModel->getSystemStatus();
        $jobStatusList = $SkylineModel->getCompletionStatuses();
        
        //Manufacturer List
        if($this->user->NetworkID)
        {
            $manufacturerList = $SkylineModel->getManufacturer('', $this->user->NetworkID);
        }
        else
        {
            $manufacturerList = $SkylineModel->getManufacturer('', null);
        }
        
        if(count($PreferredManufacturers)) {
            $ManufacturerGroupEnable = $PreferredManufacturers[0]['GroupEnable'];
            for($i=0;$i<count($manufacturerList);$i++) {
                $manufacturerList[$i]['Exists']   = false;
                $manufacturerList[$i]['Priority'] = '';

                foreach($PreferredManufacturers as $pm) {
                    if($manufacturerList[$i]['ManufacturerID']==$pm['PreferentialID']) {
                        $manufacturerList[$i]['Exists']   = true;
                        $manuPriority[] =  $manufacturerList[$i]['Priority'] = $pm['Priority'];

                        if(!$manufacturerList[$i]['ManufacturerLogo']) {
                            $manufacturerList[$i]['ManufacturerLogo'] = false;
                        }  
                        $SelectedManufacturers[] = $manufacturerList[$i];
                        break;
                    }
                }
            }

            if(count($SelectedManufacturers)>0) {
                array_multisort($manuPriority, SORT_ASC, $SelectedManufacturers);
            }
        }
        else {
            $ManufacturerGroupEnable = 0;
            for($i=0;$i<count($manufacturerList);$i++) {
                $manufacturerList[$i]['Exists'] = false;
                $manufacturerList[$i]['Priority'] = '';
            }
        }
        
        //brand List
        $brandsList = $SkylineModel->getBrands($this->user->NetworkID, $this->user->ClientID, $this->user->BranchID, $this->user->ManufacturerID, $this->user->ServiceProviderID);
        if(count($PreferredBrands)) {
            $BrandGroupEnable = $PreferredBrands[0]['GroupEnable'];
            for($i=0;$i<count($brandsList);$i++) {
                $brandsList[$i]['Exists'] = false;
                $brandsList[$i]['Priority'] = '';

                foreach($PreferredBrands as $pb) {
                    if($brandsList[$i]['BrandID']==$pb['PreferentialID']) {
                    $brandsList[$i]['Exists']   = true;
                    $brandPriority[] = $brandsList[$i]['Priority'] = $pb['Priority'];

                    if(!$brandsList[$i]['BrandLogo']) {
                        $brandsList[$i]['BrandLogo'] = false;
                    }    
                    $SelectedBrands[] = $brandsList[$i];
                    break;
                    }
                }
            }
            if(count($SelectedBrands)>0) {
                array_multisort($brandPriority, SORT_ASC, $SelectedBrands);
            }
        }  
        else {
            $BrandGroupEnable = 0;
            for($i=0;$i<count($brandsList);$i++) {
                $brandsList[$i]['Exists']   = false;
                $brandsList[$i]['Priority'] = '';
            }
        }
        
        //ServiceProvider List
        $ServiceProvidersModel = $this->loadModel('ServiceProviders');
        $serviceProvidersList  = $ServiceProvidersModel->getServiceProvidersHaveJobs($this->user);
        
        if(count($PreferredServiceProviders)) {
            $ServiceProviderGroupEnable = $PreferredServiceProviders[0]['GroupEnable'];
            for($i=0;$i<count($serviceProvidersList);$i++) {
                $serviceProvidersList[$i]['Exists'] = false;
                $serviceProvidersList[$i]['Priority'] = '';
                foreach($PreferredServiceProviders as $psp) {
                    if($serviceProvidersList[$i]['ServiceProviderID']==$psp['PreferentialID']) {
                        $serviceProvidersList[$i]['Exists']   = true;
                        $serviceProviderPriority[] = $serviceProvidersList[$i]['Priority'] = $psp['Priority'];
                        if(!isset($serviceProvidersList[$i]['ServiceProviderLogo']) || !$serviceProvidersList[$i]['ServiceProviderLogo']) {
                            $serviceProvidersList[$i]['ServiceProviderLogo'] = "no_logo.png";
                        }    
                        $SelectedServiceProviders[] = $serviceProvidersList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedServiceProviders)>0) {
                array_multisort($serviceProviderPriority, SORT_ASC, $SelectedServiceProviders);
            }
        }  
        else {
            $ServiceProviderGroupEnable = 0;
            for($i=0;$i<count($serviceProvidersList);$i++) {
                $serviceProvidersList[$i]['Exists']   = false;
                $serviceProvidersList[$i]['Priority'] = '';
            }
        }
        
        //Network    
        $networksList = $SkylineModel->getNetworks(null,$this->user);
        if(count($PreferredNetworks)) {
            $NetworkGroupEnable = $PreferredNetworks[0]['GroupEnable'];
            for($i=0;$i<count($networksList);$i++) {
                $networksList[$i]['Exists'] = false;
                $networksList[$i]['Priority'] = '';
                foreach($PreferredNetworks as $pb) {
                    if($networksList[$i]['NetworkID']==$pb['PreferentialID']) {
                        $networksList[$i]['Exists']   = true;
                        $networkPriority[] = $networksList[$i]['Priority'] = $pb['Priority'];
                        $SelectedNetworks[] = $networksList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedNetworks)>0) {
                array_multisort($networkPriority, SORT_ASC, $SelectedNetworks);
            }
        }  
        else {
            $NetworkGroupEnable = 0;
            for($i=0;$i<count($networksList);$i++) {
                $networksList[$i]['Exists']   = false;
                $networksList[$i]['Priority'] = '';
            }
        }

        //SkillSets
        $skillSetList = $SkylineModel->getRepairSkills();
        if(count($PreferredSkillset)) {
            $SkillSetGroupEnable = $PreferredSkillset[0]['GroupEnable'];
            for($i=0;$i<count($skillSetList);$i++) {
                $skillSetList[$i]['Exists'] = false;
                $skillSetList[$i]['Priority'] = '';
                foreach($PreferredSkillset as $pss) {
                    if($skillSetList[$i]['RepairSkillID']==$pss['PreferentialID']) {
                        $skillSetList[$i]['Exists']   = true;
                        $skillSetPriority[] = $skillSetList[$i]['Priority'] = $pss['Priority'];
                        $SelectedSkillSet[] = $skillSetList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedSkillSet)>0) {
                array_multisort($skillSetPriority, SORT_ASC, $SelectedSkillSet);
            }
        }  
        else {
            $SkillSetGroupEnable = 0;
            for($i=0;$i<count($skillSetList);$i++) {
                $skillSetList[$i]['Exists']   = false;
                $skillSetList[$i]['Priority'] = '';
            }
        }

        //Unit Types
        $unitTypesList = $SkylineModel->getUnitTypes();
        if(count($PreferredUnitTypes)) {
            $UnitTypeGroupEnable = $PreferredUnitTypes[0]['GroupEnable'];
            for($i=0;$i<count($unitTypesList);$i++) {
                $unitTypesList[$i]['Exists'] = false;
                $unitTypesList[$i]['Priority'] = '';
                foreach($PreferredUnitTypes as $punt) {
                    if($unitTypesList[$i]['UnitTypeID']==$punt['PreferentialID']) {
                        $unitTypesList[$i]['Exists']   = true;
                        $unitTypePriority[] = $unitTypesList[$i]['Priority'] = $punt['Priority'];
                        $SelectedUnitTypes[] = $unitTypesList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedUnitTypes)>0) {
                array_multisort($unitTypePriority, SORT_ASC, $SelectedUnitTypes);
            }
        }  
        else {
            $UnitTypeGroupEnable = 0;
            for($i=0;$i<count($unitTypesList);$i++) {
                $unitTypesList[$i]['Exists']   = false;
                $unitTypesList[$i]['Priority'] = '';
            }
        }

        //Clients
        $clientList = $SkylineModel->getClients($this->user);
        if(count($PreferredClients)) {
            $ClientGroupEnable = $PreferredClients[0]['GroupEnable'];
            for($i=0;$i<count($clientList);$i++) {
                $clientList[$i]['Exists'] = false;
                $clientList[$i]['Priority'] = '';
                foreach($PreferredClients as $clnt) {
                    if($clientList[$i]['ID']==$clnt['PreferentialID']) {
                        $clientList[$i]['Exists']   = true;
                        $clientPriority[] = $clientList[$i]['Priority'] = $clnt['Priority'];
                        $SelectedClient[] = $clientList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedClient)>0) {
                array_multisort($clientPriority, SORT_ASC, $SelectedClient);
            }
        }  
        else {
            $ClientGroupEnable = 0;
            for($i=0;$i<count($clientList);$i++) {
                $clientList[$i]['Exists']   = false;
                $clientList[$i]['Priority'] = '';
            }
        }

        //Branches
        $branchesList = $SkylineModel->getBranches($this->user->BranchID, '', $this->user->ClientID, $this->user->NetworkID);
        if(count($PreferredBranches)) {
            $BranchGroupEnable = $PreferredBranches[0]['GroupEnable'];
            for($i=0;$i<count($branchesList);$i++) {
                $branchesList[$i]['Exists'] = false;
                $branchesList[$i]['Priority'] = '';
                foreach($PreferredBranches as $pbrnch) {
                    if($branchesList[$i]['BranchID']==$pbrnch['PreferentialID']) {
                        $branchesList[$i]['Exists']   = true;
                        $branchPriority[] = $branchesList[$i]['Priority'] = $pbrnch['Priority'];
                        $SelectedBranches[] = $branchesList[$i];
                        break;
                    }
                }
            }
            if(count($SelectedBranches)>0) {
                array_multisort($branchPriority, SORT_ASC, $SelectedBranches);
            }
        }  
        else {
            $BranchGroupEnable = 0;
            for($i=0;$i<count($branchesList);$i++) {
                $branchesList[$i]['Exists']   = false;
                $branchesList[$i]['Priority'] = '';
            }
        }
        
        $this->smarty->assign('manufacturerList', $manufacturerList);
        $this->smarty->assign('brandsList', $brandsList);
        $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
        $this->smarty->assign('networkList', $networksList);
        $this->smarty->assign('branchList', $branchesList);
        $this->smarty->assign('clientList', $clientList);
        $this->smarty->assign('unitTypesList', $unitTypesList);
        $this->smarty->assign('skillSetList', $skillSetList);

        $this->smarty->assign('SelectedBrands', $SelectedBrands);
        $this->smarty->assign('SelectedManufacturers', $SelectedManufacturers);
        $this->smarty->assign('SelectedServiceProviders', $SelectedServiceProviders);
        $this->smarty->assign('SelectedNetworks', $SelectedNetworks);
        $this->smarty->assign('SelectedBranch', $SelectedBranches);
        $this->smarty->assign('SelectedClient', $SelectedClient);
        $this->smarty->assign('SelectedUnitType', $SelectedUnitTypes);
        $this->smarty->assign('SelectedSkillSet', $SelectedSkillSet);
        
        $this->smarty->assign('ServiceProviderGroupEnable', $ServiceProviderGroupEnable); 
        $this->smarty->assign('BrandGroupEnable', $BrandGroupEnable); 
        $this->smarty->assign('ManufacturerGroupEnable', $ManufacturerGroupEnable); 
        $this->smarty->assign('NetworkGroupEnable', $NetworkGroupEnable); 
        $this->smarty->assign('BranchGroupEnable', $BranchGroupEnable); 
        $this->smarty->assign('ClientGroupEnable', $ClientGroupEnable); 
        $this->smarty->assign('UnitTypeGroupEnable', $UnitTypeGroupEnable); 
        $this->smarty->assign('SkillSetGroupEnable', $SkillSetGroupEnable); 

        $SelectedBrandsCnt           = count($SelectedBrands);
        $SelectedManufacturersCnt    = count($SelectedManufacturers);
        $SelectedServiceProvidersCnt = count($SelectedServiceProviders);
        $SelectedNetworksCnt         = count($SelectedNetworks);
        $SelectedBranchesCnt         = count($SelectedBranches);
        $SelectedClientsCnt          = count($SelectedClient);
        $SelectedUnitTypesCnt        = count($SelectedUnitTypes);
        $SelectedSkillSetCnt         = count($SelectedSkillSet);

        $contentWidth  = $SelectedBrandsCnt;

        if($contentWidth<$SelectedManufacturersCnt) {
            $contentWidth = $SelectedManufacturersCnt;
        }    
        if($contentWidth<$SelectedServiceProvidersCnt) {
            $contentWidth = $SelectedServiceProvidersCnt;
        }
        if($contentWidth<$SelectedNetworksCnt) {
            $contentWidth = $SelectedNetworksCnt;
        }
        if($contentWidth<$SelectedBranchesCnt) {
            $contentWidth = $SelectedBranchesCnt;
        }
        if($contentWidth<$SelectedClientsCnt) {
            $contentWidth = $SelectedClientsCnt;
        }
        if($contentWidth<$SelectedUnitTypesCnt) {
            $contentWidth = $SelectedUnitTypesCnt;
        }
        if($contentWidth<$SelectedSkillSetCnt) {
            $contentWidth = $SelectedSkillSetCnt;
        }
        $contentWidth = $contentWidth*110;

        if($contentWidth<850)
        {
            $contentWidth = 850;
        }

        $this->smarty->assign('contentWidth', $contentWidth);
        
        if(count($UserStatusPreferences)){
            for($i=0;$i<count($jobStatusList);$i++)
            {
                $jobStatusList[$i]['Exists'] = false;
                foreach($UserStatusPreferences as $usf)
                {
                    if(in_array($jobStatusList[$i]['CompletionStatusID'], $usf))
                    {
                        $jobStatusList[$i]['Exists'] = true;
                        break;
                    }
                }
            }
        }
        else {
            for($i=0;$i<count($jobStatusList);$i++)
            {
                $jobStatusList[$i]['Exists'] = true;
            }
        }
        $this->smarty->assign('jobStatusList', $jobStatusList);
        
        $graph_title = $totalRows." ".$page['Text']['total_close_jobs'].$selectedStatusClient; 
        $this->smarty->assign('graph_title', $graph_title); 
/* Begin changes - Dials on Closed Job Page - A. Williams - 08/04/2013 */
//        $job_model = $this->loadModel('Job');
//        $users_model = $this->loadModel('Users');

        
        
        
        $openby = $args['openby'];                                          
        if ($openby == 'branch') {                                          /* Branch selected */
            $filter = "`ClosedDate` IS NOT NULL";
            $closedField = '`ClosedDate`';                                  /* Field in database we check for closed */
        } else {                                                            /* Otherwise service provider */
            $filter = '`ServiceProviderDespatchDate` IS NOT NULL';
            $closedField = '`ServiceProviderDespatchDate`';                 /* Field in database we check for closed */
        }
        
        $display = $args['display'];
        $datefrom = $args['datefrom'];
        $displaylength = $args['displaylength'];
        $dateto = $args['dateto'];
        $guage = $args['guage'];
        $showguage = $args['showguage'];
        
        
        $this->smarty->assign('openby',$openby);
        $this->smarty->assign('display',$display);
        $this->smarty->assign('displaylength',$displaylength);
        
        $this->smarty->assign('showguage', $showguage);
        
        switch ($display) {
            case 'ytd':
                $filter .= " AND YEAR(j.$closedField) = YEAR(CURDATE())";
                $this->smarty->assign('time_filter','Year to Date');
                
                $datefrom = Date("Y-1-1");
                $dateto = Date("Y-m-d");
                
                break;

            case 'lm':
                $filter .= " AND YEAR(j.$closedField) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH(j.$closedField) = MONTH(CURDATE() - INTERVAL 1 MONTH)";
                $this->smarty->assign('time_filter','Last Month');
                
                $datefrom = Date("Y-m-1",  strtotime("first day of last month"));
                $dateto = Date("Y-m-t",strtotime("last day of last month"));
                
                break;

            case '30':
                $filter .= " AND j.$closedField > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                $this->smarty->assign('time_filter','Last 30 Days');
                break;
            
            case 'dr':

                $filter .= " AND j.$closedField BETWEEN '$datefrom' AND '$dateto'";
                $this->smarty->assign('time_filter','Between '.date('d/m/Y', strtotime($datefrom)).' and '.date('d/m/Y', strtotime($dateto)));
                break;

            default: /* mtd */
                $filter .= " AND YEAR(j.$closedField) = YEAR(CURDATE()) AND MONTH(j.$closedField) = MONTH(CURDATE())";
                $this->smarty->assign('time_filter','Month to Date');
                
                $datefrom = Date("Y-m-1");
                $dateto = Date("Y-m-d");
        }
        
        $this->smarty->assign('datefrom',$datefrom);
        $this->smarty->assign('dateto',$dateto);
        
        
        $filter_guage = " AND j.CompletionStatus IS NOT NULL";
        
        if(isset($args['sType']) && $args['sType'] != '0') {
            if($args['sType'] == ""){
                $filter_guage .= ' AND j.CompletionStatus IS NULL';
            }
            else{
                if($args['sType']=="All Others"){
                    if(count($gaugePrefs) > 0){ 
                        $gaugePrefString="";
                        foreach($gaugePrefs as $ini=>$gaugePref){ 
                            if($ini < (count($gaugePrefs)-1)){
                                $gaugePrefString .= "'".$gaugePref['GaDialStatus']."',";
                            }
                        }
                    
                        $gaugePrefString = substr($gaugePrefString,0,-1);

                        if($gaugePrefString <>""){
                            $filter_guage .= " AND j.CompletionStatus NOT IN ({$gaugePrefString})";
                        }
                    }
                }
                else{
                    $filter_guage .= ' AND j.CompletionStatus=\'' .$args['sType'].'\'';
                }
            }
        }
        
        $daysFrom = 99999;      
        $daysTo = 0;
//        $this->smarty->assign('guage_filter',$guage_filter);
        $this->smarty->assign('guage',$guage);
        $this->smarty->assign('daysFrom',$daysFrom);
        $this->smarty->assign('daysTo',$daysTo);
                
        $filter .= " AND ABS(DATEDIFF($closedField, `DateBooked`)) >= $daysTo  AND ABS(DATEDIFF($closedField, `DateBooked`)) < $daysFrom ";
        
        $totalMatchingJobs = $job_model->getTotalMatchingJobs($filter.$filter_guage,$args);
        
        
        $this->smarty->assign('totalMatchingJobs', $totalMatchingJobs);
        
        /*Set Gauges values to draw Gauges*/
        
        foreach($gaugePrefs as $ini=>$gaugePref){ 

            $jobGaDial = intval($job_model->getNumberCompletedJobs($gaugePref['GaDialStatus'], $filter, $closedField,$args,$gaugePrefs));
            $gauge[] = array(
                'red' => $gaugePref['GaDialRed'],
                'yellow' => $gaugePref['GaDialYellow'],
                'Swap' => $gaugePref['GaDialSwap'],
                'min' => 0,
                'inc' => 10,                                    /* inc * div = max */
                'div' => 10,
                'value'=> ( $totalMatchingJobs == 0  ? 0 : number_format( ($jobGaDial / $totalMatchingJobs) * 100 , 2))
            );
        }
                
        
        $this->smarty->assign('gauge', $gauge);


        
        /* End changes - Dials on Closed Job Page - A. Williams - 08/04/2013 */
	
	$page = $this->messages->getPage("closedJobs", $this->lang);
	$this->smarty->assign("page", $page);
	
        if($args['ojvBy'] == "s"){
            $this->smarty->display("closedJobs_summary.tpl"); 
        }
        else{
            $this->smarty->display("closedJobs.tpl"); 
        }
    }

    


//----------add function to set session value for chart display
    public function setchartAction($args){ 

        if(isset($_REQUEST['ref'],$_REQUEST['id'])){
            switch ($_REQUEST['ref']){
                case "openJobs":
                    $_SESSION['openjob']['detail']['JobID'] = $_REQUEST['id'];
                break;
                case "overdueJobs":
                    $_SESSION['overduejob']['detail']['JobID'] = $_REQUEST['id'];
                break;
                case "closedJobs":
                    $_SESSION['closedjobs']['detail']['JobID'] = $_REQUEST['id'];
                break;
            }
        }
        if($_POST['ojvBy'] == "d"){
            $_SESSION['closedjobs']['detail']['cjBy'] = $_POST['cjBy'];
        }
        exit;
    }
    
    /**
    @action despatches jobs (home page tables)
    @input  json
    @output void
    @return void
    2013-03-21 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function despatchJobsAction() {
	
	$jobModel = $this->loadModel("Job");
	$jobModel->despatchJobs($_POST);
	
    }
    
    
     public function tableDisplayPreferenceSetupAction($args){ 
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
   
        $datatable= $this->loadModel('DataTable');
        
        $columnStrings=$datatable->getColumnStrings($this->user->UserID,$this->page['config']['pageID'],'user');
        $columnStringsSA=$datatable->getColumnStrings(1,$this->page['config']['pageID'],'sa');
        
        
       $keys=$datatable->getAllFieldNames($this->session->mainTable);
       if(isset($keys[1])){
         $this->smarty->assign('data_keys', $keys[1]);  
       }else{
        $this->smarty->assign('data_keys', $keys);
       }
       
        if($columnStrings){
        $columnDisplayString=  explode(",", $columnStrings['ColumnDisplayString']);
        $columnOrderString=explode(',',$columnStrings['ColumnOrderString']);
        $columnNameString=explode(",", $columnStrings['ColumnNameString']);
      
        
        }else{
           $columnDisplayString=array(); 
           $columnOrderString="";
           $columnNameString=array();
           
        }
        
        if($columnStringsSA){
        $columnDisplayStringSA=  explode(",", $columnStringsSA['ColumnDisplayString']);
        $columnOrderStringSA=explode(',',$columnStringsSA['ColumnOrderString']);
        $columnNameStringSA=explode(",", $columnStringsSA['ColumnNameString']);
        $columnStatusStringSA=explode(",", $columnStringsSA['ColumnStatusString']);
        $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        
        
        }else{
           $columnDisplayStringSA=array(); 
           $columnOrderStringSA="";
           $columnNameStringSA=array();
           
        }
       
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
       $this->smarty->assign('controller', "Job");
       $this->smarty->assign('admin',  $this->user->SuperAdmin);
     
        $this->smarty->assign('typeAction', $args['page']);
       
         $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');  
    }
    
    public function saveDisplayPreferencesAction($args){
       
         $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
          $model= $this->loadModel('DataTable');
       
        $model->saveDisplayPreferences($this->user->UserID,$this->page['config']['pageID'],$_POST);
         $this->redirect('JobController',$this->session->mainPage.'Action');
    }
    
    public function resetDisplayPreferencesAction($args)
    {
         $typeAction=$this->session->mainPage;
         
         $this->page =  $this->messages->getPage( $typeAction, $this->lang);
          $model= $this->loadModel('DataTable');
          
          $model->resetDisplayPreferences($this->user->UserID,$this->page['config']['pageID']);
           $this->redirect('JobController',$typeAction."Action");
    }
    
    
    
   /**
    * serviceBaseRefresh
    * 
    * Request a refresh from ServiceBase (ie Servicebase to call PutJobDetails 
    * with all the job data) for each job passed in the parameter.
    * 
    * @param array $args - 0 A list of job Ids seperatied by commas
    * @return void
    * 
    * @author Andrew J. Williams <a.williams@pccsuk.com>
    ***************************************************************************/
    
    public function serviceBaseRefreshAction($args) {
        $api_jobs_model = $this->loadModel('APIJobs');
        
        if ( !isset($args[0]) ) {
            return;                                                             /* No jobs passed so nothing to do */
        }
        $serviceBaseLink = (isset($args[1])?$args[1]:"");
        if (defined('SUB_DOMAIN')) {
            $sub_domain = SUB_DOMAIN;
        } else {
            $sub_domain = '';
        }
        if($serviceBaseLink == "")
            $okLink = "window.location.href='".$sub_domain."/Job/serviceBaseRefresh'; return false;";
        else
            $okLink = "return false;";
        $count = 0;                                                             /* Count of sent */
        $success = 0;                                                           /* Count of successfull */
        
        $jobIds = explode(',', $args[0]);                                       /* Passed job ids into array */
        
        $dlg = array();
        
        foreach ($jobIds as $jId) {                                             /* Loop through each passed JobId */
            $response = $api_jobs_model->skylineRefresh($jId);
            
            if ( strpos($response, ' has failed to refresh - ') === false) {                                         /* If not a file i.e. is a sucess */
                $success++;                                                     /* Increment success counter */
            } /* fi $response != 'FAIL' */
            $count++;                                                           /* Increment Counter */
            $dlg[] = "SL Job No: $jId $response \n";
        } /* next $jId */
        
        $this->smarty->assign('dlg', $dlg);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('success', $success);
        $this->smarty->assign('link', $okLink);
        
        $this->smarty->display('api/servicebase/servicebaseRefresh.tpl'); 
    }
    
    
    
    public function getQAQuestionsAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->getQAQuestions();
	echo json_encode($result);
	
    }
    
    
    
    public function updateQAQuestionsAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->updateQAQuestions($_POST["data"]);
	
    }
    

    
    public function getCompletionStatusesAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->getCompletionStatuses($_POST);
	echo json_encode($result);
	
    }
    
    
    
    public function loadSPPartsListAction($args)
            {
     
       $dd=$_GET;
     
        $datatable= $this->loadModel('DataTable');
       
       
      
    
       $columns=array("PartID","PartDescription","PartNo");
    
       
       
       //$joins="";
      
       $extraColumns=array(
           '<input type="checkbox">'
       );
        $data=$datatable->datatableSS('part',$columns,$dd,"",$extraColumns,0,false,"gg",false,false);
        if ($this->debug) $this->log($data,"dd");
        echo json_encode($data);
    }
    
  
    
    public function checkIfOrderSectionAction($args) {
	
	$orderStatusModel = $this->loadModel("PartOrderStatus");
        if($_POST["ID"]!=""){
	$resp = $orderStatusModel->checkIfOrderSection($_POST["ID"]);
        echo $resp;
        }
	
	
    }  

  
  
    public function serviceActionTrackingAction() {
	
	$this->page = $this->messages->getPage("serviceActionTracking", $this->lang);
	$this->smarty->assign("page", $this->page);
	//writted by srinivas for getting customer titles 
        $Skyline           = $this->loadModel('Skyline');
        $selectTitle       = $Skyline->getTitles();        
        $this->smarty->assign("selectTitle", $selectTitle);
        // end
	$skylineModel = $this->loadModel("SkylineQueriesModel");
	$serviceTypes = $skylineModel->getClientServiceTypes($this->user->ClientID, 1);
	$serviceTypes = array_map("unserialize", array_unique(array_map("serialize", $serviceTypes)));
	$this->smarty->assign("serviceTypes", $serviceTypes);
	
	$this->smarty->display("serviceActionTracking.tpl");
	
    }
    
    
    
    public function saveServiceActionTrackingAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->saveServiceActionTracking($_POST);
	
    }
    
    
    
    public function getRemoteEngineerHistoryAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->getRemoteEngineerHistory($_REQUEST["jobID"]);
	echo json_encode(["aaData" => $result]);
	
    }
    
    
    
    public function getSymptomCodesAction() {
	
	$jobModel = $this->loadModel("Job");
	$job = $jobModel->fetch($_REQUEST["jobID"]);
	
	$symptomConditionID = $_REQUEST["symptomConditionID"];
	$symptomTypeID = $_REQUEST["symptomTypeID"];
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->getSymptomCodes($job, $symptomConditionID, $symptomTypeID);
	
	echo json_encode($result);
	
    }
    
    /**
     * bulkSbRefreshAction
     *  
     * Allow entry of skyline job numbers into a text area to run a bulk refresh
     * of of jobs from skyline.
     * 
     * @param   Nothing
     * 
     * @return  Nothing
     *
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     **************************************************************************/

    public function bulkSbRefreshAction() {
        $page = $this->messages->getPage('bulkRefresh', $this->lang);
        
        $this->smarty->assign('page', $page);
        
        $this->smarty->display('api/servicebase/bulkRefresh.tpl'); 
    }
    
   /**
    * sbSingleJobRefreshAction
    * 
    * Request a refresh from ServiceBase (ie Servicebase to call PutJobDetails 
    * with all the job data) for each job passed in the parameter.
    * 
    * @param array $args - 0 A list of job Ids seperatied by commas
    * @return void
    * 
    * @author Andrew J. Williams <a.williams@pccsuk.com>
    ***************************************************************************/
    
    public function sbSingleJobRefreshAction($args) {
        $api_jobs_model = $this->loadModel('APIJobs');
        
        if ( !isset($args[0]) ) {
            return;                                                             /* No job passed so nothing to do */
        }

        $retval = $api_jobs_model->skylineRefresh($args[0]);
        if ($this->debug) $this->log($retval);
        echo $retval;
    }
    
    
    
     /**
     * settingsAction
     * 
     * This page is used for to setup job booking page settings
     * 
     * @param  array $args
     * @return void
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     ********************************************/
    public function settingsAction($args) { 
	
            $Skyline           = $this->loadModel('Skyline');
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                
                 if($this->user->SuperAdmin || ( ($this->user->NetworkID || $this->user->ClientID) && isset($this->user->Permissions['AP11009']) ))
                 {
                      $JobSettingsModel     = $this->loadModel('JobSettings');
                     
                      $fieldNames = $JobSettingsModel->fetchColumnNames();
                     
                      $dataArgs = array();
                      
                      //$this->log($_POST);
                      
                      foreach ($fieldNames as $fIndex=>$fValue)
                      {
                          if(isset($_POST[$fValue]))
                          {
                             // $this->log("Top:".$fValue);
                              if(strpos($fValue, 'Chk1') || strpos($fValue, 'Chk2') || strpos($fValue, 'Chk3'))
                              {
                                 $dataArgs[$fValue] = '1'; 
                               //  $this->log("Top-ca:".$fValue);
                              }        
                              else
                              {
                                $dataArgs[$fValue] = $_POST[$fValue];
                              }
                          }
                          else
                          {
                              if(strpos($fValue, 'Chk1') || strpos($fValue, 'Chk2') || strpos($fValue, 'Chk3'))
                              {
                                  $dataArgs[$fValue] = '0';
                              }
                              else
                              {
                                  //$this->log("Bottom:".$fValue);
                                  $dataArgs[$fValue] = NULL;
                              }
                              
                          }
                      }    
                      
                      if($dataArgs['JobTypeID'])
                      {
                         //Inserting Client Service Types.. 
                        
//                          if(!$this->user->BranchID)
//                          {    
//                                $service_type_list  = $Skyline->getServiceTypes(false, false, $_POST['NetworkID'], $_POST['ClientID']);
//
//                                $sltClientServiceTypes  = '';
//                                $sep = '';
//                                foreach($service_type_list as $stl)
//                                {
//                                   $sltClientServiceTypes =  $sltClientServiceTypes.$sep.$stl['ServiceTypeID'];
//                                }    
//
//                                $assignedClientServiceTypes = isset($_POST['ServiceTypeList'])?$_POST['ServiceTypeList']:''; 
//
//
//                                $ClientServiceTypes   =  $this->loadModel('ClientServiceTypes');
//                                $csresult      =  $ClientServiceTypes->updateClientServiceTypes($_POST['NetworkID'], $_POST['ClientID'], $assignedClientServiceTypes, $sltClientServiceTypes);
//
//                         }
                         
                          
                          
                          
                         $dateArray = explode("/", $dataArgs['DateOfPurchase']);
                         if(is_array($dateArray) && count($dateArray)==3)
                         {
                             $dataArgs['DateOfPurchase'] = $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
                         }    
                         
                         $isExists =  $JobSettingsModel->fetchRow($dataArgs);
                         
                         if(isset($isExists['JobSettingsID']) && $isExists['JobSettingsID'])
                         {
                             $dataArgs['JobSettingsID'] = $isExists['JobSettingsID'];
                         }    
                         
                         $result = $JobSettingsModel->processData($dataArgs);
                      }
                      else 
                      {
                           $result = array('status' => 'ERROR', 'message' => 'Sorry data has not been processed since there is a problem.');
                      }    
                     
                 }
                 else 
                 {
                      
                    $result = array('status' => 'ERROR', 'message' => 'Sorry you dont have permissions to update the data.');
                 }
           
               
                echo json_encode($result);

                return;
            }
        
            
            if($this->user->SuperAdmin || ( ($this->user->NetworkID || $this->user->ClientID) && isset($this->user->Permissions['AP11009']) ))
            {    
                   
                    $BranchModel       = $this->loadModel("Branches");
                    
                    $datarow = array();
                    if( (isset($args[0]) && $args[0]) || (isset($args['JobTypeID']) && $args['JobTypeID']) )
                    {
                        
                        $JobSettingsModel     = $this->loadModel('JobSettings');
                        
                        if(isset($args[0]))
                        {    
                            $datarow = $JobSettingsModel->fetchRow(array('JobSettingsID'=>$args[0]));
                        }
                        else if(isset($args['JobTypeID']))
                        {
                            $datarow = $JobSettingsModel->fetchRow(array('JobTypeID'=>$args['JobTypeID'], 'NetworkID'=>$this->user->NetworkID, 'ClientID'=>$this->user->ClientID));
                        }
                        
                        if(count($datarow))
                        {
                           $datarow['OriginalRetailerFormElement'] = '';
                          
                           $service_type_rows  = $Skyline->getServiceTypes(false, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                           
                           
                           $ServiceTypeList = array();
                           foreach($service_type_rows as $st)
                           {
                               $ServiceTypeList[] = $st['ServiceTypeID'];
                           }    
                           
                           $datarow['ServiceTypeList'] = $ServiceTypeList;
                            
                        }    
                        
                    }
                    
                    
                   


                    if(!$this->user->NetworkID)
                    {    
                        $showNetworkDropDown = true; 
                        $networks  = $Skyline->getNetworks();
                        $this->smarty->assign('networks', $networks); 
                    }
                    else
                    {
                        $showNetworkDropDown = false; 

                        $this->smarty->assign('NetworkName', $Skyline->getNetworkName($this->user->NetworkID));
                    }

                    
                   // $this->log($networks);
                    
                    if($this->user->ClientID)
                    {
                        $showClientDropDown = false;

                        $DefaultBranchID  = $Skyline->getClientDefaultBranch($this->user->ClientID);

                        $this->smarty->assign('ClientName', $Skyline->getClientName($this->user->ClientID));
                    } 
                    else
                    {
                         $showClientDropDown = true;  
                         
                         $nID = $this->user->NetworkID;
                         
                         if(!$nID && isset($datarow['NetworkID']) && $datarow['NetworkID'])
                         {    
                             $nID = $datarow['NetworkID'];
                         }
                         
                         if($nID)
                         {
                             $networkClients     = $Skyline->getNetworkClients($nID);

                         }
                         else
                         {
                             $networkClients = array();
                         }


                         $this->smarty->assign('networkClients', $networkClients);
                    }

                  //  $this->log($networkClients);
                    
                    if($this->user->BranchID)
                    {
                        $showBranchDropDown = false;
                        $BranchesModel      = $this->loadModel('Branches');

                        $BDetails         = $BranchesModel->fetchRow(array('BranchID'=>$this->user->BranchID));

                        $this->smarty->assign('BranchName', ($BDetails['BranchName'])?$BDetails['BranchName']:'');
                    } 
                    else
                    {
                         $showBranchDropDown = true;   
                         
                         $cID = $this->user->ClientID;
                         
                         if(!$cID && isset($datarow['ClientID']) && $datarow['ClientID'])
                         {    
                             $cID = $datarow['ClientID'];
                         }
                         
                         
                         if($cID)
                         {    
                            $branches     = $Skyline->getClientBranches($cID);
                         }
                         else
                         {
                             $branches = array();
                         }

                         $this->smarty->assign('branches', $branches);
                    }

                   // $this->log($branches);


                    $this->smarty->assign("showNetworkDropDown", $showNetworkDropDown);
                    $this->smarty->assign("showClientDropDown", $showClientDropDown);
                    $this->smarty->assign("showBranchDropDown", $showBranchDropDown);


                    $selectTitle     = $Skyline->getTitles();
                    $this->smarty->assign("selectTitle", $selectTitle);

                    //$jobTypes = $Skyline->getJobTypes();
                    //$this->smarty->assign("jobTypes", $jobTypes);

                    $afn_product = $this->messages->getAFN('AFN-PRODUCT');
                    $afn_stock_code = $this->messages->getAFN('AFN-STOCK CODE');


                    $this->page = $this->messages->getPage("JobSettings", $this->lang);

                    $page = $this->page;

                    $page['Labels']['product_location'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_location']); 
                    $page['Labels']['stock_code']     = str_replace("%%afn-stock code%%", $afn_stock_code, $page['Labels']['stock_code']);
                    $page['Labels']['product_description'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_description']); 
                    $page['Labels']['product_type'] = str_replace("%%afn-product%%", $afn_product, $page['Labels']['product_type']);


                    $this->smarty->assign("page", $page);

                    
                    
                    if(count($datarow)==0)
                    {
                        $JobTypeID = (isset($args['JobTypeID']))?$args['JobTypeID']:'';
                        
                        $datarow = array(

                            'JobSettingsID'=>'',
                            'NetworkID'=>$this->user->NetworkID,
                            'ClientID'=>$this->user->ClientID,
                            'BranchID'=>$this->user->BranchID,
                            'BranchIDAddress'=>'',
                            'JobTypeID'=>$JobTypeID,
                            'JobTypeName'=>'',
                            'Priority'=>'',
                            'TypeCode'=>'',
                            'ServiceTypeList'=>array(),
                            'CustomerType'=>'Consumer',
                            'CustomerTitleID'=>'',
                            'ContactFirstName'=>'',
                            'ContactLastName'=>'',
                            'ContactEmail'=>'',
                            'ContactHomePhone'=>'',
                            'ContactWorkPhoneExt'=>'',
                            'ContactMobile'=>'',
                            'ContactAltMobile'=>'',
                            'PostalCode'=>'',
                            'CompanyName'=>'',
                            'BuildingNameNumber'=>'',
                            'Street'=>'',
                            'LocalArea'=>'',
                            'TownCity'=>'',
                            'CountyID'=>'',
                            'CountryID'=>'',
                            'ProductLocation'=>'',
                            'AutoPopulateBranchID'=>'',
                            'DefaultBranchID'=>'',
                            'ColBranchAddressID'=>'',
                            'isProductAtBranch'=>'',
                            'ColAddPostcode'=>'',
                            'ColAddCompanyName'=>'',
                            'ColAddBuildingNameNumber'=>'',
                            'ColAddStreet'=>'',
                            'ColAddLocalArea'=>'',
                            'ColAddTownCity'=>'',
                            'ColAddCountyID'=>'',
                            'ColAddCountryID'=>'',
                            'ColAddEmail'=>'',
                            'ColAddPhone'=>'',
                            'ColAddPhoneExt'=>'',
                            'StockCode'=>'',
                            'LocateBy'=>'1',
                            'ModelName'=>'',
                            'ProductDescription'=>'',
                            'ManufacturerID'=>'',
                            'UnitTypeID'=>'',
                            'ImeiNo'=>'',
                            'SerialNo'=>'',
                            'DateOfPurchase'=>'',
                            'ServiceTypeID'=>'',
                            'reported_fault'=>'',
                            'Accessories'=>'',
                            'UnitCondition'=>'',
                            'MobilePhoneNetworkID'=>'',
                            'OriginalRetailerFormElement'=>'',
                            'OriginalRetailerPart1'=>'',
                            'OriginalRetailerPart2'=>'',
                            'OriginalRetailerPart3'=>'',
                            'ReceiptNo'=>'',
                            'Insurer'=>'',
                            'PolicyNo'=>'',
                            'AuthorisationNo'=>'',
                            'Notes'=>'',
                            'ReferralNo'=>'',
                            'ReportedFault'=>'',
                            'ReferralNumberRequired'=>'',
                            'Status'=>'Active',
                            
                            'CustomerTypeChk1'=>0,
                            'CustomerTypeChk2'=>0,
                            'CustomerTypeChk3'=>0,
                            'ContactTitleChk1'=>1,
                            'ContactTitleChk2'=>1,
                            'ContactTitleChk3'=>0,
                            'BranchIDAddressChk1'=>0,
                            'BranchIDAddressChk2'=>0,
                            'BranchIDAddressChk3'=>0,
                            'ContactEmailChk1'=>1,
                            'ContactEmailChk2'=>1,
                            'ContactEmailChk3'=>0,
                            'ContactHomePhoneChk1'=>1,
                            'ContactHomePhoneChk2'=>0,
                            'ContactHomePhoneChk3'=>0,
                            'ContactMobileChk1'=>1,
                            'ContactMobileChk2'=>1,
                            'ContactMobileChk3'=>0,
                            'ContactAltMobileChk1'=>0,
                            'ContactAltMobileChk2'=>0,
                            'ContactAltMobileChk3'=>0,
                            'PostalCodeChk1'=>1,
                            'PostalCodeChk2'=>1,
                            'PostalCodeChk3'=>0,
                            'CompanyNameChk1'=>0,
                            'CompanyNameChk2'=>0,
                            'CompanyNameChk3'=>0,
                            'BuildingNameNumberChk1'=>1,
                            'BuildingNameNumberChk2'=>0,
                            'BuildingNameNumberChk3'=>0,
                            'StreetChk1'=>1,
                            'StreetChk2'=>1,
                            'StreetChk3'=>0,
                            'LocalAreaChk1'=>1,
                            'LocalAreaChk2'=>0,
                            'LocalAreaChk3'=>0,
                            'TownCityChk1'=>1,
                            'TownCityChk2'=>1,
                            'TownCityChk3'=>0,
                            'CountyIDChk1'=>1,
                            'CountyIDChk2'=>0,
                            'CountyIDChk3'=>0,
                            'CountryIDChk1'=>1,
                            'CountryIDChk2'=>1,
                            'CountryIDChk3'=>0,
                            'ProductLocationChk1'=>1,
                            'ProductLocationChk2'=>1,
                            'ProductLocationChk3'=>0,
                            'ColBranchAddressIDChk1'=>0,
                            'ColBranchAddressIDChk2'=>0,
                            'ColBranchAddressIDChk3'=>0,
                            'isProductAtBranchChk1'=>0,
                            'isProductAtBranchChk2'=>0,
                            'isProductAtBranchChk3'=>0,
                            'ColAddPostcodeChk1'=>0,
                            'ColAddPostcodeChk2'=>0,
                            'ColAddPostcodeChk3'=>0,
                            'ColAddCompanyNameChk1'=>0,
                            'ColAddCompanyNameChk2'=>0,
                            'ColAddCompanyNameChk3'=>0,
                            'ColAddBuildingNameNumberChk1'=>0,
                            'ColAddBuildingNameNumberChk2'=>0,
                            'ColAddBuildingNameNumberChk3'=>0,
                            'ColAddStreetChk1'=>0,
                            'ColAddStreetChk2'=>0,
                            'ColAddStreetChk3'=>0,
                            'ColAddLocalAreaChk1'=>0,
                            'ColAddLocalAreaChk2'=>0,
                            'ColAddLocalAreaChk3'=>0,
                            'ColAddTownCityChk1'=>0,
                            'ColAddTownCityChk2'=>0,
                            'ColAddTownCityChk3'=>0,
                            'ColAddCountyIDChk1'=>0,
                            'ColAddCountyIDChk2'=>0,
                            'ColAddCountyIDChk3'=>0,
                            'ColAddCountryIDChk1'=>0,
                            'ColAddCountryIDChk2'=>0,
                            'ColAddCountryIDChk3'=>0,
                            'ColAddEmailChk1'=>0,
                            'ColAddEmailChk2'=>0,
                            'ColAddEmailChk3'=>0,
                            'ColAddPhoneChk1'=>0,
                            'ColAddPhoneChk2'=>0,
                            'ColAddPhoneChk3'=>0,
                            'StockCodeChk1'=>1,
                            'StockCodeChk2'=>0,
                            'StockCodeChk3'=>0,
                            'ModelNameChk1'=>1,
                            'ModelNameChk2'=>1,
                            'ModelNameChk3'=>0,
                            'ProductDescriptionChk1'=>1,
                            'ProductDescriptionChk2'=>0,
                            'ProductDescriptionChk3'=>0,
                            'ManufacturerIDChk1'=>1,
                            'ManufacturerIDChk2'=>1,
                            'ManufacturerIDChk3'=>0,
                            'UnitTypeIDChk1'=>1,
                            'UnitTypeIDChk2'=>1,
                            'UnitTypeIDChk3'=>0,
                            'ImeiNoChk1'=>0,
                            'ImeiNoChk2'=>0,
                            'ImeiNoChk3'=>0,
                            'SerialNoChk1'=>1,
                            'SerialNoChk2'=>0,
                            'SerialNoChk3'=>0,
                            'DateOfPurchaseChk1'=>1,
                            'DateOfPurchaseChk2'=>0,
                            'DateOfPurchaseChk3'=>0,
                            'ServiceTypeIDChk1'=>1,
                            'ServiceTypeIDChk2'=>1,
                            'ServiceTypeIDChk3'=>0,
                            'ReportedFaultChk1'=>1,
                            'ReportedFaultChk2'=>1,
                            'ReportedFaultChk3'=>0,
                            'AccessoriesChk1'=>0,
                            'AccessoriesChk2'=>1,
                            'AccessoriesChk3'=>0,
                            'UnitConditionChk1'=>1,
                            'UnitConditionChk2'=>0,
                            'UnitConditionChk3'=>0,
                            'MobilePhoneNetworkIDChk1'=>0,
                            'MobilePhoneNetworkIDChk2'=>0,
                            'MobilePhoneNetworkIDChk3'=>0,
                            'OriginalRetailerChk1'=>1,
                            'OriginalRetailerChk2'=>0,
                            'OriginalRetailerChk3'=>0,
                            'ReceiptNoChk1'=>1,
                            'ReceiptNoChk2'=>0,
                            'ReceiptNoChk3'=>0,
                            'InsurerChk1'=>1,
                            'InsurerChk2'=>0,
                            'InsurerChk3'=>0,
                            'PolicyNoChk1'=>0,
                            'PolicyNoChk2'=>0,
                            'PolicyNoChk3'=>0,
                            'AuthorisationNoChk1'=>1,
                            'AuthorisationNoChk2'=>0,
                            'AuthorisationNoChk3'=>0,
                            'NotesChk1'=>1,
                            'NotesChk2'=>0,
                            'NotesChk3'=>0,
                            'ReferralNoChk1'=>1,
                            'ReferralNoChk2'=>0,
                            'ReferralNoChk3'=>0


                        );
                    
                    }
                    
                    if($datarow['JobTypeID']=='')
                    {
                        $this->redirect('index',null, null);
                    }    
                           
                    $datarow['JobTypeName']   = $Skyline->getJobTypeName($datarow['JobTypeID']);
                    
                    
                    //$this->log($datarow);
                    
                  
                    $this->smarty->assign("datarow", $datarow);

                    $BranchDetails = array();
                    
                    if($datarow['ClientID'] && $datarow['NetworkID'])
                    {
                         $BranchDetails   =  $BranchModel->getBranchesWithAddress($datarow['ClientID'], $datarow['NetworkID']);
                    }
                   
                    
                    
                    $this->smarty->assign("BranchDetails", $BranchDetails);

                    $countries  = $Skyline->getCountriesCounties();

                    $this->smarty->assign("countries", $countries);

                    $product_location_rows   = $Skyline->getProductLocations(true);
                    $this->smarty->assign("product_location_rows", $product_location_rows);

                    $this->smarty->assign('page_legend', $this->page['Text']['booking_legend']);
                    $this->smarty->assign('page_title', $this->page['Text']['booking_page_title']);

                    //Getting Catalogue numbers.
                    $ProductNumbersModel = $this->loadModel('ProductNumbers');
                    $stockcode_rows      = $ProductNumbersModel->fetchProductNumbers($datarow['NetworkID'], $datarow['ClientID']);
                    
                    $this->smarty->assign('stockcode_rows', $stockcode_rows);

                   
                    $mfg_rows   = $Skyline->getManufacturer('', null, $datarow['ClientID']);
            
                    if(!count($mfg_rows))
                    {
                        $mfg_rows   = $Skyline->getManufacturer('', $datarow['NetworkID']);
                    }
                    if(!count($mfg_rows))
                    {
                         $mfg_rows   = $Skyline->getManufacturer();
                    }
                    
                    $this->smarty->assign('mfg_rows', $mfg_rows);


                    $product_type_rows   = $Skyline->getUnitTypes('', $datarow['ClientID']);
                    if(!count($product_type_rows))
                    {
                         $product_type_rows   = $Skyline->getUnitTypes();
                    }

                    $this->smarty->assign('product_type_rows', $product_type_rows); 
                    
                    $service_type_rows = array();
                    if($datarow['NetworkID'])
                    {
                        $service_type_rows  = $Skyline->getServiceTypes(false, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                    }
                    $this->smarty->assign('service_type_rows', $service_type_rows);
                    
                    
                    
                    $service_type_list = array();
                  
                    $service_type_list  = $Skyline->getServiceTypes(false, false, $datarow['NetworkID'], $datarow['ClientID']);
                    $this->smarty->assign('service_type_list', $service_type_list);
                    
                    
                    
                    $MobilePhoneNetworkModel = $this->loadModel('MobilePhoneNetwork');
                    $mobile_phone_networks = $MobilePhoneNetworkModel->fetchAll();
            
                    $this->smarty->assign('mobile_phone_networks', $mobile_phone_networks);


                    $this->smarty->display('JobSettings.tpl'); 
                    return;
                
                
                
            
            }
            else
            {
                $this->redirect('index',null, null);
            }
       
      }
   
    

   /**
    * hideNonSbJobs
    * 
    * creates session variable to hideNonSbJobs in openjobs table
    * 
    * 
    * 
    * 
    * 
    * @author Andris Polnikovs <a.polnikovs@pccsuk.com>
    ***************************************************************************/
    
    public function hideNonSbJobsAction($args) 
    {
       
        if(isset($_POST['hideNonSbJobs'])){
       $this->session->hideNonSbJobs=$_POST['hideNonSbJobs'];
        }else{
            $this->session->hideNonSbJobs="";
        }
     
    }
  
    
    
    public function getAuthTypeAllowedAction() {
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$result = $jobUpdateModel->getAuthTypeAllowed($_POST);
	echo($result);
	
    }
    
    
    
      public function exportjobAction($args){
        
        set_time_limit(10000);
        
        $params = array_merge($args, $_REQUEST);
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        if ($this->debug) $this->log($_REQUEST);
        
        $TatModel   = $this->loadModel('TAT');
        $Job = $this->loadModel('Job');
        
        if($params['pgetitle'] == "overdueJobs"){
            $TatResult  = $TatModel->fetchRow(array('TatType'=>'OverdueJobs'));
            $results = $Job->fetchAllOverdueSummary($params,$this->user->UserID,$this->page,$TatResult);
            $keys_data = $Job->getSummaryFields($this->page,$TatResult,"overdueJobs",$params);
            $new_result =  array();
            foreach($results['aaData'] as $data){
                unset($data[count($data)-1]);
                $new_result[] = $data;
}
            $results = $new_result;
        }
        else if($params['pgetitle'] == "closedJobs"){
            $TatResult  = $TatModel->fetchRow(array('TatType'=>'ClosedJobs'));
            $results = $Job->getClosedJobsSummary($params,$this->user->UserID,$this->page,$TatResult);
            $keys_data = $Job->getSummaryFields($this->page,$TatResult,"closedJobs",$params);
            $new_result =  array();
            foreach($results['aaData'] as $data){
                unset($data[count($data)-1]);
                $new_result[] = $data;
            }
            $results = $new_result;
        }
        else{
            $TatResult  = $TatModel->fetchRow(array('TatType'=>'OpenJobs'));
            $results = $Job->fetchAllOpenSummary_excel($params,$this->user->UserID,$this->page,$TatResult);
            $keys_data = $Job->getSummaryFields($this->page,$TatResult,"OpenJobs");
        }
        
        $helper_model = $this->loadModel("Helpers");
        
	require_once("libraries/PHPExcel/PHPExcel.php");
	require_once("libraries/PHPExcel/PHPExcel/Writer/Excel5.php");
	$excel = new PHPExcel();
        
       
        if($args['pgetitle'] == "overdueJobs"){
        $excel->setActiveSheetIndex(0)
                ->setTitle('Overduejobs Summary');
            $excel->getActiveSheet()->setCellValue("A1", "Overdue Jobs Summary Report");
        }
        else if($params['pgetitle'] == "closedJobs"){
             $excel->setActiveSheetIndex(0)
                ->setTitle('Closedjobs Summary');
            $excel->getActiveSheet()->setCellValue("A1", "Closed Jobs Summary Report");
        }
        else{
             $excel->setActiveSheetIndex(0)
                ->setTitle('Openjobs Summary');
            $excel->getActiveSheet()->setCellValue("A1", "Open Jobs Summary Report");
        }
        
        $excel->getActiveSheet()->setCellValue("A2", "Created Date");
        $excel->getActiveSheet()->setCellValue("B2", Date("d/m/Y"));
        $excel->getActiveSheet()->setCellValue("A3", "Created By");
        $excel->getActiveSheet()->setCellValue("B3", $this->user->ContactFirstName." ".$this->user->ContactLastName);
        
        
        if(isset($keys_data[1])){
            $colCount = count($keys_data[1])-1;
            $colNum = 0;
	    foreach($keys_data[1] as $name) {
		$colName = $helper_model->getExcelColName($colNum);
		$excel->getActiveSheet()->SetCellValue($colName . "5", trim($name));
		$colNum++;
	    }
        }
        if(count($results) > 0){
           foreach($results as $index=>$data) {
                $colNum = 0;
                $colCount = count($data)-1;
                $colName = $helper_model->getExcelColName($colNum);
                $excel->getActiveSheet()->fromArray($data,NULL,$colName . ($index +6));
                $colNum++;
            }
        }
        
        $filename = $this->user->UserID.time().'.xls';
        $excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
	$excelWriter->save($filename);
        
        require_once 'libraries/PHPExcel/PHPExcel/IOFactory.php';
        $excelReader =  PHPExcel_IOFactory::createReader('Excel5');
        $excelReader->setReadDataOnly(true);
        
        $objPHPExcel = $excelReader->load($filename);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        
        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            foreach ($cellIterator as $cell) {
                echo trim($cell->getValue())."\t";
            }
            echo "\n";
        }
//        echo trim($this->user->UserID.time().'.xlsx');
        
        unlink($filename);
	exit();
    }
    
    
    //changeing job model from service_provider_model id provided
 public function changeJobModelAction($args){
     $this->log($_POST);
     $Job = $this->loadModel('Job');
     $Job->changeJobModel($_POST['ServiceProviderModelID'],$_POST['JobID']);
 }
}

?>
